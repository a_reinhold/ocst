/**
 */

#ifndef OCST_BOUNDARY_CONDITION
#define OCST_BOUNDARY_CONDITION

#ifndef ALPHA
#define ALPHA 10.0
#endif

#include <math.h>
#include <mfem.hpp>
void 
meshNormal(const mfem::Vector& x, mfem::Vector& normal); //fwd


double
y_desired_func(const mfem::Vector& x, const double t)
{
   return (1+t*t) * std::atan(ALPHA* x(0)-x(1)); 
}


void
y_grad(const mfem::Vector& x, const double t, mfem::Vector& grad)
{
   double res1 = ALPHA * (1+t*t);
   double res2 = ALPHA*ALPHA*(x(0)-x(1))*(x(0)-x(1))+1;
   grad(0) =  res1/res2;
   grad(1) = -grad(0);
}

double
kappa_func(const mfem::Vector& x) 
{ return x(0)*std::cos(x(1))+2.0; } 

double
xi_func(const mfem::Vector& x) 
{
   return ALPHA * x(0)*x(0) * x(1) * x(1) + 1.0;  
}


// Shoud be the same for eaxh function
double
u_init_func(const mfem::Vector& x, const double t)
{
   return 0.0;
}
double
u_a_func(const mfem::Vector& x, const double t)
{
   return -10.0;
}
double	
u_b_func(const mfem::Vector& x, const double t)
{
   return 10.0;
}

double
eta_func(const mfem::Vector& x, const double t)
{
   //make shure that a mesh header is loaded! 

   mfem::Vector n(x.Size());
   mfem::Vector grad(x.Size());

   meshNormal(x,n);
   y_grad(x,t,grad);
   return kappa_func(x) * (n * grad) + xi_func(x) * y_desired_func(x,t) ; 
}

#endif // OCST_BOUNDARY_CONDITION
