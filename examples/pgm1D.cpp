//                                MFEM example spaceTime
//
//
//
#include "../data/bc/symmetric_1D.hpp"
#include "1Djump.hpp"

#include <mfem.hpp>
#include <ocst.hpp>

#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;

int
main(int argc, char* argv[])
{
   cout << scientific;
   cout.precision(5);
   // Parse command-line options.
   int initial_nodes_space = 30;
   int order_space = 1;
   int nof_timesteps = 11;
   int levels_space = 2;
   bool type1 = true;
   bool visualization = false;
   const char* device_config = "cpu";
   double omega_1 = 1.0;
   double omega_2 = 1.0;
   double lambda = 1E-3;

   OptionsParser args(argc, argv);
   args.AddOption(&type1,
                  "-type1",
                  "--type1",
                  "-type2",
                  "--type2",
                  "type of the time discretization");
   args.AddOption(&order_space,
                  "-o",
                  "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   args.AddOption(&initial_nodes_space,
                  "-n",
                  "--initial_nodes_space",
                  "Number of initial nodes space");
   args.AddOption(&levels_space,
                  "-ls",
                  "--levels_space",
                  "levels for MG space dimension");
   args.AddOption(
       &nof_timesteps, "-K", "--nof_timesteps", "Number of timesteps");
   args.AddOption(&visualization,
                  "-v",
                  "--visualization",
                  "-no-visualization",
                  "--no-visualization",
                  "save files for visualization");
   args.AddOption(
       &omega_1, "-w1", "--omega_1", "scalar for time contionus tracking");
   args.AddOption(
       &omega_2, "-w2", "--omega_2", "scalar for end time contionus tracking");
   args.AddOption(&lambda,
                  "-lambda",
                  "--lambda",
                  "scalar for regularazation of the control");

   args.Parse();
   if (!args.Good()) {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);
   ParaViewDataCollection* pd;

   // Setup the device
   Device device(device_config);
   // Meshes, FE_Collections and spaces

   Mesh mesh_time = Mesh::MakeCartesian1D(nof_timesteps - 1, 1.0);
   Mesh mesh_time_refined(mesh_time);
   mesh_time_refined.UniformRefinement();

   H1_FECollection fec_time_trial(1, 1);
   L2_FECollection fec_time_test(0, 1);

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test((type1) ? &mesh_time : &mesh_time_refined,
                                    &fec_time_test);

   Mesh mesh_space = Mesh::MakeCartesian1D(initial_nodes_space, xmax);

   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);

   // MG Prep
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(
       fes_time_trial, 1, fes_space, levels_space);

   ocst::TimeMatrices time_mats(fes_time_trial, fes_time_test, type1);
   Array<ocst::SpaceMatrices*> space_mats;
   Array<SparseMatrix*> A_space;
   Array<SparseMatrix*> M_space;

   mfem::FiniteElementSpaceHierarchy& hierarchy_space
       = st_hierarchy.FESHierarchySpace();
   space_mats.Append(
       new ocst::SpaceMatrices(hierarchy_space.GetFESpaceAtLevel(0)));
   FunctionCoefficient kappa_coeff(kappa_func);
   FunctionCoefficient robin_lhs(xi_func);
   space_mats[0]->SetBLF(robin_lhs, kappa_coeff);
   space_mats[0]->Assemble();

   A_space.Append(&space_mats[0]->A());
   M_space.Append(&space_mats[0]->M());
   for (int i = 1; i < levels_space; ++i) {
      space_mats.Append(new ocst::SpaceMatrices(
          *space_mats[i - 1], hierarchy_space.GetFESpaceAtLevel(i)));
      space_mats[i]->Assemble();
      A_space.Append(&space_mats[i]->A());
      M_space.Append(&space_mats[i]->M());
   }
   int dof_time_test = time_mats.M_test().Height() + 1;
   int dof_time_trial = time_mats.M_trial().Height();
   int dof_space = space_mats.Last()->A().Height();
   cout << " n_h = " << dof_space << endl;
   cout << " K_test = " << dof_time_test << endl;
   cout << " K_trial = " << dof_time_trial << endl;

   double* buffer = new double[dof_time_test * dof_space];
   using OT = ocst::GeometricMultigrid::OperatorType;
   ocst::GeometricMultigrid prec_A(hierarchy_space, A_space, OT::SPARSE);
   ocst::GeometricMultigrid prec_M(hierarchy_space, M_space, OT::SPARSE);

   ocst::SolveMGPCG solve_A(prec_A);
   ocst::SolveMGPCG solve_M(prec_M);

   ocst::OptimalControlProblem ocp(fes_time_trial,
                                   fes_time_test,
                                   hierarchy_space.GetFinestFESpace(),
                                   lambda,
                                   omega_1,
                                   omega_2);

   FunctionCoefficient y_desired_coeff(y_desired_func);
   FunctionCoefficient u_init_coeff(u_init_func);
   FunctionCoefficient u_a_coeff(u_a_func);
   FunctionCoefficient u_b_coeff(u_b_func);
   FunctionCoefficient robin_rhs(eta_func);

   ocp.SetDesiredState(y_desired_coeff);
   ocp.SetControlConstrains(u_a_coeff, u_b_coeff);

   ocp.Control().SetM_time(time_mats.M_test());
   ocp.Control().SetM_space(*M_space.Last());
   ocp.Control().SetValue(u_init_coeff);

   ocp.State().SetM_time(time_mats.M_trial());
   ocp.State().SetM_space(*M_space.Last());

   // RHS
   FunctionCoefficient y_0_coeff(y_0_func);
   ocp.SetConstantPartRHS(y_0_coeff, robin_rhs, time_mats.M_mixed());

   // Solver
   ocst::NormOperatorTrial* M_norm = nullptr;
   ocst::NormOperatorTest* N_norm = nullptr;
   tensor::GeneralSylvesterOperator* B = nullptr;
   ocst::SpaceTimeSolver* solver = nullptr;

   if (type1) {
      solver = new ocst::IterativeType1Solver(ocp.PrimalRHS(),
                                              ocp.AdjointRHS(),
                                              M_space,
                                              A_space,
                                              solve_M,
                                              hierarchy_space,
                                              nof_timesteps - 1,
                                              1.0);
   } else {

      M_norm = new ocst::NormOperatorTrial(time_mats.M_trial(),
                                           time_mats.A_trial(),
                                           *M_space.Last(),
                                           *A_space.Last(),
                                           solve_A,
                                           false,
                                           buffer);

      N_norm = new ocst::NormOperatorTest(
          time_mats.M_test(), *M_space.Last(), *A_space.Last(), buffer);
      M_norm->SetInverseOperator(M_space, A_space, hierarchy_space);
      N_norm->SetInverseOperator(solve_A, solve_M);
      static_cast<ocst::InvNormOperatorTrial*>(M_norm->GetInverseOperator())
          ->SetTolerance(1e-6);

      B = new tensor::GeneralSylvesterOperator(time_mats.C_mixed_mod(),
                                               *M_space.Last(),
                                               time_mats.M_mixed_mod(),
                                               *A_space.Last());
      solver = new ocst::LSQRSolver(*B,
                                    ocp.PrimalRHS(),
                                    ocp.AdjointRHS(),
                                    *M_norm->GetInverseOperator(),
                                    *N_norm->GetInverseOperator());
      static_cast<ocst::LSQRSolver*>(solver)->SetSolverParameters(1e-5, 12, 0);
   }
   ocst::ProjectedGradientMethod pg(ocp, *solver, 141, 1 / ocp.Lambda());
   ocst::BenchTiming timing(1);
   std::cout << "Start PG " << std::endl;
   timing.Start(0);
   pg.Solve();
   timing.End(0);
   cout << "elapsed_time = " << timing.PopTime(0) << endl;
   std::cout << "Finished PG " << std::endl << endl;

   pg.PrintResults();

   if (visualization) {
      std::ofstream y_file("y_surfdata.txt", std::ofstream::binary);
      ocst::Export1DSurfData(fes_time_trial,
                             hierarchy_space.GetFinestFESpace(),
                             ocp.State().Mat(),
                             y_file,
                             "y");
      std::ofstream u_file("u_surfdata.txt", std::ofstream::binary);
      ocst::Export1DSurfData(fes_time_test,
                             hierarchy_space.GetFinestFESpace(),
                             ocp.Control().Mat(),
                             u_file,
                             "u");
      std::ofstream y_d_file("y_d_surfdata.txt", std::ofstream::binary);
      ocst::Export1DSurfData(fes_time_trial,
                             hierarchy_space.GetFinestFESpace(),
                             ocp.DesiredState().Mat(),
                             y_d_file,
                             "y_d");
      std::ofstream p_file("p_surfdata.txt", std::ofstream::binary);
      ocst::Export1DSurfData(fes_time_test,
                             hierarchy_space.GetFinestFESpace(),
                             ocp.AdjointState().AdjointState_I(),
                             p_file,
                             "p");
   }
   delete M_norm;
   delete N_norm;
   delete B;
   if (type1){
      delete static_cast<ocst::IterativeType1Solver*>(solver);
   } else {
      delete static_cast<ocst::LSQRSolver*>(solver);
   }
   for (int i = 0; i < levels_space; ++i) {
      delete space_mats[i];
   }
   delete[] buffer;
   return 0;
}
