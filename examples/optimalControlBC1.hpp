/**
 * Analytic solution Parabolic Problem
 *
 * problem 2 trigonometric 2D
 */

#ifndef OCST_BOUNDARY_CONDITION
#define OCST_BOUNDARY_CONDITION

#include <math.h>
#include <mfem.hpp>
void 
meshNormal(const mfem::Vector& x, mfem::Vector& normal); //fwd


double
y_desired_func(const mfem::Vector& x, const double t)
{
   return std::sin(t * x(0)) * std::cos(t * x(1)); 
}


void
y_grad(const mfem::Vector& x, const double t, mfem::Vector& grad)
{
   grad(0) =  t * std::cos(x(0)*t) *std::cos(x(1)*t);
   grad(1) = -t * std::sin(x(0)*t) *std::sin(x(1)*t);
}

double
kappa_func(const mfem::Vector& x) 
{ return 1.0; } 

double
xi_func(const mfem::Vector& x) 
{
   return 0.25 * x(0)*x(0) * x(1) * x(1) + 0.1;  
}


// Shoud be the same for eaxh function
double
u_init_func(const mfem::Vector& x, const double t)
{
   return x(0)*t* std::cos(x(1));
}
double
u_a_func(const mfem::Vector& x, const double t)
{
   return -1.0-t;
}
double
u_b_func(const mfem::Vector& x, const double t)
{
   return 1.0+t;
}

double
eta_func(const mfem::Vector& x, const double t)
{
   //make shure that a mesh header is loaded! 

   mfem::Vector n(x.Size());
   mfem::Vector grad(x.Size());

   meshNormal(x,n);
   y_grad(x,t,grad);
   return kappa_func(x) * (n * grad) + xi_func(x) * y_desired_func(x,t) ; 
}

#endif // OCST_BOUNDARY_CONDITION
