sed -i '5s/.*/#include "coupled1.hpp"/' pgm.cpp 
make clean && make
for level in  2 3 4  
do
   for K in 11 21 51 101 501 1001 
   do
      fname="results/20210805_coupled_1_K_${K}_ls_${level}_type1"
      echo "#coupled1" | tee $fname 
      ./pgm -rs 3 -K ${K} -ls ${level} -lambda 0.01 -type1 | tee -a $fname
      fname="results/20210805_coupled_1_K_${K}_ls_${level}_type2"
      echo "#coupled1" | tee $fname 
      ./pgm -rs 3 -K ${K} -ls ${level} -lambda 0.01 -type2 | tee -a $fname
   done
done

sed -i '5s/.*/#include "coupled2.hpp"/' pgm.cpp 
make clean && make
for level in  2 3 4  
do
   for K in 11 21 51 101 501 1001 
   do
      fname="results/20210805_coupled_2_K_${K}_ls_${level}_type1"
      echo "#coupled2" | tee $fname 
      ./pgm -rs 3 -K ${K} -ls ${level} -lambda 0.01 -type1 | tee -a $fname
      fname="results/20210805_coupled_2_K_${K}_ls_${level}_type2"
      echo "#coupled2" | tee $fname 
      ./pgm -rs 3 -K ${K} -ls ${level} -lambda 0.01 -type2 | tee -a $fname
   done
done

#retrieve results

datfile="results/20210805_coupled_1.dat"
echo -e "K\tls\tn_h\tt1_J\tt2_J\tt1_iter\tt2_iter\tt1_time\tt2_time" | tee $datfile
for level in  2 3 4  
do
   for K in 11 21 51 101 501 1001 
   do
      t1_fname="results/20210805_coupled_1_K_${K}_ls_${level}_type1"
      t2_fname="results/20210805_coupled_1_K_${K}_ls_${level}_type2"
      n_h=$(egrep "n_h" $t2_fname | cut -d " " -f 4) 
      t1_J=$(tail -1 $t1_fname | cut -f 3)
      t2_J=$(tail -1 $t2_fname | cut -f 3)
      t1_iter=$(tail -1 $t1_fname | cut -f 1)
      t2_iter=$(tail -1 $t2_fname | cut -f 1)
      
      t1_time=$(egrep "elapsed_time" $t1_fname | cut -d " " -f 3) 
      t2_time=$(egrep "elapsed_time" $t2_fname | cut -d " " -f 3) 

      echo -e "${K}\t${level}\t${n_h}\t${t1_J}\t${t2_J}\t${t1_iter}\t${t2_iter}\t${t1_time}\t${t2_time}" | tee -a $datfile
   done
   echo "" | tee -a $datfile
done

datfile="results/coupled_2.dat"
echo -e "K\tls\tn_h\tt1_J\tt2_J\tt1_iter\tt2_iter\tt1_time\tt2_time" | tee $datfile

for level in  2 3 4  
do
   for K in 11 21 51 101 501 1001 
   do
      t1_fname="results/20210805_coupled_2_K_${K}_ls_${level}_type1"
      t2_fname="results/20210805_coupled_2_K_${K}_ls_${level}_type2"
      n_h=$(egrep "n_h" $t2_fname | cut -d " " -f 4) 
      t1_J=$(tail -1 $t1_fname | cut -f 3)
      t2_J=$(tail -1 $t2_fname | cut -f 3)
      t1_iter=$(tail -1 $t1_fname | cut -f 1)
      t2_iter=$(tail -1 $t2_fname | cut -f 1)
      
      t1_time=$(egrep "elapsed_time" $t1_fname | cut -d " " -f 3) 
      t2_time=$(egrep "elapsed_time" $t2_fname | cut -d " " -f 3) 

      echo -e "${K}\t${level}\t${n_h}\t${t1_J}\t${t2_J}\t${t1_iter}\t${t2_iter}\t${t1_time}\t${t2_time}" | tee -a $datfile
   done
   echo "" | tee -a $datfile
done
