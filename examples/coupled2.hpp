/**
 */

#ifndef OCST_BOUNDARY_CONDITION
#define OCST_BOUNDARY_CONDITION

#include <math.h>
#include <mfem.hpp>
void 
meshNormal(const mfem::Vector& x, mfem::Vector& normal); //fwd


double
y_desired_func(const mfem::Vector& x, const double t)
{
   return ((x(1) -x(0)) > 0)? t*x(0)*x(1) : - t * x(0)*x(1)  ; 
}

double 
y_0_func(const mfem::Vector& x)
{
   if (x(0) > 0.25 && x(0) < 0.75 && x(1) > 0.25 && x(1) < 0.75){
      return ((x(1)-x(0)) > 0) ? -1.0 : 1.0;
   } else {
      return y_desired_func(x, 0.0); 
   }
}

void
y_grad(const mfem::Vector& x, const double t, mfem::Vector& grad)
{
   grad(0) = ((x(1)- x(0)) > 0)? t * x(1)  : -t * x(1) ;
   grad(1) = ((x(1)- x(0)) > 0)? t * x(0)  : -t * x(0) ;
}

double
kappa_func(const mfem::Vector& x) 
{ return 1.0; } 

double
xi_func(const mfem::Vector& x) 
{
   return 0.25 * x(0)*x(0) * x(1) * x(1) + 0.1;  
}


// Shoud be the same for eaxh function
double
u_init_func(const mfem::Vector& x, const double t)
{
   return 0.0;
}
double
u_a_func(const mfem::Vector& x, const double t)
{
   return -10*t;
}
double
u_b_func(const mfem::Vector& x, const double t)
{
   return 10*t;
}

double
eta_func(const mfem::Vector& x, const double t)
{
   //make shure that a mesh header is loaded! 

   mfem::Vector n(x.Size());
   mfem::Vector grad(x.Size());

   meshNormal(x,n);
   y_grad(x,t,grad);
   return kappa_func(x) * (n * grad) + xi_func(x) * y_desired_func(x,t) ; 
}

#endif // OCST_BOUNDARY_CONDITION
