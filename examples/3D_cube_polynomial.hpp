/**
 */

#ifndef OCST_BOUNDARY_CONDITION
#define OCST_BOUNDARY_CONDITION

#include <math.h>
#include <mfem.hpp>
void 
meshNormal(const mfem::Vector& x, mfem::Vector& normal); //fwd


double
y_desired_func(const mfem::Vector& x, const double t)
{
   return 2.0-(x(0)*x(0)*(-x(1))*x(1) * x(2)*x(2)*x(2) + 1.0)*(t-0.5)*(t-0.5)*(t-0.5); 
   //return t; 
}

double 
y_0_func(const mfem::Vector& x)
{
   return y_desired_func(x, 0.0); 
}

void
y_grad(const mfem::Vector& x, const double t, mfem::Vector& grad)
{
   double t_cube = -(t-0.5)*(t-0.5)*(t-0.5);
   grad(0) = t_cube* ( 2.0 * x(0)*(-x(1))*x(1)*x(2)*x(2)*x(2));
   grad(1) = t_cube* (-2.0 *x(0)*x(0)*x(1)*x(2)*x(2)*x(2));
   grad(2) = t_cube* ( 3.0 *x(0)* x(0)*(-x(1))*x(1)*x(2)*x(2));
}

double
kappa_func(const mfem::Vector& x) 
{ 
   return x(0)*x(0) + x(1)*x(1)  * x(2)*x(2); 
} 

double
xi_func(const mfem::Vector& x) 
{
   return  x(0)*  x(1) * x(1) + x(2) + 3.0;  
}


// Shoud be the same for eaxh function
double
u_init_func(const mfem::Vector& x, const double t)
{
   return 0.0;
}
double
u_a_func(const mfem::Vector& x, const double t)
{
   return -1.0;
}
double
u_b_func(const mfem::Vector& x, const double t)
{
   return 1.0;
}

double
eta_func(const mfem::Vector& x, const double t)
{
   //make shure that a mesh header is loaded! 

   mfem::Vector n(x.Size());
   mfem::Vector grad(x.Size());

   meshNormal(x,n);
   y_grad(x,t,grad);
   return kappa_func(x) * (n * grad) + xi_func(x) * y_desired_func(x,t) ; 
}

#endif // OCST_BOUNDARY_CONDITION
