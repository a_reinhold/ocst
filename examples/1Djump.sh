#!/bin/bash 


# set ALPHA
for ALPHA in 10 20 50 
do
   sed -i "12s/.*/constexpr double ALPHA = ${ALPHA}\.0;/" 1Djump.hpp 
   make clean && make pgm1D
   for level in  2 3 4  
   do
      for K in 11 21 51 101 501 1001 
      do
	 fname="results/jump_1D_ALPHA_${ALPHA}_K_${K}_ls_${level}_type1"
	 echo -e "#jump1D type1\n # ALPHA = ${ALPHA}" | tee $fname 
	 ./pgm1D -n 40 -K ${K} -ls ${level} -w2 0.0 -type1 | tee -a $fname
	 fname="results/jump_1D_ALPHA_${ALPHA}_K_${K}_ls_${level}_type2"
	 echo -e "#jump1D type2\n # ALPHA = ${ALPHA}" | tee $fname 
	 ./pgm1D -n 40 -K ${K} -ls ${level} -w2 0.0 -type2 | tee -a $fname
      done
   done
done

#retrieve results

datfile="results/jump1D.dat"
echo -e "K\tls\tn_h\talpha\tt1_J\tt2_J\tt1_iter\tt2_iter\tt1_time\tt2_time" | tee $datfile
for ALPHA in 10 20 50
do
   for level in  2 3 4  
   do
      for K in 11 21 51 101 501 1001 
      do
	 t1_fname="results/jump_1D_ALPHA_${ALPHA}_K_${K}_ls_${level}_type1"
	 t2_fname="results/jump_1D_ALPHA_${ALPHA}_K_${K}_ls_${level}_type2"
	 n_h=$(egrep "n_h" $t2_fname | cut -d " " -f 4) 
	 t1_J=$(tail -1 $t1_fname | cut -f 3)
	 t2_J=$(tail -1 $t2_fname | cut -f 3)
	 t1_iter=$(tail -1 $t1_fname | cut -f 1)
	 t2_iter=$(tail -1 $t2_fname | cut -f 1)
	 
	 t1_time=$(egrep "elapsed_time" $t1_fname | cut -d " " -f 3) 
	 t2_time=$(egrep "elapsed_time" $t2_fname | cut -d " " -f 3) 

	 echo -e "${K}\t${level}\t${n_h}\t${ALPHA}\t${t1_J}\t${t2_J}\t${t1_iter}\t${t2_iter}\t${t1_time}\t${t2_time}" | tee -a $datfile
      done
      echo "" | tee -a $datfile
   done
   echo "" | tee -a $datfile
done

# for surf plots 
for ALPHA in 10 50 
do
   sed -i "12s/.*/constexpr double ALPHA = ${ALPHA}\.0;/" 1Djump.hpp 
   make clean && make pgm1D
   for level in 3  
   do
      for K in 21 501 
      do
	 fname="results/surf_jump_1D_ALPHA_${ALPHA}_K_${K}_ls_${level}_type1"
	 echo -e "#jump1D type1\n # ALPHA = ${ALPHA}" 
	 ./pgm1D -n 40 -K ${K} -ls ${level} -w2 0.0 -type1 -v
	 mkdir ${fname} &&  mv *.txt ${fname}
	 fname="results/surf_jump_1D_ALPHA_${ALPHA}_K_${K}_ls_${level}_type2"
	 echo -e "#jump1D type2\n # ALPHA = ${ALPHA}" 
	 ./pgm1D -n 40 -K ${K} -ls ${level} -w2 0.0 -type2 -v
	 mkdir ${fname} &&  mv *.txt ${fname}
      done
   done
done
