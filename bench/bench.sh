##INTEL
make clean 
cd ../ && make clean && cp _Makefile_intel Makefile && cd bench/ 
make bench_sparseDenseMM 
./bench_sparseDenseMM -l 7 -n 12 -c 1 -k 1   -ph | tee    20210521_sparseDenseMM_intel.dat 
./bench_sparseDenseMM -l 8 -n 13 -c 1 -k 10  -nh | tee -a 20210521_sparseDenseMM_intel.dat 
./bench_sparseDenseMM -l 8 -n 13 -c 1 -k 25  -nh | tee -a 20210521_sparseDenseMM_intel.dat 

make clean 
make bench_sparseDenseMM_Kfixed
./bench_sparseDenseMM_Kfixed -l 8 -n 12 -c 1 -k 100   -ph | tee    20210521_sparseDenseMM_K_fixed_intel.dat 
./bench_sparseDenseMM_Kfixed -l 8 -n 12 -c 1 -k 1000  -nh | tee -a 20210521_sparseDenseMM_K_fixed_intel.dat 
./bench_sparseDenseMM_Kfixed -l 8 -n 12 -c 1 -k 10000 -nh | tee -a 20210521_sparseDenseMM_K_fixed_intel.dat 

##NAIVE
make clean 
cd ../ && make clean && cp _Makefile_naive Makefile && cd bench/ 
make bench_sparseDenseMM 

./bench_sparseDenseMM -l 7 -n 12 -c 1 -k 1   -ph | tee    20210521_sparseDenseMM_naive.dat
./bench_sparseDenseMM -l 8 -n 13 -c 1 -k 10  -nh | tee -a 20210521_sparseDenseMM_naive.dat
./bench_sparseDenseMM -l 8 -n 13 -c 1 -k 25  -nh | tee -a 20210521_sparseDenseMM_naive.dat

make clean 
make bench_sparseDenseMM_Kfixed
./bench_sparseDenseMM_Kfixed -l 8 -n 12 -c 1 -k 100   -ph | tee    20210521_sparseDenseMM_K_fixed_naive.dat 
./bench_sparseDenseMM_Kfixed -l 8 -n 12 -c 1 -k 1000  -nh | tee -a 20210521_sparseDenseMM_K_fixed_naive.dat 
./bench_sparseDenseMM_Kfixed -l 8 -n 12 -c 1 -k 10000 -nh | tee -a 20210521_sparseDenseMM_K_fixed_naive.dat 
#./bench_sparseDenseMM -l 7 -n 9  -c 0 -k 1   | tee -a 20210518_sparseDenseMM_1.dat
#./bench_sparseDenseMM -l 8 -n 10 -c 0 -k 10  | tee -a 20210518_sparseDenseMM_2.dat
#./bench_sparseDenseMM -l 9 -n 11 -c 0 -k 100 | tee -a 20210518_sparseDenseMM_3.dat

#./bench_MGPCG -mgl 9 -ir 3 -o 1 -rel_tol 1e-5 -abs_tol 1e-4 -nof_runs 1 | tee -a 20210517_MGPCG_1.dat
#./bench_MGPCG -mgl 9 -ir 3 -o 1 -rel_tol 1e-7 -abs_tol 1e-6 -nof_runs 1 | tee -a 20210517_MGPCG_1.dat
#./bench_MGPCG -mgl 9 -ir 3 -o 1 -rel_tol 1e-9 -abs_tol 1e-8 -nof_runs 1 | tee -a 20210517_MGPCG_1.dat
#
#./bench_MGPCG -mgl 7 -ir 2 -o 4 -nof_runs 1 | tee -a 20210517_MGPCG_2.dat
