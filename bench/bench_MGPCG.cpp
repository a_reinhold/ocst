
#include <mfem.hpp>
#include <ocst.hpp>


#include <fstream>
#include <iostream>


using namespace std;
using namespace mfem;

/**
 * BENCH multigrid preconditiend cg method
 *
 * comparision between sparse and matrix free aproach, 
 * random reference solution
 */
int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   int initial_refinement = 2;
   int multigrid_levels = 2;
   int order_space = 1;
   
   double abs_tol = 1e-6;
   double rel_tol = 0.1 * abs_tol; 
   int max_it = 30; 

   int nof_runs =multigrid_levels + 3; 
   bool static_cond = false;
   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) ");
   args.AddOption(&initial_refinement, "-ir", "--initial_refinement",
                  "refinements for the coarsest level");
   args.AddOption(&multigrid_levels, "-mgl", "--multigrid_levels", 
                  "max numbers of multigrid levels");
   args.AddOption(&abs_tol, "-abs_tol", "--abs_tol", "absolute tolerance");
   args.AddOption(&rel_tol, "-rel_tol", "--rel_tol", "relative tolerance");
   args.AddOption(&max_it, "-max_it", "--max_it", "max nof iterations");

   args.AddOption(&nof_runs, "-nof_runs", "--nof_runs", 
                  "nof runs for each refinement = max_refinements - nof_runs");
 

   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   //args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);

   cout << "# Bench MGPCG " << endl 
        << "#initial_refinement= " << initial_refinement 
	<< "\trel_tol = " << rel_tol 
	<< "\tabs_tol = " << abs_tol
	<< "\torder = " << order_space 
	<< "\tmesh = " << mesh_file_space << endl; 
   
   
   //Setup data for bench 
   ocst::BenchData bench_data;
   ocst::BenchTiming bench_timing(4);
   
   bench_data.RegisterField("mg_levels", ocst::BenchData::Int);
   bench_data.RegisterField("nof_runs", ocst::BenchData::Int);
   bench_data.RegisterField("dim", ocst::BenchData::Int);
   bench_data.RegisterField("nnz", ocst::BenchData::Int);
   bench_data.RegisterField("time_setup_sparse", ocst::BenchData::Double);
   bench_data.RegisterField("time_setup_mfree", ocst::BenchData::Double);
   bench_data.RegisterField("time_solve_sparse", ocst::BenchData::Double);
   bench_data.RegisterField("time_solve_mfree", ocst::BenchData::Double);
   bench_data.RegisterField("error_sparse", ocst::BenchData::Double);
   bench_data.RegisterField("error_mfree", ocst::BenchData::Double);

   //Mesh, FES, Hierarchy
   Mesh mesh(mesh_file_space, 1, 1);
   for (int i=0; i<initial_refinement; ++i) mesh.UniformRefinement();

   H1_FECollection fec(order_space, mesh.Dimension());
   FiniteElementSpace fes(&mesh, &fec);
   
   ocst::SpaceTimeFiniteElementSpaceHierarchy 
                                   st_hierarchy(fes, 0, fes, multigrid_levels);
   FiniteElementSpaceHierarchy& hierarchy = st_hierarchy.FESHierarchySpace(); 

   mfem::Array<ocst::SpaceMatrices*> mats;
  
   mats.Append(new ocst::SpaceMatrices(fes));

   FunctionCoefficient xi([](const Vector& x){
         double x1 = (x.Size()>1) ? x(1) : 1.0;
         double x2 = (x.Size()>2) ? x(2) : 1.0;
         return x(0)*x1*x1*x2 + 0.3245*x(0)*x(0) + 0.02347*x1*x2; 
      });
   FunctionCoefficient kappa([](const Vector& x){ 
         double x1 = (x.Size()>1) ? x(1) : 1.0;
         double x2 = (x.Size()>2) ? x(2) : 1.0;
         return std::abs(x(0)) +0.2* std::abs(x1) +0.3333*std::abs(x2);
      }); 

   mats[0]->SetBLF(xi, kappa);
   mats[0]->Assemble(); 

   for (int l=1; l<multigrid_levels; ++l){
      mats.Append(new ocst::SpaceMatrices(*mats[0], 
                                          hierarchy.GetFESpaceAtLevel(l))); 
      mats.Last()->Assemble();
   }
   

   Array<SparseMatrix*> mat_A;
   mat_A.Append(&mats[0]->A());

   //begin bench 
   for (int l=1; l<multigrid_levels; ++l){
      mat_A.Append(&mats[l]->A());
      int n_A = mat_A.Last()->Height();
      bench_data.AddValue("mg_levels", l);
      bench_data.AddValue("dim", n_A);
      bench_data.AddValue("nnz", mat_A.Last()->NumNonZeroElems());
      int nnz_A = mat_A.Last()->NumNonZeroElems();
      Vector x_ref(n_A);
      Vector f_A(n_A);
      Vector x_A(n_A);
      
      //Setup Solvers
      int nof_runs_ = std::max(nof_runs - l, 1);
      bench_data.AddValue("nof_runs", nof_runs_);
      double err_sps_mat = 0.0;
      double err_mfree = 0.0;
      for (int k=0; k<nof_runs_; ++k){
	 x_ref.Randomize();
	 mat_A.Last()->Mult(x_ref,f_A);

	 //Set up solver 
	 bench_timing.Start(0);
	 ocst::GeometricMultigrid prec_A(hierarchy, mat_A, true);
	 ocst::SolveMGPCG solve_A(prec_A);
	 bench_timing.End(0);
	 solve_A.SetSolverParameters(rel_tol, abs_tol, max_it, 0);

	 bench_timing.Start(1);
	 ocst::GeometricMultigrid prec_B(hierarchy, mat_A, false);
	 ocst::SolveMGPCG solve_B(prec_B);
	 bench_timing.End(1);
	 solve_B.SetSolverParameters(rel_tol, abs_tol, max_it, 0);

	 x_A = 0.0;
	 bench_timing.Start(2);
	 solve_A.Mult(f_A, x_A);
	 bench_timing.End(2);
	 x_A -= x_ref;
	 err_sps_mat += x_A*x_A;

	 x_A = 0.0;
	 bench_timing.Start(3);
	 solve_B.Mult(f_A, x_A);
	 bench_timing.End(3);
	 x_A -= x_ref;
	 err_mfree += x_A*x_A;
      }
      bench_data.AddValue("time_setup_sparse", bench_timing.PopTime(0));
      bench_data.AddValue("time_setup_mfree", bench_timing.PopTime(1));
      bench_data.AddValue("time_solve_sparse", bench_timing.PopTime(2));
      bench_data.AddValue("time_solve_mfree", bench_timing.PopTime(3));
      bench_data.AddValue("error_sparse", err_sps_mat/double(nof_runs_));
      bench_data.AddValue("error_mfree", err_mfree/double(nof_runs_));

   }

   bench_data.PrintData();  
   for (int i=0; i<mats.Size(); ++i) delete mats[i]; 
   return 0; 
}
