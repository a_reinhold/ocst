//                                MFEM example spaceTime
//
//
//

//#include "../data/bc/unitSquare_2D.hpp"
#include "../data/bc/L_shaped_2D.hpp"
#include "../data/bc/case_2.hpp"

#include <mfem.hpp>
#include <ocst.hpp>

#include <chrono>
#include <ctime> 
#include <iostream>

using namespace std;
using namespace mfem;


using OperatorType = ocst::GeometricMultigrid::OperatorType;
int main(int argc, char *argv[])
{
   // Parse command-line options.
   cout << scientific; cout.precision(5);
   time_t today = chrono::system_clock::to_time_t(chrono::system_clock::now());
   cout << "# Bench norm inducing operators" << endl 
        << "# mesh= " << mesh_file << endl
	<< "# started " << ctime(&today); 
   int refinement_space = 2;
   int order_space = 1;
   int max_level = 3;

   //TODO tolerances

   const std::vector<int> nof_timesteps{11, 21, 51}; //, 101, 201};//, 501, 1001};
   
   ocst::BenchData bench_data;
   ocst::BenchTiming timing(6);
   bench_data.RegisterField("mg_levels", ocst::BenchData::Int);
   bench_data.RegisterField("K", ocst::BenchData::Int);
   bench_data.RegisterField("n_h", ocst::BenchData::Int);
   bench_data.RegisterField("time_setup_inv_trial", ocst::BenchData::Double);
   bench_data.RegisterField("time_setup_inv_test", ocst::BenchData::Double);
   bench_data.RegisterField("time_mult_trial", ocst::BenchData::Double);
   bench_data.RegisterField("time_mult_test", ocst::BenchData::Double);
   bench_data.RegisterField("time_inverse_trial", ocst::BenchData::Double);
   bench_data.RegisterField("time_inverse_test", ocst::BenchData::Double);
   bench_data.RegisterField("error_trial", ocst::BenchData::Double);
   bench_data.RegisterField("error_test", ocst::BenchData::Double);


   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;


   // Setup the device
   Device device(device_config);

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);

   Mesh mesh_space(mesh_file,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   FiniteElementSpaceHierarchy hierarchy(&mesh_space, &fes_space, false, false);

   Array<ocst::SpaceMatrices*> space_mats;
   Array<SparseMatrix*> A_space; 
   Array<SparseMatrix*> M_space; 

   space_mats.Append(new ocst::SpaceMatrices(fes_space));
   FunctionCoefficient robin_lhs(xi_func);
   FunctionCoefficient kappa_coeff(kappa_func);
   space_mats[0]->SetBLF(robin_lhs, kappa_coeff); 
   space_mats[0]->Assemble(); 
   A_space.Append(&space_mats[0]->A());
   M_space.Append(&space_mats[0]->M());
   

   for (int ls=1; ls<=max_level; ++ls){
      // Add level to space hierarchy 
      FiniteElementSpace* fes_coarse = &hierarchy.GetFinestFESpace();
      Mesh* mesh_refined = new Mesh(*fes_coarse->GetMesh());
      mesh_refined->UniformRefinement();
      FiniteElementSpace* fes_fine= 
	 new FiniteElementSpace(*fes_coarse, mesh_refined);
      ocst::SparseProlongation* prol= 
	 new ocst::SparseProlongation(*fes_coarse, *fes_fine);
      hierarchy.AddLevel(mesh_refined, fes_fine, prol, true, true, true);
      
      space_mats.Append(
	 new ocst::SpaceMatrices(*space_mats[ls-1], *fes_fine));
      space_mats[ls]->Assemble(); 
      M_space.Append(&space_mats[ls]->M());
      A_space.Append(&space_mats[ls]->A());
      int dof_space = space_mats.Last()->A().Height();
      ocst::GeometricMultigrid prec_A(hierarchy, A_space, OperatorType::SPARSE);
      ocst::GeometricMultigrid prec_M(hierarchy, M_space, OperatorType::SPARSE);

      ocst::SolveMGPCG solve_A(prec_A);
      ocst::SolveMGPCG solve_M(prec_M);

      for (auto K: nof_timesteps){
	 cout << "\r>>> Running Case K = " << K 
	      << ", n_h =  " << dof_space << " ...";
	 Mesh mesh_time(K-1,1.0);
	 Mesh mesh_time_ref(mesh_time);
	 FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
	 FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);
	 ocst::TimeMatrices time_mats(fes_time_trial, fes_time_test);
	 double* buffer = new double[K*dof_space];
	 ocst::SpaceTimeData x_ref(K, dof_space);
	 ocst::SpaceTimeData x(K, dof_space);
	 ocst::SpaceTimeData y(K, dof_space);
	 // x_ref.Mat() = 1.0;
	 x_ref.Vec().Randomize();

	 //Setup
	 ocst::NormOperatorTrial op_trial(time_mats.M_trial(), 
	                                  time_mats.A_trial(),
					  *M_space.Last(), *A_space.Last(), 
					  solve_A, false, buffer);
	 ocst::NormOperatorTest op_test(time_mats.M_test(), 
					*M_space.Last(), *A_space.Last(),
					buffer);

	 timing.Start(0);
	 op_trial.SetInverseOperator(M_space, A_space, hierarchy);
	 timing.End(0);

	 timing.Start(1);
	 op_test.SetInverseOperator(solve_A, solve_M); 
	 timing.End(1);
	 do {
	    y.Mat()= 0.0;
	    timing.Start(2);
	    op_trial.Mult(x_ref, y);
	    timing.End(2);
	 } while (timing.GetCumulatedTime(2)<1000.0);
	 do {
	    x.Mat()= 0.0;
	    timing.Start(4);
	    op_trial.MultInverse(y, x);
	    timing.End(4);
	 } while (timing.GetCumulatedTime(4)<1000.0);
	 x.Mat() -= x_ref.Mat();
	 bench_data.AddValue("error_trial", x.Vec()*x.Vec());
	 
	 
	 do {
	    y.Mat()= 0.0;
	    timing.Start(3);
	    op_test.Mult(x_ref, y);
	    timing.End(3);
	 } while (timing.GetCumulatedTime(3)<1000.0);
	 do {
	    x.Mat()= 0.0;
	    timing.Start(5);
	    op_test.MultInverse(y, x);
	    timing.End(5);
	 } while (timing.GetCumulatedTime(5)<1000.0);
	 x.Mat() -= x_ref.Mat();
	 bench_data.AddValue("error_test", x.Vec()*x.Vec());

	 bench_data.AddValue("mg_levels", ls);
	 bench_data.AddValue("K",K );
	 bench_data.AddValue("n_h", dof_space );
	 bench_data.AddValue("time_setup_inv_trial", timing.PopTime(0));
	 bench_data.AddValue("time_setup_inv_test", timing.PopTime(1));
	 bench_data.AddValue("time_mult_trial", timing.PopTime(2));
	 bench_data.AddValue("time_mult_test", timing.PopTime(3));
	 bench_data.AddValue("time_inverse_trial", timing.PopTime(4));
	 bench_data.AddValue("time_inverse_test", timing.PopTime(5));

	 delete[] buffer;
      }
   }

   for (int i=0; i<space_mats.Size(); ++i){
      delete space_mats[i];
   }
   cout << "\r" 
        << "                                              "
        << "                                              "
	<< "\r";
   bench_data.PrintData();

   return 0;
} 

