//                                MFEM example spaceTime
//
//
//

//#include "../data/bc/unitSquare_2D.hpp"
#include "../data/bc/L_shaped_2D.hpp"
#include "../data/bc/case_2.hpp"

#include <mfem.hpp>
#include <ocst.hpp>

#include <chrono>
#include <ctime> 
#include <iostream>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   cout << scientific; cout.precision(5);
   time_t today = chrono::system_clock::to_time_t(chrono::system_clock::now());
   cout << "# Bench iterative type 2 solver " << endl 
        << "# mesh= " << mesh_file << endl
	<< "# started " << ctime(&today); 
   int refinement_space = 2;
   int order_space = 1;
   int max_level = 3;
   //const std::vector<int> nof_timesteps{11, 21, 51, 101, 201, 501, 1001};
   const std::vector<int> nof_timesteps{11, 21, 51 , 101};
   
   ocst::BenchData bench_data;
   ocst::BenchTiming timing(3);
   bench_data.RegisterField("mg_levels", ocst::BenchData::Int);
   bench_data.RegisterField("K", ocst::BenchData::Int);
   bench_data.RegisterField("n_h", ocst::BenchData::Int);
   bench_data.RegisterField("time_setup", ocst::BenchData::Double);
   bench_data.RegisterField("time_primal", ocst::BenchData::Double);
   bench_data.RegisterField("time_adjoint", ocst::BenchData::Double);
   bench_data.RegisterField("error_primal", ocst::BenchData::Double);

   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;


   // Setup the device
   Device device(device_config);

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);

   Mesh mesh_space(mesh_file,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   FiniteElementSpaceHierarchy hierarchy(&mesh_space, &fes_space, false, false);

   Array<ocst::SpaceMatrices*> space_mats;
   Array<SparseMatrix*> A_space; 
   Array<SparseMatrix*> M_space; 

   space_mats.Append(new ocst::SpaceMatrices(fes_space));
   FunctionCoefficient robin_lhs(xi_func);
   FunctionCoefficient kappa_coeff(kappa_func);
   space_mats[0]->SetBLF(robin_lhs, kappa_coeff); 
   space_mats[0]->Assemble(); 
   A_space.Append(&space_mats[0]->A());
   M_space.Append(&space_mats[0]->M());
   

   for (int ls=1; ls<=max_level; ++ls){
      // Add level to space hierarchy 
      FiniteElementSpace* fes_coarse = &hierarchy.GetFinestFESpace();
      Mesh* mesh_refined = new Mesh(*fes_coarse->GetMesh());
      mesh_refined->UniformRefinement();
      FiniteElementSpace* fes_fine= 
	 new FiniteElementSpace(*fes_coarse, mesh_refined);
      ocst::SparseProlongation* prol= 
	 new ocst::SparseProlongation(*fes_coarse, *fes_fine);
      hierarchy.AddLevel(mesh_refined, fes_fine, prol, true, true, true);
      
      space_mats.Append(
	 new ocst::SpaceMatrices(*space_mats[ls-1], *fes_fine));
      space_mats[ls]->Assemble(); 
      M_space.Append(&space_mats[ls]->M());
      A_space.Append(&space_mats[ls]->A());
      int dof_space = space_mats.Last()->A().Height();
      ocst::GeometricMultigrid prec_A(hierarchy, A_space, true);
      ocst::GeometricMultigrid prec_M(hierarchy, M_space, true);

      ocst::SolveMGPCG solve_A(prec_A);
      ocst::SolveMGPCG solve_M(prec_M);

      for (auto K: nof_timesteps){
	 Mesh mesh_time(K-1,1.0);
	 Mesh mesh_time_ref(mesh_time);
	 mesh_time_ref.UniformRefinement();
	 FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
	 FiniteElementSpace fes_time_test(&mesh_time_ref, &fec_time_test);
	 ocst::TimeMatrices time_mats(fes_time_trial, fes_time_test, false);
	 double* buffer = new double[2*K*dof_space];

	 ocst::Control u(fes_time_test, hierarchy.GetFinestFESpace());
	 u.SetM_time(time_mats.M_test());
	 u.SetM_space(*M_space.Last());
	 FunctionCoefficient u_init(f_func);
	 u.SetValue(u_init); 

	 ocst::State y(fes_time_trial, hierarchy.GetFinestFESpace());
	 y.SetM_time(time_mats.M_trial());
	 y.SetM_space(*M_space.Last());
	 ocst::State y_ref(y);
	 ocst::AdjointState p(fes_time_test, hierarchy.GetFinestFESpace());
	 //RHS
	 FunctionCoefficient robin_rhs(eta_func);
	 FunctionCoefficient y_analytic_coeff(y_analytic);
	 y_analytic_coeff.SetTime(0.0);
	 ocst::PrimalRHS prhs(2*K-1, dof_space);
	 prhs.SetConstantPart(fes_time_trial, hierarchy.GetFinestFESpace(), 
			      robin_rhs, y_analytic_coeff, time_mats.M_mixed()); 
	 
	 ocst::SpaceTimeData y_d(K, dof_space,1.0);
	 Vector y_d_T; 
	 y_d.Mat().GetColumnReference(y_d.Mat().Width()-1, y_d_T); 
	 ocst::AdjointRHS arhs(K, dof_space, 1.0, 1.0);
	 arhs.SetConstantPart(y_d,y_d_T, time_mats.M_trial(), *M_space.Last());

	 ocst::SpaceTimeParabolicOperator B(time_mats.C_mixed(), 
	                                    time_mats.M_mixed(),
					    *M_space.Last(), *A_space.Last());
	 timing.Start(0);
	 ocst::NormOperatorTrial op_trial(time_mats.M_trial(), 
	                                  time_mats.A_trial(),
					  *M_space.Last(), *A_space.Last(), 
					  solve_A, false, buffer);
	 ocst::NormOperatorTest op_test(time_mats.M_test(), 
					*M_space.Last(), *A_space.Last(),
					buffer);
	 op_trial.SetInverseOperator(M_space, A_space, hierarchy);
	 op_test.SetInverseOperator(solve_A, solve_M); 

	 B.AssembleGeneralSylvester();
         ocst::LSQRSolver solver(B, prhs, arhs, 
                                 *op_trial.GetInverseOperator(),
				 *op_test.GetInverseOperator());
	 solver.SetSolverParameters(1e-5,25,1);
	 static_cast<ocst::InvNormOperatorTrial*>(
	    op_trial.GetInverseOperator())->SetTolerance(5e-6);
	 timing.End(0);

	 do {
	    y.Mat() = 0.0;
	    timing.Start(1);
	    solver.SolvePrimal(u, y, buffer);
	    timing.End(1);
	 } while (timing.GetCumulatedTime(1)<1000.0);
	 do {
	    p.Mat()= 0.0;
	    timing.Start(2);
	    solver.SolveAdjoint(y, p, buffer);
	    timing.End(2);
	 } while (timing.GetCumulatedTime(2)<1000.0);


	 GridFunction y_k(&hierarchy.GetFinestFESpace());
	 y_k.MakeRef(y.Vec(),0,dof_space);
	 for (int k=0; k<K; ++k){
	    y_k.MakeRef(y_ref.Vec(), k*dof_space, dof_space);
	    y_analytic_coeff.SetTime(k*1.0/(K-1));
	    y_k.ProjectCoefficient(y_analytic_coeff);
	 }
	 y.Mat() -= y_ref.Mat();
	 bench_data.AddValue("mg_levels", ls);
	 bench_data.AddValue("K",K-1 );
	 bench_data.AddValue("n_h", dof_space );
	 bench_data.AddValue("time_setup", timing.PopTime(0) );
	 bench_data.AddValue("time_primal",timing.PopTime(1) );
	 bench_data.AddValue("time_adjoint", timing.PopTime(2));
	 bench_data.AddValue("error_primal", y.SquaredInnerProductNorm() );

	 delete[] buffer;
      }
   }

   for (int i=0; i<space_mats.Size(); ++i){
      delete space_mats[i];
   }
   bench_data.PrintData();

   return 0;
} 

