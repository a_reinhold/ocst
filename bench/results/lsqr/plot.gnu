set term qt noenhanced

fname = '20210608_case2_lshaped2D.dat'


set  logscale xy 
set xlabel 'n_h*K'
set ylabel 'runtime'

set title "run times over n_h*K"
set autoscale
plot fname u ($2*$3):4 t columnheader(4) ,\
     fname u ($2*$3):5 t columnheader(5) ,\
     fname u ($2*$3):6 t columnheader(6)

 

pause -1 "press enter for next plot"
set xlabel 'n_h'
set autoscale
set title "runtimes and error over N"
plot for [i=0:7] fname u 3:5 every 7::i t sprintf("time_primal  K(%d)",i),\
     for [i=0:7] fname u 3:6 every 7::i t sprintf("time_adjoint K(%d)",i),\
     for [i=0:7] fname u 3:7 every 7::i t sprintf("error_primal K(%d)",i)

pause -1 #weird bug
pause -1 "press enter for next plot"
