set term qt noenhanced

fname = '20210607_case2_Lshaped.dat'


set  logscale xy 
set xlabel 'n_h*K'
set ylabel 'runtime'

set title "run times over n_h*K"
set autoscale
plot fname u ($2*$3):4 t columnheader(4) ,\
     fname u ($2*$3):5 t columnheader(5) ,\
     fname u ($2*$3):6 t columnheader(6)

 

pause -1 "press enter for next plot"
set xlabel 'n_h'
set autoscale
set title "runtimes and error over N"
plot for [i=0:8] fname u 3:5 every 8::i t sprintf("time_primal  K(%d)",i),\
     for [i=0:8] fname u 3:6 every 8::i t sprintf("time_adjoint K(%d)",i),\
     for [i=0:8] fname u 3:7 every 8::i t sprintf("error_primal K(%d)",i)

pause -1 #weird bug
pause -1 "press enter for next plot"
