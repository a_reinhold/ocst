set term qt noenhanced

fname = '20210608_lshaped2D_rand.dat'


set  logscale xy 
set xlabel 'n_h*K'
set ylabel 'runtime'

set title "run times over n_h*K"
set autoscale
plot fname u ($2*$3):4 t columnheader(4) ,\
     fname u ($2*$3):5 t columnheader(5) ,\
     fname u ($2*$3):6 t columnheader(6) ,\
     fname u ($2*$3):7 t columnheader(7) ,\
     fname u ($2*$3):8 t columnheader(8) ,\
     fname u ($2*$3):9 t columnheader(9)

 

pause -1 "press enter for next plot"

set ylabel 'error'
set title "error over n_h*K"
set autoscale
plot fname u ($2*$3):10 t columnheader(10) ,\
     fname u ($2*$3):11 t columnheader(11) 
pause -1 #weird bug
pause -1 "press enter for next plot"
