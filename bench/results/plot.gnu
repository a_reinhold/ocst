# All Values in one plot 

set logscale xy 
set autoscale
set xlabel 'n_h*K'
set ylabel 'cputime in ms'
datfile='bench_Norm.dat'

plot datfile every 2::0 using ($2*$3):'Mult' title 'Testspace Mult()' ,\
     datfile every 2::0 using ($2*$3):'MultInv' title "Testspace MultInverse()" ,\
     datfile every 2::1 using ($2*$3):'Mult' title "Trialspace Mult()" ,\
     datfile every 2::1 using ($2*$3):'MultInv' title "Trialspace MultInverse()" ,\
     [10000: 100000] 0.000005*x lc 'black' notitle,\
     [10000: 100000] 0.05 lc 'black' notitle

set arrow from 100000,0.05 to 100000,0.5 nohead
