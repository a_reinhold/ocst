set term wxt
fname1='20210521_sparseDenseMM.dat'
fname2='20210521_sparseDenseMM_K_fixed.dat'

                                 
set key outside
set logscale xy
set ylabel 'runtime in ms'


set title "all runtimes"
set xlabel 'K * N'
plot fname1 u ($1*$3):4 t 'intel' ls 1,\
     fname2 u ($1*$3):4 notitle   ls 1,\
     fname1 u ($1*$3):5 t 'naive' ls 2,\
     fname2 u ($1*$3):5 notitle ls 2

pause 1
pause -1  "Hit return to continue"

set title "variable K"

set xlabel 'N'
plot for [i=0:2] fname1 u 1:4 every :::i::i t sprintf("intel k=%d",i),\
     for [i=0:2] fname1 u 1:5 every :::i::i t sprintf("naive k=%d",i)

pause 1
pause -1  "Hit return to continue" #weird bug :D 
pause -1  "Hit return to continue"

set title "fixed K"

set xlabel 'N'
plot for [i=0:2] fname2 u 1:4 every :::i::i t sprintf("intel k=%d",i),\
     for [i=0:2] fname2 u 1:5 every :::i::i t sprintf("naive k=%d",i)

pause 1
pause -1  "Hit return to continue"
unset logscale y
set ylabel "speedup"
set title "variable K"
plot for[i=0:2] fname1 u 1:($5/$4) every :::i::i w linespoints t sprintf("k= %d",i),\
	1 w lines dt 2 


pause 1
pause -1  "Hit return to continue"
set title "fixed K"
plot for[i=0:2] fname2 u 1:($5/$4) every :::i::i w linespoints t sprintf("k= %d",i) ,\
	1 w lines dt 2 
