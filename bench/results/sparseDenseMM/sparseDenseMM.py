datfiles=['20210518_sparseDenseMM_1.dat',
          '20210518_sparseDenseMM_2.dat',
          '20210518_sparseDenseMM_3.dat']

for fname in datfiles:
    time = []
    with open(fname) as fobj:
        for line in fobj:
            if line[0].isdigit():
                time.append(float(line.split('\t')[-3]))
    n_elems = len(time)/2
    for i in range(n_elems/2):
        print("{:0.3f}".format(time[i]/time[i+n_elems]))
    print('')
