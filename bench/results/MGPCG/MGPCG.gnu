set term qt noenhanced

fname = '20210517_MGPCG_1.dat'


set  logscale xy 
set xlabel 'N'
set ylabel 'runtime'

set title "run times and setup times standard tolerance"
set autoscale
plot fname u 3:5 every :::2::2 t columnheader(5) ,\
     fname u 3:6 every :::2::2 t columnheader(6) ,\
     fname u 3:7 every :::2::2 t columnheader(7) ,\
     fname u 3:8 every :::2::2 t columnheader(8) ,\
 

pause -1 "press enter for next plot"
set autoscale
set title "runtimes for different tolerances"
plot for [i=0:2] fname u 3:7 every :::i::i t sprintf("%s, i = %d",columnheader(7),i),\
     for [i=0:2] fname u 3:8 every :::i::i t sprintf("%s, i = %d",columnheader(8),i)

pause -1 #weird bug
pause -1 "press enter for next plot"
set autoscale
set title "average error for different tolerances"
plot for [i=0:2] fname u 3:9  every :::i::i t sprintf("%s, i = %d",columnheader(9) ,i),\
     for [i=0:2] fname u 3:10 every :::i::i t sprintf("%s, i = %d",columnheader(10),i)

pause -1 "press enter for next plot"
unset logscale y
set ylabel "\# iterations"
set title " iterations for different tolerances"
plot for [i=0:2] fname u 3:11 every :::i::i t sprintf("%s, i = %d",columnheader(11),i) w linespoints,\
     for [i=0:2] fname u 3:12 every :::i::i t sprintf("%s, i = %d",columnheader(12),i) w linespoints

pause -1 "press enter for next plot"
set ylabel "speedup"
set title " speedup sparse/mat_free"
plot for [i=0:2] fname u 3:($8/$7) every :::i::i t sprintf("%s, i = %d","runtime",i) w linespoints,\
     for [i=0:2] fname u 3:($12/$11) every :::i::i t sprintf("%s, i = %d","iterations",i) w linespoints




#fname = '20210517_MGPCG_2.dat'


