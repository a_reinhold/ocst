#include <mfem.hpp>
#include <ocst.hpp>

#include <string>
#include <iostream>
#include <chrono>
#include <random>

using namespace std;
using namespace mfem;
/*
 * BENCH to test intels sparse dense MM product vs a naive loop based 
 * implementation, for intel the sparse format must be altered which is measured
 * as well 
 *
 * The test matrix is a stiffnesmatrix for an uniformly refined mesh, 
 * the inner dimension k is set as a fraction of the dimension n 
 */
int 
main(int argc, char* argv[]){
  
   cout << scientific; cout.precision(5);
   //const char *mesh_file = "../data/unitCube_3D.mesh";
   const char *mesh_file = "../data/unitSquare_2D.mesh";
   const char *device_config = "cpu";
   
   // Setup the device
   Device device(device_config);
   OptionsParser args(argc, argv);
   
   int nof_runs = 12;
   int max_refinement = 6;
   int order=2; 
   int K=100;
   int case_flag = 0; /* 0 -> alpha = 1.0, beta 0.0; 
			 1 -> alpha , beta random betweeen (-10, 10); */
   bool print_header=true;
   args.AddOption(&nof_runs, "-n", "--nof_runs", 
		  "nof_runs = nof_runs- current refinement");
   args.AddOption(&order, "-o", "--order", 
	          "order of the finite element space");
   args.AddOption(&max_refinement, "-l", "--max_refinement",
	          "max number of refinements");
   args.AddOption(&case_flag, "-c", "--case_flag", 
	          "mode for selection of alpha and beta");
   args.AddOption(&K, "-k", "--K", 
	          "matrix diimension K ");
   args.AddOption(&print_header, "-ph", "--header", "-nh", "--no-header","" );


   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   std::string implementation;
   #ifdef _OCST_USE_INTEL_MKL
      implementation = "intel mkl";
   #else
      implementation = "naive";
   #endif
   double alpha;
   double beta;
   switch (case_flag){
      case 1: {
	 int seed = 1;
	 mt19937  mt(seed);
	 //mt19937 mt;
	 uniform_real_distribution<double> uniform_real(-10.0,10.0);
	 alpha = uniform_real(mt);
	 beta  = uniform_real(mt); 
	 }
	 break;
      default:
	 alpha = 1.0;
	 beta  = 0.0;
   }

   ocst::BenchData bench_data;
   bench_data.RegisterField("m", ocst::BenchData::Int);
   bench_data.RegisterField("n", ocst::BenchData::Int);
   bench_data.RegisterField("nnz(A)", ocst::BenchData::Int);
   bench_data.RegisterField("k", ocst::BenchData::Int);
   bench_data.RegisterField("elpased_time[ms]", ocst::BenchData::Double);
   bench_data.RegisterField("nof_runs", ocst::BenchData::Int);
   std::cout << 
      "#SparseDenseMM Bench " << endl <<
      "# alpha = " << alpha << "\tbeta = " << beta << "\torder = " << order <<
      " implementation = "<< implementation <<endl; 

   ocst::BenchTiming timing(1); 
   for (int i=0; i<max_refinement; ++i){
      int nof_runs_ = std::max(1,nof_runs - i);
      

      //Prepare Sparse Matrix
      Mesh mesh(mesh_file,1,1);
      H1_FECollection fec(order,mesh.Dimension());
      for (int l=0; l<i; ++l){
	 mesh.UniformRefinement();
      }
      FiniteElementSpace fes(&mesh, &fec);

      BilinearForm a(&fes);
      mfem::ConstantCoefficient coeff{1.2};
      a.AddDomainIntegrator(new DiffusionIntegrator(coeff));
      a.Assemble();
      a.Finalize();
      mfem::SparseMatrix& A = a.SpMat(); 
      
      //quadratic case
      int m_A = A.Height();
      int n_A = A.Width();


      DenseMatrix B(n_A,K);
      DenseMatrix C(m_A,K);
      
      Vector b(B.GetData(), B.Width()*B.Height());
      b.Randomize();

      Vector c(C.GetData(), C.Width()*C.Height());
      c.Randomize();
      
      if (i==0){
	 ocst::SparseDenseMM(ocst::Transposed::NON_TRANSPOSED,
			     ocst::Transposed::NON_TRANSPOSED,
			     ocst::Transposed::NON_TRANSPOSED,
			     alpha, A, B, beta, C);
      }
      for (int l=0; l<nof_runs_; ++l){
	 timing.Start(0);
	 ocst::SparseDenseMM(ocst::Transposed::NON_TRANSPOSED,
			     ocst::Transposed::NON_TRANSPOSED,
			     ocst::Transposed::NON_TRANSPOSED,
			     alpha, A, B, beta, C);
	 timing.End(0);
      }
      bench_data.AddValue("nof_runs", nof_runs_);
      bench_data.AddValue("m", m_A);
      bench_data.AddValue("n", n_A);
      bench_data.AddValue("k", K);

      bench_data.AddValue("nnz(A)", A.NumNonZeroElems());
      bench_data.AddValue("elpased_time[ms]", timing.PopTime(0)); 

   }
   bench_data.PrintData(print_header); 
}
