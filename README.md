# OCST

MFEM [(mfem.org)](https://mfem.org) based library to solve parabolic control 
constrained optimal control problems 

The current supported mfem version is 4.6

An automatic generated doxygen documentation can be found 
[here](https://a_reinhold.gitlab.io/ocst/)
