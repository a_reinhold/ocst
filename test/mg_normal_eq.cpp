// #include "../data/bc/unitSquare_2D.hpp"
#include "../data/bc/L_shaped_2D.hpp"
#include "../data/bc/case_1.hpp"
#include "linalg/solvers.hpp"

#include <mfem.hpp>
#include <ocst.hpp>

#include <iostream>

using namespace std;
using namespace mfem;

void create_prolongations_time_test(
    mfem::FiniteElementSpaceHierarchy &hierarchy,
    mfem::Array<mfem::SparseMatrix *> &prolongations) {
   int n_levels{hierarchy.GetNumLevels()};
   for (int level = 0; level < n_levels - 1; ++level) {
      const mfem::SparseMatrix &original_prol =
          static_cast<ocst::SparseProlongation *>(
              hierarchy.GetProlongationAtLevel(level))
              ->SpMat();
      int m = original_prol.Height();
      int n = original_prol.Width();

      const int *I = original_prol.GetI();
      const int *J = original_prol.GetJ();
      const double *data = original_prol.GetData();

      int *I_ = new int[m + 2];
      int *J_ = new int[I[m] + 1];
      double *data_ = new double[I[m] + 1];

      for (int i = 0; i < m + 1; ++i) {
         I_[i] = I[i];
      }
      I_[m + 1] = I[m] + 1;

      for (int l = 0; l < I[m]; ++l) {
         J_[l] = J[l];
         data_[l] = data[l];
      }
      J_[I_[m + 1] - 1] = n;
      data_[I_[m + 1] - 1] = 1.0;

      prolongations[level] =
          new mfem::SparseMatrix(I_, J_, data_, m + 1, n + 1);
   }
}

class NormalOperator : public ocst::TensorOperator {
 private:
   const ocst::TensorOperator *op;
   mutable mfem::Vector tmp;

 public:
   NormalOperator(const ocst::TensorOperator &op_)
       : ocst::TensorOperator{op_.Width(), op_.Width()}, op{&op_},
         tmp{op_.Height()} {}

   virtual void Mult(const mfem::Vector &x, mfem::Vector &y) const override {
      op->Mult(x, tmp);
      op->MultTranspose(tmp, y);
   }
   virtual void Assemble() const override {
      if (mat) {
         return;
      }
      tmp = 1.0;
      mat = mfem::Mult_AtDA(op->SpMat(), tmp);
      if (diag) {
         delete diag;
      }
      diag = new mfem::Vector(mat->Height());
      mat->GetDiag(*diag);
   }
   virtual void GetDiag(mfem::Vector &d) const override {
      if (!diag) {
         diag = new mfem::Vector(op->Width());
         // *diag = 1.0;
	 this->SpMat().GetDiag(*diag);
      }
      d = *diag;
      return;
   }
};

int main(int argc, char *argv[]) {
   // Parse command-line options.

   bool paraview = false;
   const char *pvname = "non_sym_mg";
   int initial_refinement = 1;
   int order_space = 1;
   int nof_nodes_time = 11;
   int levels_mg = 3; // f for now same nof levels in space and time

   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&paraview, "-pv", "--paraview", "-no-paraview",
                  "--no-paraview", "Save files for paraview Visualization");
   args.AddOption(&levels_mg, "-l", "--levels_mg",
                  "levels for MG space dimension");
   args.AddOption(&nof_nodes_time, "-K", "--nof_nodes_time",
                  "levels for MG time dimension");

   args.Parse();
   if (!args.Good()) {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);
   // Setup the device
   Device device(device_config);
   // Meshes, FE_Collections and spaces

   Mesh mesh_time = Mesh::MakeCartesian1D(nof_nodes_time - 1, 1.0);

   H1_FECollection fec_time_trial(1, 1);
   L2_FECollection fec_time_test(0, 1);

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);

   Mesh mesh_space(mesh_file, 1, 1);
   for (std::size_t i = 0; i < initial_refinement; ++i) {
      mesh_space.UniformRefinement();
   }
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);

   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy_trial(
       fes_time_trial, levels_mg, fes_space, levels_mg);

   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy_test(
       fes_time_test, levels_mg, fes_space, levels_mg);

   mfem::Array<mfem::SparseMatrix *> prolongations_time_test{levels_mg - 1};
   mfem::Array<mfem::Operator *> prolongations_test{levels_mg - 1};
   mfem::Array<mfem::Operator *> prolongations_trial{levels_mg - 1};
   mfem::FiniteElementSpaceHierarchy &hierarchy_time_test =
       st_hierarchy_test.FESHierarchyTime();
   mfem::FiniteElementSpaceHierarchy &hierarchy_time_trial =
       st_hierarchy_trial.FESHierarchyTime();
   create_prolongations_time_test(hierarchy_time_test, prolongations_time_test);
   mfem::FiniteElementSpaceHierarchy &hierarchy_space =
       st_hierarchy_trial.FESHierarchySpace();

   for (int level = 0; level < levels_mg - 1; ++level) {
      mfem::SparseMatrix *prol_time_trial =
          &static_cast<ocst::SparseProlongation *>(
               hierarchy_time_trial.GetProlongationAtLevel(level))
               ->SpMat();
      mfem::SparseMatrix *prol_space =
          &static_cast<ocst::SparseProlongation *>(
               hierarchy_space.GetProlongationAtLevel(level))
               ->SpMat();
      prolongations_trial[level] =
          new ocst::KroneckerOperator(*prol_time_trial, *prol_space);
      prolongations_test[level] = new ocst::KroneckerOperator(
          *prolongations_time_test[level], *prol_space, true, false);
   }

   Array<ocst::TimeMatrices *> time_mats{levels_mg};
   Array<ocst::SpaceMatrices *> space_mats{levels_mg};
   mfem::Array<mfem::SparseMatrix *> A_space{levels_mg}, M_space{levels_mg};
   mfem::Array<mfem::SparseMatrix *> C_time{levels_mg}, M_time{levels_mg};

   space_mats[0] =
       new ocst::SpaceMatrices(hierarchy_space.GetFESpaceAtLevel(0));
   FunctionCoefficient robin_lhs(xi_func);
   FunctionCoefficient kappa_coeff(kappa_func);
   ConstantCoefficient one(1.0);
   space_mats[0]->SetBLF(robin_lhs, kappa_coeff);
   space_mats[0]->Assemble();
   for (int level = 1; level < levels_mg; ++level) {
      space_mats[level] = new ocst::SpaceMatrices(
          *space_mats[level - 1], hierarchy_space.GetFESpaceAtLevel(level));
      space_mats[level]->Assemble();
   }
   for (int level = 0; level < levels_mg; ++level) {
      time_mats[level] =
          new ocst::TimeMatrices(hierarchy_time_trial.GetFESpaceAtLevel(level),
                                 hierarchy_time_test.GetFESpaceAtLevel(level));
      A_space[level] = &space_mats[level]->A();
      M_space[level] = &space_mats[level]->M();
      M_time[level] = &time_mats[level]->M_mixed_mod();
      C_time[level] = &time_mats[level]->C_mixed_mod();
   }
   int dof_time = M_time.Last()->Height();
   int dof_space = A_space.Last()->Height();
   cout << " n = " << dof_space << endl;
   cout << " K_trial = " << dof_time << endl;

   // Reference Solution
   ocst::Control u(hierarchy_time_test.GetFinestFESpace(),
                   hierarchy_space.GetFinestFESpace());
   u.SetM_time(time_mats.Last()->M_test());
   u.SetM_space(*M_space.Last());
   FunctionCoefficient u_init(f_func);
   u.SetValue(u_init);
   //
   ocst::State y_iter(hierarchy_time_trial.GetFinestFESpace(),
                      hierarchy_space.GetFinestFESpace());
   y_iter.SetM_time(time_mats.Last()->M_trial());
   y_iter.SetM_space(*M_space.Last());

   ocst::AdjointState p(hierarchy_time_test.GetFinestFESpace(),
                        hierarchy_space.GetFinestFESpace());
   // RHS
   FunctionCoefficient robin_rhs(eta_func);
   FunctionCoefficient y_analytic_coeff(y_analytic);

   y_analytic_coeff.SetTime(0.0);
   ocst::State y_ref(y_iter);
   GridFunction y_k(&hierarchy_space.GetFinestFESpace());
   for (int k = 0; k < dof_time; ++k) {
      y_k.MakeRef(y_ref.Vec(), k * dof_space, dof_space);
      y_analytic_coeff.SetTime(k * 1.0 / (dof_time - 1));
      y_k.ProjectCoefficient(y_analytic_coeff);
   }
   y_analytic_coeff.SetTime(0.0);
   y_k.MakeRef(y_iter.Vec(), 0, dof_space);
   ocst::PrimalRHS prhs(dof_time, dof_space);
   prhs.SetConstantPart(hierarchy_time_trial.GetFinestFESpace(),
                        hierarchy_space.GetFinestFESpace(), robin_rhs,
                        y_analytic_coeff, time_mats.Last()->M_mixed());

   ocst::SpaceTimeData y_d(dof_time, dof_space, 1.0);
   Vector y_d_T;
   y_d.Mat().GetColumnReference(y_d.Mat().Width() - 1, y_d_T);
   ocst::AdjointRHS arhs(dof_time, dof_space, 1.0, 1.0);
   arhs.SetConstantPart(y_d, y_d_T, time_mats.Last()->M_trial(),
                        *M_space.Last());
   using OperatorType = ocst::GeometricMultigrid::OperatorType;

   mfem::Array<ocst::GeneralSylvesterOperator *> B_parabolic{levels_mg};
   mfem::Array<NormalOperator *> B_normal{levels_mg};
   for (int level = 0; level < levels_mg; ++level) {
      B_parabolic[level] = new ocst::GeneralSylvesterOperator(
          *C_time[level], *M_space[level], *M_time[level], *A_space[level]);
      B_normal[level] = new NormalOperator(*B_parabolic[level]);
   }

   mfem::FiniteElementSpaceHierarchy hierarchy_trial{nullptr, nullptr, false,
                                                     false};
   for (int level = 0; level < levels_mg - 1; ++level) {
      hierarchy_trial.AddLevel(nullptr, nullptr, prolongations_trial[level],
                               false, false, true);
   }
   ocst::GeometricMultigrid prec_B{hierarchy_trial, B_normal,
                                   OperatorType::TENSOR};
   prec_B.SetCycleType(mfem::Multigrid::CycleType::VCYCLE, 1, 1); 

   // mfem::OperatorChebyshevSmoother smoother()
   const NormalOperator &B_normal_finest{*B_parabolic.Last()};
   mfem::CGSolver solver;
   solver.SetOperator(B_normal_finest);
   solver.SetPreconditioner(prec_B);
   solver.SetPrintLevel(1);
   solver.SetAbsTol(1e-5);
   solver.SetRelTol(1e-6);
   solver.SetMaxIter(1000);
   prhs.Assemble(u);
   mfem::Vector rhs{B_parabolic.Last()->Width()};
   B_parabolic.Last()->MultTranspose(prhs.Vec(), rhs);

   ocst::State y_direct{y_iter};
   solver.Mult(rhs, y_iter.Vec());
   // Direct Solver
   mfem::UMFPackSolver direct_solver;
   direct_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
   direct_solver.SetOperator(B_normal_finest.SpMat());
   direct_solver.Mult(rhs, y_direct.Vec());

   y_direct.Mat() -= y_iter.Mat();
   cout << "||y_iter - y_direct||_L2 = " << y_direct.SquaredInnerProductNorm()
        << endl;

   // solver.SetOperator();
   // solver.SolvePrimal(u, y);
   // solver.SolveAdjoint(y, p);
   // Iterative Solver
   // {
   //    ocst::GeometricMultigrid prec_A(hierarchy_space, A_space,
   //                                    OperatorType::SPARSE);
   //    ocst::GeometricMultigrid prec_M(hierarchy_space, M_space,
   //                                    OperatorType::SPARSE);
   //
   //    ocst::SolveMGPCG solve_A(prec_A);
   //    ocst::SolveMGPCG solve_M(prec_M);
   //
   //    ocst::IterativeType1Solver solver(prhs, arhs, M_space, A_space,
   //    solve_M,
   //                                      hierarchy_space, dof_time - 1, 1.0);
   //    cout << "Iterative Solver" << endl;
   //    cout << "Solve Primal" << endl;
   //    solver.SolvePrimal(u, y);
   //    cout << "Solve Adjoint" << endl;
   //    solver.SolveAdjoint(y, p);
   // }
   if (paraview) {
      pd = new mfem::ParaViewDataCollection(
          pvname, hierarchy_space.GetFinestFESpace().GetMesh());
      pd->SetPrefixPath("paraview");
      pd->SetLevelsOfDetail(order_space);
      pd->SetDataFormat(VTKFormat::BINARY);
      pd->SetHighOrderOutput(true);
      ocst::ExportToParaview(pd, "y", y_iter.Mat(), y_iter.FES_Space(),
                             &y_iter.FES_Time());
      // ocst::ExportToParaview(pd, "y_ref", y_ref.Mat(),
      //                        y.FES_Space(), &y.FES_Time());
      delete pd;
      pd = new mfem::ParaViewDataCollection(
          "reference", hierarchy_space.GetFinestFESpace().GetMesh());
      pd->SetPrefixPath("paraview");
      pd->SetLevelsOfDetail(order_space);
      pd->SetDataFormat(VTKFormat::BINARY);
      pd->SetHighOrderOutput(true);
      ocst::ExportToParaview(pd, "y_ref", y_ref.Mat(), y_iter.FES_Space(),
                             &y_iter.FES_Time());
      delete pd;
   }
   y_iter.Mat() -= y_ref.Mat();
   cout << "||y_h - y_ref||^2_{L_2(I,Omega)} = "
        << y_iter.SquaredInnerProductNorm() << endl;
   for (int level = 0; level < levels_mg - 1; ++level) {
      delete prolongations_test[level];
   }
   for (int level = 0; level < levels_mg; ++level) {
      delete B_parabolic[level];
      delete B_normal[level];
      delete time_mats[level];
      delete space_mats[level];
   }

   return 0;
}
