//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;

namespace mfem{
void add(const mfem::Vector &v1, double alpha,const mfem::Vector &v2, mfem::Vector &v);   
}
int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   int refinement_space = 11;
   int order_space = 1;
   int levels_space = 1;
   double gamma = 1e4;


   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
                  "Number of initial refinements to perform on the spatial mesh");
   args.AddOption(&levels_space, "-ls", "--levels_space",
                  "levels for MG space dimension");
   args.AddOption(&gamma, "-gamma", "--gamma", "  "); 
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Meshes, FE_Collections and spaces


   Mesh mesh_space(refinement_space-1,1.0);
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);


   //MG Prep
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(fes_space, 
                                                           1,
                                                           fes_space, 
							   levels_space);
   FiniteElementSpaceHierarchy& hierarchy_space = 
                                     st_hierarchy.FESHierarchySpace();
   
   int dof_space = hierarchy_space.GetFinestFESpace().GetVSize();

  


   Array<ocst::SpaceMatrices*> space_mats;
   Array<SparseMatrix*> A_space; 
   Array<SparseMatrix*> M_space; 

   space_mats.Append(new ocst::SpaceMatrices(
       st_hierarchy.FESHierarchySpace().GetFESpaceAtLevel(0)));
   FunctionCoefficient robin_lhs(xiFunc);
   space_mats[0]->SetBLF(robin_lhs); 
   space_mats[0]->Assemble(); 

   A_space.Append(&space_mats[0]->A());
   M_space.Append(&space_mats[0]->M());
   for (int i=1; i<levels_space; ++i){
      space_mats.Append(new ocst::SpaceMatrices(*space_mats[i-1],
          hierarchy_space.GetFESpaceAtLevel(i)));
      space_mats[i]->Assemble();
      A_space.Append(&space_mats[i]->A());
      M_space.Append(&space_mats[i]->M());
   }


   Array<ocst::HelmholtzNormalReal*> helmholtz_normal;
   Array<ocst::HelmholtzReal*> helmholtz_real;
   for (int i=0; i<levels_space; ++i){
      helmholtz_normal.Append(
	 new ocst::HelmholtzNormalReal(*M_space[i], *A_space[i], gamma));
   }
   
   ocst::GeometricMultigrid prec_hh_normal(hierarchy_space, 
                                           helmholtz_normal, ocst::GeometricMultigrid::OperatorType::MATRIX_FREE);
   ocst::SolveMGPCG solve_hh_normal(prec_hh_normal);
   solve_hh_normal.SetSolverParameters(1e-6, 1e-4, 50, 2); 

   mfem::Vector gamma_v(1); gamma_v=sqrt(gamma);
   ocst::SolveHelmholtzRealValued solve_hh_real(M_space, A_space, 
                                                hierarchy_space, gamma_v);
   solve_hh_real.SetTimeStep(0);

   Vector f(dof_space);

   Vector x_normal(dof_space);
   Vector x_real(dof_space);
   Vector x_ref(dof_space);
   f= 1.0; 
   Vector Af(dof_space);
   
    
   A_space.Last()->Mult(f,Af);
   cout << "Solve Normal" << endl;
   solve_hh_normal.Mult(Af,x_normal);
   cout << "Solve Real" << endl; 
   solve_hh_real.Mult(f,x_real);

   //cout << "f" << endl;
   //f.Print();
   //cout << "x_ref" << endl;
   //x_ref.Print();
   //cout << "x_real" << endl;
   //x_real.Print();

   cout << endl;
   cout << "norm real " << x_real * x_real << endl;
   cout << "norm normal " << x_normal * x_normal << endl;



   for (int i=0; i<levels_space; ++i){
      delete space_mats[i];
      delete helmholtz_normal[i]; 
   }
   return 0; 
}
