//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp> 
#include <ocst.hpp>


#include <fstream>
#include <iostream>
#include <functional>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   int order_space = 1;
   int nof_timesteps = 11;
   int nof_space_nodes=21;
   const char *device_config = "cpu";

   OptionsParser args(argc, argv);

   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
                  "Number of timesteps");
   args.AddOption(&nof_space_nodes, "-nh", "--nof_space_nodes",
                  "Number of spatial nodess");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();
   // Meshes, FE_Collections and spaces

   Mesh mesh_time = Mesh::MakeCartesian1D(nof_timesteps-1,1.0);

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);

   
   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);

   Mesh mesh_space= Mesh::MakeCartesian1D(nof_space_nodes-1,1.0);
  
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);

   ocst::OptimalControlProblem ocp(fes_time_trial, fes_time_test, fes_space, 
				   1e-3, 1.0, 0.0); 
   ocp.SetControlConstrains(-2.0,2.0);

   // mfem::FunctionCoefficient y_d_coeff([](mfem::Vector& x, double t){
   //                                                       return x[0]*x[0]*t;});
   
   mfem::FunctionCoefficient y_d_coeff(std::function<double(const mfem::Vector&, double)>{[](const mfem::Vector& x, double t)-> double { return x[0]*x[0]*t;}});
   mfem::FunctionCoefficient robin_rhs_coeff([](mfem::Vector& x, double t){
                                                  return (1.0-x[0])*(t-1.0);});
  
   mfem::FunctionCoefficient robin_lhs_coeff([](mfem::Vector& x){
                                               return (x[0]+0.1)*(x[0]+0.1);});

   mfem::ConstantCoefficient y_0_coeff(0.2); 
   ocp.SetDesiredState(y_d_coeff);

   ocst::SpaceMatrices space_matrices(fes_space);
   space_matrices.SetBLF(robin_lhs_coeff);
   space_matrices.Assemble();

   ocst::TimeMatrices time_matrices(fes_time_trial, fes_time_test);

   ocp.State().SetM_time(time_matrices.M_trial());
   ocp.State().SetM_space(space_matrices.M());
   ocp.Control().SetM_time(time_matrices.M_test());
   ocp.Control().SetM_space(space_matrices.M());
   ocp.SetConstantPartRHS(y_0_coeff, robin_rhs_coeff, 
                             time_matrices.M_mixed());
   
   ocst::SpaceTimeParabolicOperator parabolic_op(time_matrices.C_mixed(),
                                                 time_matrices.M_mixed(),
				                 space_matrices.M(), 
						 space_matrices.A());
   parabolic_op.AssembleMonolithic();

   ocst::SpaceTimeDirectSolver solver(parabolic_op, 
                                      ocp.PrimalRHS(), ocp.AdjointRHS());
   solver.SetOperator();
   ocst::ProjectedGradientMethod pgm(ocp, solver, 100, 100);

   pgm.Solve();
   pgm.PrintResults();
   return 0;
}
