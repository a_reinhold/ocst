//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include <ocst.hpp> 

#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;

double
reference_solution_func(const Vector& x, const double t)
{ return t * x(0) + (1 - t*t) * x(1)*x(1);}

double
kappa_func(const mfem::Vector& x) 
{ return 1.0; } 

double
xi_func(const mfem::Vector& x) 
{
   return 0.25 * x(0)*x(0) * x(1) + 0.1;  
}

int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   int refinement_space = 2;
   int order_space = 2;
   int levels_space = 2;
   
   int initial_timesteps= 11;
   int order_time= 2;
   int levels_time= 2;

   bool static_cond = false;
   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");

   args.AddOption(&order_space, "-os", "--order_space",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of initial refinements to perform on the spatial mesh");
   args.AddOption(&levels_space, "-ls", "--levels_space",
   		  "No. of spatial levels for the multigrid solver");

   args.AddOption(&order_time, "-ot", "--order_time",
                  "Finite element order (polynomial degree) or -1 ");
   args.AddOption(&initial_timesteps, "-K", "--initial_timesteps", 
	          "Number of initial nodes in the time mesh");
   args.AddOption(&levels_time, "-lt", "--levels_time",
   		  "No. of levels in time for the multigrid solver");

   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);

   // Meshes, FE_Collections and spaces
   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }
   Mesh mesh_time= Mesh::MakeCartesian1D(initial_timesteps-1,1.0);

   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   
   H1_FECollection fec_time(order_time, mesh_time.Dimension());
   FiniteElementSpace fes_time(&mesh_time, &fec_time);
   
   // MG Prep
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(fes_time, 
   							   levels_time,
                                                           fes_space,
							   levels_space); 

   mfem::FiniteElementSpaceHierarchy& hierarchy_time = 
                                               st_hierarchy.FESHierarchyTime();
			     
   mfem::FiniteElementSpaceHierarchy& hierarchy_space = 
                                               st_hierarchy.FESHierarchySpace();

   char fname[128];
   for (int i = 0; i < levels_space-1; ++i){
      std::sprintf(fname, "prolongation_space_%d.mtx", i);
      ofstream file;
      file.open(fname);
      mfem::Operator* op = hierarchy_space.GetProlongationAtLevel(i);
      ocst::SparseProlongation* prol  = static_cast<ocst::SparseProlongation*>(op);
      prol->SpMat().PrintMM(file);
      file.close();
   }
   for (int i = 0; i < levels_time-1; ++i){
      std::sprintf(fname, "prolongation_time_%d.mtx", i);
      ofstream file;
      file.open(fname);
      mfem::Operator* op = hierarchy_time.GetProlongationAtLevel(i);
      ocst::SparseProlongation* prol  = static_cast<ocst::SparseProlongation*>(op);
      prol->SpMat().PrintMM(file);
      file.close();
   }
   return 0; 
}
