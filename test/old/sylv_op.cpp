#include "fem/bilinearform.hpp"
#include <mfem.hpp>
#include <ocst.hpp>

int main (int argc, char *argv[]) {

   int n = 30;
   mfem::Mesh msh = mfem::Mesh::MakeCartesian1D(n);

   mfem::H1_FECollection fec_A_trial{1, msh.Dimension()};
   mfem::L2_FECollection fec_B_trial{2, msh.Dimension()};

   mfem::FiniteElementSpace fes_A_trial{&msh, &fec_A_trial};
   mfem::FiniteElementSpace fes_B_trial{&msh, &fec_B_trial};

   mfem::ConstantCoefficient one{1.0};

   mfem::BilinearForm a_sym{&fes_A_trial};
   mfem::BilinearForm b_sym{&fes_B_trial};

   a_sym.AddDomainIntegrator(new mfem::MassIntegrator(one));
   b_sym.AddDomainIntegrator(new mfem::MassIntegrator(one));

   a_sym.Assemble(); a_sym.Finalize();
   b_sym.Assemble(); b_sym.Finalize();
   
   {
   //Symmetric Case
   
   int n_A{a_sym.Height()};
   int n_B{b_sym.Height()};
   
   ocst::KroneckerOperator kron{a_sym.SpMat(), b_sym.SpMat()}; //, buffer};mfree
   ocst::GeneralSylvesterOperator sylv{a_sym.SpMat(), b_sym.SpMat(), a_sym.SpMat(), b_sym.SpMat()};

   mfem::Vector x{n_A*n_B};
   x = 3.0;
   mfem::Vector y_kron{n_A*n_B};
   mfem::Vector y_sylv{n_A*n_B};
   kron.Mult(x, y_kron);
   y_kron *= 2.0;

   sylv.Mult(x, y_sylv);
   mfem::Vector diff{y_sylv};
   diff -= y_kron;
   std::cout << "norm y " <<y_sylv.Norml2() <<std::endl;
   std::cout << "norm y diff " <<diff.Norml2() <<std::endl;

   mfem::Vector x_mfree{n_A*n_B};
   mfem::Vector x_assembled{n_A*n_B};
   mfem::Vector x_direct{n_A*n_B};
   x_mfree = 0.0;
   x_assembled = 0.0;
   mfem::CGSolver CG; 
   CG.SetPrintLevel(0);
   CG.SetMaxIter(30);
   CG.SetAbsTol(1e-8);
   mfem::UMFPackSolver UMF;
   UMF.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
   CG.SetOperator(sylv);
   CG.Mult(y_sylv, x_mfree);

   CG.SetOperator(sylv.SpMat());
   CG.Mult(y_sylv, x_assembled);

   UMF.SetOperator(sylv.SpMat());
   UMF.Mult(y_sylv, x_direct);
   diff = x_direct;
   diff -= x;
   std::cout << "x norm diff_direct" <<diff.Norml2() <<std::endl;
   diff = x_mfree;
   diff -= x;
   std::cout << "x norm diff_mfree" <<diff.Norml2() <<std::endl;

   diff = x_assembled;
   diff -= x;
   std::cout << "x norm diff_assembled " <<diff.Norml2() <<std::endl;
   
   }
   return 0;
}
