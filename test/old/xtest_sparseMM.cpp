#include <mfem.hpp>
#include <linalgAuxiliary.hpp>

#include <iostream>
#include <mkl.h>

int 
main(){
   {
   const int m = 4;
   const int n = 3;
   const int k = 2;


   //A
   int I[m+1] = {0, 1, 2, 3, 5};
   int J[5] = {0, 0, 1, 0, 1};
   double data[5] = {0.5, 0.2, 0.4, 0.1, 0.2};
   mfem::SparseMatrix A(I,J,data,m,k,false, false, true);

   mfem::DenseMatrix B(k,n);
   double init=1.0;
   for (int i=0; i<B.Height(); ++i){
      for (int j=0; j<B.Width(); ++j){
	 B(i,j) = init;
	 init += 1.0;
      }
   }

   std::cout << "B = " << std::endl;
   B.Print();
   
   mfem::DenseMatrix C(m,n);

   double alpha = 1.0; 
   double beta = 0.0; 

   ocst::SparseDenseMM(ocst::Transposed::NON_TRANSPOSED,
		       ocst::Transposed::NON_TRANSPOSED,
		       ocst::Transposed::NON_TRANSPOSED,
		       alpha, A, B, beta, C);

   std::cout << "C = " << std::endl;
   C.Print();


   }
   std::cout << std::endl << std::endl;  
   {
   const int m = 4;
   const int n = 3;
   const int k = 2;


   //A
   int I[k+1] = {0, 2, 4};
   int J[4] = {0, 3, 0, 2};
   double data[4] =  {1.0, 4.0, 0.5, 2.0};
   mfem::SparseMatrix A(I,J,data,k,m,false, false, true);

   mfem::DenseMatrix B(k,n);
   double init=1.0;
   for (int i=0; i<B.Height(); ++i){
      for (int j=0; j<B.Width(); ++j){
	 B(i,j) = init;
	 init += 1.0;
      }
   }

   std::cout << "B = " << std::endl;
   B.Print();
   
   mfem::DenseMatrix C(m,n);

   double alpha = 1.0; 
   double beta = 0.0; 

   ocst::SparseDenseMM(ocst::Transposed::TRANSPOSED,
		       ocst::Transposed::NON_TRANSPOSED,
		       ocst::Transposed::NON_TRANSPOSED,
		       alpha, A, B, beta, C);

   std::cout << "C = " << std::endl;
   C.Print();


   }
   std::cout << std::endl << std::endl;  
   {
   const int m = 3;
   const int n = 2;
   const int k = 4;


   //A
   int I[m+1] = {0, 2, 3, 5};
   int J[5] = {0, 2, 2, 0, 3};
   double data[5] =  {0.25, 0.1, 0.2, 0.2, 0.3};
   mfem::SparseMatrix A(I,J,data,m ,k ,false, false, true);

   mfem::DenseMatrix B(n,k);
   double init=1.0;
   for (int i=0; i<B.Height(); ++i){
      for (int j=0; j<B.Width(); ++j){
	 B(i,j) = init;
	 init += 1.0;
      }
   }

   std::cout << "B = " << std::endl;
   B.Print();
   
   mfem::DenseMatrix C(n,m);

   double alpha = 1.0; 
   double beta = 0.0; 

   ocst::SparseDenseMM(ocst::Transposed::NON_TRANSPOSED,
		       ocst::Transposed::TRANSPOSED,
		       ocst::Transposed::TRANSPOSED,
		       alpha, A, B, beta, C);

   std::cout << "C = " << std::endl;
   C.Print();

   }

   std::cout << std::endl << std::endl;  
   {
   const int m = 3;
   const int n = 2;
   const int k = 4;


   //A
   int I[k+1] = {0, 2, 3, 4, 7};
   int J[7] = {0, 2, 0, 2, 0, 1 , 2};
   double data[7] =  {0.25, 0.1, 0.5, 0.2, 0.1, 0.1, 0.5};
   mfem::SparseMatrix A(I,J,data,k ,m ,false, false, true);

   mfem::DenseMatrix B(n,k);
   double init=1.0;
   for (int i=0; i<B.Height(); ++i){
      for (int j=0; j<B.Width(); ++j){
	 B(i,j) = init;
	 init += 1.0;
      }
   }

   std::cout << "B = " << std::endl;
   B.Print();
   
   mfem::DenseMatrix C(n,m);

   double alpha = 1.0; 
   double beta = 0.0; 

   ocst::SparseDenseMM(ocst::Transposed::TRANSPOSED,
		       ocst::Transposed::TRANSPOSED,
		       ocst::Transposed::TRANSPOSED,
		       alpha, A, B, beta, C);

   std::cout << "C = " << std::endl;
   C.Print();

   }
}
