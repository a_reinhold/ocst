//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   int refinement_space = 2;
   int order_space = 1;
   int nof_timesteps = 11;
   int mg_levels = 2;


   // bool static_cond = false;
   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of refinements to perform on the spatial mesh");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
   	          "Number of timesteps K");
   args.AddOption(&mg_levels, "-mgl", "--mg_level",
   	          "Multigrid levels_space");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Meshes, FE_Collections and spaces

   Mesh mesh_time = Mesh::MakeCartesian1D(nof_timesteps-1,1.0);

   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();

   }

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);

   //MG Prep
   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   FiniteElementSpaceHierarchy hierarchy(&mesh_space, &fes_space,false,false);
   mfem::Array<ocst::SpaceMatrices*> space_mats;
   space_mats.Append(new ocst::SpaceMatrices( hierarchy.GetFinestFESpace()));
   FunctionCoefficient robin_lhs(xiFunc);
   space_mats[0]->SetBLF(robin_lhs); 
   space_mats[0]->Assemble(); 

   mfem::Array<mfem::SparseMatrix*> A_space;
   mfem::Array<mfem::SparseMatrix*> M_space;
   A_space.Append(&space_mats[0]->A());
   M_space.Append(&space_mats[0]->M());
   
   for (int i=0; i<mg_levels;++i){
      FiniteElementSpace coarse = hierarchy.GetFinestFESpace();
      Mesh*  mesh_ = new Mesh(*coarse.GetMesh());
      mesh_->UniformRefinement();

      FiniteElementSpace* fes_ = new FiniteElementSpace(coarse, mesh_);
      ocst::SparseProlongation* prol = new ocst::SparseProlongation(coarse, *fes_);
      
      hierarchy.AddLevel(mesh_, fes_, prol, true, true, true);
      //FEM Matrices
      space_mats.Append(new ocst::SpaceMatrices(*space_mats[0],
	 					hierarchy.GetFinestFESpace()));
      space_mats.Last()->Assemble();
      A_space.Append(&space_mats.Last()->A());
      M_space.Append(&space_mats.Last()->M());
   }
   ocst::TimeMatrices time_matrices(fes_time_trial, fes_time_test);
   

   ocst::GeometricMultigrid prec_A(hierarchy, A_space, 
	 ocst::GeometricMultigrid::OperatorType::SPARSE);
   ocst::GeometricMultigrid prec_M(hierarchy, M_space,
	 ocst::GeometricMultigrid::OperatorType::SPARSE);
   ocst::SolveMGPCG solve_A(prec_A);
   solve_A.SetSolverParameters(0, 1e-4,50,-1); 
   ocst::SolveMGPCG solve_M(prec_M);

   ocst::NormOperatorTest N(time_matrices.M_test(),*M_space.Last(),
			    *A_space.Last()); 
   N.SetInverseOperator(solve_A, solve_M); 

   ocst::SpaceTimeData x_ref(nof_timesteps,solve_A.Height());
   ocst::SpaceTimeData x(nof_timesteps,solve_A.Height());
   ocst::SpaceTimeData y(nof_timesteps,solve_A.Height());
   x_ref.Vec().Randomize();
   N.Mult(x_ref,y);
   N.MultInverse(y,x);
   x_ref.Mat() -= x.Mat(); 
   
   cout << "Test Space" <<endl;
   cout << "||x_ref-x||^2_2 = " << x_ref.Vec() * x_ref.Vec() << endl;  

   cout << endl << "TrialSpace" <<endl;
   ocst::NormOperatorTrial M (time_matrices.M_trial(),time_matrices.A_trial(),
                              *M_space.Last(), *A_space.Last(), solve_A, false);


   x_ref.Vec().Randomize();
   cout <<  "Start Forward" <<endl;
   M.Mult(x_ref,y);
   cout <<  "finished Forward" <<endl;

   cout << "Set Inverse" << endl;
   M.SetInverseOperator(A_space, M_space, hierarchy);
   cout << "finished setup" << endl; 

   cout << "Start MultInverse" <<endl;
   M.MultInverse(y.Vec(),x.Vec());
   cout << "Finished MultInverse" <<endl;
   x.Mat() -= x_ref.Mat(); 
   cout << "||x_ref-x||^2_2 = " << x.Vec() * x.Vec() << endl;  
   for (int i=0; i<space_mats.Size(); ++i){
      delete space_mats[i];
   }
   return 0; 
}
