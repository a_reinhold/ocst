// Fuctions for the 2D Unit Square [0,1]^2 
// yExactFunc => given exact solution
// f = dy_t - \Laplace y
// xi(x) given > 0
// eta(x,t) = dy_nu + xi(x) y(x,t) 
# include "mfem.hpp"
# include <cmath>
namespace mfem{

using namespace mfem;


double yExactFunc(const Vector& x, const double t)
{
   double x1 = x(0);
   double x2 = x(1);

   return (1-2*t)* x1*x1 * x2*x2*x2 + t*t;
}


double fFunc(const Vector& x, const double t)
{
   double x1 = x(0);
   double x2 = x(1);
   double dydt = -2 * x1*x1 * x2*x2*x2 + 2*t;
   double laplace_y = (1-2*t)*(2*x2*x2*x2+ 6*x1*x1*x2);

   return dydt - laplace_y;
}

double xiFunc(const Vector& x)
{
   double x1 = x(0);
   double x2 = x(1);

   return x1*x1 - x2 + 1.5;
}

double etaFunc(const Vector& x, double t)
{

   double x1 = x(0);
   double x2 = x(1);

   double dydnu = 0.0;
   if (x2 ==1.0){
      dydnu = (1-2*t)*3*x1*x1;
   }else if (x1==1.0){
      dydnu = (1-2*t)*2*x2*x2;
   }

   return dydnu + xiFunc(x) * yExactFunc(x,t);
}

} //mfem
