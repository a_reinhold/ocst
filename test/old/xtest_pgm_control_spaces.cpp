//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   bool paraview = false;
   int refinement_space = 2;
   int order_space = 1;
   int nof_timesteps = 11;


   bool static_cond = false;
   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of refinements to perform on the spatial mesh");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
   	          "Number of timesteps K");
   args.AddOption(&paraview, "-paraview", "--paraview", 
                  "-no-paraview", "--no-paraview",
                  "Save files for paraview Visualization");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);

   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   L2_FECollection fec_space_control(order_space-1, mesh_space.Dimension());

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);

   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   FiniteElementSpace fes_space_control(&mesh_space, &fec_space_control);
   //optimal Control Problem
   ocst::OptimalControlProblem ocp(fes_time_trial, fes_time_test, 
                                   fes_space, fes_space_control, 0.01);

   mfem::FunctionCoefficient y_d_coeff([] (const Vector& x){
                                               return -x(0)*x(1)+0.125;});
   ocp.SetDesiredState(y_d_coeff);
   ocp.SetControlConstrains(-1.0,1.0);

   // parabolic operator
   ocst::SpaceTimeParabolicOperator ops(fes_time_trial, 
				         fes_time_test,
				         fes_space);

   FunctionCoefficient robin_lhs(muFunc);
   ops.SetOperators(robin_lhs);
   ops.AssembleOperators();
   

   
   // Control 
   ocp.Control().SetM_time();
   ocp.Control().SetM_space();
   ocp.Control().SetM_control_space(fes_space); 
   FunctionCoefficient u_init(fFunc);
   ocp.Control().SetInitialValue(u_init); 
   std::cout << "Size of linear system" << ocp.Control().Vec().Size() << std::endl;

   // State
   ocp.State().SetM_time(); 
   ocp.State().SetM_space(ops.M_space());

   mfem::FunctionCoefficient robin_rhs(etaFunc);
   ocst::PrimalRHS primal_rhs(fes_time_trial.GetVSize(), fes_space.GetVSize());
   mfem::ConstantCoefficient y_0(0.0);
   primal_rhs.SetConstantPart(fes_time_trial, fes_space, robin_rhs, y_0,
                              ops.M_time_mixed());

   ocst::AdjointRHS adjoint_rhs(fes_time_trial.GetVSize(), fes_space.GetVSize());
   adjoint_rhs.SetConstantPart(fes_space, y_d_coeff, ops.M_space());
   
   
   ocst::SpaceTimeDirectSolver direct_solver(ops, primal_rhs, adjoint_rhs);
   std::cout << "Start solver Assembly" << std::endl; 
   direct_solver.SetOperator();
   std::cout << "Finished solver Assembly" << std::endl; 
   std::cout << "Start lu() " << std::endl; 
   direct_solver.SetOperator();
   std::cout << "Finished lu() " << std::endl; 

   ocst::ProjectedGradientMethod pg(ocp, direct_solver, 30, 1/ocp.Lambda()); 
   std::cout << "Start PG " << std::endl; 
   pg.Solve();
   std::cout << "Finished PG " << std::endl; 
}
