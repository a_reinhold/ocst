//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   bool paraview = false;
   int refinement_space = 2;
   int order_space = 1;
   int nof_timesteps = 11;


   bool static_cond = false;
   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of refinements to perform on the spatial mesh");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
   	          "Number of timesteps K");
   args.AddOption(&paraview, "-paraview", "--paraview", 
                  "-no-paraview", "--no-paraview",
                  "Save files for paraview Visualization");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);

   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);

   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   
   ocst::Control u(fes_time_test, fes_space); 

   u.Mat() = 4.2; 

   u.SetM_time();
   u.SetM_space(); 

   cout << "||u|| = " << u.SquaredInnerProductNorm() << endl;  

   ocst::Control u_(u);
   u_.Mat() = 1.3;
   cout << "||u_|| = " << u_.SquaredInnerProductNorm() << endl;  
   u.SwapData(u_); 
   cout << "||u|| = " << u.SquaredInnerProductNorm() << endl;  
   cout << "||u_|| = " << u_.SquaredInnerProductNorm() << endl;  
   }
