//                                MFEM example spaceTime
//
//
//

#include "mfem.hpp"
#include <fstream>
#include <iostream>

#include "parabolicBC.hpp"

using namespace std;
using namespace mfem;
class SpaceTimeData{
private: 
   std::size_t dof_space, dof_time;
   mfem::Vector* vec;
   mfem::DenseMatrix* mat;
   bool owns_data; 
public:
   SpaceTimeData()
      : dof_space{0}
      , dof_time{0}
      , vec{NULL}
      , mat{NULL}
      , owns_data{false}
   {   }
   SpaceTimeData(std::size_t dof_space, std::size_t dof_time,
                 double init)
      : dof_space{dof_space}
      , dof_time{dof_time}
      , vec{new mfem::Vector(dof_space * dof_time)}
      , mat{new mfem::DenseMatrix(vec->GetData(), int(dof_space), int(dof_time))}
      , owns_data{true}
   {
      *vec = init;  
   }
   SpaceTimeData(std::size_t dof_space, std::size_t dof_time)
      : SpaceTimeData(dof_space,dof_time, 0)
   {   }
   ~SpaceTimeData()
   {
      if (owns_data)
      {
	 delete mat;
	 delete vec;
      }
   }
   mfem::DenseMatrix& Mat()
   {
      return *mat; 
   }
   mfem::Vector& Vec()
   {
      return *vec; 
   }
};
int main(int argc, char *argv[])
{
   // Parse command-line options.
   /*const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   const char *mesh_file_time  = "../data/unitIntervall_1D.mesh";
   int order_space = 1;
   bool static_cond = false;
   const char *device_config = "cpu";
   bool paraview = true;
   int refinement_space = 2;
   int refinement_time= 5;
   int nofOutput = 10; 
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of refinements to perform on the spatial mesh");
   args.AddOption(&refinement_time, "-rt", "--refinement_time", 
	          "Number of refinements to perform on the time mesh");
   args.AddOption(&paraview, "-paraview", "--paraview", 
                  "-no-paraview", "--no-paraview",
                  "Save files for paraview Visualization");
   args.AddOption(&nofOutput, "-nOut", "--nofOutput", 
                  "number of Outputs over Trajectorie");

   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Read the mesh from the given mesh file
   Mesh mesh_space(mesh_file_space, 1, 1);
   int dim_space = mesh_space.Dimension();
   for( int i=0; i<refinement_space; ++i)
   {
      mesh_space.UniformRefinement();
   }

   Mesh mesh_time(mesh_file_time, 1, 1);
   int dim_time = mesh_time.Dimension();
   for( int i=0; i<refinement_time; ++i)
   {
      mesh_time.UniformRefinement();
   }

   // 5. Define a finite element spaces on the meshes.
   H1_FECollection h1_space(order_space, dim_space);
   FiniteElementSpace fes_space(&mesh_space, &h1_space);
  


   H1_FECollection h1_time(1, dim_time);
   L2_FECollection l2_time(0, dim_time);
   FiniteElementSpace fes_time_trial(&mesh_time, &h1_time);
   FiniteElementSpace fes_time_test(&mesh_time,  &l2_time);

   cout << "Number of unknowns (spatial): "
        << fes_space.GetTrueVSize()  << endl;
   cout << "Number of unknowns (time_trial): "
        << fes_time_trial.GetTrueVSize()  << endl;
   cout << "Number of unknowns(time_test): "
        << fes_time_test.GetTrueVSize()  << endl;

   // Set up the linear form f(.) which corresponds to the right-hand side of
   //    the FEM linear system in space
   LinearForm f_space(&fes_space);
   
   FunctionCoefficient fCoeff(fFunc);
   FunctionCoefficient etaCoeff(etaFunc);
   
   f_space.AddDomainIntegrator(new DomainLFIntegrator(fCoeff));
   f_space.AddBoundaryIntegrator(new BoundaryLFIntegrator(etaCoeff));

   // 9. Set up the bilinear form a(.,.) and m(.,.)on the finite element space
   
   FunctionCoefficient muCoeff(muFunc);
   ConstantCoefficient one(1.0);
   
   BilinearForm a_space(&fes_space);
   a_space.AddDomainIntegrator(new DiffusionIntegrator(one));
   a_space.AddBoundaryIntegrator(new MassIntegrator(muCoeff));
   
   BilinearForm m_space(&fes_space);
   m_space.AddDomainIntegrator(new MassIntegrator(one));
   int skip_zeros = 0; //Skip zeros to false bc otherwise sparsity of the 
                       //matrices may not be guranteed
   a_space.Assemble(skip_zeros); 
   a_space.Finalize(skip_zeros);
   m_space.Assemble(skip_zeros);
   m_space.Finalize(skip_zeros);
   
   //c_time = (theta', xi)_L2(I) (trial -> H1(I), span(theta)
   //                            (test  -> L2(I), span(xi)
   MixedBilinearForm c_time(&fes_time_trial,&fes_time_test);
   c_time.AddDomainIntegrator(new MixedScalarDerivativeIntegrator(one));
   c_time.Assemble(skip_zeros);
   c_time.Finalize(skip_zeros); 
   MixedBilinearForm n_time(&fes_time_trial,&fes_time_test);
   n_time.AddDomainIntegrator(new MassIntegrator(one));
   n_time.Assemble(skip_zeros);
   n_time.Finalize(skip_zeros); 
   
   int space_dim = m_space.Height();
   int time_dim = c_time.Width();
   //Create operator B as Block Matrix
   //
   int offset = 0;
   Array<int> offsets(time_dim + 1);
   for (int i=0; i<offsets.Size(); ++i){
      offsets[i] = offset;
      offset += space_dim;
      //cout << "offset[" << i << "] = " <<  offsets[i]<< endl;
   }
   
   BlockMatrix B(offsets);
   //Loop through time matrices 
   B.owns_blocks = 1; //on destruction the blocks get deleted 
   const int *Ip = c_time.SpMat().GetI();
   const int *Jp = c_time.SpMat().GetJ();
   for (int i=0; i<time_dim-1 ; ++i){
      for (int k=Ip[i]; k<Ip[i+1]; ++k){
         SparseMatrix* tmp = new SparseMatrix(m_space.SpMat());
	 *tmp *= c_time.SpMat()(i,Jp[k]);
	 tmp->Add(n_time.SpMat()(i,Jp[k]), a_space.SpMat());
	 B.SetBlock(i,Jp[k],tmp);
      }
   }
   B.SetBlock(time_dim-1,0, new SparseMatrix(m_space.SpMat()));

   SparseMatrix* B_mono = B.CreateMonolithic();
   
   //Set solver for B_mono
   UMFPackSolver umf_solver;
   umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
   umf_solver.SetOperator(*B_mono);


   int vector_dim = B_mono->Width();



   Vector f_vec(vector_dim);
   DenseMatrix f_mat;
   f_mat.UseExternalData(f_vec.GetData(), space_dim, time_dim);
   //Loop over time steps to assemble rhs for each timestep
   for (int i =0; i < time_dim; ++i){
      //cout << "Vertex(i)= " << mesh_time.GetVertex(i)[0] << endl;
      double current_time = mesh_time.GetVertex(i)[0]; 
      fCoeff.SetTime(current_time);
      etaCoeff.SetTime(current_time);
      f_space.Assemble();
      f_mat.SetCol(i, f_space);
   }
   //Perform Integration in time with matrix Vector multiplikation
   Vector tmp_a(time_dim); 
   Vector tmp_b(time_dim);
   Vector tmp_b_(tmp_b.GetData(), time_dim-1); //b and b_ use the same array 

   for (int i=0; i<space_dim; ++i){
      f_mat.GetRow(i, tmp_a); 
      n_time.Mult(tmp_a,tmp_b_);
      f_mat.SetRow(i, tmp_b);
   }

   Vector tmp_c(space_dim); 
   Vector tmp_d(space_dim); 
   tmp_d = 0.0; // y_0 = 0; 
   m_space.Mult(tmp_d, tmp_c);
   f_mat.SetCol(time_dim-1,tmp_c);
   Vector y_vec(vector_dim);
   y_vec = 0.0; 

   umf_solver.Mult(f_vec, y_vec); 
   //TODO Check y(0) 
   if (paraview)
   {
      FunctionCoefficient y_exact_coeff(yExactFunc);
      GridFunction y_exact(&fes_space);
      GridFunction y_temp(&fes_space);
      y_temp.MakeRef(y_vec, 0, space_dim);
      pd = new ParaViewDataCollection("parabolic", &mesh_space);
      pd->SetPrefixPath("paraview");
      pd->RegisterField("y", &y_temp);
      pd->RegisterField("yRef", &y_exact); 
      pd->SetLevelsOfDetail(order_space);
      pd->SetDataFormat(VTKFormat::BINARY);
      pd->SetHighOrderOutput(true);
      for (int i=0; i< time_dim; ++i){
	 double current_time = mesh_time.GetVertex(i)[0]; 
	 pd->SetCycle(i);
	 pd->SetTime(current_time);
	 y_temp.MakeRef(y_vec, i*space_dim, space_dim);
	 y_exact_coeff.SetTime(current_time);
	 y_exact.ProjectCoefficient(y_exact_coeff);
	 double err = y_temp.ComputeL2Error(y_exact_coeff);
	 cout << "time step: " << i << " , time: " << current_time << 
	        "||y-y_ref||_{L_2} = "<< err << endl;
	 pd->Save();
      }
   }

   // 15. Free the used memory.
   if (paraview) { delete pd;}

  */
   return 0;
}
