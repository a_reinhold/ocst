//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
//#include "parabolicBC.hpp"
#include "example1BC.hpp"
#include <ocst.hpp>
#include <postProcessing.hpp>

#include <fstream>
#include <iostream>
#include <random>

using namespace std;
using namespace mfem;
using OperatorType = ocst::GeometricMultigrid::OperatorType;
class SpaceTimeGram{
   private:
      ConstantCoefficient one{1.0};
      BilinearForm m_time;
      BilinearForm m_space;
      
   public:
      SpaceTimeGram(FiniteElementSpace& fes_time, FiniteElementSpace& fes_space)
	 : m_time{&fes_time}, m_space{&fes_space}
      {
	 m_time.AddDomainIntegrator(new MassIntegrator(one));
	 m_space.AddDomainIntegrator(new MassIntegrator(one));
	 m_time.Assemble(); m_time.Finalize();
	 m_space.Assemble(); m_space.Finalize();
      }
      SparseMatrix&
      M_time(){ return m_time.SpMat();}
      SparseMatrix&
      M_space(){ return m_space.SpMat();}

};
int main(int argc, char *argv[])
{
   ocst::BenchTiming timing{6};
   // Parse command-line options.
   // const char *mesh_file_space = "../data/L_shaped_2D.mesh";
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   bool paraview = false;
   int refinement_space = 2;
   int order_space = 2;
   int nof_timesteps = 16;
   int levels_space = 2;
   int levels_time = 2;
   int nof_runs = 1;
   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&levels_space, "-ls", "--levels_space",
                  "levels for MG space dimension");
   args.AddOption(&levels_time, "-lt", "--levels_time",
                  "levels for MG time dimension");
   
   args.AddOption(&nof_runs, "-n", "--nof_runs",
                  "number of runs for timing");
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();
   // Meshes, FE_Collections and spaces

   Mesh mesh_time= Mesh::MakeCartesian1D(nof_timesteps-1,1.0);

   H1_FECollection fec_time(1, 1);

   FiniteElementSpace fes_time(&mesh_time, &fec_time);

   Mesh mesh_space(mesh_file_space,1,2);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);


   //MG Prep
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(fes_time, 
							   levels_time,
                                                           fes_space,
							   levels_space);
   int n_levels{st_hierarchy.GetNumLevels()};
   Array<SpaceTimeGram*> gram{n_levels};
   Array<ocst::KroneckerOperator*> kron_ops{n_levels};
   Array<SparseMatrix*> mat_ops{n_levels};

   for (int i=0; i<n_levels; ++i){
      gram[i] = new SpaceTimeGram(st_hierarchy.GetFESTimeAtLevel(i), 
	                          st_hierarchy.GetFESSpaceAtLevel(i));
      kron_ops[i] = new ocst::KroneckerOperator(gram[i]->M_time(), gram[i]->M_space());
      mat_ops[i] = &kron_ops[i]->SpMat();
   }

   ocst::KroneckerOperator& M_finest = *kron_ops.Last();
   double* buffer = new double[M_finest.Height()];
   st_hierarchy.SetBuffer(buffer);
   cout << "system size: " << M_finest.Height() << endl;
   Vector x_ref{M_finest.Width()};
   x_ref.Randomize();

   Vector y{M_finest.Height()};
   M_finest.Mult(x_ref, y);

   //Direct Solver
   // UMFPackSolver direct_solver;
   // direct_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
   // direct_solver.SetOperator(M_finest.SpMat());
   //
   // Vector x_direct{x_ref};
   // x_direct = 0.0;
   // direct_solver.Mult(y, x_direct);
   //
   // x_direct -= x_ref;
   // cout << "direct\t||x - x_ref||_2 = " << x_direct.Norml2() <<endl;
   Vector x_pcg{x_ref};
   for (int i=0; i<nof_runs; ++i){
   timing.Start(0);
   ocst::GeometricMultigrid mg_mfree{st_hierarchy, kron_ops, 
                                     OperatorType::MATRIX_FREE};
   timing.End(0);
   mg_mfree.SetCoarsestParameters(1e-10, 1e-12, 180);
   CGSolver cg_mfree;
   cg_mfree.SetOperator(M_finest);
   cg_mfree.SetPreconditioner(mg_mfree);
   cg_mfree.SetPrintLevel(0);
   cg_mfree.SetAbsTol(1e-7);
   cg_mfree.SetRelTol(1e-8);
   cg_mfree.SetMaxIter(100);
   x_pcg = 0.0;
   timing.Start(1);
   cg_mfree.Mult(y, x_pcg);
   timing.End(1);
   x_pcg -= x_ref;

   if (i==0){
      cout << "pcg_mfree\t||x - x_ref||_2 = " << x_pcg.Norml2() <<endl;
      cout << "pcg_mfree\t iter =" << cg_mfree.GetNumIterations() << endl;
   }
   timing.Start(4);
   ocst::GeometricMultigrid mg_tensor{st_hierarchy, kron_ops, 
                                     OperatorType::TENSOR};
   timing.End(4);
   CGSolver cg_tensor;
   cg_tensor.SetOperator(M_finest);
   cg_tensor.SetPreconditioner(mg_tensor);
   cg_tensor.SetPrintLevel(0);
   cg_tensor.SetAbsTol(1e-7);
   cg_tensor.SetRelTol(1e-8);
   cg_tensor.SetMaxIter(30);
   x_pcg = 0.0;
   timing.Start(5);
   cg_tensor.Mult(y, x_pcg);
   timing.End(5);
   x_pcg -= x_ref;

   if (i==0){
      cout << "pcg_tensor\t||x - x_ref||_2 = " << x_pcg.Norml2() <<endl;
      cout << "pcg_tensor\t iter =" << cg_tensor.GetNumIterations() << endl;
   }
   timing.Start(2);
   ocst::GeometricMultigrid mg_sparse{st_hierarchy, mat_ops, 
                                      OperatorType::SPARSE};
   timing.End(2);
   CGSolver cg_sparse;
   cg_sparse.SetOperator(M_finest);
   cg_sparse.SetPreconditioner(mg_sparse);
   cg_sparse.SetPrintLevel(0);
   cg_sparse.SetAbsTol(1e-7);
   cg_sparse.SetRelTol(1e-8);
   cg_sparse.SetMaxIter(30);
   x_pcg = 0.0;
   timing.Start(3);
   cg_sparse.Mult(y, x_pcg);
   timing.End(3);
   x_pcg -= x_ref;
   if (i==0){
      cout << "pcg_sparse\t||x - x_ref||_2 = " << x_pcg.Norml2() <<endl;
      cout << "pcg_sparse\t iter =" << cg_sparse.GetNumIterations() << endl;
   }
   }
   cout << "setup_mfree\t" << timing.GetTime(0) 
        << "\nsetup_tensor\t" << timing.GetTime(4) 
        << "\nsetup_sparse\t" << timing.GetTime(2)
        << "\nsolve_mfree\t" << timing.GetTime(1)
        << "\nsolve_tensor\t" << timing.GetTime(5)
        << "\nsolve_sparse\t" << timing.GetTime(3) << endl;
   for (int i=0; i<n_levels; ++i){
      delete gram[i]; 
      delete kron_ops[i];
   }
   delete[] buffer;

   //if (paraview){
   //   pd = new mfem::ParaViewDataCollection("lsqr", 
   //                        hierarchy_space.GetFinestFESpace().GetMesh());
   //   pd->SetPrefixPath("paraview");
   //   pd->SetLevelsOfDetail(order_space);
   //   pd->SetDataFormat(VTKFormat::BINARY);
   //   pd->SetHighOrderOutput(true);
   //   //p.ExportToParaview(*pd, "p" , *fes_time_test,
   //   //                   hierarchy_space.GetFinestFESpace());
   //   y.ExportToParaview(*pd, "y", fes_time_trial, 
   //                      hierarchy_space.GetFinestFESpace());
   //   //y_ref.ExportToParaview(*pd, "y_ref", fes_time_trial, 
   //   //                   hierarchy_space.GetFinestFESpace());
   //   delete pd;
   //}
   return 0;
}
