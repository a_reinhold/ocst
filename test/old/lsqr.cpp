//                                MFEM example spaceTime
//
//
//

#include "../data/bc/symmetric_1D.hpp"
#include "../data/bc/case_5.hpp"

#include <mfem.hpp>
#include <ocst.hpp>



#include <fstream>
#include <iostream>
#include <random>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   bool paraview = false;
   int refinement_space = 1;
   int order_space = 1;
   int nof_timesteps = 11;
   int levels_space = 2;
   int type = 1;

   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&paraview, "-paraview", "--paraview", 
                  "-no-paraview", "--no-paraview",
                  "Save files for paraview Visualization");
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&type, "-t", "--type",
                  "type of the .");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
                  "Number of initial refinements to"
		  " perform on the spatial mesh");
   args.AddOption(&levels_space, "-ls", "--levels_space",
                  "levels for MG space dimension");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
                  "levels for MG time dimension");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();
   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);
   Mesh mesh_time_ref(mesh_time);
   mesh_time_ref.UniformRefinement();

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test_(&mesh_time, &fec_time_test);
   FiniteElementSpace fes_time_test_ref(&mesh_time_ref, &fec_time_test);
   
   FiniteElementSpace* fes_time_test;

   Mesh mesh_space(30,1.0); 
   //Mesh mesh_space(mesh_file_space,1,1);
   //for (std::size_t i= 0; i<refinement_space; ++i){
   //   mesh_space.UniformRefinement();
   //}
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);


   //MG Prep
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(fes_time_trial, 1,
                                                           fes_space,
							   levels_space);


   bool type_;
   if (type==1){
      cout 
	 <<"/*--------------------------------------"  
	 <<"--------------------------------------*/"  << endl
	 <<"                                  TYPE 1"
	 <<"                                  " << endl
	 <<"/*--------------------------------------"  
	 <<"--------------------------------------*/"  << endl;
      fes_time_test = &fes_time_test_;
      type_ = true;

   } else {
      cout 
	 <<"/*--------------------------------------"  
	 <<"--------------------------------------*/"  << endl
	 <<"                                  TYPE 2"
	 <<"                                  " << endl
	 <<"/*--------------------------------------"  
	 <<"--------------------------------------*/"  << endl;
      fes_time_test = &fes_time_test_ref;
      type_ = false;
   }

   ocst::TimeMatrices time_mats(fes_time_trial, *fes_time_test, type_);
   Array<ocst::SpaceMatrices*> space_mats;
   Array<SparseMatrix*> A_space; 
   Array<SparseMatrix*> M_space; 

   mfem::FiniteElementSpaceHierarchy& hierarchy_space = 
       st_hierarchy.FESHierarchySpace();
   space_mats.Append(
      new ocst::SpaceMatrices(hierarchy_space.GetFESpaceAtLevel(0)));
   FunctionCoefficient robin_lhs(xi_func);
   ConstantCoefficient one(1.0); 
   space_mats[0]->SetBLF(robin_lhs); 
   space_mats[0]->Assemble(); 

   A_space.Append(&space_mats[0]->A());
   M_space.Append(&space_mats[0]->M());
   for (int i=1; i<levels_space; ++i){
      space_mats.Append(new ocst::SpaceMatrices(*space_mats[i-1],
                                         hierarchy_space.GetFESpaceAtLevel(i)));
      space_mats[i]->Assemble();
      A_space.Append(&space_mats[i]->A());
      M_space.Append(&space_mats[i]->M());
   }
   int dof_time_test = time_mats.M_test().Height()+ 1;
   int dof_time_trial= time_mats.M_trial().Height();
   int dof_space = space_mats.Last()->A().Height();
   cout <<" n = "<< dof_space << endl;
   cout <<" K_test = "<< dof_time_test << endl;
   cout <<" K_trial = "<< dof_time_trial<< endl;

   double* buffer = new double[dof_time_test*dof_space];
   ocst::GeometricMultigrid prec_A(hierarchy_space, A_space, true);
   ocst::GeometricMultigrid prec_M(hierarchy_space, M_space, true);

   ocst::SolveMGPCG solve_A(prec_A);
   ocst::SolveMGPCG solve_M(prec_M);
   
   
   //Operators
   ocst::NormOperatorTrial M_norm(time_mats.M_trial(), time_mats.A_trial(),
        			  *M_space.Last(), *A_space.Last(), solve_A, 
				  false, buffer);
   M_norm.SetInverseOperator(M_space, A_space, hierarchy_space);
   static_cast<ocst::InvNormOperatorTrial*>(
                             M_norm.GetInverseOperator())->SetTolerance(1e-6);

   ocst::NormOperatorTest N_norm(time_mats.M_test(), *M_space.Last(), 
                                 *A_space.Last(), buffer);
   N_norm.SetInverseOperator(solve_A, solve_M); 

   ocst::SpaceTimeParabolicOperator B(time_mats.C_mixed(), time_mats.M_mixed(),
                                      *M_space.Last(), *A_space.Last());
   B.AssembleGeneralSylvester(); 


   ocst::Control u(*fes_time_test, hierarchy_space.GetFinestFESpace());
   u.SetM_time(time_mats.M_test());
   u.SetM_space(*M_space.Last());
   //u.Mat()=1.0; 
   FunctionCoefficient u_init(f_func);
   u.SetValue(u_init); 

   ocst::State y(fes_time_trial, hierarchy_space.GetFinestFESpace());
   y.SetM_time(time_mats.M_trial());
   y.SetM_space(*M_space.Last());
   
   ocst::AdjointState p(*fes_time_test, hierarchy_space.GetFinestFESpace());
   //RHS
   FunctionCoefficient robin_rhs(eta_func);
   FunctionCoefficient y_exact(y_analytic);
   y_exact.SetTime(0.0);

   mt19937 gen(12345);
   uniform_real_distribution<> dis(0.0,1.0);
   //FunctionCoefficient y_0([&dis, &gen] (const Vector& x)
   //   {
   //      double x1 = x(0);
   //      double x2 = x(1);
   //      if (x1 > 0.25 && x1 < 0.75 && x2 > 0.25 && x2 < 0.75){
   //         return 10.0;
   //         //return dis(gen);
   //      } else {
   //         return yExactFunc(x,0.0);
   //      }
   //    });
   ConstantCoefficient y_0(0.0);
   ocst::State y_d(y); 
   y_d.SetValue(one);

   GridFunction y_init(&hierarchy_space.GetFinestFESpace());
   y_init.ProjectCoefficient(y_0);
   GridFunctionCoefficient rhs(&y_init);

   ocst::PrimalRHS prhs(dof_time_test, dof_space);
   prhs.SetConstantPart(fes_time_trial, hierarchy_space.GetFinestFESpace(), 
                        robin_rhs, rhs, time_mats.M_mixed()); 
   
   ocst::AdjointRHS arhs(dof_time_trial, dof_space,0.0,1.0);
   Vector y_d_T;
   y_d.Mat().GetColumnReference(0, y_d_T);
   arhs.SetConstantPart(y_d, y_d_T, y_d.M_time(), y_d.M_space());


   //Solver
   ocst::LSQRSolver solver(B, prhs, arhs, 
                           *M_norm.GetInverseOperator(), 
        		   *N_norm.GetInverseOperator());

   solver.SetSolverParameters(1e-5, 50,2); 
   
   //ocst::State y_lsqr(y);
   //ocst::State y_direct(y);
   

   //B.AssembleMonolithic();
   //ocst::SpaceTimeDirectSolver solver(B,prhs,arhs);
   //solver.SetOperator(); 

   //solver.SolvePrimal(u,y_lsqr);
   //direct_solver.SolvePrimal(u,y_direct);

   //cout  <<"y_lsqr = " << endl;
   //y_lsqr.Mat().Print(); 
   //cout  << endl;
   //cout  <<"y_direct = " << endl;
   //y_direct.Mat().Print(); 
   //cout  << endl;

   //y.Mat()= y_direct.Mat();
   //y.Mat() -= y_lsqr.Mat();
   //cout  <<"difference  " << endl;
   //y.Mat().Print(); 
   //cout  << endl;
   //cout << "||y_direct - y_lsqr||^2_{L_2(I,Omega)} = " 
   //     << y.SquaredInnerProductNorm() << endl; 

   cout << "Solve Primal" << endl;
   solver.SolvePrimal(u,y);
   cout << "Solve Adjoint" << endl;
   solver.SolveAdjoint(y,p);
  
   ocst::State y_ref(y);
   GridFunction y_k(&hierarchy_space.GetFinestFESpace());
   for (int k=0; k<dof_time_trial; ++k){
      y_k.MakeRef(y_ref.Vec(), k*dof_space, dof_space);
      y_exact.SetTime(k*1.0/(dof_time_trial-1));
      y_k.ProjectCoefficient(y_exact);
   }

   y_k.MakeRef(y.Vec(),0,dof_space);
   y_init-=y_k;
   cout << "error y_0 = " << y_init* y_init<< endl; 
   /*
   if (paraview){
      pd = new mfem::ParaViewDataCollection("lsqr", 
                           hierarchy_space.GetFinestFESpace().GetMesh());
      pd->SetPrefixPath("paraview");
      pd->SetLevelsOfDetail(order_space);
      pd->SetDataFormat(VTKFormat::BINARY);
      pd->SetHighOrderOutput(true);
      //p.ExportToParaview(*pd, "p" , *fes_time_test,
      //                   hierarchy_space.GetFinestFESpace());
      y.ExportToParaview(*pd, "y", fes_time_trial, 
                         hierarchy_space.GetFinestFESpace());
      //y_ref.ExportToParaview(*pd, "y_ref", fes_time_trial, 
      //                   hierarchy_space.GetFinestFESpace());
      delete pd;
   }
   */
   y.Mat() -= y_ref.Mat();
   cout << "||y_h - y_ref||^2_{L_2(I,Omega)} = " << y.SquaredInnerProductNorm()
        << endl; 
   for (int i=0; i<levels_space; ++i){
      delete space_mats[i];
   }
   delete[] buffer;
   return 0;
}
