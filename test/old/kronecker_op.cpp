#include "fem/bilinearform.hpp"
#include <mfem.hpp>
#include <ocst.hpp>

int main (int argc, char *argv[]) {

   int n = 30;
   mfem::Mesh msh = mfem::Mesh::MakeCartesian1D(n);

   mfem::H1_FECollection fec_A_trial{1, msh.Dimension()};
   mfem::H1_FECollection fec_A_test{2, msh.Dimension()};
   mfem::L2_FECollection fec_B_trial{2, msh.Dimension()};
   mfem::L2_FECollection fec_B_test{3, msh.Dimension()};

   mfem::FiniteElementSpace fes_A_trial{&msh, &fec_A_trial};
   mfem::FiniteElementSpace fes_A_test{&msh, &fec_A_test};
   mfem::FiniteElementSpace fes_B_trial{&msh, &fec_B_trial};
   mfem::FiniteElementSpace fes_B_test{&msh, &fec_B_test};

   mfem::ConstantCoefficient one{1.0};

   mfem::BilinearForm a_sym{&fes_A_trial};
   mfem::BilinearForm b_sym{&fes_B_trial};
   mfem::MixedBilinearForm a_rect{&fes_A_trial, &fes_A_test};
   mfem::MixedBilinearForm b_rect{&fes_B_trial, &fes_B_test};

   a_sym.AddDomainIntegrator(new mfem::MassIntegrator(one));
   b_sym.AddDomainIntegrator(new mfem::MassIntegrator(one));
   a_rect.AddDomainIntegrator(new mfem::MassIntegrator(one));
   b_rect.AddDomainIntegrator(new mfem::MassIntegrator(one));

   a_sym.Assemble(); a_sym.Finalize();
   b_sym.Assemble(); b_sym.Finalize();
   a_rect.Assemble(); a_rect.Finalize();
   b_rect.Assemble(); b_rect.Finalize();
   
   {
   //Symmetric Case
   
   int n_A{a_sym.Height()};
   int n_B{b_sym.Height()};
   
   double* buffer = new double[n_A*n_B];
   ocst::KroneckerOperator sym{a_sym.SpMat(), b_sym.SpMat()}; //, buffer};

   mfem::Vector x{n_A*n_B};
   x = 3.0;
   mfem::Vector y_mfree{n_A*n_B};
   mfem::Vector y_assembled{n_A*n_B};
   sym.Mult(x, y_mfree);
   sym.SpMat().Mult(x, y_assembled);
   mfem::Vector diff{y_mfree};
   diff -= y_assembled;
   std::cout << "norm y " <<y_mfree.Norml2() <<std::endl;
   std::cout << "norm y diff " <<diff.Norml2() <<std::endl;

   mfem::Vector x_mfree{n_A*n_B};
   mfem::Vector x_assembled{n_A*n_B};
   mfem::Vector x_direct{n_A*n_B};
   x_mfree = 0.0;
   x_assembled = 0.0;
   mfem::CGSolver CG; 
   CG.SetPrintLevel(1);
   CG.SetMaxIter(30);
   CG.SetAbsTol(1e-8);
   mfem::UMFPackSolver UMF;
   UMF.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
   CG.SetOperator(sym);
   CG.Mult(y_mfree, x_mfree);

   CG.SetOperator(sym.SpMat());
   CG.Mult(y_mfree, x_assembled);

   UMF.SetOperator(sym.SpMat());
   UMF.Mult(y_mfree, x_direct);
   diff = x_direct;
   diff -= x;
   std::cout << "x norm diff_direct" <<diff.Norml2() <<std::endl;
   diff = x_mfree;
   diff -= x;
   std::cout << "x norm diff_mfree" <<diff.Norml2() <<std::endl;

   diff = x_assembled;
   diff -= x;
   std::cout << "x norm diff_assembled " <<diff.Norml2() <<std::endl;
   
   delete[] buffer;
   }
   {
      int m_A{a_rect.Height()}, n_A{a_rect.Width()};
      int m_B{b_rect.Height()}, n_B{b_rect.Width()};
      double *buffer = new double[m_A * n_A * m_B * n_B];
      // rectangualr case
      // ocst::KroneckerOperator rect{a_rect.SpMat(), b_rect.SpMat(), buffer};
      ocst::KroneckerOperator rect{a_rect.SpMat(), b_rect.SpMat()};

      mfem::Vector x{n_A * n_B}, y_mfree{m_A * m_B}, y_assembled{m_A * m_B};
      x = 11.0;
      rect.Mult(x, y_mfree);
      rect.SpMat().Mult(x, y_assembled);
      mfem::Vector diff{y_assembled};
      diff -= y_mfree;
      std::cout << "rect norm diff " << diff.Norml2() << std::endl;

      mfem::Vector x_mfree{x}; 
      mfem::Vector x_assembled{x};
      rect.MultTranspose(y_mfree, x_mfree);
      rect.SpMat().MultTranspose(y_mfree, x_assembled);
      x = x_mfree;
      x -= x_assembled;
      std::cout << "rect norm diff " << x.Norml2() << std::endl;
      delete[] buffer;
   }
   return 0;
}
