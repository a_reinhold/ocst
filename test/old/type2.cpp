//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   int nof_timesteps = 11;

   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
   	          "Number of timesteps K");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);

   //Mesh mesh_time_ref(2*nof_timesteps-1,1.0);
   Mesh mesh_time_ref(mesh_time);
   mesh_time_ref.UniformRefinement(); 

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);
   
   FiniteElementSpace fes_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_test(&mesh_time, &fec_time_test);

   FiniteElementSpace fes_trial_ref(&mesh_time_ref, &fec_time_trial); 
   FiniteElementSpace fes_test_ref(&mesh_time_ref, &fec_time_test); 
   ocst::SparseProlongation prol_test(fes_test, fes_test_ref);
   ocst::SparseProlongation prol_trial(fes_trial, fes_trial_ref);
   
   ocst::TimeMatrices mats(fes_trial_ref, fes_test_ref);

   SparseMatrix* C_mixed_ref;
   SparseMatrix* M_mixed_ref;


   C_mixed_ref = Mult(mats.C_mixed(),prol_trial.SpMat()); 
   M_mixed_ref = Mult(mats.M_mixed(),prol_trial.SpMat()); 

   cout << "C_mixed" << endl;
   mats.C_mixed().Print();
   cout << "C_mixed_ref" << endl;
   C_mixed_ref->Print();
   cout << "M_mixed" << endl;
   mats.M_mixed().Print();
   cout << "M_mixed_ref" << endl;
   M_mixed_ref->Print();
   delete C_mixed_ref;
   return 0; 
}
