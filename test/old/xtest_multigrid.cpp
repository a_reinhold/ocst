//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>
#include <random>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   int refinement_space = 2;
   int order_space = 1;
   int nof_timesteps = 11;

   int levels_space = 2;
   int levels_time = 2; 


   bool static_cond = false;
   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of refinements to perform on the spatial mesh "
		  "(coarsest level)");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
   	          "Number of timesteps K in the coarsest level");
   args.AddOption(&levels_space, "-ls", "--levels_space",
                  "Number of Multigridlevels within the space mesh");
   args.AddOption(&levels_time, "-lt", "--levels_time",
                  "Number of Multigridlevels within the time mesh");
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);

   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);

   
   // parabolic operator
   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   ocst::SpaceTimeParabolicOperator coarse_op(fes_time_trial, 
					      fes_time_test,
					      fes_space);

   FunctionCoefficient robin_lhs(muFunc);
   coarse_op.SetOperator(robin_lhs);
   coarse_op.AssembleOperator();

   ocst::SpaceTimeMultigrid mg(coarse_op, levels_time, levels_space);
   mg.SetBuffer(); 
   mg.SetSmootherProperties(20); 
   
   ocst::SpaceTimeParabolicOperator* finest = mg.operators.Last(); 

   int dof_time_ref = mg.fes_time_trials.Last()->GetVSize(); 
   int dof_space_ref = mg.fes_spaces.Last()->GetVSize(); 

   ocst::SpaceTimeData y(dof_time_ref, dof_space_ref);
   ocst::SpaceTimeData x(y); 
   ocst::SpaceTimeData x_ref(y); 

   std::mt19937 mt; 
   std::uniform_real_distribution<double> uniform(0.0, 1.0); 

   mg.pre_smoothing_steps=2;
   mg.post_smoothing_steps=2; 
   for (int i=0; i< x_ref.Vec().Size(); ++i){
      x_ref.Vec()(i) = uniform(mt);
   }
   finest->Mult(x_ref.Vec(),y.Vec());

   for (int i=0; i<100; ++i){
      mg.Mult(y.Vec(), x.Vec()); 
   }

   x_ref.Mat() -= x.Mat(); 

   std::cout << "|x-x_ref|" << x_ref.Vec().Norml1() << std::endl;


   
   
   return 0; 
}
