//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include <ocst.hpp> 

#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;

//define pi
const double PI = 4 * atan(1);

// forward declarations
double f_func(const Vector&);
double xi_func(const Vector&);
double eta_func(const Vector&);
double y_exact(const Vector&);

class myMultiGrid: public Multigrid{
   public:
      // Constructs an empty multigrid hierarchy
      myMultiGrid()
      : Multigrid{}{ }
      
      void
      addProlongation(Operator& prolongation, bool own_prol = true){
	 prolongations.Append(&prolongation);
	 ownedProlongations.Append(own_prol);
      }
};


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   bool paraview = false;
   int refinement_space = 2;
   int order_space = 1;
   int mg_levels = 3;
   int print_level = 2; 

   bool static_cond = false;
   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of initial refinements to perform on the spatial mesh");
   args.AddOption(&print_level ,"-pl", "--print_level", 
	          " ");
   args.AddOption(&mg_levels, "-mgl", "--multigrid_levels",
   		  "Levels for the multigrid solver");
   args.AddOption(&paraview, "-paraview", "--paraview", 
                  "-no-paraview", "--no-paraview",
                  "Save files for paraview Visualization");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);

   // Meshes, FE_Collections and spaces coarsest level
   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }

   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   
   FiniteElementSpaceHierarchy hierarchy(&mesh_space, &fes_space, false, false);

   for (int i=0; i<mg_levels; ++i){
      FiniteElementSpace* coarse = &(hierarchy.GetFinestFESpace());
      Mesh* mesh_ = new Mesh(*(coarse->GetMesh()));
      mesh_->UniformRefinement();
      FiniteElementSpace* fine = new FiniteElementSpace(*coarse, mesh_);
      ocst::SparseProlongation*	prolongation_ = new ocst::SparseProlongation(*coarse, *fine);
      hierarchy.AddLevel(mesh_, fine, prolongation_, true, true, true);
   }

   FunctionCoefficient xi_coeff(xi_func);   
   FunctionCoefficient eta_coeff(eta_func);
   FunctionCoefficient f_coeff(f_func);
   ConstantCoefficient one(1.0);
   myMultiGrid mg;
   for (int i=0; i<mg_levels+1; ++i){
      BilinearForm a(&hierarchy.GetFESpaceAtLevel(i));
      a.AddDomainIntegrator(new DiffusionIntegrator(one));
      a.AddBoundaryIntegrator(new MassIntegrator(xi_coeff));
      a.Assemble(); a.Finalize();
      SparseMatrix* oper = new SparseMatrix(a.SpMat());
      if (i == 0){
	 mg.AddLevel(oper, new UMFPackSolver(*oper), true, true);
      }
      else{
	 Vector d{oper->Height()};
	 Array<int> ess_tdof;
	 oper->GetDiag(d);
	 // d = 1.0;
	 OperatorChebyshevSmoother* smother = new OperatorChebyshevSmoother(*oper, d, ess_tdof, 2);
	 mg.AddLevel(oper, smother, true, true);
	 mg.addProlongation(*hierarchy.GetProlongationAtLevel(i-1), false);
      }

   }


   LinearForm b(&hierarchy.GetFinestFESpace());
   b.AddDomainIntegrator(new DomainLFIntegrator(f_coeff));
   b.AddBoundaryIntegrator(new BoundaryLFIntegrator(eta_coeff));

   b.Assemble();
   int ndof = b.Size();

   GridFunction y_mg(&hierarchy.GetFinestFESpace());
   y_mg = 0;


   CGSolver PCG;
   PCG.SetRelTol(1e-7);
   PCG.SetAbsTol(1e-8);
   PCG.SetPrintLevel(print_level);
   PCG.SetMaxIter(1000);
   PCG.SetOperator(*mg.GetOperatorAtFinestLevel());
   PCG.SetPreconditioner(mg);
   //
   cout <<"size of the system to solve= " << PCG.Height() << endl; 
   PCG.Mult(b,y_mg);
   //
   GridFunction y_ref(&hierarchy.GetFinestFESpace());
   GridFunction err(&hierarchy.GetFinestFESpace());
   FunctionCoefficient y_ex_coeff(y_exact);
   y_ref.ProjectCoefficient(y_ex_coeff);
   // UMFPackSolver direct_solver;
   // direct_solver.SetOperator(*mg.GetOperatorAtFinestLevel());
   // direct_solver.Mult(b, y_ref); 
   err = y_ref; err -= y_mg;
   cout << "|| y_mg - y_exakt ||_2 = " << err.Norml2() << endl; 
   
   // if (paraview){
   //   pd = new mfem::ParaViewDataCollection("mg_space", 
   //                           hierarchy.GetFinestFESpace().GetMesh());
   //   pd->SetPrefixPath("paraview");
   //   pd->SetLevelsOfDetail(order_space);
   //   pd->SetDataFormat(VTKFormat::BINARY);
   //   pd->SetHighOrderOutput(true);
   //   pd->RegisterField("y_mg", &y_mg);
   //   pd->RegisterField("y_ref", &y_ref);
   //   pd->RegisterField("err", &err);
   //   pd->Save();
   //   delete pd;
   //}
   // for (int i=0; i<=mg_levels; ++i){
   //   delete fes_array[i+1];
   // }
      
   return 0; 
}
double f_func(const Vector& p)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.5;
   double z = (dim==3) ? p(2) : 0.0;

   return -4 * sin( PI * y) * (PI*PI * (x*x -x) - 2);
   //return 1.0;
}

double xi_func(const Vector& p)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.5;
   double z = (dim==3) ? p(2) : 0.0;

   return (x+1)*(y+1);
   //return 1;
}
double eta_func(const Vector& p)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.5;
   double z = (dim==3) ? p(2) : 0.0;

   return 4* (PI*(x*x-x) - sin(PI*y));
}

double y_exact(const Vector&p)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.5;
   double z = (dim==3) ? p(2) : 0.0;

   return -4 * (x*x - x) * sin(PI * y);
}
