//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include <ocst.hpp> 
#include "../data/bc/L_shaped_2D.hpp"



#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;

double
reference_solution_func(const Vector& x, const double t)
{ return t * x(0) + (1 - t*t) * x(1)*x(1);}

double
kappa_func(const mfem::Vector& x) 
{ return 1.0; } 

double
xi_func(const mfem::Vector& x) 
{
   return 0.25 * x(0)*x(0) * x(1) + 0.1;  
}

int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   int refinement_space = 2;
   int order_space = 2;
   int levels_space = 2;
   
   int initial_timesteps= 11;
   int order_time= 2;
   int levels_time= 2;

   int print_level = 2; 

   bool print_matrices = false;

   bool static_cond = false;
   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");

   args.AddOption(&order_space, "-os", "--order_space",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of initial refinements to perform on the spatial mesh");
   args.AddOption(&levels_space, "-ls", "--levels_space",
   		  "No. of spatial levels for the multigrid solver");

   args.AddOption(&order_time, "-ot", "--order_time",
                  "Finite element order (polynomial degree) or -1 ");
   args.AddOption(&initial_timesteps, "-K", "--initial_timesteps", 
	          "Number of initial nodes in the time mesh");
   args.AddOption(&levels_time, "-lt", "--levels_time",
   		  "No. of levels in time for the multigrid solver");

   args.AddOption(&print_level ,"-pl", "--print_level", 
	          "print level for the iterative solver");
   args.AddOption(&print_matrices, "-pm", "--print_matrices", 
                  "-npm", "--no_print_matrices", 
		  "print the matrices for the coarsest level");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);

   // Meshes, FE_Collections and spaces
   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }
   Mesh mesh_time= Mesh::MakeCartesian1D(initial_timesteps-1,1.0);

   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   
   H1_FECollection fec_time(order_time, mesh_time.Dimension());
   FiniteElementSpace fes_time(&mesh_time, &fec_time);
   
   // MG Prep
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(fes_time, 
   							   levels_time,
                                                           fes_space,
							   levels_space); 

   FunctionCoefficient kappa_coeff(kappa_func);
   FunctionCoefficient robin_lhs(xi_func);


   Array<ocst::TimeMatrices*> time_mats;
   Array<ocst::SpaceMatrices*> space_mats;

   mfem::FiniteElementSpaceHierarchy& hierarchy_time = 
                                               st_hierarchy.FESHierarchyTime();
   time_mats.Append(
      new ocst::TimeMatrices(hierarchy_time.GetFESpaceAtLevel(0),
                             hierarchy_time.GetFESpaceAtLevel(0),
			     true)
   );
   for(int i=1; i<levels_time; ++i){
      time_mats.Append(
	 new ocst::TimeMatrices(hierarchy_time.GetFESpaceAtLevel(i),
                                hierarchy_time.GetFESpaceAtLevel(i),
		                true)
      );
   }
			     
   mfem::FiniteElementSpaceHierarchy& hierarchy_space = 
                                               st_hierarchy.FESHierarchySpace();
   space_mats.Append(
      new ocst::SpaceMatrices(hierarchy_space.GetFESpaceAtLevel(0)));
   space_mats[0]->SetBLF(robin_lhs,kappa_coeff); 
   space_mats[0]->Assemble(); 

   for (int i=1; i<levels_space; ++i){
      space_mats.Append(new ocst::SpaceMatrices(*space_mats[i-1],
                                         hierarchy_space.GetFESpaceAtLevel(i)));
      space_mats[i]->Assemble();
   }

   int dof_time = time_mats.Last()->M_trial().Height();
   int dof_space = space_mats.Last()->A().Height();
   cout <<" n_h = "<< dof_space << endl;
   cout <<" K = "<< dof_time << endl;

   double* buffer = new double[dof_time*dof_space];
   st_hierarchy.SetBuffer(buffer);
   //Build actual Sylvester Operator 
   Array<ocst::GeneralSylvesterOperator*> op_hierarchy; 

   // M_time \otimes A_space 
   int diff = levels_time - levels_space;
   int offset_time = (diff<0) ? -diff : 0;
   int offset_space = (diff>0) ? diff :0;
   int levels = (levels_space>levels_time)? levels_space : levels_time;
   for (int i=0; i<levels; ++i){
      int index_time = (i - offset_time > 0) ? i - offset_time : 0 ;
      int index_space = (i - offset_space > 0) ? i - offset_space : 0 ;
      // cout << "level " << i << endl;
      // cout << "index_time " << index_time << endl;
      // cout << "index_space " << index_space << endl;
      // cout << endl;

      SparseMatrix& A_0 = time_mats[index_time]->M_trial();
      SparseMatrix& B_0 = space_mats[index_space]->M();
      SparseMatrix& A_1 = time_mats[index_time]->A_trial();
      SparseMatrix& B_1 = space_mats[index_space]->A();
      int m_A = A_0.Height();
      int m_B = B_0.Height();
      op_hierarchy.Append(
	 new ocst::GeneralSylvesterOperator(m_A, m_A, m_B, m_B, 1)
      );
      op_hierarchy[i]->SetMatrixA(A_0, 0, false);
      op_hierarchy[i]->SetMatrixB(B_0, 0, false);
      op_hierarchy[i]->SetMatrixA(A_1, 1, false);
      op_hierarchy[i]->SetMatrixB(B_1, 1, false);
      op_hierarchy[i]->SetBuffer(buffer);
   }


   // Set up solver and preconditioner
   ocst::GeometricMultigrid prec(st_hierarchy, op_hierarchy, false); 
   ocst::SolveMGPCG pcg(prec);

   // Set up reference solution and right hand side
   ocst::State x_ref(hierarchy_time.GetFinestFESpace(),
                     hierarchy_space.GetFinestFESpace());
   FunctionCoefficient x_ref_coeff(reference_solution_func);
   
   		     
   ocst::State x(x_ref);
   ocst::State b(x_ref);
   
   x_ref.SetValue(x_ref_coeff); 
   op_hierarchy.Last()->Mult(x_ref.Vec(), b.Vec());

   // Solve
   pcg.SetSolverParameters(1e-5, 1e-6, 40, print_level); 
   pcg.Mult(b.Vec(), x.Vec());
   
   
   // Print finest grid matrices 
   if(print_matrices){
      ofstream file_A0;
      file_A0.open("A0.mtx");
      time_mats.Last()->M_trial().PrintMM(file_A0);
      file_A0.close();
      
      ofstream file_B0;
      file_B0.open("B0.mtx");
      time_mats.Last()->M_trial().PrintMM(file_B0);
      file_B0.close();
      
      ofstream file_A1;
      file_A1.open("A1.mtx");
      time_mats.Last()->M_trial().PrintMM(file_A1);
      file_A1.close();
      
      ofstream file_B1;
      file_B1.open("B1.mtx");
      time_mats.Last()->M_trial().PrintMM(file_B1);
      file_B1.close();
   
      // Vectors 
      ofstream file_x;
      file_x.open("x.vec");
      x.Vec().Print(file_x);
      file_x.close();
      
      ofstream file_b;
      file_b.open("b.vec");
      b.Vec().Print(file_b);
      file_b.close();

      ofstream file_x_ref;
      file_x_ref.open("x_ref.vec");
      x_ref.Vec().Print(file_x_ref);
      file_x_ref.close();
   }
   // error 
   x.Vec() -= x_ref.Vec(); 

   double err = x.Vec().Norml2()/x_ref.Vec().Norml2();
   cout << "|| x - x_ref||_2/||x_ref||_2 = "<< err << endl;
   delete[] buffer;
   for (auto elem: op_hierarchy){delete elem;}
   for (auto elem: space_mats){ delete elem;}
   for (auto elem: time_mats){ delete elem;}
   return 0; 
}
