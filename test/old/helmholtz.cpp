//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;

int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   int refinement_space = 2;
   int order_space = 1;
   int nof_timesteps = 11;
   int levels_time = 1;
   int levels_space = 4;
   double tol = 1e-3;


   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
                  "Number of initial refinements to perform on the spatial mesh");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
   	          "Number of initial timesteps K");
   //args.AddOption(&levels_time, "-lt", "--levels_time",
   //               "levels for MG time dimension");
   args.AddOption(&levels_space, "-ls", "--levels_space",
                  "levels for MG space dimension");
   args.AddOption(&tol, "-tol", "--tol", "  "); 
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);

   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }
   //Mesh mesh_space(refinement_space-1,1.0);
   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);

   FiniteElementSpace fes_space(&mesh_space, &fec_space);


   //MG Prep
   int levels = (levels_time > levels_space)? levels_time : levels_space;
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(fes_time_trial, 
                                                           levels_time,
                                                           fes_space, 
							   levels_space);
   FiniteElementSpaceHierarchy& hierarchy_space = 
                                     st_hierarchy.FESHierarchySpace();
   
   int K_finest = st_hierarchy.GetFESTimeAtLevel(levels-1).GetVSize();
   int n_finest = st_hierarchy.GetFESSpaceAtLevel(levels-1).GetVSize();
   // Test Prolongation/refinement 

   //Type "1"
  
   ocst::TimeMatrices time_matrices(fes_time_trial, fes_time_test); 


   Array<ocst::SpaceMatrices*> space_mats;
   Array<SparseMatrix*> A_space; 
   Array<SparseMatrix*> M_space; 

   space_mats.Append(new ocst::SpaceMatrices(
       hierarchy_space.GetFESpaceAtLevel(0)));
   FunctionCoefficient robin_lhs(xiFunc);
   ConstantCoefficient one(1.0); 
   space_mats[0]->SetBLF(one); 
   space_mats[0]->Assemble(); 

   A_space.Append(&space_mats[0]->A());
   M_space.Append(&space_mats[0]->M());
   for (int i=1; i<levels_space; ++i){
      space_mats.Append(new ocst::SpaceMatrices(*space_mats[i-1],
          hierarchy_space.GetFESpaceAtLevel(i)));
      space_mats[i]->Assemble();
   A_space.Append(&space_mats[i]->A());
   M_space.Append(&space_mats[i]->M());
   }

   //Array<HelmHoltzNormal*> helmholtz;
   //for (int i=0; i<levels_space; ++i){
   //   helmholtz.Append(new HelmHoltzNormal(*M_space[i], *A_space[i], theta));
   //}
   //
   //ocst::GeometricMultigrid prec_helmholtz(hierarchy_space, helmholtz, false);
   //prec_helmholtz.SetCycleType(Multigrid::CycleType::VCYCLE, 2, 2);
   //ocst::SolveMGPCG solve_helmholtz(prec_helmholtz);
   //solve_helmholtz.SetSolverParameters(1e-6, 1e-4, 50, 1); 

   //Vector x_ref(n_finest); x_ref = 1.0; 
   //Vector f(n_finest); f=0.0;
   //helmholtz.Last()->Mult(x_ref, f); 
   //Vector x(n_finest); x = 0.0; 

   //solve_helmholtz.Mult(f, x);
   //x -= x_ref;
   //cout << "|x-xref| = " << x*x << endl; 

   //cout << endl << "Test ChebyshevSmoother" <<endl;
   //Array<int> empty;
   //Vector diag_ones(helmholtz.Last()->Height()); 
   //diag_ones = 1.0;

   //cout << "mfem " << endl;
   //OperatorChebyshevSmoother mfem_version(helmholtz.Last(),diag_ones, empty ,4);
   //cout << "mfem " << endl;
   //cout << "ocst" << endl;
   //ocst::ChebyshevSmoother   ocst_version(helmholtz.Last(), 4);
   //cout << "ocst" << endl;

   //mfem_version.Mult(x_ref, f);
   //ocst_version.Mult(x_ref, x);
   //x -= f;
   //cout << "error  chebyshev versions = " << x*x << endl; 

   //int K = 14;
   //theta = 1e-4;
   //mfem::Vector gamma(K);
   //for (int k=0; k<K; ++k){gamma(k) = theta; theta*=10;}
   //gamma.Print(); 

   //ocst::HelmholtzNormalReal hh_nr(*M_space.Last(), *A_space.Last(), gamma(5));
   ////hh_nr.Mult(x_ref, f); 
   //ocst::SolveHelmholtzNormalMG hh_mg(M_space, A_space, hierarchy_space, gamma);

   //hh_mg.SetTimeStep(5);
   //cout << "Solve for timestep 5, gamma = " << gamma(5) << endl;
   //hh_mg.Mult(f,x);
   //x -= x_ref;
   //cout << "|x-xref| = " << x*x << endl; 

   int dim_time = time_matrices.M_trial().Height();
   ocst::GeometricMultigrid prec_A(hierarchy_space, A_space, true);
   ocst::SolveMGPCG solve_A(prec_A);
   solve_A.SetSolverParameters(0,1e-6,50,-1); 
   ocst::NormOperatorTrial M_trial(time_matrices.M_trial(), 
                                   time_matrices.A_trial(),
				   *M_space.Last(), *A_space.Last(), solve_A, 
				   false);

   ocst::SpaceTimeData x_ST_ref(dim_time,n_finest);
   x_ST_ref.Vec().Randomize() ;
   ocst::SpaceTimeData x_ST(dim_time,n_finest);
   ocst::SpaceTimeData f_ST(dim_time,n_finest);
   cout <<  "Start Forward" <<endl;
   M_trial.Mult(x_ST_ref,f_ST);
   cout <<  "finished Forward" <<endl;
   cout << "K = " << dim_time <<endl; 
   cout << "n_h = " << n_finest <<endl; 
   //cout << "f = " << endl;
   //f_ST.Mat().Print(); 
   M_trial.SetInverseOperator(M_space, A_space, hierarchy_space);
   auto m_inv = static_cast<ocst::InvNormOperatorTrial*>(M_trial.GetInverseOperator());
   m_inv->SetTolerance(tol); 
   
					
   cout <<  "Start Inverse" <<endl;
   M_trial.MultInverse(f_ST.Vec(), x_ST.Vec());
   cout <<  "finished Inverse" <<endl;
   //x_ST.Mat().Print(); 
   x_ST.Vec()-= x_ST_ref.Vec();
   cout << "|x-xref| = " << x_ST.Vec()*x_ST.Vec() << endl; 


   for (int i=0; i<levels_space; ++i){
      delete space_mats[i];
      //delete helmholtz[i]; 
   }
   return 0; 
}
