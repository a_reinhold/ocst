//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   int refinement_space = 0;
   int order_space = 1;
   int nof_timesteps = 11;
   int levels_time = 1;
   int levels_space = 2;


   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
                  "Number of initial refinements to perform on the spatial mesh");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
   	          "Number of initial timesteps K");
   args.AddOption(&levels_time, "-lt", "--levels_time",
                  "levels for MG time dimension");
   args.AddOption(&levels_space, "-ls", "--levels_space",
                  "levels for MG space dimension");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);

   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);

   FiniteElementSpace fes_space(&mesh_space, &fec_space);


   //MG Prep
   int levels = (levels_time > levels_space)? levels_time : levels_space;
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(fes_time_trial, 
                                                           levels_time,
                                                           fes_space, 
							   levels_space);
   

   //Type "1"
   Mesh* mesh_time_ref = 
        	  st_hierarchy.FESHierarchyTime().GetFinestFESpace().GetMesh();
   FiniteElementSpace fes_time_test(mesh_time_ref, &fec_time_test);
   Array<ocst::TimeMatrices*> time_mats;
  

   for (int i=0; i<levels_time; ++i){
      time_mats.Append(new ocst::TimeMatrices(
         st_hierarchy.FESHierarchyTime().GetFESpaceAtLevel(i), fes_time_test));
      time_mats.Last()->Assemble((i<levels_time-1)?true:false);
   }

   Array<ocst::SpaceMatrices*> space_mats;
   Array<SparseMatrix*> A_space; 
   Array<SparseMatrix*> M_space; 

   space_mats.Append(new ocst::SpaceMatrices(
       st_hierarchy.FESHierarchySpace().GetFESpaceAtLevel(0)));
   FunctionCoefficient robin_lhs(muFunc);
   ConstantCoefficient one(1.0); 
   space_mats[0]->SetBLF(robin_lhs); 
   space_mats[0]->Assemble(); 

   A_space.Append(&space_mats[0]->A());
   M_space.Append(&space_mats[0]->M());
   for (int i=1; i<levels_space; ++i){
      space_mats.Append(new ocst::SpaceMatrices(*space_mats[i-1],
          st_hierarchy.FESHierarchySpace().GetFESpaceAtLevel(i)));
      space_mats[i]->Assemble();
      A_space.Append(&space_mats[i]->A());
      M_space.Append(&space_mats[i]->M());
   }
   int dof_time = time_mats.Last()->M_trial().Height();
   int dof_space = space_mats.Last()->A().Height();
   cout <<" n = "<< dof_space << endl;
   cout <<" K = "<< dof_time << endl;

   double* buffer = new double[dof_time*dof_space];
   ocst::GeometricMultigrid prec_A(st_hierarchy.FESHierarchySpace(), A_space, true);

   Array<ocst::SolveMGPCG*> solve_A;
   for (int i=0; i<levels_space; ++i){
      solve_A.Append(new ocst::SolveMGPCG(prec_A,i));
      solve_A.Last()->SetSolverParameters(0,1e-5,50,-1); 
   }
   
   Array<ocst::NormOperatorTrial*> M_norm;
   int diff_levels = levels_time - levels_space;
   int index_t = 0; 
   int index_s = 0;
   for (int i=0; i<levels; ++i){
      index_t = (diff_levels > 0) ? (0+i) : (diff_levels+i);
      index_t = (index_t<0)?0:index_t;
      index_s = (diff_levels < 0) ? (0+i) : (-diff_levels+i);
      index_s = (index_s<0)?0:index_s;
      M_norm.Append(new ocst::NormOperatorTrial(time_mats[index_t]->M_trial(),
        					time_mats[index_t]->A_trial(),
        					space_mats[index_s]->M(),
        					space_mats[index_s]->A(),
        					*solve_A[index_s], false, buffer));
   }
   ocst::GeometricMultigrid prec_M_norm(st_hierarchy,M_norm, false);

   ocst::SolveMGPCG M_norm_inv(prec_M_norm);
   M_norm_inv.SetSolverParameters(1e-3,1e-2,100,1); 

   cout << "Start Setup MG" << endl; 
   M_norm.Last()->SetInverseOperator(M_norm_inv);
   cout << "finished Setup " << endl; 
  
   st_hierarchy.SetBuffer(buffer);
   ocst::SpaceTimeData x_ref(dof_time,dof_space);
   ocst::SpaceTimeData x(dof_time,dof_space);
   ocst::SpaceTimeData y(dof_time,dof_space);


   
   x_ref.Vec().Randomize();
   std::cout<<"forward"<< endl;
   M_norm.Last()->Mult(x_ref,y);
   std::cout<<"finished forward"<< endl;


   std::cout<<"ST MGPCG"<< endl;
   M_norm.Last()->MultInverse(y,x);
   std::cout<<"finished st MGPCG"<< endl;
   x.Mat() -= x_ref.Mat(); 
   cout << "||x_ref-x||^2_2 = " << x.Vec() * x.Vec() << endl;  


   cout << "Start Setup HH" << endl; 
   M_norm.Last()->SetInverseOperator(M_space, A_space, st_hierarchy.FESHierarchySpace());
   cout << "Finished Setup HH" << endl; 
   std::cout<<"Helmholtz "<< endl;
   M_norm.Last()->MultInverse(y,x);
   std::cout<<"finished Helmholtz"<< endl;
   x.Mat() -= x_ref.Mat(); 
   cout << "||x_ref-x||^2_2 = " << x.Vec() * x.Vec() << endl;  




   for (int i=0; i<levels_time; ++i){
      delete time_mats[i];
   }
   for (int i=0; i<levels_space; ++i){
      delete solve_A[i]; 
      delete space_mats[i];
   }
   for (int i=0; i<levels; ++i){
      delete M_norm[i];
   }
   delete[] buffer;
   return 0; 
}


