//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;

class SparseProlongation: public mfem::Operator{
private:
   const int dim;
   const double restriction_factor;
   mfem::SparseMatrix* mat;
public:
   SparseProlongation(mfem::FiniteElementSpace& coarse, 
                      mfem::FiniteElementSpace& fine):
      dim{coarse.GetMesh()->Dimension()},
      restriction_factor{(dim==3)? 1.0/8.0 : 1.0/double(2*dim)}
   {
      cout << "restricction factor = " << restriction_factor << endl;
      mfem::OperatorHandle handle(mfem::Operator::Type::MFEM_SPARSEMAT);                                      
      fine.GetTransferOperator(coarse, handle);                                                               
      mat = new mfem::SparseMatrix();
      mfem::SparseMatrix* tmp = static_cast<mfem::SparseMatrix*>(handle.Ptr());                               
      tmp->Swap(*mat);  
   }
   ~SparseProlongation()
   {
      delete mat;
   }
   //Prolongation
   void
   Mult(const mfem::Vector& x, mfem::Vector& y) const override
   {
      mat->Mult(x,y);
   }
   //Restriction
   void
   MultTranspose(const mfem::Vector& x, mfem::Vector& y) const override
   {
      mat->MultTranspose(x,y);
      y *= restriction_factor;
   }

};
int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   bool paraview = false;
   int refinement_space = 2;
   int order_space = 1;


   bool static_cond = false;
   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of refinements to perform on the spatial mesh");
   args.AddOption(&paraview, "-paraview", "--paraview", 
                  "-no-paraview", "--no-paraview",
                  "Save files for paraview Visualization");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);

   // Meshes, FE_Collections and spaces


   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }

   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   
   // parabolic operator
   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   
  

   FunctionCoefficient f_coeff([](const Vector& x){
   	int sz = x.Size();
	if (sz==1) { return 4.0*x(0)*(1.0-x(0));}
	//if (sz==2) { return 12.0*x(0)*(1.0-8.0*x(1));}
	//if (sz==3) { return 8*x(0)*(1.0-4.0*x(2))-4.5*x(0)*x(1);}
	else {return 10.0;}});

   // FES Hierachy with implicit Prolongations: 
   FiniteElementSpaceHierarchy hierarchy_1(&mesh_space, &fes_space,false,false);
   hierarchy_1.AddUniformlyRefinedLevel();

   // FES with SparseMatrix as Prolongation
   FiniteElementSpaceHierarchy hierarchy_2(&mesh_space, &fes_space,false,false);
   
   Mesh mesh_refined(mesh_space);

   mesh_refined.UniformRefinement();
   FiniteElementSpace fes_refined(fes_space, &mesh_refined);
   SparseProlongation prol(fes_space, fes_refined);
   hierarchy_2.AddLevel(&mesh_refined, &fes_refined, &prol, false, false, false);


   cout << "#Elements coarse = " << mesh_space.GetNE() << endl;
   cout << "#Elements fine = " << mesh_refined.GetNE() << endl;


   GridFunction coarse_ref(&hierarchy_1.GetFESpaceAtLevel(0));
   GridFunction coarse_1(&hierarchy_1.GetFESpaceAtLevel(0));
   GridFunction coarse_2(&hierarchy_2.GetFESpaceAtLevel(0));
   GridFunction fine_ref(&hierarchy_1.GetFESpaceAtLevel(1));
   GridFunction fine_1(&hierarchy_1.GetFESpaceAtLevel(1));
   GridFunction fine_2(&hierarchy_2.GetFESpaceAtLevel(1));
   
   coarse_ref.ProjectCoefficient(f_coeff);
   fine_ref.ProjectCoefficient(f_coeff);
   
   mfem::Operator* P_1 = hierarchy_1.GetProlongationAtLevel(0);
   P_1->Mult(coarse_ref, fine_1);
   P_1->MultTranspose(fine_ref, coarse_1);
   mfem::Operator* P_2 = hierarchy_2.GetProlongationAtLevel(0);
   P_2->Mult(coarse_ref, fine_2);
   P_2->MultTranspose(fine_ref, coarse_2);
	
 
    if (paraview){
      pd = new mfem::ParaViewDataCollection("coarse", 
                              hierarchy_1.GetFESpaceAtLevel(0).GetMesh());
      pd->SetPrefixPath("paraview");
      pd->SetLevelsOfDetail(order_space);
      pd->SetDataFormat(VTKFormat::BINARY);
      pd->SetHighOrderOutput(true);
      pd->RegisterField("coarse_reference", &coarse_ref);
      pd->RegisterField("coarse_1", &coarse_1);
      pd->RegisterField("coarse_2", &coarse_2);
      pd->Save();
      delete pd;
      
      pd = new mfem::ParaViewDataCollection("fine", 
                              hierarchy_1.GetFESpaceAtLevel(1).GetMesh());
      pd->SetPrefixPath("paraview");
      pd->SetLevelsOfDetail(order_space);
      pd->SetDataFormat(VTKFormat::BINARY);
      pd->SetHighOrderOutput(true);
      pd->RegisterField("fine_reference", &fine_ref);
      pd->RegisterField("fine_1", &fine_1);
      pd->RegisterField("fine_2", &fine_2);
      pd->Save();
      delete pd;
   }

   return 0; 
}
