//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   bool paraview = false;
   int refinement_space = 2;
   int order_space = 1;
   int nof_timesteps = 11;


   bool static_cond = false;
   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of refinements to perform on the spatial mesh");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
   	          "Number of timesteps K");
   args.AddOption(&paraview, "-paraview", "--paraview", 
                  "-no-paraview", "--no-paraview",
                  "Save files for paraview Visualization");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);

   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);

   
   // parabolic operator
   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   ocst::SpaceTimeParabolicOperator parabolic_operators(fes_time_trial, 
						         fes_time_test,
						         fes_space);

   FunctionCoefficient robin_lhs(muFunc);
   parabolic_operators.SetOperators(robin_lhs);
   parabolic_operators.AssembleOperators();
   
   
   // Control 
   ocst::Control u(fes_time_test, fes_space);
   
   u.SetM_time();
   u.SetM_space(parabolic_operators.M_space());
   FunctionCoefficient u_init(fFunc);
   u.SetInitialValue(u_init); 

   // State
   ocst::State y(fes_time_trial, fes_space);
   y.SetM_time(); 
   y.SetM_space(parabolic_operators.M_space());

   ocst::AdjointState p(fes_time_test, fes_space);



   mfem::FunctionCoefficient robin_rhs(etaFunc);
   mfem::ConstantCoefficient y_0(0.0);
   ocst::PrimalRHS primal_rhs(fes_time_trial.GetVSize(), fes_space.GetVSize());
   primal_rhs.SetConstantPart(fes_time_trial, fes_space, robin_rhs, y_0,
                              parabolic_operators.M_time_mixed());

   ocst::AdjointRHS adjoint_rhs(fes_time_trial.GetVSize(), fes_space.GetVSize());
   mfem::FunctionCoefficient y_d([] (const Vector& x){
                                  return -x(0)*x(1)+0.125;});
   adjoint_rhs.SetConstantPart(fes_space, y_d, y.M_space());

   
   
   ocst::SpaceTimeDirectSolver direct_solver(parabolic_operators, 
                                             primal_rhs, adjoint_rhs);
   direct_solver.AssembleMonolithic();
   direct_solver.SetOperator();
   direct_solver.SolvePrimal(u, y);
   direct_solver.SolveAdjoint(y, p);

   std::cout << "||y||^2 = " << y.SquaredInnerProductNorm() << std::endl; 
   if (paraview){
      pd = new mfem::ParaViewDataCollection("xtest3", &mesh_space);
      pd->SetPrefixPath("paraview");
      pd->SetLevelsOfDetail(order_space);
      pd->SetDataFormat(VTKFormat::BINARY);
      pd->SetHighOrderOutput(true);
      y.ExportToParaview(*pd, "y", fes_time_trial, fes_space);
   //   u.ExportToParaview(*pd, "u", fes_time_test, fes_space);
      delete pd;
   }
   return 0; 
}
