//                                MFEM Example 1
//
// Compile with: make myParabolic
//
// Sample runs:  ex1 -m ../data/star.mesh
//
// Device sample runs:
//
// Description:  
//  AR: First test of a Laplace Problem: 
//  		\Laplace y = f        \in \Omega \times I
//		d/dn y + \mu(x) y= \eta(x)  \in \Gamma_rb \times I
//	Robin boundaries in the first attempt w. constants \mu, \eta,
// 	Domains to Test: 1D -> [0;1]
// 			 2D -> [0;1]^2
// 			 3D -> [0;1]^3

#include "mfem.hpp"
#include <cmath>
#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;

//define pi
constexpr double PI = 4 * atan(1);

// forward declarations
double f_func(const Vector&);
double mu_func(const Vector&);
double eta_func(const Vector&);
double u_exact(const Vector&);

int main(int argc, char *argv[])
{
   // 1. Parse command-line options.
   const char *mesh_file = "../data/star.mesh";
   int order = 1;
   bool static_cond = false;
   const char *device_config = "cpu";
   bool visualization = false;
   bool paraview = false;
   int refinement = 0;
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   args.AddOption(&static_cond, "-sc", "--static-condensation", "-no-sc",
                  "--no-static-condensation", "Enable static condensation.");
   args.AddOption(&visualization, "-vis", "--visualization", "-no-vis",
                  "--no-visualization",
                  "Enable or disable GLVis visualization.");
   args.AddOption(&refinement, "-r", "--refinement", 
	          "Number of refinements to perform on the mesh");
   args.AddOption(&paraview, "-paraview", "--paraview", 
                  "-no-paraview", "--no-paraview",
                  "Save files for paraview Visualization");
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // 2. Enable hardware devices such as GPUs, and programming models such as
   //    CUDA, OCCA, RAJA and OpenMP based on command line options.
   Device device(device_config);
   device.Print();

   // 3. Read the mesh from the given mesh file. We can handle triangular,
   //    quadrilateral, tetrahedral, hexahedral, surface and volume meshes with
   //    the same code.
   Mesh *mesh = new Mesh(mesh_file, 1, 1);
   int dim = mesh->Dimension();
   for( int i=0; i<refinement; ++i)
   {
      mesh->UniformRefinement();
   }


   // 5. Define a finite element space on the mesh. Here we use continuous
   //    Lagrange finite elements of the specified order. If order < 1, we
   //    instead use an isoparametric/isogeometric space.
   FiniteElementCollection *fec;
   if (order > 0)
   {
      fec = new H1_FECollection(order, dim);
   }
   else if (mesh->GetNodes())
   {
      fec = mesh->GetNodes()->OwnFEC();
      cout << "Using isoparametric FEs: " << fec->Name() << endl;
   }
   else
   {
      fec = new H1_FECollection(order = 1, dim);
   }
   FiniteElementSpace *fespace = new FiniteElementSpace(mesh, fec);
   cout << "Number of finite element unknowns: "
        << fespace->GetTrueVSize() << endl;

   // 6. Determine the list of true (i.e. conforming) essential boundary dofs.
   //    In this example, the boundary conditions are defined by marking all
   //    the boundary attributes from the mesh as essential (Dirichlet) and
   //    converting them to a list of true dofs.
   Array<int> ess_tdof_list;
   if (mesh->bdr_attributes.Size())
   {
      Array<int> ess_bdr(mesh->bdr_attributes.Max());
      ess_bdr = 0; //if zero we have no dirichlet? 
      fespace->GetEssentialTrueDofs(ess_bdr, ess_tdof_list);
   }
   //AR: determine where robin bc are in place 
   //Array<int> rb_bdry(mesh->bdr_attributes.Max());
   //rb_bdry=1; //all rb_bdry
   double etaVal = 1.0;
   double muVal = 1.0; // >= 0 
   double fVal = 1.0;

   // 7. Set up the linear form b(.) which corresponds to the right-hand side of
   //    the FEM linear system, which in this case is 
   //    (f,phi_i)_L2(Omega) + (eta,phi_i)_L2(Gamma)  where phi_i are
   //    the basis functions in the finite element fespace.
   LinearForm *b = new LinearForm(fespace);
   // ConstantCoefficient fCoeff(fVal);
   // ConstantCoefficient etaCoeff(etaVal);
   FunctionCoefficient fCoeff(f_func);
   FunctionCoefficient etaCoeff(eta_func);
   b->AddDomainIntegrator(new DomainLFIntegrator(fCoeff));
   b->AddBoundaryIntegrator(new BoundaryLFIntegrator(etaCoeff));

   b->Assemble();

   // 8. Define the solution vector x as a finite element grid function
   //    corresponding to fespace. Initialize x with initial guess of zero,
   //    which satisfies the boundary conditions.
   GridFunction x_iter(fespace);
   GridFunction x_direct(fespace);
   x_iter = 0.0;
   x_direct = 0.0;

   // 9. Set up the bilinear form a(.,.) on the finite element space
   //    corresponding to the Laplacian operator -Delta, by adding the Diffusion
   //    domain integrator.
   // ConstantCoefficient muCoeff(muVal);
   FunctionCoefficient muCoeff(mu_func);
   ConstantCoefficient one(1.0);
   
   BilinearForm *a = new BilinearForm(fespace);
   a->AddDomainIntegrator(new DiffusionIntegrator(one));
   a->AddBoundaryIntegrator(new MassIntegrator(muCoeff));
   // 10. Assemble the bilinear form and the corresponding linear system,
   //     applying any necessary transformations such as: eliminating boundary
   //     conditions, applying conforming constraints for non-conforming AMR,
   //     static condensation, etc.
   if (static_cond) { a->EnableStaticCondensation(); }
   a->Assemble();

   OperatorPtr A;
   Vector B, X_iter, X_direct;
   a->FormLinearSystem(ess_tdof_list, x_iter, *b, A, X_iter, B);
   a->FormLinearSystem(ess_tdof_list, x_direct, *b, A, X_direct, B);
   cout << "Size of linear system: " << A->Height() << endl;

   // 11. Solve the linear system A X = B.
   // Use a simple symmetric Gauss-Seidel preconditioner with PCG.
   GSSmoother M((SparseMatrix&)(*A));
   PCG(*A, M, B, X_iter, 1, 1000, 1e-8, 0.0);
   a->RecoverFEMSolution(X_iter, *b, x_iter);
   
   UMFPackSolver umf_solver;
   umf_solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
   umf_solver.SetOperator(*A);
   umf_solver.Mult(B, X_direct);
   a->RecoverFEMSolution(X_direct, *b, x_direct);

   // 12. Recover the solution as a finite element grid function.

   //Try to compute exact solution and save it in paraview
   FunctionCoefficient uExactCoeff(u_exact);
   GridFunction u(fespace);
   u.ProjectCoefficient(uExactCoeff);
   
   GridFunction uDiff(fespace);
   uDiff.GetTrueVector() = x_direct.GetTrueVector() - u.GetTrueVector();

   cout << "L2 error ||u_iter-u_ref||_L2 = " 
	    << x_iter.ComputeL2Error(uExactCoeff) << endl;
   cout << "L2 error ||u_direct-u_ref||_L2 = " 
	    << x_direct.ComputeL2Error(uExactCoeff) << endl;


   // 13. Save the refined mesh and the solution. This output can be viewed later
   //     using GLVis: "glvis -m refined.mesh -g sol.gf".
   /*
   ofstream mesh_ofs("refined.mesh");
   mesh_ofs.precision(8);
   mesh->Print(mesh_ofs);
   ofstream sol_ofs("sol.gf");
   sol_ofs.precision(8);
   x.Save(sol_ofs);
   */

   // Save solution for paraview output 
   if (paraview)
   {
      pd = new ParaViewDataCollection("poisson", mesh);
      pd->SetPrefixPath("paraview");
      pd->RegisterField("iterative_solution", &x_iter);
      pd->RegisterField("direct_solution", &x_direct);
      pd->RegisterField("referenceSolution", &u);
      pd->RegisterField("u-u_ref", &uDiff);
      pd->SetLevelsOfDetail(order);
      pd->SetDataFormat(VTKFormat::BINARY);
      pd->SetHighOrderOutput(true);
      pd->SetCycle(0);
      pd->SetTime(0.0);
      pd->Save();
   }

   // 14. Send the solution by socket to a GLVis server.
   if (visualization)
   {
      char vishost[] = "localhost";
      int  visport   = 19916;
      socketstream sol_sock(vishost, visport);
      sol_sock.precision(8);
      sol_sock << "solution\n" << *mesh << x_direct << flush;
   }

   // 15. Free the used memory.
   delete a;
   delete b;
   delete fespace;
   if (order > 0) { delete fec; }
   if (paraview) { delete pd;}
   delete mesh;

   return 0;
}

double f_func(const Vector& p)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.0;
   double z = (dim==3) ? p(2) : 0.0;

   return -4 * sin( PI * y) * (PI*PI * (x*x -x) - 2);
   //return 1.0;
}

double mu_func(const Vector& p)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.0;
   double z = (dim==3) ? p(2) : 0.0;

   return (x+1)*(y+1);
   //return 1;
}
double eta_func(const Vector& p)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.0;
   double z = (dim==3) ? p(2) : 0.0;

   return 4* (PI*(x*x-x) - sin(PI*y));
}

double u_exact(const Vector&p)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.0;
   double z = (dim==3) ? p(2) : 0.0;

   return -4 * (x*x - x) * sin(PI * y);
}
