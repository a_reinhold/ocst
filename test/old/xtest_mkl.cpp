#include <mfem.hpp>
#include <mkl.h>
#include <mkl_spblas.h>

#include <iostream>

void
SparseDenseRef(const double alpha, 
               const mfem::SparseMatrix& A, const mfem::DenseMatrix& B,
	       const double beta, 
	       mfem::DenseMatrix& C)
{
   if (beta){
      C *= beta;
   } else {
      C = 0.0;
   }

   const int *I = A.GetI();
   const int *K = A.GetJ();
   const double* data = A.GetData(); 
   const int m = C.Height();
   const int n = C.Width();
   const int k = B.Height();

   for (int i = 0; i<m; ++i){
      for (int r=I[i]; r<I[i+1]; ++r){
	 int l = K[r];
	 for (int j=0; j<n; ++j){
	    C(i,j) += alpha * data[r] * B(l,j);
	 }
      }
   }
}

void
SparseDense(const double alpha, 
            const mfem::SparseMatrix& A, const mfem::DenseMatrix& B,
	    const double beta, 
	    mfem::DenseMatrix& C)
{
   const int *I = A.GetI();
   int *K = const_cast<int*>(A.GetJ());
   double* data = const_cast<double*>(A.GetData()); 
   const int m = C.Height();
   const int n = C.Width();
   const int k = B.Height();


   sparse_matrix_t A_= NULL;
   int* rows_start = new int[m]; 
   int* rows_end = new int[m];

   for (int l=0; l<m; ++l){
      rows_start[l] = I[l];
      rows_end[l] = I[l+1];
   }

   mkl_sparse_d_create_csr( &A_,
                            SPARSE_INDEX_BASE_ZERO,
			    m, k, rows_start, rows_end, K, data); 
   matrix_descr descr;
   descr.type = SPARSE_MATRIX_TYPE_GENERAL;
   descr.mode = SPARSE_FILL_MODE_FULL;
   descr.diag = SPARSE_DIAG_NON_UNIT;
   mkl_sparse_d_mm(SPARSE_OPERATION_NON_TRANSPOSE, alpha, A_, descr,
                   SPARSE_LAYOUT_COLUMN_MAJOR, B.GetData(), n, k, 
		   beta, C.GetData(), m);
   delete rows_start;
   delete rows_end;
}
int 
main(){
   /*{
   int m = 30;
   int n = 25;
   int k = 12;

   mfem::DenseMatrix A(m,k);
   A = 4.2; 
   mfem::DenseMatrix B(k,n);
   B = 1.3; 

   mfem::DenseMatrix C(m,n); 
   mfem::DenseMatrix C_ref(C);

   mfem::Mult(A, B, C_ref);

   
   CBLAS_LAYOUT layout = CblasColMajor;
   CBLAS_TRANSPOSE op = CblasNoTrans; 

   double alpha = 1.0; 
   double beta = 0.0; 

   cblas_dgemm(layout, op, op, m , n, k, 
               alpha, A.GetData(), A.Height(), B.GetData(), B.Height(), 
	       beta, C.GetData(), C.Height());

   C_ref -= C;

   std::cout << "||C_-C||_max = " << C_ref.MaxMaxNorm() << std::endl; 
               

   }*/
   {
   // Sparse Dense

   int m = 17;
   int k = m;
   int n = 12;

   mfem::Mesh mesh(m-1,1.0);
   mfem::H1_FECollection fec(1,1);
   mfem::FiniteElementSpace fes(&mesh, &fec); 

   mfem::BilinearForm a(&fes);

   mfem::ConstantCoefficient one(1.0);

   a.AddDomainIntegrator(new mfem::MassIntegrator(one)); 
   a.Assemble();
   a.Finalize();

   mfem::SparseMatrix A = a.SpMat();

   mfem::DenseMatrix B(k,n);
   B = 4.2;

   mfem::DenseMatrix C(m,n);
   mfem::DenseMatrix C_ref(C);



   double alpha = 1.0;
   double beta = 0.0;

   SparseDenseRef(alpha, A, B, beta, C_ref);
   SparseDense(alpha, A, B, beta, C);


   C_ref -= C;

   std::cout << "||C_-C||_max = " << C_ref.MaxMaxNorm() << std::endl; 
   }
   



}
