//      Boundary Conditions and reference Solution for parabolic.cpp
//
// Fuctions for the 2D Unit Square [0,1]^2 
// yExactFunc => given exact solution
// f = dy_t - \Laplace y
// xi(x) given > 0
// eta(x,t) = dy_nu + xi(x) y(x,t) 
# include "mfem.hpp"
# include <cmath>
namespace mfem{

using namespace mfem;
/*----------------------------- Ex 1----------------------------------------- */
/*
double fFunc(const Vector& p, const double t)
{
   return 1.0;
}

double muFunc(const Vector& p)
{
   return 1;
}
double etaFunc(const Vector& p, const double t)
{
   return muFunc(p) * t;
}

double yExactFunc(const Vector&p, const double t)
{
   return t;
}

*/
/*----------------------------- Ex 2----------------------------------------- */
double yExactFunc(const Vector& x, const double t)
{
   double x1 = x(0);
   double x2 = x(1);

   return t * ((x1 * x1 -x1) + (x2 * x2 - x2));
}

double fFunc(const Vector& x, const double t)
{
   double x1 = x(0);
   double x2 = x(1);

   return (x1 * x1 -x1) + (x2 * x2 - x2) - 4*t;
}

double xiFunc(const Vector& x)
{
   double x1 = x(0);
   double x2 = x(1);

   return x1 + x2 + 1.0;
}

double etaFunc(const Vector& x, double t)
{
   return t + xiFunc(x) * yExactFunc(x,t);
}
/*----------------------------- Ex 3----------------------------------------- */
/*
//define pi
constexpr double PI = 4 * atan(1);
double fFunc(const Vector& p, const double t)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.0;
   double z = (dim==3) ? p(2) : 0.0;

   return -4 * t * sin( PI * y) * ((x*x -x) * (2 + t* PI * PI) - 2*t);
   //return 1.0;
}

double muFunc(const Vector& p)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.0;
   double z = (dim==3) ? p(2) : 0.0;

   return 0.1*(x+1)*(y+1);
   //return 1;
}
double etaFunc(const Vector& p, const double t)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.0;
   double z = (dim==3) ? p(2) : 0.0;

   return 4*t*t* (PI*(x*x-x) - sin(PI*y));
}

double y_exact(const Vector&p, const double t)
{
   int dim = p.Size();
   
   double x = p(0);
   double y = (dim > 1) ? p(1) : 0.0;
   double z = (dim==3) ? p(2) : 0.0;

   return t*t*(-4 * (x*x - x) * sin(PI * y));
}
*/
} // namespace mfem
