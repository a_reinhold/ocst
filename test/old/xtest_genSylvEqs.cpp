//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>
#include <chrono> 

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   bool paraview = false;
   int refinement_space = 2;
   int order_space = 1;
   int nof_timesteps = 11;


   bool static_cond = false;
   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of refinements to perform on the spatial mesh");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
   	          "Number of timesteps K");
   args.AddOption(&paraview, "-paraview", "--paraview", 
                  "-no-paraview", "--no-paraview",
                  "Save files for paraview Visualization");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();

   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);

   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);

   
   // parabolic operator
   FiniteElementSpace fes_space(&mesh_space, &fec_space);
   ocst::SpaceMatrices space_mat(fes_space);
   FunctionCoefficient robin_lhs(muFunc);
   space_mat.SetBLF(robin_lhs);
   space_mat.Assemble();

   ocst::TimeMatrices time_mat(fes_time_trial, fes_time_test); 
   time_mat.Assemble();
   ocst::SpaceTimeParabolicOperator parabolic_operator(time_mat.C_mixed(), time_mat.M_mixed(),
						       space_mat.M(), space_mat.A());

   parabolic_operator.AssembleMonolithic(); 
   parabolic_operator.AssembleGeneralSylvester(); 
   
   
   // Control 
   ocst::Control u(fes_time_test, fes_space);
   
   u.SetM_time();
   u.SetM_space();
   FunctionCoefficient u_init(fFunc);
   u.SetInitialValue(u_init); 

   // State
   ocst::State y_direct(fes_time_trial, fes_space);
   y_direct.SetM_time(); 
   y_direct.SetM_space();
   
   ocst::State y_gmres(y_direct); 



   mfem::FunctionCoefficient robin_rhs(etaFunc);
   //mfem::ConstantCoefficient y_0(0.0);
   mfem::FunctionCoefficient y_0( [] (const Vector& x) {
					  return x(0)* x(1);} );
   ocst::PrimalRHS primal_rhs(fes_time_trial.GetVSize(), fes_space.GetVSize());
   primal_rhs.SetConstantPart(fes_time_trial, fes_space, robin_rhs, y_0,
                              time_mat.M_mixed());

   
   ocst::AdjointRHS adjoint_rhs(fes_time_trial.GetVSize(), fes_space.GetVSize());
   mfem::FunctionCoefficient y_d([] (const Vector& x){
                                  return -x(0)*x(1)+0.125;});
   adjoint_rhs.SetConstantPart(fes_space, y_d, y_direct.M_space());
   
   ocst::SpaceTimeDirectSolver direct_solver(parabolic_operator, 
                                             primal_rhs, adjoint_rhs);



  //Init time variables
  using Clock =std::chrono::high_resolution_clock; 
  auto start = Clock::now();
  auto end =   Clock::now();

  std::chrono::duration<double, std::milli> elapsed_time ;
   
   // Matrix Vector Product 
   ocst::State y_in(y_direct);
   ocst::State y_out(y_direct); 

   y_in.Mat() = 42.0;
   
   std::cout << endl << "----- Matrix Vector Multiplication------- " 
             << endl << endl; 

   parabolic_operator.SetModeMonolithic();
   start = Clock::now();
   parabolic_operator.MultTranspose(y_in.Vec(), y_out.Vec()); 
   end = Clock::now(); elapsed_time = end - start; 

   std::cout << ">>||y_mono||^2 = " << y_out.SquaredInnerProductNorm() << std::endl; 
   std::cout << ">> MV product calculated in  " << elapsed_time.count() << std::endl; 

   double* buffer = new double[parabolic_operator.Height()];
   parabolic_operator.GeneralSylvesterOp().SetBuffer(buffer);
   parabolic_operator.SetModeKronecker();
   start = Clock::now();
   parabolic_operator.MultTranspose(y_in.Vec(), y_out.Vec()); 
   end = Clock::now(); elapsed_time = end - start; 
   std::cout << "||y_sylv||^2 = " << y_out.SquaredInnerProductNorm() << std::endl; 
   std::cout << ">> MV product calculated in  " << elapsed_time.count() << std::endl; 


   std::cout << endl << "----- SOLVE Problem ------- " << endl << endl; 
  
   //reference solution direct
   start = Clock::now();
   direct_solver.SetOperator();
   direct_solver.SolvePrimal(u, y_direct);
   end = Clock::now(); elapsed_time = end - start; 
   std::cout << ">> ||y_direct||^2 = " << y_direct.SquaredInnerProductNorm() << std::endl; 
   std::cout << ">> factored and solved in in  " << elapsed_time.count() << std::endl; 
   int max_it = 1000;
   mfem::GMRESSolver gmres; 
   gmres.SetKDim(max_it);
   gmres.SetOperator(parabolic_operator);
   gmres.SetMaxIter(max_it);
   gmres.SetPrintLevel(2); 
   gmres.SetRelTol(1E-6);
   gmres.SetAbsTol(1E-8); 

 
   parabolic_operator.SetModeKronecker();
   //Solve with GMRES 
   primal_rhs.Assemble(u);

   start = Clock::now();
   gmres.Mult(primal_rhs.Vec(), y_gmres.Vec()); 
   end = Clock::now(); elapsed_time = end - start; 
   std::cout << ">> ||y_gmres||^2 = " << y_gmres.SquaredInnerProductNorm() << std::endl; 
   std::cout << ">> solved in  " << elapsed_time.count() << std::endl; 
   delete[]  buffer; 
   if (paraview){
      pd = new mfem::ParaViewDataCollection("xtest3", &mesh_space);
      pd->SetPrefixPath("paraview");
      pd->SetLevelsOfDetail(order_space);
      pd->SetDataFormat(VTKFormat::BINARY);
      pd->SetHighOrderOutput(true);
      y_direct.ExportToParaview(*pd, "y_direct", fes_time_trial, fes_space);
      delete pd;
   }
   return 0; 
}
