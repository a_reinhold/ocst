//                                MFEM example spaceTime
//
//
//

#include "../data/bc/symmetric_1D.hpp"
#include "../data/bc/case_5.hpp"

#include <mfem.hpp>
#include <ocst.hpp>

#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   int nodes_space= 30;
   int order_space = 1;
   int nof_timesteps = 21;
   int levels_space = 2;
   bool type1 = true;
   bool visualization = true;

   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   args.AddOption(&visualization, "-visualization", "--visualization", 
                  "-no-visualization", "--no-visualization",
                  "Save files for visualization");
   args.AddOption(&type1, "-t1", "--type1", "-t2", "--type2",
                  "type of the time discretization");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   args.AddOption(&nodes_space, "-ns", "--nodes_space", 
                  "Number of initial refinements to"
		  " perform on the spatial mesh");
   args.AddOption(&levels_space, "-ls", "--levels_space",
                  "levels for MG space dimension");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
                  "levels for MG time dimension");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   device.Print();
   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);
   Mesh mesh_time_ref(mesh_time);
   mesh_time_ref.UniformRefinement();

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test((type1) ? &mesh_time : &mesh_time_ref, 
                                   &fec_time_test);
   

   Mesh mesh_space(nodes_space,1.0); 
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);

   //MG Prep
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(fes_time_trial, 1,
                                                           fes_space,
							   levels_space);

   if (type1){
      cout 
	 <<"                                  TYPE 1"
	 <<"                                  " << endl;
   } else {
      cout 
	 <<"                                  TYPE 2"
	 <<"                                  " << endl;
   }

   ocst::TimeMatrices time_mats(fes_time_trial, fes_time_test, type1);
   Array<ocst::SpaceMatrices*> space_mats;
   Array<SparseMatrix*> A_space; 
   Array<SparseMatrix*> M_space; 

   mfem::FiniteElementSpaceHierarchy& hierarchy_space = 
       st_hierarchy.FESHierarchySpace();
   space_mats.Append(
      new ocst::SpaceMatrices(hierarchy_space.GetFESpaceAtLevel(0)));
   FunctionCoefficient robin_lhs(xi_func);
   ConstantCoefficient one(1.0); 
   space_mats[0]->SetBLF(robin_lhs); 
   space_mats[0]->Assemble(); 

   A_space.Append(&space_mats[0]->A());
   M_space.Append(&space_mats[0]->M());
   for (int i=1; i<levels_space; ++i){
      space_mats.Append(new ocst::SpaceMatrices(*space_mats[i-1],
                                         hierarchy_space.GetFESpaceAtLevel(i)));
      space_mats[i]->Assemble();
      A_space.Append(&space_mats[i]->A());
      M_space.Append(&space_mats[i]->M());
   }
   int dof_time_test = time_mats.M_test().Height()+ 1;
   int dof_time_trial= time_mats.M_trial().Height();
   int dof_space = space_mats.Last()->A().Height();
   cout <<" n = "<< dof_space << endl;
   cout <<" K_test = "<< dof_time_test << endl;
   cout <<" K_trial = "<< dof_time_trial<< endl;

   double* buffer = new double[dof_time_test*dof_space];
   ocst::GeometricMultigrid prec_A(hierarchy_space, A_space, true);
   ocst::GeometricMultigrid prec_M(hierarchy_space, M_space, true);

   ocst::SolveMGPCG solve_A(prec_A);
   ocst::SolveMGPCG solve_M(prec_M);
   
   // Coefficients
   ocst::Control u(fes_time_test, hierarchy_space.GetFinestFESpace());
   u.SetM_time(time_mats.M_test());
   u.SetM_space(*M_space.Last());
   FunctionCoefficient u_init(f_func);
   u.SetValue(u_init); 

   ocst::State y(fes_time_trial, hierarchy_space.GetFinestFESpace());
   y.SetM_time(time_mats.M_trial());
   y.SetM_space(*M_space.Last());
   
   ocst::State y_d(y); 
   ocst::AdjointState p(fes_time_test, hierarchy_space.GetFinestFESpace());
   
   //RHS
   FunctionCoefficient robin_rhs(eta_func);
   FunctionCoefficient y_exact(y_analytic);
   y_exact.SetTime(0.0);

   y_d.SetValue(y_exact);

   GridFunction y_init(&hierarchy_space.GetFinestFESpace());

   ocst::PrimalRHS prhs(dof_time_test, dof_space);
   prhs.SetConstantPart(fes_time_trial, hierarchy_space.GetFinestFESpace(), 
                        robin_rhs, y_exact, time_mats.M_mixed()); 
   
   ocst::AdjointRHS arhs(dof_time_trial, dof_space,1.0,0.0);
   Vector y_d_T;
   y_d.Mat().GetColumnReference(0, y_d_T);
   arhs.SetConstantPart(y_d, y_d_T, y_d.M_time(), y_d.M_space());

   ocst::NormOperatorTrial* M_norm = nullptr;
   ocst::NormOperatorTest* N_norm = nullptr;
   ocst::SpaceTimeParabolicOperator* B= nullptr;
   ocst::SpaceTimeSolver* solver = nullptr;
   if (type1){
      solver = new ocst::IterativeType1Solver(prhs, arhs,
                                              M_space, A_space,
                                              solve_M, hierarchy_space, 
					      nof_timesteps-1, 1.0);
   }else {
   
      M_norm = new ocst::NormOperatorTrial(time_mats.M_trial(), 
                                           time_mats.A_trial(),
        			           *M_space.Last(), *A_space.Last(),
					   solve_A, false, buffer);

      N_norm = new ocst::NormOperatorTest(time_mats.M_test(), 
                                          *M_space.Last(), *A_space.Last(), 
					  buffer);
      M_norm->SetInverseOperator(M_space, A_space, hierarchy_space);
      N_norm->SetInverseOperator(solve_A, solve_M); 

      B = new ocst::SpaceTimeParabolicOperator(time_mats.C_mixed(), 
                                               time_mats.M_mixed(),
					       *M_space.Last(), 
					       *A_space.Last());
      B->AssembleGeneralSylvester(); 
      solver = new ocst::LSQRSolver(*B, prhs, arhs, 
				    *M_norm->GetInverseOperator(), 
				    *N_norm->GetInverseOperator());
      static_cast<ocst::LSQRSolver*>(solver)->SetSolverParameters(1e-5, 12, 2); 
   }


   
   cout << "Solve Primal" << endl;
   solver->SolvePrimal(u,y);
   cout << "Solve Adjoint" << endl;
   solver->SolveAdjoint(y,p);
  
   if (visualization){
      std::ofstream y_file ("y_surfdata.txt", std::ofstream::binary);
      ocst::Export1DSurfData(fes_time_trial, 
                             hierarchy_space.GetFinestFESpace(), 
			     y.Mat(), y_file, "y");
      std::ofstream u_file ("u_surfdata.txt", std::ofstream::binary);
      ocst::Export1DSurfData(fes_time_test, 
                             hierarchy_space.GetFinestFESpace(), 
			     u.Mat(), u_file, "u");
      std::ofstream y_d_file ("y_d_surfdata.txt", std::ofstream::binary);
      ocst::Export1DSurfData(fes_time_trial, 
                             hierarchy_space.GetFinestFESpace(), 
			     y_d.Mat(), y_d_file, "y_d");
      std::ofstream p_file ("p_surfdata.txt", std::ofstream::binary);
      ocst::Export1DSurfData(fes_time_test, 
                             hierarchy_space.GetFinestFESpace(), 
			     p.AdjointState_I(), p_file, "p");

   }
   y.Mat() -= y_d.Mat();
   cout << "||y_h - y_ref||^2_{L_2(I,Omega)} = " << y.SquaredInnerProductNorm()
        << endl; 

   if (visualization){
      std::ofstream error_file("error_y.txt", std::ofstream::binary);
      ocst::Export1DSurfData(fes_time_trial, 
                             hierarchy_space.GetFinestFESpace(), 
			     y.Mat(), error_file, "error y-y_d");
                               
   }
   if (M_norm) delete M_norm;
   if (N_norm) delete N_norm;
   if (B) delete B; 
   delete solver;
   for (int i=0; i<levels_space; ++i){
      delete space_mats[i];
   }
   delete[] buffer;
   return 0;
}
