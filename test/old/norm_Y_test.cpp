//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include <ocst.hpp>
#include <postProcessing.hpp>

#include <iostream>

using namespace std;
using namespace mfem;
using OperatorType = ocst::GeometricMultigrid::OperatorType;

mfem::SparseMatrix mat_time(FiniteElementSpace& fes_time, bool mass)
{
   BilinearForm blf{&fes_time};
   ConstantCoefficient one{1.0};
   
   if (mass) blf.AddDomainIntegrator(new MassIntegrator(one));
   else blf.AddDomainIntegrator(new DiffusionIntegrator{one});
   blf.Assemble();
   blf.Finalize();
   return blf.SpMat();
}

int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/L_shaped_2D.mesh";
   bool paraview = false;
   int refinement_space = 2;
   int order_space = 1;
   int nof_timesteps = 11;
   int levels_space = 2;
   int levels_time = 3;
   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   args.AddOption(&levels_space, "-ls", "--levels_space",
                  "levels for MG space dimension");
   args.AddOption(&levels_time, "-lt", "--levels_time",
                  "levels for MG time dimension");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
                  "number of time steps");
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   Device device(device_config);
   // ocst::BenchTiming timing{0};
   
   Mesh mesh_time= Mesh::MakeCartesian1D(nof_timesteps-1,1.0);

   H1_FECollection fec_time(1, 1);
   FiniteElementSpace fes_time(&mesh_time, &fec_time);

   Mesh mesh_space(mesh_file_space,1,2);
   for (std::size_t i= 0; i<refinement_space; ++i){
      mesh_space.UniformRefinement();
   }
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);


   //MG Prep
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(fes_time, 
							   levels_time,
                                                           fes_space,
							   levels_space);

   FiniteElementSpaceHierarchy& t_hierarchy = st_hierarchy.FESHierarchyTime();
   mfem::Array<SparseMatrix*> M_time{levels_time};
   mfem::Array<SparseMatrix*> A_time{levels_time};
   for (int i=0; i<levels_time; ++i){
      A_time[i] = new SparseMatrix(mat_time(t_hierarchy.GetFESpaceAtLevel(i), false));
      M_time[i] = new SparseMatrix(mat_time(t_hierarchy.GetFESpaceAtLevel(i), true));
   }

   ConstantCoefficient one{1.0};
   FiniteElementSpaceHierarchy& s_hierarchy = st_hierarchy.FESHierarchySpace();
   mfem::Array<ocst::SpaceMatrices*> space_mat{levels_space};
   mfem::Array<SparseMatrix*> M_space{levels_space};
   mfem::Array<SparseMatrix*> A_space{levels_space};
   mfem::Array<UMFPackSolver*> solve_A{levels_space};
   for (int i=0; i<levels_space; ++i){
      space_mat[i] = new ocst::SpaceMatrices(s_hierarchy.GetFESpaceAtLevel(i));
      space_mat[i]->SetBLF(one);
      space_mat[i]->Assemble();
      M_space[i] = &space_mat[i]->M();
      A_space[i] = &space_mat[i]->A();
      solve_A[i] = new UMFPackSolver(*A_space[i]);
   }

   // ocst::GeometricMultigrid prec_A{s_hierarchy, A_space, OperatorType::SPARSE};
   // ocst::SolveMGPCG solve_A{prec_A};
   // solve_A.SetSolverParameters(1e-8, 1e-9, 30, 0);

   // ocst::NormOperatorTrial norm_Y{M_time, A_time, *M_space.Last(), *A_space.Last(), solve_A, false};
   int mg_levels = (levels_time > levels_space) ? levels_time : levels_space;

   mfem::Array<ocst::NormOperatorTrial*> norm_Y_array{mg_levels};
   int diff_ts_levels = levels_space- levels_time;
   int current_level_space=0;
   int current_level_time=0;
   for (int l=0; l<mg_levels; ++l){
      norm_Y_array[l] = new ocst::NormOperatorTrial(*M_time[current_level_time],
						    *A_time[current_level_time],
						    *M_space[current_level_space],
						    *A_space[current_level_space],
						    *solve_A[current_level_space],
						    false);
      if (diff_ts_levels<0){
	 // coarsest space
	 diff_ts_levels +=1;
	 current_level_time +=1;
	 continue;
      }
      if (diff_ts_levels>0){
	 //coarsest time
	 current_level_space +=1;
	 diff_ts_levels -=1;
	 continue;
      }
      if (diff_ts_levels==0){
	 current_level_space +=1;
	 current_level_time +=1;
	 continue;
      }
   }

    ocst::NormOperatorTrial& norm_Y = *norm_Y_array.Last();
   mfem::Vector x_ref{norm_Y.Width()};
   mfem::Vector y{norm_Y.Height()};
   mfem::Vector x{x_ref};

   // x_ref.Randomize();
   x_ref = 10.0;
   x = 0.0;
   norm_Y.Mult(x_ref,y);

   norm_Y.SetInverseOperator(M_space, A_space, s_hierarchy);

   norm_Y.MultInverse(y, x);

   x -= x_ref;
   cout << "error helmholtz" << x.Norml2() << endl;

   double* buffer = new double[A_space.Last()->Height() * A_time.Last()->Height()];
   st_hierarchy.SetBuffer(buffer);
   ocst::GeometricMultigrid prec_norm_Y{st_hierarchy, norm_Y_array, OperatorType::MATRIX_FREE};
   ocst::SolveMGPCG solve_NormY{prec_norm_Y};
   solve_NormY.SetSolverParameters(1e-8, 1e-7, 50, 1);
   prec_norm_Y.SetCoarsestParameters(1e-12, 1e-12,150);
   // CGSolver solve_NormY;
   // mfem::Vector diag_norm_Y{norm_Y.Height()};
   // diag_norm_Y =1.0;
   // mfem::Array<int> ess_tdof;
   // solve_NormY.SetMaxIter(300);
   // solve_NormY.SetAbsTol(1e-8);
   // solve_NormY.SetRelTol(1e-7);
   // solve_NormY.SetPrintLevel(1);
   // solve_NormY.SetOperator(norm_Y);
   x = 0.0;
   solve_NormY.Mult(y, x);
   x -= x_ref;
   cout << "error cg" << x.Norml2() << endl;


   delete[] buffer;
   for (int i=0; i<levels_space; ++i){
      delete space_mat[i];
      delete solve_A[i];
   }
   for (int i=0; i<levels_time; ++i){
      delete A_time[i];
      delete M_time[i];
   }
   for (int i=0; i<mg_levels; ++i){
      delete norm_Y_array[i];
   }
   return 0;
}
