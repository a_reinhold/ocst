//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   bool paraview = false;
   int refinement_coarse_space = 3;
   int refinement_space = 4; 
   int order_space = 1;
   int nof_timesteps = 11; //coarsest level
   int refinement_time = 4;


   bool static_cond = false;
   const char *device_config = "cpu";
   ParaViewDataCollection *pd = NULL;

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
	          "Number of refinements to perform on the spatial mesh");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
   	          "Number of timesteps for the coarsest level");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);

   // Setup the device
   //Device device(device_config);

   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);

   Mesh mesh_space(mesh_file_space,1,1);
   for (std::size_t i= 0; i<refinement_coarse_space; ++i){
      mesh_space.UniformRefinement();
   }

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test(&mesh_time, &fec_time_test);

   FiniteElementSpace fes_space(&mesh_space, &fec_space);

   int mg_levels = 3;
/*
   mfem::FiniteElementSpaceHierarchy hierarchy_time_trial(
                                   &mesh_time, &fes_time_trial, false, false);
   mfem::FiniteElementSpaceHierarchy hierarchy_time_test(
                                   &mesh_time, &fes_time_test, false, false);
   mfem::FiniteElementSpaceHierarchy hierarchy_space(
                                   &mesh_space, &fes_space, false, false);


   for (int i=0; i<mg_levels; ++i) {
      hierarchy_time_trial.AddUniformlyRefinedLevel();
      hierarchy_time_test.AddUniformlyRefinedLevel();
      hierarchy_space.AddUniformlyRefinedLevel();
   }
   mfem::Operator* prolong_time_trial= 
                              hierarchy_time_trial.GetProlongationAtLevel(0);
   cout << "Type of Prolongation =" << prolong_time_trial->GetType() << endl; 
*/

   mfem::Mesh mesh_time_ref(mesh_time);
   mesh_time_ref.UniformRefinement();
   mfem::FiniteElementSpace fes_time_trial_ref(&mesh_time_ref, &fec_time_trial);
   mfem::FiniteElementSpace fes_time_test_ref(&mesh_time_ref, &fec_time_test);
   
   mfem::Mesh mesh_space_ref(mesh_space);
   mesh_space_ref.UniformRefinement();
   mfem::FiniteElementSpace fes_space_ref(&mesh_space_ref, &fec_space);


/*
   mfem::OperatorHandle handle_trial(mfem::Operator::Type::MFEM_SPARSEMAT) ;
   fes_time_trial_ref.GetTransferOperator(fes_time_trial, handle_trial); 
   mfem::SparseMatrix* prolong_time_trial = static_cast<mfem::SparseMatrix*>(handle_trial.Ptr());
   cout << "Prolongation Time Trial: " << endl << ">> Dimensions ="
        << prolong_time_trial->Height() << " x " << prolong_time_trial->Width() << endl; 
  // prolong_time_trial->Print();
   cout << endl;
   mfem::FiniteElementSpace fes_time_test_ref(&mesh_time_ref, &fec_time_test);
   mfem::OperatorHandle handle_test(mfem::Operator::Type::MFEM_SPARSEMAT) ;
   fes_time_test_ref.GetTransferOperator(fes_time_test, handle_test); 
   mfem::SparseMatrix* prolong_time_test = static_cast<mfem::SparseMatrix*>(handle_test.Ptr());
   cout << "Prolongation Time Test: " << endl << ">> Dimensions ="
        << prolong_time_test->Height() << " x " << prolong_time_test->Width() << endl; 
   //prolong_time_test->Print();
   cout << endl;
   mfem::OperatorHandle handle_space(mfem::Operator::Type::MFEM_SPARSEMAT) ;
   fes_space_ref.GetTransferOperator(fes_space, handle_space); 

   mfem::SparseMatrix* prolong_space = static_cast<mfem::SparseMatrix*>(handle_space.Ptr());
   cout << "Prolongation Space: " << endl << ">> Dimensions ="
        << prolong_space->Height() << " x " << prolong_space->Width() << endl; 
   //prolong_space->Print();
   cout << endl;
   

   //mfem::FunctionCoefficient identity([](const mfem::Vector& x){return x(0)*x(0) -0.7773*x(0) + 0.443;});
   //mfem::FunctionCoefficient time_func([](const mfem::Vector& x){return x(0);});
   mfem::FunctionCoefficient time_func([](const mfem::Vector& x){return 1.0;});

   mfem::GridFunction time_test_coarse(&fes_time_test);
   time_test_coarse.ProjectCoefficient(time_func);
   mfem::GridFunction time_test_fine(&fes_time_test_ref);
   time_test_fine.ProjectCoefficient(time_func);

   mfem::Vector prolongated_test(fes_time_test_ref.GetVSize());
   prolong_time_test->Mult(time_test_coarse, prolongated_test);
   mfem::Vector restricted_test(fes_time_test.GetVSize());
   prolong_time_test->MultTranspose(time_test_fine, restricted_test);
   restricted_test *=0.5; 

   cout << endl <<  "Prolongation Time Test" << endl; 
   cout << "coarse Vector" << endl ;
   time_test_coarse.Print();
   cout << "fine Vector" << endl ;
   time_test_fine.Print();
   cout << "prolongated Vector" << endl ;
   prolongated_test.Print();
   cout << "restricted Vector" << endl ;
   restricted_test.Print();


   mfem::GridFunction time_trial_coarse(&fes_time_trial);
   time_trial_coarse.ProjectCoefficient(time_func);
   mfem::GridFunction time_trial_fine(&fes_time_trial_ref);
   time_trial_fine.ProjectCoefficient(time_func);

   mfem::Vector prolongated_trial(fes_time_trial_ref.GetVSize());
   prolong_time_trial->Mult(time_trial_coarse, prolongated_trial);
   mfem::Vector restricted_trial(fes_time_trial.GetVSize());
   prolong_time_trial->MultTranspose(time_trial_fine, restricted_trial);
   restricted_trial *=0.5; 

   cout <<endl << "Prolongation Test" << endl; 
   cout << "coarse Vector" << endl ;
   time_trial_coarse.Print();
   cout << "fine Vector" << endl ;
   time_trial_fine.Print();
   cout << "prolongated Vector" << endl ;
   prolongated_trial.Print();
   cout << "restricted Vector" << endl ;
   restricted_trial.Print();



   //mfem::FunctionCoefficient space_func([](const mfem::Vector& x)
   //                                             {return x(0)*x(1);});
   mfem::FunctionCoefficient space_func([](const mfem::Vector& x)
                                                {return 1.0;});
   mfem::GridFunction space_coarse(&fes_space);
   space_coarse.ProjectCoefficient(space_func);
   mfem::GridFunction space_fine(&fes_space_ref);
   space_fine.ProjectCoefficient(space_func);

   mfem::Vector prolongated_space(fes_space_ref.GetVSize());
   prolong_space->Mult(space_coarse, prolongated_space);
   mfem::Vector restricted_space(fes_space.GetVSize());
   prolong_space->MultTranspose(space_fine, restricted_space);
   double dim = fes_space.GetMesh()->Dimension(); 
   restricted_space *= 1.0/(2*dim);

   cout << endl << "Prolongation Space" << endl; 
   cout << "coarse Vector" << endl ;
   space_coarse.Print();
   cout << "fine Vector" << endl ;
   space_fine.Print();
   cout << "prolongated Vector" << endl ;
   prolongated_space.Print();
   cout << "restricted Vector" << endl ;
   restricted_space.Print();
   */

   /* -------------------TEST ST Prolongation ------------------------------*/
   // CASE SPACE AND TIME TRIAL  
   double* buffer = new double[fes_time_trial_ref.GetVSize()  
                                          *  fes_space_ref.GetVSize()];
  /* {
   ocst::SpaceTimeProlongation prol(fes_time_trial, fes_time_trial_ref,
				    fes_time_test, fes_time_test_ref,
				    fes_space, fes_space_ref, buffer); 
   prol.SetTargetTrial(); 

   ocst::Control coarse(fes_time_trial, fes_space); 
   ocst::Control fine(fes_time_trial_ref, fes_space_ref); 

   mfem::FunctionCoefficient init([](const mfem::Vector& x, const double t){
				      // return 2.0;});
				       return x(0)*x(1) - t* x(0)*0.33 + t*x(1)*0.6678 ;});
   coarse.SetInitialValue(init);

   fine.SetInitialValue(init);

   mfem::ParaViewDataCollection pd_coarse("coarse", &mesh_space);
   mfem::ParaViewDataCollection pd_fine("fine", &mesh_space_ref);
   pd_coarse.SetPrefixPath("paraview");
   pd_coarse.SetDataFormat(VTKFormat::BINARY);
   pd_fine.SetPrefixPath("paraview");
   pd_fine.SetDataFormat(VTKFormat::BINARY);
   mfem::ParaViewDataCollection pd_restricted("restricted", &mesh_space);
   mfem::ParaViewDataCollection pd_prolongated("prolongated", &mesh_space_ref);
   pd_restricted.SetPrefixPath("paraview");
   pd_restricted.SetDataFormat(VTKFormat::BINARY);
   pd_prolongated.SetPrefixPath("paraview");
   pd_prolongated.SetDataFormat(VTKFormat::BINARY);

   ocst::Control prolongated(fine);
   ocst::Control restricted(coarse); 
   
   prol.Prolongation(coarse, prolongated, false);
   prol.Restriction(fine, restricted);
   coarse.ExportToParaview(pd_coarse, "original_coarse", fes_time_trial, fes_space); 
   restricted.ExportToParaview(pd_restricted, "restricted", fes_time_trial, fes_space); 
   fine.ExportToParaview(pd_fine, "original_fine", fes_time_trial_ref, fes_space_ref); 
   prolongated.ExportToParaview(pd_prolongated, "prolongated", fes_time_trial_ref, fes_space_ref); 
   }*/
   /*{
   ocst::SpaceTimeProlongation prol(fes_time_trial, fes_time_trial_ref,
				    fes_time_test, fes_time_test_ref,
				    buffer); 
   prol.SetTargetTest(); 

   ocst::Control coarse(fes_time_trial, fes_space_ref); 
   ocst::Control fine(fes_time_trial_ref, fes_space_ref); 

   mfem::FunctionCoefficient init([](const mfem::Vector& x, const double t){
				      // return 2.0;});
				       return x(0)*x(1) - t* x(0)*0.33 + t*x(1)*0.6678 ;});
   coarse.SetInitialValue(init);

   fine.SetInitialValue(init);

   mfem::ParaViewDataCollection pd_coarse("coarse", &mesh_space_ref);
   mfem::ParaViewDataCollection pd_fine("fine", &mesh_space_ref);
   pd_coarse.SetPrefixPath("paraview");
   pd_coarse.SetDataFormat(VTKFormat::BINARY);
   pd_fine.SetPrefixPath("paraview");
   pd_fine.SetDataFormat(VTKFormat::BINARY);
   mfem::ParaViewDataCollection pd_restricted("restricted", &mesh_space_ref);
   mfem::ParaViewDataCollection pd_prolongated("prolongated", &mesh_space_ref);
   pd_restricted.SetPrefixPath("paraview");
   pd_restricted.SetDataFormat(VTKFormat::BINARY);
   pd_prolongated.SetPrefixPath("paraview");
   pd_prolongated.SetDataFormat(VTKFormat::BINARY);

   ocst::Control prolongated(fine);
   ocst::Control restricted(coarse); 
   
   prol.Prolongation(coarse, prolongated, false);
   prol.Restriction(fine, restricted);
   coarse.ExportToParaview(pd_coarse, "original_coarse", fes_time_trial, fes_space_ref); 
   restricted.ExportToParaview(pd_restricted, "restricted", fes_time_trial, fes_space_ref); 
   fine.ExportToParaview(pd_fine, "original_fine", fes_time_trial_ref, fes_space_ref); 
   prolongated.ExportToParaview(pd_prolongated, "prolongated", fes_time_trial_ref, fes_space_ref); 
   }*/
   {
   ocst::SpaceTimeProlongation prol(fes_space, fes_space_ref,
				    buffer); 
   prol.SetTargetTest(); 

   ocst::Control coarse(fes_time_trial, fes_space); 
   ocst::Control fine(fes_time_trial, fes_space_ref); 

   mfem::FunctionCoefficient init([](const mfem::Vector& x, const double t){
				      // return 2.0;});
				       return x(0)*x(1) - t* x(0)*0.33 + t*x(1)*0.6678 ;});
   coarse.SetInitialValue(init);

   fine.SetInitialValue(init);

   mfem::ParaViewDataCollection pd_coarse("coarse", &mesh_space);
   mfem::ParaViewDataCollection pd_fine("fine", &mesh_space_ref);
   pd_coarse.SetPrefixPath("paraview");
   pd_coarse.SetDataFormat(VTKFormat::BINARY);
   pd_fine.SetPrefixPath("paraview");
   pd_fine.SetDataFormat(VTKFormat::BINARY);
   mfem::ParaViewDataCollection pd_restricted("restricted", &mesh_space);
   mfem::ParaViewDataCollection pd_prolongated("prolongated", &mesh_space_ref);
   pd_restricted.SetPrefixPath("paraview");
   pd_restricted.SetDataFormat(VTKFormat::BINARY);
   pd_prolongated.SetPrefixPath("paraview");
   pd_prolongated.SetDataFormat(VTKFormat::BINARY);

   ocst::Control prolongated(fine);
   ocst::Control restricted(coarse); 
   
   prol.Prolongation(coarse, prolongated, false);
   prol.Restriction(fine, restricted);
   coarse.ExportToParaview(pd_coarse, "original_coarse", fes_time_trial, fes_space); 
   restricted.ExportToParaview(pd_restricted, "restricted", fes_time_trial, fes_space); 
   fine.ExportToParaview(pd_fine, "original_fine", fes_time_trial, fes_space_ref); 
   prolongated.ExportToParaview(pd_prolongated, "prolongated", fes_time_trial, fes_space_ref); 
   } 
   // parabolic operator
   /*
   ocst::SpaceTimeParabolicOperator parabolic_op(fes_time_trial, 
						         fes_time_test,
						         fes_space);

   FunctionCoefficient robin_lhs(muFunc);
   parabolic_op.SetOperator(robin_lhs);
   parabolic_op.AssembleOperator();
   parabolic_op.AssembleGeneralSylvester(); 
   
   
   // Control 
   ocst::Control u(fes_time_test, fes_space);
   
   u.SetM_time();
   u.SetM_space(parabolic_op.M_space());
   FunctionCoefficient u_init(fFunc);
   u.SetInitialValue(u_init); 

   // State
   ocst::State y(fes_time_trial, fes_space);
   y.SetM_time(); 
   y.SetM_space(parabolic_op.M_space());

   ocst::AdjointState p(fes_time_test, fes_space);


   mfem::FunctionCoefficient robin_rhs(etaFunc);
   mfem::ConstantCoefficient y_0(0.0);
   ocst::PrimalRHS primal_rhs(fes_time_trial.GetVSize(), fes_space.GetVSize());
   primal_rhs.SetConstantPart(fes_time_trial, fes_space, robin_rhs, y_0,
                              parabolic_op.M_time_mixed());

   ocst::AdjointRHS adjoint_rhs(fes_time_trial.GetVSize(), fes_space.GetVSize());
   mfem::FunctionCoefficient y_d([] (const Vector& x){
                                  return -x(0)*x(1)+0.125;});
   adjoint_rhs.SetConstantPart(fes_space, y_d, y.M_space());

   
   */
   delete[] buffer; 
   return 0; 
}
