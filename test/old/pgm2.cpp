//                                MFEM example spaceTime
//
//
//

#include <mfem.hpp>
#include "parabolicBC.hpp"
#include <ocst.hpp>


#include <fstream>
#include <iostream>

using namespace std;
using namespace mfem;


int main(int argc, char *argv[])
{
   // Parse command-line options.
   const char *mesh_file_space = "../data/unitSquare_2D.mesh";
   int refinement_space = 1;
   int order_space = 1;
   int nof_timesteps = 11;
   int levels_space = 2;
   bool type1 = true;
   bool conform_time=true;
   bool conform_space=true; 
   bool paraview = false;
   const char *device_config = "cpu";

   OptionsParser args(argc, argv);
   args.AddOption(&mesh_file_space, "-m", "--mesh",
                  "Mesh file to use.");
   args.AddOption(&type1, "-type1", "--type1", "-type2", "--type2",
                  "type of the time discretization");
   args.AddOption(&conform_time, "-conform_time", "--conform_time", 
                  "-non_conform_time", "--non_conform_time",
                  "discretization of the control");
   args.AddOption(&conform_space, "-conform_space", "--conform_space", 
                  "-non_conform_space", "--non_conform_space",
                  "discretization of the control");
   args.AddOption(&order_space, "-o", "--order",
                  "Finite element order (polynomial degree) or -1 for"
                  " isoparametric space.");
   args.AddOption(&refinement_space, "-rs", "--refinement_space", 
                  "Number of initial refinements to"
		  " perform on the spatial mesh");
   args.AddOption(&levels_space, "-ls", "--levels_space",
                  "levels for MG space dimension");
   args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
                  "Number of timesteps");
   args.AddOption(&paraview, "-pv", "--paraview", 
                  "-no-paraview", "--no-paraview",
		  "save files in paraview data collection");
   
   args.Parse();
   if (!args.Good())
   {
      args.PrintUsage(cout);
      return 1;
   }
   args.PrintOptions(cout);
   ParaViewDataCollection* pd;

   // Setup the device
   Device device(device_config);
   device.Print();
   // Meshes, FE_Collections and spaces

   Mesh mesh_time(nof_timesteps-1,1.0);
   Mesh mesh_time_ref(mesh_time);
   mesh_time_ref.UniformRefinement();

   H1_FECollection fec_time_trial(1,1);
   L2_FECollection fec_time_test(0,1);

   L2_FECollection fec_time_control(3,1);
   

   FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
   FiniteElementSpace fes_time_test_(&mesh_time, &fec_time_test);
   FiniteElementSpace fes_time_test_ref(&mesh_time_ref, &fec_time_test);
   
   FiniteElementSpace* fes_time_test;

   Mesh mesh_space(20,1.0); 
   // Mesh mesh_space(mesh_file_space,1,1);
   // for (std::size_t i= 0; i<refinement_space; ++i){
   //    mesh_space.UniformRefinement();
   // }
   
   H1_FECollection fec_space(order_space, mesh_space.Dimension());
   L2_FECollection fec_space_control(order_space, mesh_space.Dimension());
   FiniteElementSpace fes_space(&mesh_space, &fec_space);


   //MG Prep
   ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(fes_time_trial, 1,
                                                           fes_space,
							   levels_space);


   bool type_;
   if (type1){
      cout 
	 <<"/*--------------------------------------"  
	 <<"--------------------------------------*/"  << endl
	 <<"                                  TYPE 1"
	 <<"                                  " << endl
	 <<"/*--------------------------------------"  
	 <<"--------------------------------------*/"  << endl;
      fes_time_test = &fes_time_test_;

   } else {
      cout 
	 <<"/*--------------------------------------"  
	 <<"--------------------------------------*/"  << endl
	 <<"                                  TYPE 2"
	 <<"                                  " << endl
	 <<"/*--------------------------------------"  
	 <<"--------------------------------------*/"  << endl;
      fes_time_test = &fes_time_test_ref;
   }

   ocst::TimeMatrices time_mats(fes_time_trial, *fes_time_test, type1);
   Array<ocst::SpaceMatrices*> space_mats;
   Array<SparseMatrix*> A_space; 
   Array<SparseMatrix*> M_space; 

   mfem::FiniteElementSpaceHierarchy& hierarchy_space = 
       st_hierarchy.FESHierarchySpace();

   FiniteElementSpace fes_space_control(hierarchy_space.GetFinestFESpace().GetMesh(), 
                                        &fec_space_control);
   FiniteElementSpace fes_time_control(fes_time_test->GetMesh(), 
                                       &fec_time_control);
   space_mats.Append(
      new ocst::SpaceMatrices(hierarchy_space.GetFESpaceAtLevel(0)));
   FunctionCoefficient robin_lhs(xiFunc);
   ConstantCoefficient one(1.0); 
   space_mats[0]->SetBLF(one); 
   space_mats[0]->Assemble(); 

   A_space.Append(&space_mats[0]->A());
   M_space.Append(&space_mats[0]->M());
   for (int i=1; i<levels_space; ++i){
      space_mats.Append(new ocst::SpaceMatrices(*space_mats[i-1],
                                         hierarchy_space.GetFESpaceAtLevel(i)));
      space_mats[i]->Assemble();
      A_space.Append(&space_mats[i]->A());
      M_space.Append(&space_mats[i]->M());
   }
   int dof_time_test = time_mats.M_test().Height()+ 1;
   int dof_time_trial= time_mats.M_trial().Height();
   int dof_space = space_mats.Last()->A().Height();
   cout <<" n = "<< dof_space << endl;
   cout <<" K_test = "<< dof_time_test << endl;
   cout <<" K_trial = "<< dof_time_trial<< endl;

   double* buffer = new double[dof_time_test*dof_space];
   ocst::GeometricMultigrid prec_A(hierarchy_space, A_space, true);
   ocst::GeometricMultigrid prec_M(hierarchy_space, M_space, true);

   ocst::SolveMGPCG solve_A(prec_A);
   ocst::SolveMGPCG solve_M(prec_M);
   
   
   //Operators
   ocst::NormOperatorTrial M_norm(time_mats.M_trial(), time_mats.A_trial(),
        			  *M_space.Last(), *A_space.Last(), solve_A, 
				  false, buffer);
   M_norm.SetInverseOperator(M_space, A_space, hierarchy_space);

   ocst::NormOperatorTest N_norm(time_mats.M_test(), *M_space.Last(), 
                                 *A_space.Last(), buffer);
   N_norm.SetInverseOperator(solve_A, solve_M); 

   ocst::SpaceTimeParabolicOperator B(time_mats.C_mixed(), time_mats.M_mixed(),
                                      *M_space.Last(), *A_space.Last());
   B.AssembleGeneralSylvester(); 

   ocst::OptimalControlProblem ocp(
      fes_time_trial, *fes_time_test, 
      hierarchy_space.GetFinestFESpace(),
      conform_time ? *fes_time_test : fes_time_control, 
      conform_space? hierarchy_space.GetFinestFESpace() : fes_space_control, 
      conform_time, conform_space,
      0.01);

   mfem::FunctionCoefficient y_d_coeff([] (const Vector& x, const double t){
                                               return t*(-x(0)*x(1)+0.125);});
   ocp.SetDesiredState(y_d_coeff);
   ocp.SetControlConstrains(-5.0,5.0);


   ocp.Control().SetM_time();
   ocp.Control().SetM_space();
   ocp.Control().SetM_control_time(*fes_time_test);
   ocp.Control().SetM_control_space(hierarchy_space.GetFinestFESpace());
   //ocp.Control().Vec().Randomize(); 

   ocp.State().SetM_time(time_mats.M_trial());
   ocp.State().SetM_space(*M_space.Last());
   
   //RHS
   FunctionCoefficient robin_rhs(etaFunc);
   ConstantCoefficient y_0(0.3333);
   ocp.SetConstantPartRHS(y_d_coeff,robin_rhs,time_mats.M_mixed());

   //Solver
   ocst::SpaceTimeSolver* solver;
   if (type1){
      solver = new ocst::IterativeType1Solver(ocp.PrimalRHS(), ocp.AdjointRHS(),
                                              M_space, A_space,
                                              solve_M, hierarchy_space, 
					      nof_timesteps-1, 1.0);
   }else {
      solver = new ocst::LSQRSolver(B, ocp.PrimalRHS(), ocp.AdjointRHS(), 
				 *M_norm.GetInverseOperator(), 
				 *N_norm.GetInverseOperator());
   
      static_cast<ocst::LSQRSolver*>(solver)->SetSolverParameters(1e-4, 12); 
   }
   //B.AssembleMonolithic();
   //ocst::SpaceTimeDirectSolver solver(B, prhs, arhs);
   //solver.SetOperator();

   ocst::ProjectedGradientMethod pg(ocp, *solver, 50, 1/ocp.Lambda());//, 
                                    //1e-4, 0.0, 0.0, 1e-4 ); 
   std::cout << "Start PG " << std::endl; 
   pg.Solve();
   std::cout << "Finished PG " << std::endl; 
   pg.PrintResults(); 

   std::vector<int> ind{ 0, 4, 7, 10};
   //ocp.State().Mat().Print();
   std::ofstream y_file ("y_surfdata.txt", std::ofstream::binary);
   ocst::Export1DSurfData(fes_time_trial, 
                          hierarchy_space.GetFinestFESpace(), 
			                 ocp.State().Mat(), y_file, "y");
   std::ofstream u_file ("u_surfdata.txt", std::ofstream::binary);
   ocst::Export1DSurfData(*fes_time_test, 
                          hierarchy_space.GetFinestFESpace(), 
			                 ocp.Control().Mat(), u_file, "u");
   std::ofstream p_file ("p_surfdata.txt", std::ofstream::binary);
   ocst::Export1DSurfData(*fes_time_test, 
                          hierarchy_space.GetFinestFESpace(), 
			                 ocp.AdjointState().AdjointState_I(), p_file, "p");                                                    
   if (paraview){
      pd = new mfem::ParaViewDataCollection("test", 
	                        hierarchy_space.GetFinestFESpace().GetMesh());
      pd->SetPrefixPath("paraview");
      pd->SetLevelsOfDetail(order_space);
      pd->SetDataFormat(VTKFormat::BINARY);
      pd->SetHighOrderOutput(true);
//      ocst::ExportToParaview(pd, "test_1", ocp.State().Mat(), 
//    			      hierarchy_space.GetFinestFESpace());
//      ocst::ExportToParaview(pd, "test_2", ocp.State().Mat(), 
//    			     hierarchy_space.GetFinestFESpace(), 
//			     &fes_time_trial);
      ocst::ExportToParaview(pd, "test_3", ocp.AdjointState().Mat(), 
      		             hierarchy_space.GetFinestFESpace(), nullptr, &ind);
      delete pd;
   }
   for (int i=0; i<levels_space; ++i){
      delete space_mats[i];
   }
   delete solver;
   delete[] buffer;
   return 0;
}
