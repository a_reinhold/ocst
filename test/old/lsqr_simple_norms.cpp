//                                MFEM example spaceTime
//
//
//

// #include "../data/bc/unitSquare_2D.hpp"
#include "../data/bc/L_shaped_2D.hpp"
#include "../data/bc/case_2.hpp"

#include <mfem.hpp>
#include <ocst.hpp>

#include <iostream>
#include <random>

using namespace std;
using namespace mfem;

int main(int argc, char *argv[]) {
  // Parse command-line options.

  bool paraview = false;
  const char *pvname = "test_bc";
  int refinement_space = 2;
  int order_space = 1;
  int nof_timesteps = 11;
  int levels_space = 2;

  const char *device_config = "cpu";
  ParaViewDataCollection *pd = NULL;

  OptionsParser args(argc, argv);
  args.AddOption(&paraview, "-pv", "--paraview", "-no-paraview",
                 "--no-paraview", "Save files for paraview Visualization");
  args.AddOption(&pvname, "-pvname", "--paraview-name",
                 "Name of the paraview file");
  args.AddOption(&levels_space, "-ls", "--levels_space",
                 "levels for MG space dimension");
  args.AddOption(&nof_timesteps, "-K", "--nof_timesteps",
                 "levels for MG time dimension");

  args.Parse();
  if (!args.Good()) {
    args.PrintUsage(cout);
    return 1;
  }
  args.PrintOptions(cout);

  // Setup the device
  Device device(device_config);
  device.Print();
  // Meshes, FE_Collections and spaces

  Mesh mesh_time = Mesh::MakeCartesian1D(nof_timesteps - 1, 1.0);
  Mesh mesh_time_ref(mesh_time);
  mesh_time_ref.UniformRefinement();

  H1_FECollection fec_time_trial(1, 1);
  L2_FECollection fec_time_test(0, 1);

  FiniteElementSpace fes_time_trial(&mesh_time, &fec_time_trial);
  FiniteElementSpace fes_time_test(&mesh_time_ref, &fec_time_test);

  Mesh mesh_space(mesh_file, 1, 1);
  for (std::size_t i = 0; i < refinement_space; ++i) {
    mesh_space.UniformRefinement();
  }

  H1_FECollection fec_space(order_space, mesh_space.Dimension());
  FiniteElementSpace fes_space(&mesh_space, &fec_space);

  // MG Prep
  ocst::SpaceTimeFiniteElementSpaceHierarchy st_hierarchy(
      fes_time_test, 1, fes_space, levels_space);

  // FiniteElementSpace &fes_time_test =
  //     st_hierarchy.FESHierarchyTime().GetFESpaceAtLevel(1);
  ocst::TimeMatrices time_mats(fes_time_trial, fes_time_test, false);
  Array<ocst::SpaceMatrices *> space_mats;
  Array<SparseMatrix *> A_space;
  Array<SparseMatrix *> M_space;

  mfem::FiniteElementSpaceHierarchy &hierarchy_space =
      st_hierarchy.FESHierarchySpace();
  space_mats.Append(
      new ocst::SpaceMatrices(hierarchy_space.GetFESpaceAtLevel(0)));
  FunctionCoefficient robin_lhs(xi_func);
  FunctionCoefficient kappa_coeff(kappa_func);
  ConstantCoefficient one(1.0);
  space_mats[0]->SetBLF(robin_lhs, kappa_coeff);
  space_mats[0]->Assemble();

  A_space.Append(&space_mats[0]->A());
  M_space.Append(&space_mats[0]->M());
  for (int i = 1; i < levels_space; ++i) {
    space_mats.Append(new ocst::SpaceMatrices(
        *space_mats[i - 1], hierarchy_space.GetFESpaceAtLevel(i)));
    space_mats[i]->Assemble();
    A_space.Append(&space_mats[i]->A());
    M_space.Append(&space_mats[i]->M());
  }
  int dof_time_test = time_mats.M_test().Height() + 1;
  int dof_time_trial = time_mats.M_trial().Height();
  int dof_space = space_mats.Last()->A().Height();
  cout << " n = " << dof_space << endl;
  cout << " K_test = " << dof_time_test << endl;
  cout << " K_trial = " << dof_time_trial << endl;

  // double* buffer = new double[dof_time_test*dof_space];
  using OperatorType = ocst::GeometricMultigrid::OperatorType;
  ocst::GeometricMultigrid prec_A(hierarchy_space, A_space, OperatorType::SPARSE);
  ocst::GeometricMultigrid prec_M(hierarchy_space, M_space, OperatorType::SPARSE);

  ocst::SolveMGPCG solve_A(prec_A);
  ocst::SolveMGPCG solve_M(prec_M);

  // Operators

  ocst::Control u(fes_time_test, hierarchy_space.GetFinestFESpace());
  u.SetM_time(time_mats.M_test());
  u.SetM_space(*M_space.Last());
  // u.Mat()=1.0;
  FunctionCoefficient u_init(f_func);
  u.SetValue(u_init);

  ocst::State y(fes_time_trial, hierarchy_space.GetFinestFESpace());
  y.SetM_time(time_mats.M_trial());

  y.SetM_space(*M_space.Last());

  ocst::AdjointState p(fes_time_test, hierarchy_space.GetFinestFESpace());
  // RHS
  FunctionCoefficient robin_rhs(eta_func);
  FunctionCoefficient y_analytic_coeff(y_analytic);
  y_analytic_coeff.SetTime(0.0);

  ocst::PrimalRHS prhs(dof_time_test, dof_space);
  prhs.SetConstantPart(fes_time_trial, hierarchy_space.GetFinestFESpace(),
                       robin_rhs, y_analytic_coeff, time_mats.M_mixed());

  ocst::SpaceTimeData y_d(dof_time_trial, dof_space, 1.0);
  Vector y_d_T;
  y_d.Mat().GetColumnReference(y_d.Mat().Width() - 1, y_d_T);
  ocst::AdjointRHS arhs(dof_time_trial, dof_space, 1.0, 1.0);
  arhs.SetConstantPart(y_d, y_d_T, time_mats.M_trial(), *M_space.Last());

  double *buffer = new double[dof_time_test * dof_space];
  st_hierarchy.SetBuffer(buffer);
  // Solver
  // Operators
  ocst::NormOperatorTrial M_norm(time_mats.M_trial(), time_mats.A_trial(),
                                 *M_space.Last(), *A_space.Last(), solve_A,
                                 false, buffer);
  M_norm.SetInverseOperator(M_space, A_space, hierarchy_space);
  mfem::Array<ocst::KroneckerOperator*> kron_ops{levels_space};
  
  mfem::Array<ocst::GeneralSylvesterOperator*> sylv_ops{levels_space};
  for (int i=0; i< levels_space; ++i){
      kron_ops[i] = new ocst::KroneckerOperator(time_mats.M_trial(), *M_space[i]);
      sylv_ops[i] = new ocst::GeneralSylvesterOperator(time_mats.M_trial().Height(),
	                                               time_mats.M_trial().Width(),
						       M_space[i]->Height(),
						       M_space[i]->Width(),2);
      sylv_ops[i]->SetMatrixA(time_mats.M_trial(), 0, false);
      sylv_ops[i]->SetMatrixA(time_mats.A_trial(), 1, false);
      sylv_ops[i]->SetMatrixB(*M_space[i], 0, false);
      sylv_ops[i]->SetMatrixB(*A_space[i], 1, false);
      sylv_ops[i]->SetBuffer(buffer);
  }
  // ocst::GeometricMultigrid prec_M_trial{st_hierarchy, sylv_ops, OperatorType::MATRIX_FREE};
  ocst::GeometricMultigrid prec_M_trial{st_hierarchy, kron_ops, OperatorType::TENSOR};
  // ocst::SolveMGPCG inv_M_trial{prec_M_trial};
  mfem::UMFPackSolver inv_M_trial;
  inv_M_trial.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
  inv_M_trial.SetOperator(kron_ops.Last()->SpMat());
  // inv_M_trial.SetSolverParameters(1e-6, 1e-6, 20, 2);

  ocst::NormOperatorTest N_norm(time_mats.M_test(), *M_space.Last(),
                                *A_space.Last(), buffer);
  N_norm.SetInverseOperator(solve_A, solve_M);

  ocst::SpaceTimeParabolicOperator B(time_mats.C_mixed(), time_mats.M_mixed(),
                                     *M_space.Last(), *A_space.Last());
  B.AssembleGeneralSylvester();
  ocst::LSQRSolver solver{B, prhs, arhs, 
                          *M_norm.GetInverseOperator(),
                          *N_norm.GetInverseOperator()};
   // ocst::LSQRSolver solver{B, prhs, arhs, 
			//    inv_M_trial,
   //                         *N_norm.GetInverseOperator()};
   solver.SetSolverParameters(1e-8, 50, 2);
  cout << "Solve Primal" << endl;
  solver.SolvePrimal(u, y);
  
  // cout << "Solve Adjoint" << endl;
  // solver.SolveAdjoint(y, p);

  ocst::State y_ref(y);
  GridFunction y_k(&hierarchy_space.GetFinestFESpace());
  for (int k = 0; k < dof_time_trial; ++k) {
    y_k.MakeRef(y_ref.Vec(), k * dof_space, dof_space);
    y_analytic_coeff.SetTime(k * 1.0 / (dof_time_trial - 1));
    y_k.ProjectCoefficient(y_analytic_coeff);
  }

  y_k.MakeRef(y.Vec(), 0, dof_space);
  if (paraview) {
    pd = new mfem::ParaViewDataCollection(
        pvname, hierarchy_space.GetFinestFESpace().GetMesh());
    pd->SetPrefixPath("paraview");
    pd->SetLevelsOfDetail(order_space);
    pd->SetDataFormat(VTKFormat::BINARY);
    pd->SetHighOrderOutput(true);
    ocst::ExportToParaview(pd, "y", y.Mat(), y.FES_Space(), &y.FES_Time());
    // ocst::ExportToParaview(pd, "y_ref", y_ref.Mat(),
    //                        y.FES_Space(), &y.FES_Time());
    delete pd;
    pd = new mfem::ParaViewDataCollection(
        "reference", hierarchy_space.GetFinestFESpace().GetMesh());
    pd->SetPrefixPath("paraview");
    pd->SetLevelsOfDetail(order_space);
    pd->SetDataFormat(VTKFormat::BINARY);
    pd->SetHighOrderOutput(true);
    ocst::ExportToParaview(pd, "y_ref", y_ref.Mat(), y.FES_Space(),
                           &y.FES_Time());
    delete pd;
  }
  y.Mat() -= y_ref.Mat();
  cout << "||y_h - y_ref||^2_{L_2(I,Omega)} = " << y.SquaredInnerProductNorm()
       << endl;
  for (int i = 0; i < levels_space; ++i) {
    delete space_mats[i];
    delete sylv_ops[i];
     delete kron_ops[i];
  }
  delete[] buffer;
  return 0;
}
