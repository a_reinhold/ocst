#ifndef _OCST_OPTIMAL_CONTROL_PROBLEM
#define _OCST_OPTIMAL_CONTROL_PROBLEM

#include <mfem.hpp>
#include <optimalControlCoefficients.hpp>
#include <spaceTimeData.hpp>
#include <spaceTimeRHS.hpp>


namespace ocst{
/**
 * @brief Class representing the optimal constrol problem
 *
 * Problem with the objective functional
 *   \f[ J(y,u) = \frac{\omega_1}{2} ||y(x,t) - y_{d} ||_{L_2(I;L_2(\Omega))}
 *              + \frac{\omega_2}{2} ||y(x,T) - y_{d,T} ||_{L_2(\Omega)}
 *              + \frac{\lambda}{2} || u(x,t) ||_{L_2(I,L_2(\Omega))}
 *   \f]
 * control with box constrains \f$ u_a \leq u \leq u\f$, the state equation is
 * not encoded in the class
 *
 * The right hand sides for the primal and adjoint problem can be accesed via 
 * this class 
 */
class OptimalControlProblem{
private:

   ocst::Control u;
   ocst::Control u_a, u_b;
   ocst::State y;
   ocst::AdjointState p;

   mfem::Vector y_d_T;
   ocst::State  y_d;    

   ocst::PrimalRHS prhs;
   ocst::AdjointRHS arhs;
   const double lambda;
   const double omega_1;
   const double omega_2;

public:
   OptimalControlProblem(mfem::FiniteElementSpace& fes_time_trial,
                         mfem::FiniteElementSpace& fes_time_test,
                         mfem::FiniteElementSpace& fes_space,
			 mfem::FiniteElementSpace& fes_time_control,
			 mfem::FiniteElementSpace& fes_space_control,
			 bool conform_time, bool connform_space,
			 const double lambda, 
			 const double omega_1 = 1.0, 
			 const double omega_2 = 1.0);

   OptimalControlProblem(mfem::FiniteElementSpace& fes_time_trial,
                         mfem::FiniteElementSpace& fes_time_test,
                         mfem::FiniteElementSpace& fes_space,
			 const double lambda, 
			 const double omega_1 = 1.0, 
			 const double omega_2 = 1.0);

   double
   EvalObjectiveFunction();

   double
   EvalObjectiveFunction(double* buffer_1, double* buffer_2);

   void
   SetControlConstrains(mfem::Coefficient& u_a_coeff,
                        mfem::Coefficient& u_b_coeff);

   void
   SetControlConstrains(double, double);

   /**
    * @brief set y_d and y_d_T
    *
    * use the same coefficient for y_d and the end time y_d_T
    */
   void
   SetDesiredState(mfem::Coefficient& y_d_coeff);

   /**
    * @brief set y_d and y_d_T
    *
    * use different coefficients for y_d and the end time y_d_T
    */
   void
   SetDesiredState(mfem::Coefficient& y_d_coeff, 
                   mfem::Coefficient& y_d_T_coeff,
		   double end_time = 1.0);
   /**
    * @brief Set the constant part for both right hand sides
    *
    * Caller must ensure that the desired state is already set ! 
    */
   void
   SetConstantPartRHS(mfem::Coefficient& y_0, mfem::Coefficient& robin_rhs, 
		      mfem::SparseMatrix& M_time_mixed);

   const ocst::Control&
   ControlLB() const {return u_a;}

   const ocst::Control&
   ControlUB() const {return u_b;}

   ocst::Control&
   Control(){return u;}

   ocst::State&
   State(){return y;}

   ocst::AdjointState&
   AdjointState(){return p;}

   const double
   Lambda() const{return lambda;}
   
   ocst::PrimalRHS&
   PrimalRHS(){ return prhs;}

   ocst::AdjointRHS&
   AdjointRHS(){ return arhs;}
   
   ocst::State&
   DesiredState(){ return y_d;} 
};



} // ocst


#endif //_OCST_OPTIMAL_CONTROL_PROBLEM
