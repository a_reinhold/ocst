#ifndef OCST_HPP
#define OCST_HPP

#include <tensor/tensor.hpp>
#include <spaceTimeData.hpp>
#include <spaceTimeFEMMatrices.hpp>
#include <spaceTimeRHS.hpp>
#include <spaceTimeSolver.hpp> 
#include <optimalControlCoefficients.hpp>
#include <optimalControlProblem.hpp> 
#include <optimalControlSolver.hpp> 

#include <ocstMultigrid.hpp>
#include <postProcessing.hpp>


#include <normInducingOperators.hpp>
//#include <helmholtzEquation.hpp>


#endif //OCST_HPP
