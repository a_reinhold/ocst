#include "spaceTimeData.hpp"

namespace ocst{
SpaceTimeData::SpaceTimeData(int dof_time, int dof_space,
			     double init) : 
   owns_data{true},
   data{new double[dof_space*dof_time]},
   dof_time{dof_time}, 
   dof_space{dof_space}, 
   vec{mfem::Vector(data,dof_space * dof_time)} , 
   mat{mfem::DenseMatrix(data, dof_space, dof_time)}  
{
   vec = init;  
}

SpaceTimeData::SpaceTimeData(int dof_time, int dof_space)
   : SpaceTimeData(dof_time ,dof_space, 0.0)
{   }

SpaceTimeData::SpaceTimeData(int dof_time, int dof_space, 
                             double* data):
   owns_data{false},
   data{data},
   dof_time{dof_time}, 
   dof_space{dof_space}, 
   vec{mfem::Vector(data, dof_space * dof_time)} , 
   mat{mfem::DenseMatrix(data, dof_space, dof_time)}  
{   }

SpaceTimeData::SpaceTimeData(const SpaceTimeData& other):
   owns_data{true},
   data{new double[other.dof_space*other.dof_time]},
   dof_time{other.dof_time}, 
   dof_space{other.dof_space}, 
   vec{mfem::Vector(data,dof_space * dof_time)} , 
   mat{mfem::DenseMatrix(data, dof_space, dof_time)}  
{
   for (int i=0; i<dof_time*dof_space; ++i){
      data[i] = other.data[i];
   }
}

SpaceTimeData::~SpaceTimeData()
{
   if (owns_data){
      delete[] data;
   }
}

void
SpaceTimeData::SwapData(SpaceTimeData& other)
{
   MFEM_VERIFY(dof_time*dof_space == other.dof_time*other.dof_space, 
               "different sizes, failed to swap");
   double* my_new_data = other.data;
   bool own_new_data = other.owns_data;

   other.data=data;
   other.owns_data = owns_data;
   other.vec.SetData(data);
   other.mat.UseExternalData(data, other.dof_space, other.dof_time);

   data = my_new_data;
   owns_data = own_new_data; 
   vec.SetData(data);
   mat.UseExternalData(data, dof_space, dof_time); 
}
} //ocst
