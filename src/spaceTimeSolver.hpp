#ifndef OCST_SPACE_TIME_SOLVER
#define OCST_SPACE_TIME_SOLVER

#include <mfem.hpp>
#include <tensor/general_sylvester.hpp>
#include <optimalControlCoefficients.hpp> 
#include <spaceTimeRHS.hpp>
#include <normInducingOperators.hpp>


namespace ocst{
/**
 * @brief Abstract base class for a space time solver
 */
class SpaceTimeSolver{
protected:
   ocst::PrimalRHS* primal_rhs;
   ocst::AdjointRHS* adjoint_rhs;
public:
   SpaceTimeSolver( ocst::PrimalRHS& primal_rhs, 
		   ocst::AdjointRHS& adjoint_rhs);

   
   virtual void 
   SolvePrimal(ocst::Control&, ocst::OptimalControlCoefficients&, 
               double* buffer= nullptr) const =0;
   

   virtual void 
   SolveAdjoint(ocst::State&, ocst::OptimalControlCoefficients&,
                double* buffer= nullptr) const =0;
};

/**
 * @brief Direct solver
 *
 * Works only if the full system matrix is built, ussually only possible for 
 * small problems
 */
class SpaceTimeDirectSolver:public SpaceTimeSolver{
private:
   mfem::UMFPackSolver solver;
   tensor::GeneralSylvesterOperator* op;
public:
   SpaceTimeDirectSolver(tensor::GeneralSylvesterOperator&,
                         ocst::PrimalRHS&, 
			 ocst::AdjointRHS&);
   ~SpaceTimeDirectSolver();
   void 
   SetOperator();

   void 
   SolvePrimal(ocst::Control&, ocst::OptimalControlCoefficients&,
	       double* buffer = nullptr) const override;

   void 
   SolveAdjoint(ocst::State&, ocst::OptimalControlCoefficients&,
		double* buffer = nullptr) const override;


   			  
};
/**
 * @brief LSQR solver, iterative least squares solution
 *
 * solves the system 
 * \f[
 *  \min_{y\in Y} ||By - f||_N 
 * \f]
 * using M as apreconditoner
 *
 * This method is implemented as proposed in: 
 * "Space-time discretization of the heat equation, A concise Matlab 
 * implementation" by R.Adreev (2013)
 */
class LSQRSolver : public SpaceTimeSolver{
private:

   tensor::GeneralSylvesterOperator* op;
   const int m; 
   const int n;
   mfem::Operator* inv_norm_trial;
   mfem::Operator* inv_norm_test;

   mutable mfem::Vector vec_mn;
   mutable mfem::Vector vec_m_1;
   mutable mfem::Vector vec_m_2;
   mutable mfem::Vector vec_n_1;
   mutable mfem::Vector vec_n_2;

   double tol;
   int max_it;
   int print_level= 0;

   mutable double alpha, beta, gamma , delta, rho;
public:
   LSQRSolver(tensor::GeneralSylvesterOperator& B, 
              ocst::PrimalRHS& primal, ocst::AdjointRHS& adjoint, 
	      mfem::Operator& inv_norm_trial, mfem::Operator& inv_norm_test);

   void 
   SolvePrimal(ocst::Control&, ocst::OptimalControlCoefficients&,
	       double* buffer = nullptr) const override;

   void 
   SolveAdjoint(ocst::State&, ocst::OptimalControlCoefficients&,
		double* buffer = nullptr) const override;

   
   void 
   SetSolverParameters(double tol_, int max_it_=20, int print_level_=0)
   {tol=tol_; max_it=max_it_; print_level=print_level_;}

private: 
   double
   Normalize(mfem::Operator* S,const mfem::Vector& s, 
	     mfem::Vector& z, mfem::Vector& z_hat) const;
   /** 
    * general solver implementation
    */
   void
   Solve(mfem::Operator* N, mfem::Operator* M, mfem::Vector& y,
	 const mfem::Vector& f, mfem::Vector& d,
         mfem::Vector& v_hat, mfem::Vector& v, 
         mfem::Vector& w_hat, mfem::Vector& w) const;

};


/**
 * @brief Iterative blockwise solver
 *
 * Solves the square system in a time stepping like manner, currently only 
 * implemented for equdistant (ordered) time meshes. The systems are solved 
 * subseqential in time using multigrid preconditioned method in space.
 *
 * Two additional sparse matrices same dimension as M and A are built and stored.
 */
class IterativeType1Solver: public SpaceTimeSolver{
private:
   const int n_h;
   const int K;
   const double T;
   const double dt;
   mfem::Operator* solve_M;
   mutable mfem::Vector rhs;

   mfem::Array<mfem::SparseMatrix*> G_plus;
   mfem::SparseMatrix* G_minus = nullptr;

   ocst::GeometricMultigrid* prec_G = nullptr;
   ocst::SolveMGPCG* solve_G_plus = nullptr;

public:
   IterativeType1Solver(ocst::PrimalRHS& prhs, ocst::AdjointRHS& arhs,
                              mfem::Array<mfem::SparseMatrix*>& M, 
                              mfem::Array<mfem::SparseMatrix*>& A,
			      mfem::Operator& solve_M,
			      mfem::FiniteElementSpaceHierarchy& hierarchy,
			      const int K, const double T);
   ~IterativeType1Solver();


   void 
   SolvePrimal(ocst::Control&, ocst::OptimalControlCoefficients&,
	       double* buffer = nullptr) const override;

   void 
   SolveAdjoint(ocst::State&, ocst::OptimalControlCoefficients&,
		double* buffer = nullptr) const override;

};

}
#endif //OCST_SPACE_TIME_SOLVER

