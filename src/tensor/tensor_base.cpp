#include "tensor_base.hpp"

namespace tensor {

TensorOperator::TensorOperator(int m, int n)
    : mfem::Operator{m, n}
{ }

TensorOperator::~TensorOperator()
{
   delete mat;
   delete diag;
}

mfem::SparseMatrix&
TensorOperator::SpMat()
{
   if (!mat) {
      Assemble();
   }
   return *mat;
}

const mfem::SparseMatrix&
TensorOperator::SpMat() const
{
   if (!mat) {
      Assemble();
   }
   return *mat;
}

void
TensorOperator::GetDiag(mfem::Vector& d) const
{
   if (!diag) {
      AssembleDiag();
   }
   d = *diag;
};

void
TensorOperator::AssembleDiag() const
{
   MFEM_VERIFY(height == width,
               "TensorOperator must be square, not height = "
                   << height << ", width = " << width);
   if (diag) {
      diag->SetSize(height);
   } else {
      diag = new mfem::Vector{height};
   }
   *diag = 1.0;
}

// TensorOperator::TensorOperator (const TensorOperator &other)
//     : TensorOperator(other.height, other.width)
// {
//    mfem::out << "Base Copy Constructor called \n";
//    transposed = other.transposed;
//    // create deep copys of mat and diag
//    if (other.diag)
//       {
//          diag = new mfem::Vector{ *other.diag };
//       }
//    if (other.mat)
//       {
//          mat = new mfem::SparseMatrix{ *other.mat };
//       }
// }
//
// TensorOperator::TensorOperator (TensorOperator &&other) noexcept
//     : TensorOperator{ other.height, other.width }
// {
//    mfem::out << "Base Move Constructor called \n";
//    transposed = other.transposed;
//    std::swap (mat, other.mat);
//    std::swap (diag, other.diag);
// }
} // namespace tensor
