#include "linalg_auxiliary.hpp"

namespace tensor {

void
SparseDenseMM(Transposed op_A,
              Transposed op_B,
              Transposed op_C,
              const double alpha,
              const mfem::SparseMatrix& A,
              const mfem::DenseMatrix& B,
              const double beta,
              mfem::DenseMatrix& C)
{
   if (op_B == op_C) {
#ifdef _OCST_USE_INTEL_MKL
      const int* I = A.GetI();
      int* J = const_cast<int*>(A.GetJ());
      double* data = const_cast<double*>(A.GetData());

      // prepare intel mkl sparse matrix
      sparse_matrix_t A_ = NULL;
      int* rows_start = new int[A.Height()];
      int* rows_end = new int[A.Height()];

      for (int l = 0; l < A.Height(); ++l) {
         rows_start[l] = I[l];
         rows_end[l] = I[l + 1];
      }
      sparse_status_t create;
      create = mkl_sparse_d_create_csr(&A_,
                                       SPARSE_INDEX_BASE_ZERO,
                                       A.Height(),
                                       A.Width(),
                                       rows_start,
                                       rows_end,
                                       J,
                                       data);
      MFEM_VERIFY(create == SPARSE_STATUS_SUCCESS,
                  "mkl_sparse_d_create_csr()"
                  " returned "
                      << int(create));
      matrix_descr descr;
      descr.type = SPARSE_MATRIX_TYPE_GENERAL;
      descr.mode = SPARSE_FILL_MODE_FULL;
      descr.diag = SPARSE_DIAG_NON_UNIT;
      sparse_status_t mm;
      if (op_A == ocst::Transposed::NON_TRANSPOSED
          && op_B == ocst::Transposed::NON_TRANSPOSED) {
         MFEM_VERIFY(C.Height() == A.Height(), " ");
         MFEM_VERIFY(C.Width() == B.Width(), " ");
         MFEM_VERIFY(A.Width() == B.Height(), " ");
         // C += beta C * alpha A B
         mm = mkl_sparse_d_mm(SPARSE_OPERATION_NON_TRANSPOSE,
                              alpha,
                              A_,
                              descr,
                              SPARSE_LAYOUT_COLUMN_MAJOR,
                              B.GetData(),
                              C.Width(),
                              B.Height(),
                              beta,
                              C.GetData(),
                              C.Height());
      } else if (op_A == ocst::Transposed::TRANSPOSED
                 && op_B == ocst::Transposed::NON_TRANSPOSED) {
         MFEM_VERIFY(C.Height() == A.Width(), " ");
         MFEM_VERIFY(C.Width() == B.Width(), " ");
         MFEM_VERIFY(A.Height() == B.Height(), " ");
         // C += beta C * alpha A^T B
         mm = mkl_sparse_d_mm(SPARSE_OPERATION_TRANSPOSE,
                              alpha,
                              A_,
                              descr,
                              SPARSE_LAYOUT_COLUMN_MAJOR,
                              B.GetData(),
                              C.Width(),
                              B.Height(),
                              beta,
                              C.GetData(),
                              C.Height());
      } else if (op_A == ocst::Transposed::NON_TRANSPOSED
                 && op_B == ocst::Transposed::TRANSPOSED) {
         // C^T += alpha * A * B^T
         MFEM_VERIFY(C.Width() == A.Height(), " ");
         MFEM_VERIFY(C.Height() == B.Height(), " ");
         MFEM_VERIFY(A.Width() == B.Width(), " ");
         mm = mkl_sparse_d_mm(SPARSE_OPERATION_NON_TRANSPOSE,
                              alpha,
                              A_,
                              descr,
                              SPARSE_LAYOUT_ROW_MAJOR,
                              B.GetData(),
                              C.Height(),
                              B.Height(),
                              beta,
                              C.GetData(),
                              C.Height());
      } else if (op_A == ocst::Transposed::TRANSPOSED
                 && op_B == ocst::Transposed::TRANSPOSED) {
         // C^T += alpha * A^T * B^T
         MFEM_VERIFY(C.Width() == A.Width(), " ");
         MFEM_VERIFY(C.Height() == B.Height(), " ");
         MFEM_VERIFY(A.Height() == B.Width(), " ");
         mm = mkl_sparse_d_mm(SPARSE_OPERATION_TRANSPOSE,
                              alpha,
                              A_,
                              descr,
                              SPARSE_LAYOUT_ROW_MAJOR,
                              B.GetData(),
                              C.Height(),
                              B.Height(),
                              beta,
                              C.GetData(),
                              C.Height());
      }
      sparse_status_t destroy;
      destroy = mkl_sparse_destroy(A_);
      delete[] rows_start;
      delete[] rows_end;
      MFEM_VERIFY(mm == SPARSE_STATUS_SUCCESS,
                  "mkl_sparse_d_mm() returned " << mm);

      return;

#else // no use of external BLAS

      if (beta == 0.0) {
         C = 0.0;
      } else {
         C *= beta;
      }
      const int* I = A.GetI();
      const int* J = A.GetJ();
      const double* data = A.GetData();

      if (alpha == 0.0) {
         return;
      }
      if (op_A == Transposed::NON_TRANSPOSED
          && op_B == Transposed::NON_TRANSPOSED) {
         // C += alpha * A * B
         MFEM_VERIFY(C.Height() == A.Height(), " ");
         MFEM_VERIFY(C.Width() == B.Width(), " ");
         MFEM_VERIFY(A.Width() == B.Height(), " ");

         for (int i = 0; i < A.Height(); ++i) {
            for (int r = I[i]; r < I[i + 1]; ++r) {
               int l = J[r];
               for (int j = 0; j < C.Width(); ++j) {
                  C(i, j) += alpha * data[r] * B(l, j);
               }
            }
         }
         return;
      } else if (op_A == Transposed::TRANSPOSED
                 && op_B == Transposed::NON_TRANSPOSED) {
         // C += alpha * A^T * B
         MFEM_VERIFY(C.Height() == A.Width(), " ");
         MFEM_VERIFY(C.Width() == B.Width(), " ");
         MFEM_VERIFY(A.Height() == B.Height(), " ");
         for (int l = 0; l < A.Height(); ++l) {
            for (int r = I[l]; r < I[l + 1]; ++r) {
               int i = J[r];
               for (int j = 0; j < C.Width(); ++j) {
                  C(i, j) += alpha * data[r] * B(l, j);
               }
            }
         }

         return;
      } else if (op_A == Transposed::NON_TRANSPOSED
                 && op_B == Transposed::TRANSPOSED) {
         // C^T += alpha * A * B^T
         MFEM_VERIFY(C.Width() == A.Height(), " ");
         MFEM_VERIFY(C.Height() == B.Height(), " ");
         MFEM_VERIFY(A.Width() == B.Width(), " ");
         for (int i = 0; i < A.Height(); ++i) {
            for (int r = I[i]; r < I[i + 1]; ++r) {
               int l = J[r];
               for (int j = 0; j < C.Height(); ++j) {
                  C(j, i) += alpha * data[r] * B(j, l);
               }
            }
         }
         return;
      } else if (op_A == Transposed::TRANSPOSED
                 && op_B == Transposed::TRANSPOSED) {
         // C^T += alpha * A^T * B^T
         MFEM_VERIFY(C.Width() == A.Width(), " ");
         MFEM_VERIFY(C.Height() == B.Height(), " ");
         MFEM_VERIFY(A.Height() == B.Width(), " ");
         for (int l = 0; l < A.Height(); ++l) {
            for (int r = I[l]; r < I[l + 1]; ++r) {
               int i = J[r];
               for (int j = 0; j < C.Height(); ++j) {
                  C(j, i) += alpha * data[r] * B(j, l);
               }
            }
         }
         return;
      }

#endif
   } else {
      // Always no blas
      MFEM_ABORT("warning, SparseDenseMM() for this configuration "
                 "not implemented yet");
   }
}

void
SparseDenseMM(Transposed op_A,
              Transposed op_B,
              Transposed op_C,
              const mfem::SparseMatrix& A,
              const mfem::DenseMatrix& B,
              mfem::DenseMatrix& C)
{
   SparseDenseMM(op_A, op_B, op_C, 1.0, A, B, 0.0, C);
}

void
KroneckerMV(Transposed op,
            const double alpha,
            const mfem::SparseMatrix& A,
            const mfem::SparseMatrix& B,
            const mfem::DenseMatrix& X,
            const double beta,
            double* buffer,
            mfem::DenseMatrix& Y)
{

   /*
   MFEM_VERIFY(A.Height() == Y.Width(), "");
   MFEM_VERIFY(A.Width()  == X.Width(), "");
   MFEM_VERIFY(B.Height() == Y.Height(), "");
   MFEM_VERIFY(B.Width()  == X.Height(), "");
   */
   if (B.Height() * A.Width() < A.Height() * B.Width()) {
      mfem::DenseMatrix Tmp(buffer, B.Height(), A.Width());
      switch (op) {
      case Transposed::NON_TRANSPOSED:
         SparseDenseMM(Transposed::NON_TRANSPOSED,
                       Transposed::NON_TRANSPOSED,
                       Transposed::NON_TRANSPOSED,
                       1.0,
                       B,
                       X,
                       0.0,
                       Tmp);

         SparseDenseMM(Transposed::NON_TRANSPOSED,
                       Transposed::TRANSPOSED,
                       Transposed::TRANSPOSED,
                       alpha,
                       A,
                       Tmp,
                       beta,
                       Y);
         break;
      case Transposed::TRANSPOSED:
         SparseDenseMM(Transposed::TRANSPOSED,
                       Transposed::TRANSPOSED,
                       Transposed::TRANSPOSED,
                       1.0,
                       A,
                       X,
                       0.0,
                       Tmp);

         SparseDenseMM(Transposed::TRANSPOSED,
                       Transposed::NON_TRANSPOSED,
                       Transposed::NON_TRANSPOSED,
                       alpha,
                       B,
                       Tmp,
                       beta,
                       Y);
         break;
      default:
         MFEM_ABORT("Invalid Transposed::op!");
      }
   } else {
      mfem::DenseMatrix Tmp(buffer, B.Width(), A.Height());
      switch (op) {
      case Transposed::NON_TRANSPOSED:
         SparseDenseMM(Transposed::NON_TRANSPOSED,
                       Transposed::TRANSPOSED,
                       Transposed::TRANSPOSED,
                       1.0,
                       A,
                       X,
                       0.0,
                       Tmp);

         SparseDenseMM(Transposed::NON_TRANSPOSED,
                       Transposed::NON_TRANSPOSED,
                       Transposed::NON_TRANSPOSED,
                       alpha,
                       B,
                       Tmp,
                       beta,
                       Y);
         break;
      case Transposed::TRANSPOSED:
         SparseDenseMM(Transposed::TRANSPOSED,
                       Transposed::NON_TRANSPOSED,
                       Transposed::NON_TRANSPOSED,
                       1.0,
                       B,
                       X,
                       0.0,
                       Tmp);

         SparseDenseMM(Transposed::TRANSPOSED,
                       Transposed::TRANSPOSED,
                       Transposed::TRANSPOSED,
                       alpha,
                       A,
                       Tmp,
                       beta,
                       Y);
         break;
      default:
         MFEM_ABORT("Invalid Transposed::op!");
      }
   }
}

} // tensor
