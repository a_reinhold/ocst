#ifndef OCST_TENSOR_GENERAL_SYLVESTER_HPP
#define OCST_TENSOR_GENERAL_SYLVESTER_HPP
#include "linalg_auxiliary.hpp"
#include "tensor_base.hpp"
namespace tensor {

/**
 * @brief Operator class for a generalized Sylvester operator
 *
 * Operator for the generalized Sylvester equation. The operator has the
 form
 * \f$ A \otimes B +  C \otimes D \f$ where:
 *   - #A, #C \f$ \in \mathbb{R}^{m_A \times n_A} \f$
 *   - #B, #D \f$ \in \mathbb{R}^{m_B \times n_B} \f$
 *
 * The dimension of the operator is given by
 * \f$ (m_A \cdot m_B \times n_A \cdot n_B )\f$
 *
 * The application of the operator to a vector is given by:
 *
 * \f$  (A \otimes B +  C \otimes D) x = y  \Leftrightarrow B X A^\top + D X
 * C^\top = Y\f$
 */
class GeneralSylvesterOperator : public TensorOperator {
private:
   mfem::SparseMatrix *A, *B, *C, *D;
   bool owns_A, owns_B, owns_C, owns_D;

   int m_A, m_B, n_A, n_B;

   double* buffer;
   bool owns_buffer{false};


public:
   GeneralSylvesterOperator(mfem::SparseMatrix& A,
                            mfem::SparseMatrix& B,
                            mfem::SparseMatrix& C,
                            mfem::SparseMatrix& D,
                            double* buffer = nullptr);

   GeneralSylvesterOperator(mfem::SparseMatrix& A,
                            mfem::SparseMatrix& B,
                            mfem::SparseMatrix& C,
                            mfem::SparseMatrix& D,
                            bool owns_A,
                            bool owns_B,
                            bool owns_C,
                            bool owns_D,
                            double* buffer = nullptr);

   ~GeneralSylvesterOperator();

   GeneralSylvesterOperator(const GeneralSylvesterOperator& other) = delete;
   GeneralSylvesterOperator(GeneralSylvesterOperator&& other) noexcept = delete;
   GeneralSylvesterOperator& operator=(const GeneralSylvesterOperator& other) = delete;
   GeneralSylvesterOperator& operator=(GeneralSylvesterOperator&& other) = delete;

   void Assemble() const override;

   void AssembleDiag() const override;

private:
   /**
    * @brief general Matrix Vector Multiplication
    *
    * Implementation for both Mult and MultTranspose
    */
   virtual void TensorMult(const mfem::Vector& x,
                           mfem::Vector& y,
                           Transposed t) const override;
};
} // tensor

#endif // !OCST_TENSOR_GENERAL_SYLVESTER_HPP
