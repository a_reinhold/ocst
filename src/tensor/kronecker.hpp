#ifndef OCST_TENSOR_KRONECKER_HPP
#define OCST_TENSOR_KRONECKER_HPP

#include "linalg_auxiliary.hpp"
#include "tensor_base.hpp"
#include <mfem.hpp>
namespace tensor {
/**
 * @brief Tensor Operator for the Kronecker Product
 *
 * Implementation for the Kronecker Operator \f$ A \otimes B \f$ where #A and
 * #B are sparse matrices.
 * The application to this operator to a matrix  \f$ (A \otimes B) x = \f$ is
 * eqivalent to \f$ BXA^T = Y \f$ where \f$ vec(X) = x,\ vec(Y) = y \f$
 * This relation is exploited for an efficient application of the matrix vector
 * product.
 *
 * The dimension of the operator is given by
 * \f$ (m_A \cdot m_B \times n_A \cdot n_B )\f$
 */
class KroneckerOperator : public TensorOperator {
private:
   mfem::SparseMatrix* A;
   mfem::SparseMatrix* B;
   bool owns_A, owns_B;
   int m_A, m_B, n_A, n_B;

   double* buffer;
   bool owns_buffer{false};

public:
   KroneckerOperator(mfem::SparseMatrix& A,
                     mfem::SparseMatrix& B,
                     bool owns_A,
                     bool owns_B,
                     double* buffer = nullptr);
   KroneckerOperator(mfem::SparseMatrix& A,
                     mfem::SparseMatrix& B,
                     double* buffer = nullptr);
   ~KroneckerOperator();
   KroneckerOperator(const KroneckerOperator& other) = delete;
   KroneckerOperator(KroneckerOperator&& other) noexcept = delete;
   KroneckerOperator& operator=(const KroneckerOperator& other) = delete;
   KroneckerOperator& operator=(KroneckerOperator&& other) = delete;

   void Assemble() const override;
   void AssembleDiag() const override;

private:
   void TensorMult(const mfem::Vector& x,
                   mfem::Vector& y,
                   Transposed t) const override;
   // friend void swap(KroneckerOperator& lhs, KroneckerOperator& rhs)
   // noexcept;
};
} // tensor

#endif // !OCST_TENSOR_KRONECKER_HPP
