#ifndef OCST_TENSOR_LINALG_AUXILIARY_HPP
#define OCST_TENSOR_LINALG_AUXILIARY_HPP

#include <mfem.hpp>
#ifdef _OCST_USE_INTEL_MKL
#include <mkl.h>
#endif

namespace tensor {

enum class Transposed { NON_TRANSPOSED = 0, TRANSPOSED = 1 };

/**
 * @brief Sparse Dense Matrix Matrix Product
 *
 * Sparse Dense Matrix Matrix Product
 * \f$ op(C) \leftarrow  \beta \cdot op(C) + \alpha \cdot op(A) \cdot op(B)\f$.
 *
 * If intel_mkl is used, this function wraps to intels mkl_sparse_d_mm()
 * otherwise an non optimized implementation is used.
 *
 * note: if #op_B != #op_C then no efficient sparse blas method will be used!
 */
void SparseDenseMM(Transposed op_A,
                   Transposed op_B,
                   Transposed op_C,
                   const double alpha,
                   const mfem::SparseMatrix& A,
                   const mfem::DenseMatrix& B,
                   const double beta,
                   mfem::DenseMatrix& C);

/**
 * @brief Sparse Dense Matrix Matrix Product
 *
 * Sparse Dense Matrix Matrix Product \f$ op(C) \leftarrow  \cdot op(A)
 * \cdot op(B)\f$.
 *
 * Calls SparseDenseMM with fixed double values.
 */
void SparseDenseMM(Transposed op_A,
                   Transposed op_B,
                   Transposed op_C,
                   const mfem::SparseMatrix& A,
                   const mfem::DenseMatrix& B,
                   mfem::DenseMatrix& C);

/**
 * @brief Sparse Kronecker Product Matrix Vector Product
 *
 * Evaluates the Matrix Vector Product of \f$ y \leftarrow \beta \cdot y
 * + \alpha \cdot (A \otimes B) x \f$ which is eqivalent to \f$Y \leftarrow
 * \beta \cdot Y + \alpha \cdot BXA^T\f$. The transposed case \f$y \leftarrow
 * \beta \cdot y  + \alpha \cdot (A \otimes B)^T x \f$ is handled analogously.
 *
 *
 * @param buffer needs to be at least of size
 * \f$\min(m_B \cdot n_A, n_B \cdot m_A)\f$
 *
 * @param X input vector in matrix form, can share data with Y
 * @param Y output vector in matrix form, can share data with X
 */
void KroneckerMV(Transposed transposed,
                 const double alpha,
                 const mfem::SparseMatrix& A,
                 const mfem::SparseMatrix& B,
                 const mfem::DenseMatrix& X,
                 const double beta,
                 double* buffer,
                 mfem::DenseMatrix& Y);
} // namespace tensor

#endif // OCST_TENSOR_LINALG_AUXILIARY_HPP
