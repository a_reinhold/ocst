#ifndef OCST_TENSOR_TENSOR_BASE_HPP
#define OCST_TENSOR_TENSOR_BASE_HPP

#include "linalg_auxiliary.hpp"
#include <mfem.hpp>
namespace tensor {
/**
 * @brief Abstract base class for tensor type operators
 *
 * base class for tensor type operators, tensor operators need to be able to be
 * assembled, and one needs to be able to get the diagonal of the operator
 * (without assembling it first)
 */
class TensorOperator : public mfem::Operator {
protected:
   mutable Transposed transposed = Transposed::NON_TRANSPOSED; // default
   bool diag_assembleable{false};

   /** @brief pointer for the matrix, used if assembled*/
   mutable mfem::SparseMatrix* mat{nullptr};
   /** @brief pointer for the diagonal, used if diagonal is assembled*/
   mutable mfem::Vector* diag{nullptr};

   TensorOperator(int m, int n);
   ~TensorOperator();
   TensorOperator(const TensorOperator& other) = delete;     // copy
   TensorOperator(TensorOperator&& other) noexcept = delete; // move
   TensorOperator& operator=(TensorOperator&& other) noexcept
       = delete; // move assignment
   TensorOperator& operator=(const TensorOperator& other)
       = delete; // copy assignment

public:
   void
   SetTransposed(Transposed t)
   {
      transposed = t;
   }
   /**
    * @brief returns the assembled matrix
    *
    * \warning if the matrix is not assembled yet it will call Assemble()
    */
   mfem::SparseMatrix& SpMat();

   /**
    * @brief returns the assembled matrix
    *
    * \warning if the matrix is not assembled yet it will call Assemble()
    */
   const mfem::SparseMatrix& SpMat() const;

   /**
    * @brief assembles the operator as a sparse matrix
    *
    * operator gets assembled and stored in #mat, the diagonal is also
    * assembled and stored in #diag
    * \warning possibly very expensive and memory heavy operations, especcially
    * if inveres are involved
    */
   virtual void Assemble() const = 0;

   /**
    * @brief assembles the diagonal if possible
    *
    * If possible, assembles diagonal (without assembling the operator) and
    * stores it in #diag. If not possible a diagonal with all 1s is stored for
    * a square operator
    */

   virtual void AssembleDiag() const;
   /**
    * @brief returns and possibly assembles the diagonal of a square operator
    *
    *
    * If not already stored AssembleDiag() is called.
    * This function only makes sense for a square TensorOperator.
    */
   void GetDiag(mfem::Vector& d) const;

   void
   Mult(const mfem::Vector& x, mfem::Vector& y) const override
   {
      TensorMult(x, y, Transposed::NON_TRANSPOSED);
   }
   void
   MultTranspose(const mfem::Vector& x, mfem::Vector& y) const override
   {
      TensorMult(x, y, Transposed::TRANSPOSED);
   }

protected:
   virtual void
   TensorMult(const mfem::Vector& x, mfem::Vector& y, Transposed t) const
       = 0;
};

} // namespace tensor
#endif // !OCST_TENSOR_TENSOR_BASE_HPP
