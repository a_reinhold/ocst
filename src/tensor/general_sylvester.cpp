#include "general_sylvester.hpp"
namespace tensor {

GeneralSylvesterOperator::GeneralSylvesterOperator(
    mfem::SparseMatrix& A_,
    mfem::SparseMatrix& B_,
    mfem::SparseMatrix& C_,
    mfem::SparseMatrix& D_,
    double* buffer_ /* = nullptr */)
    : GeneralSylvesterOperator(
          A_, B_, C_, D_, false, false, false, false, buffer_)
{ }

GeneralSylvesterOperator::GeneralSylvesterOperator(
    mfem::SparseMatrix& A_,
    mfem::SparseMatrix& B_,
    mfem::SparseMatrix& C_,
    mfem::SparseMatrix& D_,
    bool owns_A_,
    bool owns_B_,
    bool owns_C_,
    bool owns_D_,
    double* buffer_ /* = nullptr */)
    : TensorOperator(A_.Height() * B_.Height(), A_.Width() * B_.Width())
    , A{&A_}
    , B{&B_}
    , C{&C_}
    , D{&D_}
    , owns_A{owns_A_}
    , owns_B{owns_B_}
    , owns_C{owns_C_}
    , owns_D{owns_D_}
    , m_A{A_.Height()}
    , m_B{B_.Height()}
    , n_A{A_.Width()}
    , n_B{B_.Width()}
    , buffer{buffer_}
{
   if (!buffer) {
      buffer = new double[std::min(m_B * n_A, m_A * n_B)];
      owns_buffer = true;
   }
}

GeneralSylvesterOperator::~GeneralSylvesterOperator()
{
   if (owns_buffer)
      delete[] buffer;
   if (owns_A)
      delete A;
   if (owns_B)
      delete B;
   if (owns_C)
      delete C;
   if (owns_D)
      delete D;
}

void
GeneralSylvesterOperator::TensorMult(const mfem::Vector& x,
                                     mfem::Vector& y,
                                     Transposed t) const
{
   int m_X, n_X, m_Y, n_Y;
   Transposed op;
   if (t == transposed) { // both transposed or non_transposed
      m_X = n_B;
      n_X = n_A;
      m_Y = m_B;
      n_Y = m_A;
      op = Transposed::NON_TRANSPOSED;
   } else {
      m_X = m_B;
      n_X = m_A;
      m_Y = n_B;
      n_Y = n_A;
      op = Transposed::TRANSPOSED;
   }

   const mfem::DenseMatrix X(x.GetData(), m_X, n_X);
   mfem::DenseMatrix Y(y.GetData(), m_Y, n_Y);

   // y <- 0*y + Op * x
   KroneckerMV(op, 1.0, *A, *B, X, 0.0, buffer, Y);
   // y <- y + Op* x
   KroneckerMV(op, 1.0, *C, *D, X, 1.0, buffer, Y);
}

void
GeneralSylvesterOperator::Assemble() const
{
   if (mat) {
      delete mat;
   }
   mfem::Array<int> offsets_i{m_A + 1};
   mfem::Array<int> offsets_j{n_A + 1};

   for (int i = 0; i < m_A + 1; ++i) {
      offsets_i[i] = i * m_B;
   }
   for (int j = 0; j < n_A + 1; ++j) {
      offsets_j[j] = j * n_B;
   }
   mfem::BlockMatrix block{offsets_i, offsets_j};
   block.owns_blocks = true;
   double a,c;
   const int* Ip = A->GetI();
   const int* Jp = A->GetJ();
   for (int i = 0; i < m_A; ++i) {
      for (int j = Ip[i]; j < Ip[i + 1]; ++j) {
	 a = A->Elem(i, Jp[j]);
	 c = C->Elem(i, Jp[j]);
         block.SetBlock(i, Jp[j], mfem::Add(a, *B, c, *D));
      }
   }
   mat = block.CreateMonolithic();
}

void
GeneralSylvesterOperator::AssembleDiag() const
{
   MFEM_VERIFY(height == width,
               "GeneralSylvesterOperator must be square, not height = "
                   << height << ", width = " << width);
   if (!diag) {
      diag = new mfem::Vector(height);
   } else {
      diag->SetSize(height);
   }

   if (m_A == n_A) {
      mfem::Vector diag_A{m_A};
      mfem::Vector diag_B{m_B};
      mfem::Vector diag_C{m_A};
      mfem::Vector diag_D{m_B};
      mfem::Vector tmp;
      mfem::Vector tmp2{m_B};
      A->GetDiag(diag_A);
      B->GetDiag(diag_B);
      C->GetDiag(diag_C);
      D->GetDiag(diag_D);
      for (int i = 0; i < m_A; ++i) {
         tmp.SetDataAndSize(diag->GetData() + i * m_B, m_B);
         tmp = diag_B;
         tmp *= diag_A[i];
         tmp2 = diag_D;
         tmp2 *= diag_C[i];
         tmp += tmp2;
      }
   } else {
      int i_A, j_A, i_B, j_B;
      const mfem::SparseMatrix& A_{*A};
      const mfem::SparseMatrix& B_{*B};
      const mfem::SparseMatrix& C_{*C};
      const mfem::SparseMatrix& D_{*D};
      for (size_t l = 0; l < height; ++l) {
         i_A = l / n_A;
         j_A = l / m_A;
         i_B = l % n_A;
         j_B = l % m_A;
         diag->Elem(l)
             = A_(i_A, j_A) * B_(i_B, j_B) + C_(i_A, j_A) * D_(i_B, j_B);
      }
   }
}
} // namespace tensor
