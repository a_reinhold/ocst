#ifndef OCST_TENSOR_TENSOR_HPP
#define OCST_TENSOR_TENSOR_HPP

// USE RELATIVE for stuff in src/tensor,
#include "kronecker.hpp"
#include "linalg_auxiliary.hpp"
#include "tensor_base.hpp"
#include "general_sylvester.hpp"

#endif // OCST_TENSOR_TENSOR_HPP
