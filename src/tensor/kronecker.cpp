#include "kronecker.hpp"
namespace tensor {

KroneckerOperator::KroneckerOperator(mfem::SparseMatrix& A_,
                                     mfem::SparseMatrix& B_,
                                     bool owns_A_,
                                     bool owns_B_,
                                     double* buffer_ /*nullptr*/)
    : TensorOperator{A_.Height() * B_.Height(), A_.Width() * B_.Width()}
    , A{&A_}
    , B{&B_}
    , owns_A{owns_A_}
    , owns_B{owns_B_}
    , m_A{A_.Height()}
    , m_B{B_.Height()}
    , n_A{A_.Width()}
    , n_B{B_.Width()}
    , buffer{buffer_}
{
   if (!buffer) {
      buffer = new double[std::min(m_B * n_A, m_A * n_B)];
      owns_buffer = true;
   }
}

KroneckerOperator::KroneckerOperator(mfem::SparseMatrix& A_,
                                     mfem::SparseMatrix& B_,
                                     double* buffer_ /*nullptr*/)
    : KroneckerOperator(A_, B_, false, false, buffer_)
{ }

KroneckerOperator::~KroneckerOperator()
{
   if (owns_buffer)
      delete[] buffer;
   if (owns_A)
      delete A;
   if (owns_B)
      delete B;
}

void
KroneckerOperator::TensorMult(const mfem::Vector& x,
                              mfem::Vector& y,
                              Transposed t) const
{
   if (t == transposed) {
      // Mult
      double* x_data = const_cast<double*>(x.GetData());
      const mfem::DenseMatrix X(x_data, B->Width(), A->Width());
      mfem::DenseMatrix Y(y.GetData(), B->Height(), A->Height());
      KroneckerMV(Transposed::NON_TRANSPOSED, 1.0, *A, *B, X, 0.0, buffer, Y);
   } else {
      // MultTransposed
      double* x_data = const_cast<double*>(x.GetData());
      const mfem::DenseMatrix X(x_data, B->Height(), A->Height());
      mfem::DenseMatrix Y(y.GetData(), B->Width(), A->Width());
      KroneckerMV(Transposed::TRANSPOSED, 1.0, *A, *B, X, 0.0, buffer, Y);
   }
}
void
KroneckerOperator::AssembleDiag() const
{

   MFEM_VERIFY(height == width,
               "KroneckerOperator must be square, not height = "
                   << height << ", width = " << width);
   if (!diag) {
      diag = new mfem::Vector(height);
   } else {
      diag->SetSize(height);
   }
   if (m_A == n_A) {
      mfem::Vector diag_A{m_A};
      mfem::Vector diag_B{m_B};
      mfem::Vector tmp;
      A->GetDiag(diag_A);
      B->GetDiag(diag_B);
      for (size_t i = 0; i < m_A; ++i) {
         tmp.SetDataAndSize(diag->GetData() + i * m_B, m_B);
         tmp = diag_B;
         tmp *= diag_A[i];
      }
   } else { // non diag blocks
      int i_A, j_A, i_B, j_B;
      for (size_t l = 0; l < height; ++l) {
         i_A = l / n_A;
         j_A = l / m_A;
         i_B = l % n_A;
         j_B = l % m_A;
         diag->Elem(l)
             = static_cast<const mfem::SparseMatrix*>(A)->Elem(i_A, j_A)
               * static_cast<const mfem::SparseMatrix*>(B)->Elem(i_B, j_B);
      }
   }
}

void
KroneckerOperator::Assemble() const
{
   if (mat){
      delete mat;
   }
   mfem::Array<int> offsets_i{m_A + 1};
   mfem::Array<int> offsets_j{n_A + 1};

   for (int i = 0; i < m_A + 1; ++i) {
      offsets_i[i] = i * m_B;
   }
   for (int j = 0; j < n_A + 1; ++j) {
      offsets_j[j] = j * n_B;
   }
   mfem::BlockMatrix block{offsets_i, offsets_j};
   block.owns_blocks = true;
   const int* Ip = A->GetI();
   const int* Jp = A->GetJ();
   for (int i = 0; i < m_A; ++i) {
      for (int j = Ip[i]; j < Ip[i + 1]; ++j) {
         mfem::SparseMatrix* tmp = new mfem::SparseMatrix(*B);
         *tmp *= (*A)(i, Jp[j]);
         block.SetBlock(i, Jp[j], tmp);
      }
   }
   mat = block.CreateMonolithic();
}
// KroneckerOperator::KroneckerOperator (const KroneckerOperator &other)
//     : TensorOperator (other),
//    A{ other.owns_A ? nullptr : other.A},
//    B{ other.owns_B ? nullptr : other.B},
//    owns_A{ other.owns_A },
//    owns_B{ other.owns_B },
//    m_A{ other.m_A },
//    m_B{ other.m_B },
//    n_A{ other.n_A },
//    n_B{ other.n_B },
//    buffer{ other.owns_buffer ? nullptr : other.buffer},
//    owns_buffer{ other.owns_buffer}
// {
//    mfem::out << "Kronecker Copy Constructor called \n";
//    if (owns_buffer)
//       buffer = new double[std::min (m_B * n_A, m_A * n_B)];
//    if (owns_A)
//       A = new mfem::SparseMatrix (*other.A);
//    if (owns_B)
//       B = new mfem::SparseMatrix (*other.B);
// }
//
// KroneckerOperator::KroneckerOperator (KroneckerOperator&& other) noexcept
//     : TensorOperator (std::move(other)),
//    owns_A{ other.owns_A },
//    owns_B{ other.owns_B },
//    m_A{ other.m_A },
//    m_B{ other.m_B },
//    n_A{ other.n_A },
//    n_B{ other.n_B },
//    owns_buffer{ other.owns_buffer}
// {
//    mfem::out << "Kronecker Move Constructor called \n";
//    std::swap(buffer, other.buffer);
//    std::swap(A, other.A);
//    std::swap(B, other.B);
//
// }

// void swap(KroneckerOperator& lhs, KroneckerOperator& rhs) noexcept
// {
//    using std::swap;
//    swap(lhs.A, rhs.A);
//    swap(lhs.B, rhs.B);
//    swap(lhs.owns_A, rhs.owns_A);
//    swap(lhs.owns_B, rhs.owns_B);
//    swap(lhs.m_A, rhs.m_A);
//    swap(lhs.m_B, rhs.m_B);
//    swap(lhs.n_A, rhs.n_A);
//    swap(lhs.n_B, rhs.n_B);
//    swap(lhs.buffer, rhs.buffer);
//    swap(lhs.owns_buffer, rhs.owns_buffer);
// }
} // namespace tensor
