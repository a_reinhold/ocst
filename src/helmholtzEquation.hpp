#ifndef OCST_HELMHOLTZ_EQUATION
#define OCST_HELMHOLTZ_EQUATION

#include<mfem.hpp>
#include<ocstMultigrid.hpp>

namespace ocst{

/**
 * @brief Operator \f$ B_\alpha \f$ for the real valued Iteration
 *
 * \f$B_\alpha \f$ operator for the real valued iteration (cf. Axelson Kucherov
 * ,2000) 
 *
 * \f[
 * B_\alpha = R + \alpha  S = R + \alpha  \gamma  \tilde{S} 
 * \f]
 * 
 * \f$\alpha \f$ is the scalar parameter from the real valued iteration (usually
 *  \f$\alpha \f$ = 1.0). 
 * \f$ \gamma \f$ is used as we have a scaled matrix for the imaginary part of 
 * the helmholtz equation
 */
class RealValuedIterationB_alpha :public::mfem::Operator{
private:
public:
   mfem::SparseMatrix* R;
   mfem::SparseMatrix* S;
   const double alpha; //actually we use gamma * alpha s.t R == M 
   double gamma; 
   double gamma_alpha;
public:
   RealValuedIterationB_alpha(mfem::SparseMatrix& R, mfem::SparseMatrix& S,
                              const double alpha, double gamma); 

   void
   SetGamma(double gamma_){gamma=gamma_;}

   void                                                                          
   Mult(const mfem::Vector& x, mfem::Vector& y) const override;

   void                                                                          
   MultTranspose(const mfem::Vector& x, mfem::Vector& y) const override
   {Mult(x,y);} 
};

/**
 * @brief Operator \f$ C_\alpha \f$ for the real valued Iteration
 *
 * \f$C_\alpha \f$ operator for the real valued iteration (cf. Axelson Kucherov
 * ,2000) 
 *
 * \f[
 * C_\alpha = R - \alpha  S + \sqrt{1+ alpha^2} S B_\alpha ^{-1} S  
 *          = R - \alpha \gamma  \tilde{S} 
 *            + \sqrt{1+ alpha^2} \ \gamma^2  \ \tilde{S} B_\alpha ^{-1} \tilde{S} 

 * \f]
 * 
 * \f$\alpha \f$ is the scalar parameter from the real valued iteration (usually
 *  \f$\alpha \f$ = 1.0). 
 * \f$ \gamma \f$ is used as we have a scaled matrix for the imaginary part of 
 * the helmholtz equation
 */
class RealValuedIterationC_alpha : public mfem::Operator{
private:
   mfem::SparseMatrix* R;
   mfem::SparseMatrix* S;
   const double alpha; //actually we use gamma * alpha s.t R == M 
   double gamma; 

   mfem::Solver* solve_B_alpha;
   mutable mfem::Vector tmp; 

public:
   RealValuedIterationC_alpha(mfem::SparseMatrix& R, mfem::SparseMatrix& S,
                              const double alpha, double gamma, 
			      mfem::Solver& solve_B_alpha); 
   void
   SetGamma(double gamma_){gamma=gamma_;}

   void                                                                          
   Mult(const mfem::Vector& x, mfem::Vector& y) const override;

   void                                                                          
   MultTranspose(const mfem::Vector& x, mfem::Vector& y) const override
   {Mult(x,y);} 
};

/**
 * @brief Real Valued Iteration to solve \f$ R + i \gamma \tilde{S} = f \f$
 *
 * Implementation of the method described in "Real valued iterative methods for 
 * solving complex symmetric linear systems" by O.Axelson and A.Kucherov (2000)
 *
 * Method for solving the complex system \f$ R + i \gamma \tilde{S} = f \f$ 
 * where R and S are symmetric sparse matrices. The method does not require
 * complex operations. We use only the first steps of the operation since we are
 * only interested in the real part of the solution. We also only support real 
 * vectors as the right hand side
 *
 * To solve this system we can
 * use a preconditioned cg method with a conditon number \f$ \kappa \leq 2\f$
 * To achieve that we need to apply a sparse s.p.d matrix as a preconditioner, 
 * here we can use the efficient multigrid  preconditioned cg method.
 */
class SolveHelmholtzRealValued: public mfem::Operator{
private:
   int K;
   int k=0; 
   int levels;
   static constexpr double alpha = 1.0; 
   mfem::Vector* gamma;
   mfem::DenseMatrix evs_smoother;
   mutable mfem::Vector tmp;
   mfem::SparseMatrix* R;
   mfem::SparseMatrix* S;
   mfem::Array<RealValuedIterationB_alpha*> B_alpha;
   ocst::GeometricMultigrid* prec_B;
   ocst::SolveMGPCG* solve_B;
   RealValuedIterationC_alpha* C_alpha; 
   mfem::CGSolver* solver;
public:

   /**
    * For every value in gamma the eigenvalue estimates for the smoother for 
    * \f$ B_\alpha \f$ are computed via the power method and stored in the class
   */
   SolveHelmholtzRealValued(mfem::Array<mfem::SparseMatrix*>& M, 
                            mfem::Array<mfem::SparseMatrix*>& A,
			    mfem::FiniteElementSpaceHierarchy& hierarchy,
			    mfem::Vector& gamma);
   ~SolveHelmholtzRealValued();
   
   /**
    * Set the operator to the kth value of the vector \f$ \gamma \f$, sets the 
    * precondtioner eigenvalue estimates (which are precomputed) 
    */
   void
   SetTimeStep(int k_); 

   /**
    * @brief solves the real part of the equation \f$ (R+ i \gamma S) y = x \f$
    *
    * \f$ x\f$ is a real vector!
    */
   void                                                                          
   Mult(const mfem::Vector& x, mfem::Vector& y) const override;
   
   /**
    * @brief Set parameters for the iterative solver
    *
    * @param tol: is the absolute tolerance for the pcg method for \f$C_\alpha\f$
    * the tolerances for the solver \f$B_\alpha \f$ are set relative to this 
    * tolerance
    */
   void
   SetSolverParameters(double tol, int iter_main = 10, int iter_prec = 20);

};

/**
 * @brief Real part of the normal equation of the helmholtz operator 
 * \f$ (A+ i \sqrt{\gamma} M)^H (A + i \sqrt{\gamma} M) \f$
 *
 * the real part is given by \f$ A^2 + \gamma M^2 \f$
 *
 */
class HelmholtzNormalReal : public mfem::Operator{
private:
   mfem::SparseMatrix* M;
   mfem::SparseMatrix* A;
   mutable mfem::Vector tmp;
   double gamma;
public:
HelmholtzNormalReal(mfem::SparseMatrix& M, mfem::SparseMatrix& A, double gamma);              

void 
SetGamma(double gamma_){gamma=gamma_;}

void                                                                          
Mult(const mfem::Vector& x, mfem::Vector& y) const override;

};                

/**
 * @brief Solve the helmholtz equation (real part) by solving the normal equation
 * with cg
 *
 * does not work well for ill conditioned helmholtz operator since the condition 
 * number gets squared
 */
class SolveHelmholtzNormalMG: public mfem::Operator{
private:
   int K;
   int k=0; 
   int levels;
   mfem::Vector* evs_time;
   mfem::DenseMatrix evs_smoother;
   mfem::Array<HelmholtzNormalReal*> ops;
public:
   ocst::GeometricMultigrid* prec;
   ocst::SolveMGPCG* solver;
public:
   SolveHelmholtzNormalMG(mfem::Array<mfem::SparseMatrix*>& M, 
                          mfem::Array<mfem::SparseMatrix*>& A,
			  mfem::FiniteElementSpaceHierarchy& hierarchy,
			  mfem::Vector& evs_time);
   ~SolveHelmholtzNormalMG();
   
   void
   SetTimeStep(int k_); 

   void                                                                          
   Mult(const mfem::Vector& x, mfem::Vector& y) const override
   {solver->Mult(x,y);}

};

/**
 * @brief real valued representation of the helmholtz operator 
 * \f$ A + i \sqrt{\gamma} M \f$
 */
class HelmholtzReal : public mfem::Operator{
private:
   mfem::SparseMatrix* M;
   mfem::SparseMatrix* A;
   double sqrt_gamma;
   mutable mfem::Vector x_re;
   mutable mfem::Vector x_im;
   mutable mfem::Vector y_re;
   mutable mfem::Vector y_im;
public:
   HelmholtzReal(mfem::SparseMatrix& M, mfem::SparseMatrix& A, 
                 double sqrt_gamma);

   void
   SetGamma(double sqrt_gamma_){sqrt_gamma=sqrt_gamma_;}

   void
   Mult(const mfem::Vector& x, mfem::Vector& y) const override;
};

} //ocst
#endif //OCST_HELMHOLTZ_EQUATION
