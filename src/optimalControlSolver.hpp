#ifndef _OCST_OPTIMAL_CONTROL_SOLVER_HPP
#define _OCST_OPTIMAL_CONTROL_SOLVER_HPP

#include <optimalControlProblem.hpp>
#include <spaceTimeData.hpp> 
#include <spaceTimeSolver.hpp>

#include <cmath>
#include <iostream> 

namespace ocst{

/**
 * @brief Projected gradient method fo problem with box constrains
 * 
 */
class ProjectedGradientMethod{
private:
   ocst::OptimalControlProblem* ocp; 
   ocst::SpaceTimeSolver* lin_solver; 

   const int max_it; 
   const double s_0, s_min, tol_rel, tol_abs, tol_stag;
   int nof_evaluations = 0; 
   mfem::Array<double> s;
   mfem::Array<double> J;

   double* buffer_1,* buffer_2; 
   
   ocst::Control u_; 
   ocst::SpaceTimeData v;
   ocst::Control diff;

   
public:
   ProjectedGradientMethod(ocst::OptimalControlProblem& ocp,
                           ocst::SpaceTimeSolver& lin_solver,
			   const int max_it, 
			   const double s_0 , 
			   const double s_min = 1E-4, 
			   const double tol_rel  = 1E-4, 
			   const double tol_abs  = 1E-6, 
			   const double tol_stag = 1E-6);
   
   ProjectedGradientMethod(const ProjectedGradientMethod&) = delete;
   ~ProjectedGradientMethod();

   /**
    * @brief solve the OC Problem
    *
    */
   void
   Solve(); 

private:
   /**
   * @brief Project values of the control on to the feasble set
   *
   */
   void
   Projection();
   /**
   * @brief determine search direction
   *
   * expensive if the adjoint state and the control have different basis 
   * functions
   */
   void 
   SearchDirection();
public:
   void 
   PrintResults();
};

}
#endif //_OCST_OPTIMAL_CONTROL_SOLVER_HPP
