#ifndef OCST_NORM_INDUCING_OPERATORS
#define OCST_NORM_INDUCING_OPERATORS

#include <helmholtzEquation.hpp>
#include <mfem.hpp>
#include <ocstMultigrid.hpp>
#include <spaceTimeData.hpp>
#include <tensor/linalg_auxiliary.hpp>

#ifdef _OCST_USE_INTEL_MKL
#include <mkl.h>
#define _OCST_BLAS
#define _OCST_LAPACK
#endif

#ifdef _OCST_USE_OPENBLAS
#include <cblas.h>
#include <lapacke.h>
#define _OCST_BLAS
#define _OCST_LAPACK
#endif

#ifdef _OCST_USE_ACCELERATE
#include <Accelerate/Accelerate.h>
#define _OCST_BLAS
#define _OCST_LAPACK
#endif

#ifdef _OCST_USE_BREW_LAPACK
#include <Accelerate/Accelerate.h>
#include <lapacke.h>
#define _OCST_BLAS
#define _OCST_LAPACK
#endif

namespace ocst {

class InvNormOperatorTest;  // fwd declaration
class InvNormOperatorTrial; // fwd declaration

/**
 * @brief Abstract Base Class for norm Inducing Operators
 */
class NormOperator : public mfem::Operator {
protected:
   mfem::SparseMatrix* M_time;
   mfem::SparseMatrix* M_space;
   mfem::SparseMatrix* V_space;
   int K;
   int n_h;

   double* buffer;
   bool own_buf;

   mfem::Operator* Inv = nullptr;
   bool own_Inv = false;

public:
   NormOperator(int size,
                mfem::SparseMatrix& M_time,
                mfem::SparseMatrix& M_space,
                mfem::SparseMatrix& V_space,
                int K,
                int n_h,
                double* buffer = nullptr);
   ~NormOperator();

   /**
    * @brief set any mfem::Operator to approximate the inverse, not owned
    */
   void SetInverseOperator(mfem::Operator& solver);

   mfem::Operator*
   GetInverseOperator()
   {
      return Inv;
   }

   virtual void Mult(const mfem::Vector& x, mfem::Vector& y) const override
       = 0;

   void
   MultTranspose(const mfem::Vector& x, mfem::Vector& y) const override
   {
      Mult(x, y);
   }

   /**
    * solves \f$ M y=x \f$
    */
   void MultInverse(const mfem::Vector& x, mfem::Vector& y) const;

   void Mult(const ocst::SpaceTimeData& x, ocst::SpaceTimeData& y) const;

   void
   MultTranspose(const ocst::SpaceTimeData& x, ocst::SpaceTimeData& y) const
   {
      Mult(x, y);
   }

   void MultInverse(const ocst::SpaceTimeData& x,
                    ocst::SpaceTimeData& y) const;
};

/**
 * @brief Norminducing Operator on the test Space
 *
 * Norm inducing operator for the discrete subspace of the test space
 * \f$ L_2(I,V) \times L_2(\Omega) \f$ equipped with the norm:
 * \f$ ||v_\mathrm{I}||_{L_2(I,V)} + ||v||_{L_2(\Omega)} \f$
 *
 * The operator has the form:
 * \f[ \begin{pmatrix}  M_t \otimes V_h  \\   & M_h  \end{pmatrix} \f]
 */
class NormOperatorTest : public ocst::NormOperator {
public:
   NormOperatorTest(mfem::SparseMatrix& M_time,
                    mfem::SparseMatrix& M_space,
                    mfem::SparseMatrix& V_space,
                    double* buffer = nullptr);
   using NormOperator::Mult;

   /**
    * Set owned InvOperatorTest to approximate the Inverse
    */
   void SetInverseOperator(mfem::Operator& solve_V, mfem::Operator& solve_M);

   void Mult(const mfem::Vector& x, mfem::Vector& y) const override;
};

/**
 * @brief Operator that computes the inverse of the norm inducing operator of
 * the test space
 *
 * Since the time matrix is a diagonal matrix we invert this matrix directly
 * and solve for each time step one system in the space dimension with the
 * multigrid preconditioned cg method
 */
class InvNormOperatorTest : public mfem::Operator {
private:
   mfem::SparseMatrix M_time_inv;
   mfem::Operator* solve_V;
   mfem::Operator* solve_M;
   int K;
   int n_h;

   double* buffer;

public:
   InvNormOperatorTest(const mfem::SparseMatrix& M_time,
                       mfem::Operator& solve_V,
                       mfem::Operator& solve_M,
                       int K,
                       int n_h,
                       double* buffer);

   void Mult(const mfem::Vector& x, mfem::Vector& y) const override;

   void Mult(const ocst::SpaceTimeData& x, ocst::SpaceTimeData& y) const;
};

/**
 * @brief Norm inducing operator for the trial space
 *
 * Norm inducing operator for the discrete subspace of the trial space
 * \f$ H^1(I; H^1(\Omega)) \cap L_2(I; H^{-1}(\Omega))) \f$ equipped with the
 * norm
 * \f$ ||v|| = ||v||_{L_2(I;H^1(\Omega)} +
 * ||\dot{v}||_{L_2(I;H^{-1}(\Omega))} \f$
 *
 * The operator has the form
 * \f[ M_t \otimes V_h + A_t \otimes M_h V_h^{-1} M_h  \f]
 */
class NormOperatorTrial : public NormOperator {
private:
   mfem::SparseMatrix* A_time;
   mfem::Operator* solve_V;

   bool evalEndTime;

public:
   using NormOperator::Mult;
   using NormOperator::SetInverseOperator;

   NormOperatorTrial(mfem::SparseMatrix& M_time,
                     mfem::SparseMatrix& A_time,
                     mfem::SparseMatrix& M_space,
                     mfem::SparseMatrix& V_space,
                     mfem::Operator& solve_V,
                     bool evalEndTime = true,
                     double* buffer = nullptr);

   void Mult(const mfem::Vector& x, mfem::Vector& y) const override;

   /**
    * @brief Set owned InvNormOperatorTrial
    */
   void SetInverseOperator(mfem::Array<mfem::SparseMatrix*>& M_space,
                           mfem::Array<mfem::SparseMatrix*>& A_space,
                           mfem::FiniteElementSpaceHierarchy& hierarchy);
};

/**
 * @brief Operator to approximate the Inverse of the norm inducing operators
 * of the trial space
 *
 * This method is implemented as proposed by R.Andreev in his phd thesis (2012)
 * and a paper: "Space-time discretization of the heat equation, A concise
 * Matlab implementation" (2013)
 *
 * To solve the equation first the time matrices are diagonalized via a general
 * eigenvalue decomposition and a full (!!) matrix with all eigenvectors is
 * stored then for every time step the helmholt equation is solved with a
 * different complex frequency is solved.
 */
class InvNormOperatorTrial : public mfem::Operator {
private:
   const int dim_time;
   const int dim_space;
   mfem::DenseMatrix V_time;
   mfem::Vector gamma_time;

   SolveHelmholtzRealValued* solve_hh = nullptr;

   mutable ocst::SpaceTimeData tmp;

public:
   InvNormOperatorTrial(mfem::SparseMatrix& M_time,
                        mfem::SparseMatrix& A_time,
                        mfem::Array<mfem::SparseMatrix*>& M_space,
                        mfem::Array<mfem::SparseMatrix*>& A_space,
                        mfem::FiniteElementSpaceHierarchy& hierarchy,
                        double* buffer);
   ~InvNormOperatorTrial();

   void Mult(const mfem::Vector& x, mfem::Vector& y) const override;

   void
   SetTolerance(double tol)
   {
      solve_hh->SetSolverParameters(tol);
   }
};
} // ocst

#endif // OCST_NORM_INDUCING_OPERATORS
