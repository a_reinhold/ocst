#ifndef OCST_OPTIMAL_CONTROL_COEFFICIENTS
#define OCST_OPTIMAL_CONTROL_COEFFICIENTS

#include <mfem.hpp>
#include <spaceTimeData.hpp>
#include <tensor/linalg_auxiliary.hpp>

#include <string>

namespace ocst{

/**
 * @brief Base class for nodal values of state control and adjoint state and 
 * control
 *
 */
class OptimalControlCoefficients: public SpaceTimeData{
protected:
   mfem::SparseMatrix* m_time = nullptr;  
   mfem::SparseMatrix* m_space = nullptr;
   bool owns_m_time = false;
   bool owns_m_space = false;
   mfem::FiniteElementSpace* fes_time= nullptr;
   mfem::FiniteElementSpace* fes_space= nullptr;

public:
   OptimalControlCoefficients(mfem::FiniteElementSpace&, 
                              mfem::FiniteElementSpace&,
			      int dof_time);

   OptimalControlCoefficients(mfem::FiniteElementSpace&, 
                              mfem::FiniteElementSpace&);
   
   /**
    * @brief Copy Constructor
    *
    * deep copy of the data, inner product matrices are pointing to those from 
    * orig, caller has to make sure that they are not used if owned by orig and 
    * gone out of scope
    */
   OptimalControlCoefficients(const OptimalControlCoefficients&);
   
   ~OptimalControlCoefficients();


   /**
    *@brief set (not owned) inner product matrix, 
    */
   void
   SetM_time(mfem::SparseMatrix&);

   /**
    *@brief set owned inner product matrix according to own fes 
    */
   void
   SetM_space(mfem::SparseMatrix&);

   /**
    *@brief set owned inner product matrix according to own fes 
    */
   void
   SetM_time();

   /**
    *@brief set owned inner product matrix according to own fes 
    */
   void
   SetM_space(); 

   /**
    * @brief set value for all timesteps from a mfem coefficient
    */
   void 
   SetValue(mfem::Coefficient& value);

   const mfem::SparseMatrix&
   M_time() const;

   mfem::SparseMatrix&
   M_time();

   const mfem::SparseMatrix&
   M_space() const;

   mfem::SparseMatrix&
   M_space();

   mfem::FiniteElementSpace&
   FES_Time();

   mfem::FiniteElementSpace&
   FES_Space();
   /**
    * @brief Inner product norm: \f$ ||x||^2 = x^T (M_\mathrm{time} \otimes 
    * M_\mathrm{space}) x \f$
    *
    * buffersizes need to be the same size as x
    */
   double 
   SquaredInnerProductNorm(double* buffer_1, double* buffer_2);

   /**
    * @brief Inner product norm: 
    * \f$ ||x-\tilde{x}||^2 = (x-\tilde{x})^T (M_\mathrm{time} \otimes 
    * M_\mathrm{space}) (x-\tilde{x} \f$
    *
    * buffersizes need to be the same size as x
    */
   double 
   SquaredInnerProductNorm(const ocst::SpaceTimeData& x_,
                           double* buffer_1, double* buffer_2);

   /**
    * @brief Inner product norm: \f$ ||x||^2 = x^T (M_\mathrm{time} \otimes 
    * M_\mathrm{space}) x \f$
    *
    * buffers are created end deleted locally 
    */
   double 
   SquaredInnerProductNorm();

//   /**
//    * @brief export current values to a given paraview data object
//    *
//    * default behaviour: all values are exported, if nof_timesteps is given only
//    * \f$ \left \lceil \frac{\mathrm{dof\_time}}{\mathrm{nof\_timesteps}} 
//    * \right \rceil \f$ are  printed, not when the time mesh is not sorted the 
//    * output values are not  evenly distribuited
//    *
//    * @param name new field of this name will be generated
//    * @param fes_time needs to match dof_time 
//    * @param fes_space needs to match dof_space 
//    * @param nof_timesteps default value zero does print all
//    */
//   void
//   ExportToParaview(mfem::ParaViewDataCollection&, 
//		    const std::string&  name,
//                    mfem::FiniteElementSpace& fes_time, 
//		    mfem::FiniteElementSpace& fes_space,
//                    const int nof_timesteps = 0);
protected:
   mfem::SparseMatrix*
   SetSparseMatrix(mfem::FiniteElementSpace& fes,
                   bool& ownership );

   mfem::SparseMatrix* 
   SetSparseMatrix(mfem::FiniteElementSpace& trial, 
                   mfem::FiniteElementSpace& test, 
                   bool& ownership );

};

/**
 * @brief Control of an optimal control problem
 *
 * The default behaviour is that it is assumed that the FiniteElementSpace for 
 * the space dimension is the same as in test and trial space and the 
 * time space is the one from the test space.
 * 
 * It is possible to use different FiniteElementSpaces for space and time 
 */
class Control: public OptimalControlCoefficients{
private:
   bool fes_time_is_conform;
   bool fes_space_is_conform; 
   mfem::SparseMatrix* m_control_time = nullptr;
   bool owns_m_control_time = false;
   mfem::SparseMatrix* m_control_space = nullptr;
   bool owns_m_control_space = false;
public:
   Control(mfem::FiniteElementSpace& fes_time, 
           mfem::FiniteElementSpace& fes_space,
	   const bool fes_time_is_conform = true,
	   const bool fes_space_is_conform = true);

   ~Control();
   
   /**
    * @brief Copy Constructor
    *
    * deep copy of the data, inner product matrices are pointing to those from 
    * orig, caller has to make sure that they are not used if owned by orig and 
    * gone out of scope
    */
   Control(const Control&);

   void 
   SetM_control_time(mfem::SparseMatrix&);

   void 
   SetM_control_space(mfem::SparseMatrix&);

   void 
   SetM_control_time(mfem::FiniteElementSpace&);

   void 
   SetM_control_space(mfem::FiniteElementSpace&);

   const mfem::SparseMatrix&
   M_control_time() const;

   mfem::SparseMatrix&
   M_control_time();

   const mfem::SparseMatrix&
   M_control_space() const;

   mfem::SparseMatrix&
   M_control_space();

   const bool
   ConformityTime() const {return fes_time_is_conform;}

   const bool
   ConformitySpace() const {return fes_space_is_conform;}
};

/**
 * @brief representation of the state of the optimal control problem
 */
class State: public OptimalControlCoefficients{
public:
   State(mfem::FiniteElementSpace& fes_time, 
         mfem::FiniteElementSpace& fes_space);


   /**
   * @brief Set initial value y_0 
   *
   * Set from mfem coefficient,
   * not neccesarry for most solvers but can help with convergence
   */
   void
   SetInitialValue(mfem::Coefficient& init);

   /**
   * @brief Set initial value y_0 
   *
   * Set from to constant double value,
   * not neccesarry for most solvers but can help with convergence
   */
   void
   SetInitialValue(const double init);
};

/**
 * @brief representation of the adjoint state of the OC problem
 *
 * The state consosts of two parts: \f$ (p_I, p_{II}) \f$ 
 * Inner product matrix assignement in the base class makes no sense here! 
 *
 */
class AdjointState: public OptimalControlCoefficients{
private:
   mfem::DenseMatrix part_I; // doesnt own data, just a view of 
                             // the part from the whole matrix
public:
   AdjointState(mfem::FiniteElementSpace& fes_time, 
                mfem::FiniteElementSpace& fes_space);

   const mfem::DenseMatrix& 
   AdjointState_I() const;

   mfem::DenseMatrix& 
   AdjointState_I();
};

} //ocst



#endif //OCST_OPTIMAL_CONTROL_COEFFICIENTS

