#include <postProcessing.hpp> 

namespace ocst{

BenchTiming::
BenchTiming(unsigned int nof_fields):
   start(nof_fields, Clock::now()),
   nof_evals(nof_fields, 0),
   time_values(nof_fields, 0.0),
   started(nof_fields, false)
{    }

void
BenchTiming::
AddField()
{
   start.push_back(Clock::now());
   nof_evals.push_back(0);
   time_values.push_back(0.0);
   started.push_back(false);
}

void
BenchTiming::
Start(unsigned int index)
{
   started[index] = true; 
   start[index] = Clock::now();
}

void
BenchTiming::
End(unsigned int index)
{
   if(started[index]){
      elapsed_time = Clock::now() -start[index];
      started[index] = false;
      nof_evals[index] += 1;
      time_values[index] += elapsed_time.count(); 
   } else {
      std::cout << ">>>Warning, BenchTiming called w.o. corresponding start" <<
      std::endl; 
   }
}

double
BenchTiming::
GetTime(unsigned int index)
{
   if (nof_evals[index] == 0){
      return NAN; 
   } else {
      return time_values[index]/double(nof_evals[index]); 
   }
}

double
BenchTiming::
PopTime(unsigned int index)
{
   double res = GetTime(index);
   nof_evals[index] = 0;
   time_values[index] = 0.0; 
   return res; 
}
double
BenchTiming::
GetCumulatedTime(unsigned int index)
{
   return time_values[index];
}

void
BenchData::
RegisterField(std::string name, Type t, int print_pos /*= -1*/)
{
   int pos;
   if (print_pos > 0) {
      pos = print_pos;
   } else {
      pos = print_order.empty() ? 10 : print_order.rbegin()->first + 10;
   }
   print_order.insert({{pos, name}});
   data_types.insert({{name, t}});
   switch (t) {
      case Int:
	 int_vectors.insert({{name, std::vector<int>()}});
	 break;
      case Double:
	 double_vectors.insert({{name, std::vector<double>()}});
	 break;
      default:
	 MFEM_ABORT("Type not known!");
   }
}

void
BenchData::
PrintHeader()
{
   for (auto x: print_order) {
      std::cout << x.second << "\t" ; 
   }
   std::cout << std::endl; 
}

void 
BenchData::
PrintLine(unsigned int index)
{
   MFEM_VERIFY(index<nof_lines, 
               "index = " << index << " >= nof_lines " << nof_lines); 
   for (auto x: print_order) {
      Type t = data_types[x.second];
      switch (t) {
	 case Int:
	    if (index < int_vectors[x.second].size()){
	       std::cout << int_vectors[x.second][index] << "\t";
	    } else {
	       std::cout << "NaN\t";
	    }
	    break;
	 case Double: 
	    if (index < double_vectors[x.second].size()){
	       std::cout << double_vectors[x.second][index] << "\t";
	    } else {
	       std::cout << "NaN\t";
	    }
	    break;
	 default:
	    MFEM_ABORT("Type not known!");
      }
   }
   std::cout << std::endl; 
}

void
BenchData::
PrintData(bool print_header)
{
   if (print_header) PrintHeader();
   else std::cout << std::endl; 
   for(int l=0; l<nof_lines; ++l){
      PrintLine(l);
   }
}

void 
ExportToParaview(mfem::ParaViewDataCollection* pd, std::string name, 
                 mfem::DenseMatrix& mat,
                 mfem::FiniteElementSpace& fes_space,
		 mfem::FiniteElementSpace* fes_time,
		 std::vector<int>* indices)
{
   mfem::GridFunction* time_steps =nullptr;
   if (fes_time){
      time_steps = new mfem::GridFunction(fes_time);
      mfem::FunctionCoefficient identity([](const mfem::Vector& t)
	                                              { return t(0);});
      time_steps->ProjectCoefficient(identity);
   } 

   mfem::GridFunction out(&fes_space, mat.GetData());
   pd->RegisterField(name, &out);

   double time = 0.0;
   if (indices){
      for (auto i: *indices){
	 int k = i;
	 time = (time_steps) ? time_steps->Elem(k) : double(k);
	 pd->SetCycle(k);
	 pd->SetTime(time);
	 out.SetData(mat.GetColumn(k));
	 pd->Save();
      }
   } else {
      for (int k=0; k<mat.Width(); ++k){
	 time = (time_steps) ? time_steps->Elem(k) : double(k);
	 pd->SetCycle(k);
	 pd->SetTime(time);
	 out.SetData(mat.GetColumn(k));
	 pd->Save();
      }
   }

   if (time_steps) delete time_steps;
}

void
Export1DSurfData(mfem::FiniteElementSpace& fes_time, 
                 mfem::FiniteElementSpace& fes_space, 
                 mfem::DenseMatrix& data, 
		 std::ostream&  out /* = std::cout*/, 
		 std::string name /* ="n.a."*/)
{
   mfem::GridFunction nodes_time(&fes_time);
   mfem::GridFunction nodes_space(&fes_space);

   mfem::FunctionCoefficient identity(
      [](const mfem::Vector& xi){return xi(0);}
   );
   nodes_time.ProjectCoefficient(identity);
   nodes_space.ProjectCoefficient(identity);
   
   out << "# mesh_time" << std::endl;
   nodes_time.Print(out);
   out << "# mesh_space" << std::endl;
   nodes_space.Print(out);
   out << "# data "<< name << std::endl;
   data.Print(out);
}
//void 
//ExportToParaview(mfem::ParaViewDataCollection* pd, std::string& name, 
//                 mfem::DenseMatrix& mat,
//                 mfem::FiniteElementSpace& fes_space,
//		 mfem::FiniteElementSpace& fes_time,
//		 std::vector<int>* indices /*= nullptr*/)
//{
//   ExportToParaview(pd, name, mat, fes_space, indices, &fes_time);
//}
//void
//ExportToParaview(mfem::ParaViewDataCollection* pd, std::string name, 
//                 mfem::DenseMatrix& mat,
//                 mfem::FiniteElementSpace& fes_space,
//		 std::vector<int>* indices /* = nullptr*/)
//{
//   ExportToParaview(pd, name, mat, fes_space, indices, nullptr);
//}
} //ocst
