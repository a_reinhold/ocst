#ifndef OCST_MULTIGRID_HPP
#define OCST_MULTIGRID_HPP


#include <mfem.hpp> 
#include <tensor/tensor.hpp>
namespace ocst{
/**
 * @brief Prolongation/restriction as a sparse matrix
 */
class SparseProlongation: public mfem::Operator{
private:
   mfem::SparseMatrix* mat;
public:
   /**
    * Coarse and fine need to be compatible (mesh refinement in space or 
    * different order) 
    */
   SparseProlongation(mfem::FiniteElementSpace& coarse, 
                      mfem::FiniteElementSpace& fine); 
   ~SparseProlongation(){ delete mat; }
   
   /**
    *@brief  Prolongation
    */
   void
   Mult(const mfem::Vector& x, mfem::Vector& y) const override
   { mat->Mult(x,y); }
   /**
    *brief Restriction
    */
   void
   MultTranspose(const mfem::Vector& x, mfem::Vector& y) const override
   { mat->MultTranspose(x,y); }

   mfem::SparseMatrix&
   SpMat(){return *mat;}

   const mfem::SparseMatrix&
   SpMat()const {return *mat;}
};

/**
 * @brief Porlongation in space and time 
 *
 * Kronecker type oerator for a prolongation between 2 tensor spaces
 */
class SpaceTimeProlongation: public mfem::Operator{
private:
   ocst::SparseProlongation* prol_time = nullptr;
   ocst::SparseProlongation* prol_space = nullptr;
public:
   enum Mode{NORMAL, ONLY_SPACE, ONLY_TIME};
private: 
   const Mode mode; 

   int dim_time_coarse;
   int dim_time_fine;

   int dim_space_coarse;
   int dim_space_fine;

   double* buffer; 

public:
   /**
    * @brief Construct Prolongation for space and time, mode is set to NORMAL
    */
   SpaceTimeProlongation(ocst::SparseProlongation& prol_time, 
                         ocst::SparseProlongation& prol_space,
			 double* buffer = nullptr); 
   /**
    * @brief Construct Prolongotaion only in space or time depending on MODE
    */
   SpaceTimeProlongation(ocst::SparseProlongation& prol, 
			 Mode mode,int other_dim, double* buffer = nullptr); 

   void
   SetBuffer(double* buffer_){buffer = buffer_;}; 

   /**
    * @brief Prolongation
    */
   void 
   Mult(const mfem::Vector& x, mfem::Vector&y) const override;

   /**
    * @brief Restriction
    */
   void 
   MultTranspose(const mfem::Vector& x, mfem::Vector&y) const override;
};

/**
 * @brief FES Hierarchy in space and time (for ST multigrid) 
 */
class SpaceTimeFiniteElementSpaceHierarchy : 
      public mfem::FiniteElementSpaceHierarchy{
private:
   mfem::FiniteElementSpaceHierarchy hierarchy_time;
   mfem::FiniteElementSpaceHierarchy hierarchy_space;

   int levels;
   int levels_time;
   int levels_space; 

   double* buffer; 
   
public:
   enum Mode{NORMAL, ONLY_SPACE, ONLY_TIME};
   SpaceTimeFiniteElementSpaceHierarchy( mfem::FiniteElementSpace& fes_time, 
					int levels_time, 
                                        mfem::FiniteElementSpace& fes_space, 
					int levels_space, 
					double* buffer = nullptr);

   /**
    * @brief Adds a uniformly refined level in space and time
    */
   void 
   AddLevel(){AddLevel(NORMAL);} 

private:
   /**
    * @brief Adds a uniformly refined level depending on the mode
    *
    * Note levels only in space or time can only be added during construction, 
    * later the user can only add refinments in both levels
    */
   void
   AddLevel(Mode mode);

public:
   using mfem::FiniteElementSpaceHierarchy::AddLevel;  
   mfem::FiniteElementSpaceHierarchy&
   FESHierarchyTime() {return hierarchy_time;}

   mfem::FiniteElementSpaceHierarchy&
   FESHierarchySpace() {return hierarchy_space;}

   int 
   GetNumLevelsTime() const {return levels_time;}
   int 
   GetNumLevelsSpace() const {return levels_space;}
   
   mfem::FiniteElementSpace&
   GetFESTimeAtLevel(int i); 

   mfem::FiniteElementSpace&
   GetFESSpaceAtLevel(int i); 

   /**
    * @brief sets the buffer for all prolongations
    */
   void
   SetBuffer(double* buffer_); 

};

/**
 * @brief Geometric multigrid method for a system, using Chebyshev 
 * smoothing 
 */
class GeometricMultigrid: public mfem::GeometricMultigrid
{
public:
   enum class OperatorType{MATRIX_FREE, SPARSE, TENSOR};
private:
   OperatorType operator_type;
public:
   //TODO fix Documentation
   GeometricMultigrid(const mfem::FiniteElementSpaceHierarchy& hierarchy,
	              const mfem::Array<mfem::Operator*>& operators, 
		      OperatorType operator_type = OperatorType::MATRIX_FREE,
		      int order = 2);
   

   /**
    * @brief Version for operator where assembling a sparse matrix is possible
    * 
    * Generate the multigrid preconditioner for the given hierarchy and the 
    * given sparse matrices. 
    *
    * The diagonal of the matrix is used for the smoother and on the coarsest 
    * level we use a direct solver 
    * @param mat within the array the matrices must be ordered from coarse to 
    * fine
    */
   // GeometricMultigrid(const mfem::FiniteElementSpaceHierarchy& hierarchy,
	  //             const mfem::Array<mfem::Operator*>& operators, 
		 //      bool matrix_free=false, bool has_assemble=false, int order=2);
   /**
    * @brief Version for sparse matrix operator
    * 
    * Generate the multigrid preconditioner for the given hierarchy and the 
    * given sparse matrices. 
    *
    * The diagonal of the matrix is used for the smoother and on the coarsest 
    * level we use a direct solver 
    * @param mat within the array the matrices must be ordered from coarse to 
    * fine
    */
   [[deprecated]]
   GeometricMultigrid(const mfem::FiniteElementSpaceHierarchy& hierarchy,
	              const mfem::Array<mfem::Operator*>& operators, 
		      bool matrix_free, int order=2);
   /**
    * @brief Version for matrix free operators
    * 
    * Generate the Multigrid Preconditioner for the given hierarchy and the 
    * given operators
    *
    * The diagonal of the operator does not need to be known, on the coarsest 
    * level a cg method is used
    * 
    * @param mat within the array the matrices must be ordered from coarse to 
    * fine
    */
   GeometricMultigrid(const mfem::FiniteElementSpaceHierarchy& hierarchy,
    		      const mfem::Array<mfem::Operator*>& mat,
 		      mfem::Vector& max_evs,
		      OperatorType operator_type = OperatorType::MATRIX_FREE,
		      int order=2);

   ~GeometricMultigrid(){ }; 
protected: 
   void
   AddCoarsestLevel(mfem::Operator& A);
   
   void
   AddLevelChebyshev(mfem::Operator& A, int order,
                     double max_eig_estimate);

public:
   /**
    * @brief for use as preconditioner
    *
    * uses the MG Sovler as Prec (Performs one cycle at the current level, if 
    * level < 0 the finest level is used. 
    */
   // void
   // Mult(const mfem::Vector& x, mfem::Vector& y) const override; 
   
   void
   SetCoarsestParameters(double rel_tol, double abs_tol, int max_it);
};

/**
 * @brief Solver Multigrid preconditioned CG Method for a s.p.d System  
 */
class SolveMGPCG : public mfem::Solver{
private: 
   GeometricMultigrid* prec;

   mfem::CGSolver pcg; 

public:
   SolveMGPCG(ocst::GeometricMultigrid& prec, int level = -1);

   void
   Mult(const mfem::Vector& x, mfem::Vector& y) const override;  

   void
   MultTranspose(const mfem::Vector& x, mfem::Vector& y) const override 
   { Mult(x,y);}
   void
   SetOperator(const mfem::Operator&) override
   { MFEM_ABORT("SetOperator not supported in SolveMGPCG"); }

   /**
    * @brief Set tolerances and iterations
    */
   void 
   SetSolverParameters(double relTol, double absTol, int maxIt, 
	  		int printLevel /*= -1*/);
};


/**
 * @brief Polynomial Chebyshev smoother for a matrix free operator
 *
 * Smoother for multigrid, recreates mfem version but withot diagonal, so this 
 * is more efficient for operators where the diagonal is not known. Also made 
 * for parameter dependent operator i.e the value of the estimated max eigenvalue 
 * can be changed
 */
class ChebyshevSmoother : public mfem::Solver{ //TODO depreceate
private:
   const int order; 
   double max_eig_estimate;
   mutable mfem::Array<double> coeffs;
   mutable mfem::Vector residual;
   mutable mfem::Vector helperVector;

   const Operator* oper;

public:
   ChebyshevSmoother(mfem::Operator& oper_, int order = 2, 
                     int power_iterations = 10, double power_tolerance= 1e-8);

   ChebyshevSmoother(mfem::Operator& oper_, int order, 
                     double max_eig_estimate);


   void
   Setup();

   void
   Setup(double max_eig_estimate_)
   {max_eig_estimate=max_eig_estimate_; Setup();}

   void
   SetOperator(const Operator& op) override {oper = &op; Setup();}

   void
   Mult(const mfem::Vector& x, mfem::Vector& y) const override;  

   void
   MultTranspose(const mfem::Vector& x, mfem::Vector& y) const override 
   { Mult(x,y);}

};
} //ocst

#endif //OCST_MULTIGRID_HPP
