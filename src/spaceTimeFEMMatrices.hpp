#ifndef OCST_SPACE_TIME_FEM_MATRICES_HPP
#define OCST_SPACE_TIME_FEM_MATRICES_HPP

#include <mfem.hpp>
#include <ocstMultigrid.hpp>

namespace ocst{

//TODO: remove SetBLF and move to smarter constructors
/**
 * @brief Container/Owner for the spatial Finite Element Matrices
 *
 * Owns, and assembles the spatial matrices for a parabolic IBVP
 */
class SpaceMatrices{
private:

   mfem::ConstantCoefficient one{1.0};

   mfem::Coefficient* kappa_coeff=nullptr;
   mfem::Coefficient* xi_coeff=nullptr;

   mfem::BilinearForm a;
   mfem::BilinearForm m;

   SpaceMatrices(const SpaceMatrices&) = delete; 
public:
   /**
    * @brief Constructor general case 
    *
    * i.e the equation \f$ \dot{y} - \mathrm{div} \kappa \nabla y =f\f$ 
    */
   SpaceMatrices(mfem::FiniteElementSpace& fes );

   /**
    * Copy Constructor, uses all the Integrators from other and builds the 
    * matrices for the new FiniteElementSpace (useful for multigrid) 
    */
   SpaceMatrices(SpaceMatrices& other, mfem::FiniteElementSpace& fes); 

   /**
    * Set the spatial bilinear forms for the problem 
    * \f$ \dot{y} - \Delta y =f\f$
    *
    * @param xi is the function coefficient for the robin boundary condition
    */
   void 
   SetBLF(mfem::Coefficient& xi){ SetBLF(xi, one);}
   
   /**
    * Set the spatial bilinear forms for the problem 
    *\f$ \dot{y} - \mathrm{div} \kappa \nabla y =f\f$ 
    *
    * @param xi is the function coefficient for the robin boundary condition
    * @param kappa is the diffusion coefficient 
    */
   void 
   SetBLF(mfem::Coefficient& xi, mfem::Coefficient& kappa);


   /**
    * Assemble the matrices. One has to be sure that the integrators are set 
    * before using! 
    */
   void 
   Assemble();
   
   mfem::SparseMatrix& 
   A() { return a.SpMat();} 

   const mfem::SparseMatrix& 
   A() const { return a.SpMat();} 

   mfem::SparseMatrix& 
   M() { return m.SpMat();} 

   const mfem::SparseMatrix& 
   M() const { return m.SpMat();} 

};

/**
 * @brief Container/Owner for the time Finite Element Matrices
 *
 * Owns, and assembles the time matrices for a parabolic IBVP
 */
class TimeMatrices{
private:
   mfem::ConstantCoefficient one{1.0};

   mfem::BilinearForm m_trial;
   mfem::BilinearForm a_trial;
   mfem::BilinearForm m_test;

   mfem::SparseMatrix* m_mixed;
   mfem::SparseMatrix* c_mixed;
   mfem::SparseMatrix* m_mixed_mod;
   mfem::SparseMatrix* c_mixed_mod;
   TimeMatrices(const TimeMatrices&) = delete; 
   
public:
   /**
    * Constructor, BLFs are set up directly since no specified Integrators are 
    * needed, if #conditionally_stable is true than the trial and test space are 
    * defined on the same mesh, if false then the test space is refined, the 
    * matrices are assembled on construction
    */
   TimeMatrices(mfem::FiniteElementSpace& fes_trial, 
	        mfem::FiniteElementSpace& fes_test,
		bool conditionally_stable = true); 
   
   ~TimeMatrices();
  
   mfem::SparseMatrix& 
   M_trial() { return m_trial.SpMat();} 

   const mfem::SparseMatrix& 
   M_trial() const { return m_trial.SpMat();} 

   mfem::SparseMatrix& 
   A_trial() { return a_trial.SpMat();} 

   const mfem::SparseMatrix& 
   A_trial() const { return a_trial.SpMat();} 

   mfem::SparseMatrix& 
   M_test() { return m_test.SpMat();} 

   const mfem::SparseMatrix& 
   M_test() const { return m_test.SpMat();} 

   mfem::SparseMatrix& 
   M_mixed() { return *m_mixed;} 

   const mfem::SparseMatrix& 
   M_mixed() const { return *m_mixed;} 

   mfem::SparseMatrix& 
   C_mixed() { return *c_mixed;} 

   const mfem::SparseMatrix& 
   C_mixed() const { return *c_mixed_mod;} 

   mfem::SparseMatrix& 
   M_mixed_mod() { return *m_mixed_mod;} 

   const mfem::SparseMatrix& 
   M_mixed_mod() const { return *m_mixed_mod;} 

   mfem::SparseMatrix& 
   C_mixed_mod() { return *c_mixed_mod;} 

   const mfem::SparseMatrix& 
   C_mixed_mod() const { return *c_mixed_mod;} 
private:

   /**
   * @brief Modifies time matrices s.t. initial conditions are fullfilled
   * 
   * Adds a 1.0 to #c_mixed and a 0.0 to #m_mixed, s.t the sparsity structures
   * are the same after modification. In the conditional stable case the
   * matrices become square using this operation
   */
   void
   ModifyMixedMatrices();
};

   
} //ocst

#endif //OCST_SPACE_TIME_FEM_MATRICES_HPP
