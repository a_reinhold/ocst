#include <spaceTimeSolver.hpp> 

namespace mfem{
// v = v1 + alpha v2; 
void add(const mfem::Vector &v1, double alpha, 
         const mfem::Vector &v2, mfem::Vector &v);
}

namespace ocst{

SpaceTimeSolver::SpaceTimeSolver(ocst::PrimalRHS& primal_rhs, 
		                 ocst::AdjointRHS& adjoint_rhs):
   primal_rhs{&primal_rhs},
   adjoint_rhs{&adjoint_rhs}
{  }

SpaceTimeDirectSolver::
SpaceTimeDirectSolver(tensor::GeneralSylvesterOperator& op_, 
		      ocst::PrimalRHS& primal_rhs , 
	              ocst::AdjointRHS& adjoint_rhs):
   SpaceTimeSolver(primal_rhs, adjoint_rhs),
   solver{mfem::UMFPackSolver(true)}, op{&op_}
{
   solver.Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
}

SpaceTimeDirectSolver:: ~SpaceTimeDirectSolver()
{ }


void 
SpaceTimeDirectSolver::SetOperator()
{
   solver.SetOperator(op->SpMat());
}

void
SpaceTimeDirectSolver::
SolvePrimal(ocst::Control& u, ocst::OptimalControlCoefficients& y, 
            double* buffer) const
{
   if (!buffer) primal_rhs->Assemble(u);
   else primal_rhs->Assemble(u, buffer);
   solver.Mult(primal_rhs->Vec(), y.Vec());
}

void
SpaceTimeDirectSolver::
SolveAdjoint(ocst::State& y, ocst::OptimalControlCoefficients& p,
            double* buffer) const
{
   if(!buffer) adjoint_rhs->Assemble(y);
   else adjoint_rhs->Assemble(y,buffer); 
   solver.Mult(adjoint_rhs->Vec(),p.Vec());
}

LSQRSolver::
LSQRSolver(tensor::GeneralSylvesterOperator& B, 
	   ocst::PrimalRHS& primal, ocst::AdjointRHS& adjoint, 
	   mfem::Operator& inv_norm_trial, mfem::Operator& inv_norm_test):
   SpaceTimeSolver{primal, adjoint}, op{&B}, m{B.Width()}, n{B.Height()}, 
   inv_norm_trial{&inv_norm_trial},  inv_norm_test{&inv_norm_test},
   vec_mn{(m>n)? m : n}, vec_m_1{m}, vec_m_2{m}, vec_n_1{n}, vec_n_2{n},
   tol{1e-6}, max_it{20}
{    }
void
LSQRSolver::
SolvePrimal(ocst::Control& u, ocst::OptimalControlCoefficients& y, 
            double* buffer) const
{
   if (!buffer) primal_rhs->Assemble(u);
   else primal_rhs->Assemble(u,buffer);
   mfem::Vector d;
   d.SetDataAndSize(vec_mn.GetData(),m);
   op->SetTransposed(tensor::Transposed::NON_TRANSPOSED);
   Solve(inv_norm_test, inv_norm_trial, y.Vec(), primal_rhs->Vec(),
         d, vec_n_1, vec_n_2, vec_m_1, vec_m_2);

}
void 
LSQRSolver::
SolveAdjoint(ocst::State& y, ocst::OptimalControlCoefficients& p,
            double* buffer) const
{
   if(!buffer) adjoint_rhs->Assemble(y);
   else adjoint_rhs->Assemble(y,buffer); 
   mfem::Vector d;
   d.SetDataAndSize(vec_mn.GetData(),n);
   op->SetTransposed(tensor::Transposed::TRANSPOSED);
   Solve(inv_norm_trial, inv_norm_test, p.Vec(), adjoint_rhs->Vec(),
         d, vec_m_1, vec_m_2, vec_n_1, vec_n_2);

}
double
LSQRSolver::
Normalize(mfem::Operator* S,const mfem::Vector& s, 
          mfem::Vector& z, mfem::Vector& z_hat) const
{
   S->Mult(s, z_hat);
   double zeta = std::sqrt(s * z_hat);
   z_hat *= 1.0/zeta;
   z.Set(1.0/zeta,s);
   return zeta;
}

void
LSQRSolver::
Solve(mfem::Operator* N, mfem::Operator* M, mfem::Vector& x,
      const mfem::Vector& f, mfem::Vector& d,
      mfem::Vector& v_hat, mfem::Vector& v, 
      mfem::Vector& w_hat, mfem::Vector& w) const
{
   // Initialize
   // (a)
   d = 0.0;
   // (b)
   beta = Normalize(N, f, v, v_hat);
   // (c)
   op->MultTranspose(v_hat,w);
   alpha = Normalize(M, w, w, w_hat);
   // (d)
   rho = std::sqrt(alpha*alpha + beta*beta);
   // (e)
   x  = 0.0; //TODO Test with initial value 
   // (f)
   delta = alpha; gamma = beta;
   double residual = std::abs(delta)*gamma;
   if (print_level>1){
      std::cout << "iter =\t0,\tresidual =\t" << residual << std::endl; 
   }
   for (int i=1; i<max_it; ++i){
      if (residual<tol){
	 if(print_level>0){
	    std::cout << "converged after " << i  << " iterations" << std::endl
	      	      << "final residual = " << residual << std::endl;
	 }
	 return; 
      }
      // (a)
      mfem::add(w_hat, -(alpha*beta/(rho*rho)), d, d);
      // (b)
      op->Mult(w_hat, v_hat);
      mfem::add(v_hat, -alpha, v, v); 
      beta = Normalize(N, v, v, v_hat); 
      // (c)
      op->MultTranspose(v_hat, w_hat);
      mfem::add(w_hat, -beta, w, w); 
      alpha = Normalize(M, w, w, w_hat);
      // (d)
      rho = std::sqrt(delta*delta + beta*beta);
      // (e)
      mfem::add(x, delta*gamma/(rho*rho), d, x);
      // (f)
      delta = -delta*alpha/rho; 
      gamma = gamma*beta/rho;
      residual = std::abs(delta)*gamma;
      if (print_level>1){
	 std::cout << "iter =\t"<< i 
	           << ",\tresidual =\t" << residual << std::endl; 
      }
   }
   if(print_level>0){
      std::cout << "solver did not converge after " << max_it  
                << " iterations" << std::endl
		<< "final residual = " << residual << std::endl;
   }

}

IterativeType1Solver::
IterativeType1Solver(ocst::PrimalRHS& prhs, ocst::AdjointRHS& arhs,
			   mfem::Array<mfem::SparseMatrix*>& M, 
			   mfem::Array<mfem::SparseMatrix*>& A,
			   mfem::Operator& solve_M,
			   mfem::FiniteElementSpaceHierarchy& hierarchy,
			   const int K, const double T):
   SpaceTimeSolver{prhs, arhs}, 
   n_h{M.Last()->Height()}, K{K}, T{T}, dt{T/(K-1)}, 
   solve_M{&solve_M}, rhs{n_h}
{
   MFEM_VERIFY(prhs.Vec().Size() == (K+1)*n_h, "rhs has false size!");
   MFEM_VERIFY(arhs.Vec().Size() == (K+1)*n_h, "rhs has false size!");
   mfem::SparseMatrix* G;
   int levels = M.Size();
   for (int i=0; i<levels; ++i){
      G = new mfem::SparseMatrix(*M[i]);
      G->Add(0.5*dt, *A[i]);
      G_plus.Append(G);
   }
   G_minus = new mfem::SparseMatrix(*M.Last());
   G_minus->Add(-0.5*dt, *A.Last());

   prec_G = new ocst::GeometricMultigrid(hierarchy, G_plus, 
	 ocst::GeometricMultigrid::OperatorType::SPARSE);
   solve_G_plus = new ocst::SolveMGPCG(*prec_G); 

}

IterativeType1Solver::
~IterativeType1Solver()
{
   for (int i=0; i<G_plus.Size(); ++i){
      delete G_plus[i];
   }
   if (G_minus) delete G_minus;
   if (prec_G) delete prec_G;
   if (solve_G_plus) delete solve_G_plus;
}

void 
IterativeType1Solver::
SolvePrimal(ocst::Control& u, ocst::OptimalControlCoefficients& y, 
            double* buffer) const
{
   if (!buffer) primal_rhs->Assemble(u);
   else primal_rhs->Assemble(u,buffer);
  
   mfem::Vector y_k;
   mfem::Vector f_k;
   //y_0 from last row
   f_k.MakeRef(primal_rhs->Vec(), K*n_h, n_h);
   y_k.MakeRef(y.Vec(), 0, n_h);
   solve_M->Mult(f_k, y_k);
   for (int k=0; k<K; ++k){
      f_k.MakeRef(primal_rhs->Vec(), k*n_h, n_h);
      rhs = f_k;
      G_minus->AddMult(y_k,rhs); //rhs = f_k +G⁻y_k 
      y_k.MakeRef(y.Vec(),(k+1)*n_h, n_h);
      solve_G_plus->Mult(rhs, y_k);
   }
}

void 
IterativeType1Solver::
SolveAdjoint(ocst::State& y, ocst::OptimalControlCoefficients& p,
            double* buffer) const
{	    
   if(!buffer) adjoint_rhs->Assemble(y);
   else adjoint_rhs->Assemble(y,buffer); 
   mfem::Vector p_k;
   mfem::Vector g_k;

   //from last row
   p_k.MakeRef(p.Vec(), (K-1)*n_h, n_h); //p_(K-1)
   g_k.MakeRef(adjoint_rhs->Vec(), K*n_h, n_h); //g_K
   solve_G_plus->Mult(g_k,p_k); //p_{K-1} = G⁺\g_K

   for (int k=K; k-- > 1; ){
      g_k.MakeRef(adjoint_rhs->Vec(), k*n_h, n_h);
      rhs = g_k;
      G_minus->AddMult(p_k,rhs); //rhs = g_k +G⁻p_k 
      p_k.MakeRef(p.Vec(), (k-1)*n_h, n_h);
      solve_G_plus->Mult(rhs,p_k);
   }
   //p_II from p_0 (first row)
   g_k.MakeRef(adjoint_rhs->Vec(), 0, n_h);
   rhs = g_k;
   G_minus->AddMult(p_k, rhs);
   p_k.MakeRef(p.Vec(), K*n_h, n_h);
   solve_M->Mult(rhs, p_k);
}
} //ocst
