#include <optimalControlCoefficients.hpp>

namespace ocst{

OptimalControlCoefficients::
OptimalControlCoefficients(mfem::FiniteElementSpace& _fes_time, 
			   mfem::FiniteElementSpace& _fes_space,
			   int dof_time):
   SpaceTimeData(dof_time, _fes_space.GetVSize()), 
   fes_time{&_fes_time}, fes_space{&_fes_space}
{   }

OptimalControlCoefficients::
OptimalControlCoefficients(mfem::FiniteElementSpace& _fes_time, 
			   mfem::FiniteElementSpace& _fes_space):
   SpaceTimeData(_fes_time.GetVSize(), _fes_space.GetVSize()), 
   fes_time{&_fes_time}, fes_space{&_fes_space}
{   }
   
OptimalControlCoefficients::
OptimalControlCoefficients(const OptimalControlCoefficients& other):
   SpaceTimeData(other),
   m_time{other.m_time}, m_space{other.m_space},
   owns_m_time{false}, owns_m_space{false},
   fes_time{other.fes_time},
   fes_space{other.fes_space}
{   } 

OptimalControlCoefficients::
~OptimalControlCoefficients()
{
   if (owns_m_time) delete m_time;  
   if (owns_m_space) delete m_space;
}


mfem::SparseMatrix* 
OptimalControlCoefficients::
SetSparseMatrix(mfem::FiniteElementSpace& fes, 
		bool& ownership )
{
   mfem::BilinearForm blf(&fes);
   mfem::ConstantCoefficient one(1.0);
   blf.AddDomainIntegrator(new mfem::MassIntegrator(one));
   blf.Assemble();
   blf.Finalize();
   mfem::SparseMatrix* M = new mfem::SparseMatrix(blf.SpMat());
   ownership = true;
   return M;
}

mfem::SparseMatrix* 
OptimalControlCoefficients::
SetSparseMatrix(mfem::FiniteElementSpace& fes_trial, 
                mfem::FiniteElementSpace& fes_test, 
		bool& ownership )
{
   mfem::MixedBilinearForm blf(&fes_trial, &fes_test);
   mfem::ConstantCoefficient one(1.0);
   blf.AddDomainIntegrator(new mfem::MassIntegrator(one));
   blf.Assemble();
   blf.Finalize();
   mfem::SparseMatrix* M = new mfem::SparseMatrix(blf.SpMat());
   ownership = true;
   return M;
}

void
OptimalControlCoefficients::
SetM_time(mfem::SparseMatrix& m)
{
   if (m_time && owns_m_time == true){ delete m_time;} 
   MFEM_VERIFY(m.Height() == dof_time,"Matrix dimensions (time) dont agree ");
   m_time = &m;
   owns_m_time = false;
}

void
OptimalControlCoefficients::
SetM_space(mfem::SparseMatrix& m)
{
   if (m_space && owns_m_space == true){ delete m_space;} 
   MFEM_VERIFY(m.Height() == dof_space,"Matrix dimensions (space) dont agree ");
   m_space = &m;
   owns_m_space = false;
}

void
OptimalControlCoefficients::
SetM_time()
{
   if (m_time && owns_m_time == true){ delete m_time;} 
   m_time = SetSparseMatrix(*fes_time, owns_m_time);
}

void
OptimalControlCoefficients::
SetM_space()
{
   if (m_space && owns_m_space == true){ delete m_space;} 
   m_space = SetSparseMatrix(*fes_space, owns_m_space);
}

void
OptimalControlCoefficients::
SetValue(mfem::Coefficient& value)
{
   // create evaluation points in time
   mfem::GridFunction timesteps(fes_time);
   mfem::FunctionCoefficient identity([](const mfem::Vector& t){return t(0);});
   timesteps.ProjectCoefficient(identity);
   
   mfem::GridFunction space_values(fes_space, mat.GetColumn(0));
   for (int k=0; k < dof_time; ++k){
      space_values.SetData(mat.GetColumn(k));
      value.SetTime(timesteps(k));
      space_values.ProjectCoefficient(value);
   }
}

const mfem::SparseMatrix&
OptimalControlCoefficients::
M_time() const
{
   MFEM_VERIFY(m_time, "Matrix not assigned");
   return *m_time;
}

mfem::SparseMatrix&
OptimalControlCoefficients::
M_time()
{
   MFEM_VERIFY(m_time, "Matrix not assigned");
   return *m_time;
}

const mfem::SparseMatrix& 
OptimalControlCoefficients::
M_space() const
{
   MFEM_VERIFY(m_space, "Matrix not assigned");
   return *m_space;
}

mfem::SparseMatrix&
OptimalControlCoefficients::
M_space()
{
   MFEM_VERIFY(m_space, "Matrix not assigned");
   return *m_space;
}

mfem::FiniteElementSpace&
OptimalControlCoefficients::
FES_Time(){
   MFEM_VERIFY(fes_time, "No finite element space assigned");
   return *fes_time;
}

mfem::FiniteElementSpace&
OptimalControlCoefficients::
FES_Space(){
   MFEM_VERIFY(fes_space, "No finite element space assigned");
   return *fes_space;
}

double
OptimalControlCoefficients::
SquaredInnerProductNorm(double* buffer_1, double* buffer_2)
{
   ocst::SpaceTimeData tmp(dof_time, dof_space, buffer_1);
   MFEM_VERIFY(m_space && m_time, "");
   tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                     1.0, *m_time, *m_space, mat, 0.0, buffer_2, tmp.Mat());
   return tmp.Vec()* vec;
}

double 
OptimalControlCoefficients::
SquaredInnerProductNorm(const ocst::SpaceTimeData& x_,
			double* buffer_1, double* buffer_2)
{
   MFEM_VERIFY(m_space && m_time, "");
   ocst::SpaceTimeData tmp_1(dof_time, dof_space, buffer_1);
   tmp_1.Mat() = mat;
   tmp_1.Mat() -= x_.Mat(); //tmp = x - x_ 
   tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                     1.0, *m_time, *m_space, tmp_1.Mat(), 
		     0.0, buffer_2, tmp_1.Mat());

   ocst::SpaceTimeData tmp_2(dof_time, dof_space, buffer_2);
   tmp_2.Mat() = mat;
   tmp_2.Mat() -= x_.Mat(); //tmp = x - x_ 
   return tmp_1.Vec()* tmp_2.Vec();
}

double
OptimalControlCoefficients::
SquaredInnerProductNorm()
{
   double* buffer_1 = new double[dof_time*dof_space];
   double* buffer_2 = new double[dof_time*dof_space];

   double res = SquaredInnerProductNorm(buffer_1, buffer_2); 
   delete[] buffer_1;
   delete[] buffer_2;
   return res; 
}


//void
//OptimalControlCoefficients::
//ExportToParaview(mfem::ParaViewDataCollection& pd, 
//		 const std::string&  name,
//                 mfem::FiniteElementSpace& fes_time,
//		 mfem::FiniteElementSpace& fes_space,
//		 int nof_timesteps /* =0 */)
//{
//   //MFEM_VERIFY(dof_time==fes_time.GetVSize(), "");
//   //MFEM_VERIFY(dof_space==fes_space.GetVSize(), "");
//
//   //mfem::GridFunction timesteps(&fes_time);
//   //mfem::FunctionCoefficient identity([](const mfem::Vector& t){return t(0);});
//   //timesteps.ProjectCoefficient(identity);
//   mfem::GridFunction out(&fes_space,mat.GetData());
//   pd.RegisterField(name, &out);
//
//   double time=0.0;
//   
//   if(nof_timesteps){
//      //TODO
//   } else {
//      //for (int i=0; i<timesteps.Size(); ++i){
//      for (int i=0; i<dof_time; ++i){
//	 time = double(i);//timesteps(i);
//	 pd.SetCycle(i);
//	 pd.SetTime(time);
//	 out.SetData(mat.GetColumn(i));
//	 pd.Save();
//      }
//   }
//}


//---------------------------------------------------------------------------//
//                              Class Control                                //
//---------------------------------------------------------------------------//
Control::Control(mfem::FiniteElementSpace& fes_time, 
                 mfem::FiniteElementSpace& fes_space,
		 const bool fes_time_is_conform/* = true*/,
		 const bool fes_space_is_conform/* = true*/):
   OptimalControlCoefficients(fes_time,fes_space),
   fes_time_is_conform{fes_time_is_conform},
   fes_space_is_conform{fes_space_is_conform}
{   }

Control::Control(const Control& other):
   OptimalControlCoefficients(other),
   fes_time_is_conform{other.fes_time_is_conform},
   fes_space_is_conform{other.fes_space_is_conform},
   m_control_time{other.m_control_time}, owns_m_control_time{false},
   m_control_space{other.m_control_space}, owns_m_control_space{false}
{   }

Control::~Control()
{
   if (owns_m_control_time) delete m_control_time;
   if (owns_m_control_space) delete m_control_space;
}

//void
//Control::SetInitialValue(mfem::Coefficient& init)
//{
//   // create evaluation points in time
//   mfem::GridFunction timesteps(fes_time);
//   mfem::FunctionCoefficient identity([](const mfem::Vector& t){return t(0);});
//   timesteps.ProjectCoefficient(identity);
//   
//   mfem::GridFunction space_values(fes_space, mat.GetColumn(0));
//   for (int k=0; k < dof_time; ++k){
//      space_values.SetData(mat.GetColumn(k));
//      init.SetTime(timesteps(k));
//      space_values.ProjectCoefficient(init);
//   }
//}

void 
Control::
SetM_control_time(mfem::SparseMatrix& m)
{
   if (m_control_time && owns_m_control_time == true){ 
      delete m_control_time;
   } 
   m_control_time = &m;
   owns_m_control_time = false;
}

void 
Control::
SetM_control_space(mfem::SparseMatrix& m)
{
   if (m_control_space && owns_m_control_space == true){ 
      delete m_control_space;
   } 
   m_control_space = &m;
   owns_m_control_space = false;
}

void 
Control::
SetM_control_time(mfem::FiniteElementSpace& fes_other)
{
  //TODO check order of trial and test space
  m_control_time = SetSparseMatrix(*fes_time , fes_other, owns_m_control_time);
}

void 
Control::
SetM_control_space(mfem::FiniteElementSpace& fes_other)
{
  //TODO check order of trial and test space
  m_control_space = SetSparseMatrix(*fes_space, fes_other, 
                                    owns_m_control_space);
}

const mfem::SparseMatrix&
Control::
M_control_time() const
{
   if(fes_time_is_conform){
      return M_time();
   } else { 
      MFEM_VERIFY(m_control_time, "Matrix not assigned");
      return *m_control_time;
   }
}

mfem::SparseMatrix&
Control::
M_control_time()
{
   if(fes_time_is_conform){
      return M_time();
   } else { 
      MFEM_VERIFY(m_control_time, "Matrix not assigned");
      return *m_control_time;
   }
}

const mfem::SparseMatrix&
Control::
M_control_space() const
{
   if(fes_space_is_conform){
      return M_space();
   } else { 
      MFEM_VERIFY(m_control_space, "Matrix not assigned");
      return *m_control_space;
   }
}

mfem::SparseMatrix&
Control::
M_control_space()
{
   if(fes_space_is_conform){
      return M_space();
   } else { 
      MFEM_VERIFY(m_control_space, "Matrix not assigned");
      return *m_control_space;
   }
}

//---------------------------------------------------------------------------//
//                              Class State                                  //
//---------------------------------------------------------------------------//
State::State(mfem::FiniteElementSpace& fes_time, 
             mfem::FiniteElementSpace& fes_space):
   OptimalControlCoefficients(fes_time, fes_space)
{   }

//void
//State::SetM_time()
//{
//   if (m_time && owns_m_time == true){ delete m_time;} 
//   m_time = SetSparseMatrix(*fes_time, owns_m_time);
//}
//
//void
//State::SetM_space()
//{
//   if (m_space && owns_m_space == true){ delete m_space;} 
//   m_space = SetSparseMatrix(*fes_space, owns_m_space);
//}

void
State::
SetInitialValue(mfem::Coefficient& init)
{
   mfem::GridFunction y_0(fes_space, mat.GetColumn(0));
   y_0.ProjectCoefficient(init);
}

void 
State::
SetInitialValue(const double init)
{
   mfem::Vector y_0;
   mat.GetColumnReference(0, y_0);
   y_0 = init;
}

//---------------------------------------------------------------------------//
//                          Class Adjoint State                              //
//---------------------------------------------------------------------------//
AdjointState::AdjointState(mfem::FiniteElementSpace& _fes_time, 
                           mfem::FiniteElementSpace& _fes_space):
   OptimalControlCoefficients{_fes_time, _fes_space,_fes_time.GetVSize()+1},
   part_I{mat.GetData(), dof_space, _fes_time.GetVSize()}
{   }

const mfem::DenseMatrix&
AdjointState::AdjointState_I() const
{
return part_I; 
}

mfem::DenseMatrix&
AdjointState::AdjointState_I()
{
return part_I; 
}


} //
