#ifndef OCST_SPACE_TIME_RHS
#define OCST_SPACE_TIME_RHS

#include <mfem.hpp>
#include <spaceTimeData.hpp>
#include <optimalControlCoefficients.hpp>
#include <tensor/linalg_auxiliary.hpp>
namespace ocst{

/**
 * @brief Right hand side for the primal problem 
 *
 * Right hand side for the space time formulation of the PDE for the primal 
 * Problem. Containing on a control dependent part and a constant part that 
 * includes the robin bc and the initial Value of the control. 
 * Not copy constructable.
 */
class PrimalRHS{
private:
   const int dof_time;
   const int dof_space;
   ocst::SpaceTimeData data;
   ocst::SpaceTimeData constant_part;

   PrimalRHS(const PrimalRHS&) = delete; 
public:
   PrimalRHS(int dof_time,int dof_space);
   ~PrimalRHS();

   /**
    * @brief assemble the constant part of the rhs
    *
    * assembles the constant part (part not dependant of u) 
    * of the right hand side 
    */
   void
   SetConstantPart(mfem::FiniteElementSpace& fes_time_trial,
		   mfem::FiniteElementSpace& fes_space, 
   		   mfem::Coefficient& robin_bc, 
		   mfem::Coefficient& y_init,
		   mfem::SparseMatrix& M_time_mixed);

   void
   Assemble(ocst::Control& u, double* buffer);

   void
   Assemble(ocst::Control& u);

   const mfem::Matrix&
   Mat() const {return data.Mat();}

   mfem::Matrix&
   Mat() {return data.Mat();}
   
   const mfem::Vector&
   Vec() const {return data.Vec();}

   mfem::Vector&
   Vec() {return data.Vec();}
};

/**
 * @brief Right hand side for the adjoint problem 
 */
class AdjointRHS{
private:
   const int dof_time;
   const int dof_space;
   const double omega_1; 
   const double omega_2; 
   ocst::SpaceTimeData data;
   ocst::SpaceTimeData constant_part;

   AdjointRHS(const AdjointRHS&) = delete; 
public:
   AdjointRHS(int dof_time,int dof_space,
              const double omega_1, const double omega_2);

   /**
    * 
    * Set the constant part of the adjoint right hand side 
    */
   void 
   SetConstantPart(ocst::SpaceTimeData& y_d, mfem::Vector& y_d_T, 
                   mfem::SparseMatrix& M_time, mfem::SparseMatrix& M_space);

   void 
   Assemble(ocst::State& y, double* buffer);

   void 
   Assemble(ocst::State& y);

   const mfem::Matrix&
   Mat() const {return data.Mat();}

   mfem::Matrix&
   Mat() {return data.Mat();}
   
   const mfem::Vector&
   Vec() const {return data.Vec();}

   mfem::Vector&
   Vec() {return data.Vec();}
};
   


} //ocst

#endif //OCST_SPACE_TIME_RHS

