#include "ocstMultigrid.hpp"

namespace ocst {

SparseProlongation::SparseProlongation(mfem::FiniteElementSpace& coarse,
                                       mfem::FiniteElementSpace& fine)
    : mfem::Operator{fine.GetVSize(), coarse.GetVSize()}
{
   mfem::OperatorHandle handle(mfem::Operator::Type::MFEM_SPARSEMAT);
   fine.GetTransferOperator(coarse, handle);
   mat = new mfem::SparseMatrix();
   mfem::SparseMatrix* tmp = static_cast<mfem::SparseMatrix*>(handle.Ptr());
   tmp->Swap(*mat);
}

SpaceTimeProlongation::SpaceTimeProlongation(
    ocst::SparseProlongation& prol_time,
    ocst::SparseProlongation& prol_space,
    double* buffer /* = nullptr*/)
    : Operator{prol_time.Height() * prol_space.Height(),
               prol_time.Width() * prol_space.Width()}
    , prol_time{&prol_time}
    , prol_space{&prol_space}
    , mode{NORMAL}
    , dim_time_coarse{prol_time.Width()}
    , dim_time_fine{prol_time.Height()}
    , dim_space_coarse{prol_space.Width()}
    , dim_space_fine{prol_space.Height()}
    , buffer{buffer}
{ }

SpaceTimeProlongation::SpaceTimeProlongation(ocst::SparseProlongation& prol,
                                             Mode mode,
                                             int other_dim,
                                             double* buffer /* = nullptr*/)
    : Operator{prol.Height() * other_dim, prol.Width() * other_dim}
    , mode{mode}
    , buffer{buffer}
{
   MFEM_VERIFY(mode != NORMAL, "mode NORMAL not allowed");
   if (mode == ONLY_TIME) {
      prol_time = &prol;
      dim_time_coarse = prol_time->Width();
      dim_time_fine = prol_time->Height();
      dim_space_coarse = other_dim;
      dim_space_fine = other_dim;
   } else {
      prol_space = &prol;
      dim_space_coarse = prol_space->Width();
      dim_space_fine = prol_space->Height();
      dim_time_coarse = other_dim;
      dim_time_fine = other_dim;
   }
}

void
SpaceTimeProlongation::Mult(const mfem::Vector& x, mfem::Vector& y) const
{
   double* x_data = const_cast<double*>(x.GetData());
   const mfem::DenseMatrix X(x_data, dim_space_coarse, dim_time_coarse);
   mfem::DenseMatrix Y(y.GetData(), dim_space_fine, dim_time_fine);

   switch (mode) {
   case NORMAL:
      MFEM_VERIFY(buffer, "buffer not set!");
      tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                          1.0,
                          prol_time->SpMat(),
                          prol_space->SpMat(),
                          X,
                          0.0,
                          buffer,
                          Y);
      break;
   case ONLY_TIME:
      tensor::SparseDenseMM(tensor::Transposed::NON_TRANSPOSED,
                            tensor::Transposed::TRANSPOSED,
                            tensor::Transposed::TRANSPOSED,
                            1.0,
                            prol_time->SpMat(),
                            X,
                            0.0,
                            Y);
      break;
   case ONLY_SPACE:
      tensor::SparseDenseMM(tensor::Transposed::NON_TRANSPOSED,
                            tensor::Transposed::NON_TRANSPOSED,
                            tensor::Transposed::NON_TRANSPOSED,
                            1.0,
                            prol_space->SpMat(),
                            X,
                            0.0,
                            Y);
      break;
   default:
      MFEM_ABORT("Invalid mode for Prolongation!");
   }
}

void
SpaceTimeProlongation::MultTranspose(const mfem::Vector& x,
                                     mfem::Vector& y) const
{
   double* x_data = const_cast<double*>(x.GetData());
   const mfem::DenseMatrix X(x_data, dim_space_fine, dim_time_fine);
   mfem::DenseMatrix Y(y.GetData(), dim_space_coarse, dim_time_coarse);

   switch (mode) {
   case NORMAL:
      MFEM_VERIFY(buffer, "buffer not set!");
      tensor::KroneckerMV(tensor::Transposed::TRANSPOSED,
                          1.0,
                          prol_time->SpMat(),
                          prol_space->SpMat(),
                          X,
                          0.0,
                          buffer,
                          Y);
      break;
   case ONLY_TIME:
      tensor::SparseDenseMM(tensor::Transposed::TRANSPOSED,
                            tensor::Transposed::TRANSPOSED,
                            tensor::Transposed::TRANSPOSED,
                            1.0,
                            prol_time->SpMat(),
                            X,
                            0.0,
                            Y);
      break;
   case ONLY_SPACE:
      tensor::SparseDenseMM(tensor::Transposed::TRANSPOSED,
                            tensor::Transposed::NON_TRANSPOSED,
                            tensor::Transposed::NON_TRANSPOSED,
                            1.0,
                            prol_space->SpMat(),
                            X,
                            0.0,
                            Y);
      break;
   default:
      MFEM_ABORT("Invalid mode for Prolongation!");
   }
}

SpaceTimeFiniteElementSpaceHierarchy::SpaceTimeFiniteElementSpaceHierarchy(
    mfem::FiniteElementSpace& fes_time,
    int levels_time_,
    mfem::FiniteElementSpace& fes_space,
    int levels_space_,
    double* buffer /* = nullptr*/)
    : mfem::FiniteElementSpaceHierarchy{nullptr, nullptr, false, false}
    , hierarchy_time{fes_time.GetMesh(), &fes_time, false, false}
    , hierarchy_space{fes_space.GetMesh(), &fes_space, false, false}
    , levels{1}
    , levels_time{1}
    , levels_space{1}
    , buffer{buffer}
{
   int diff = levels_time_ - levels_space_;
   int levels_ = (levels_space_ > levels_time_) ? levels_space_ : levels_time_;
   for (int i = 1; i < levels_; ++i) {
      if (diff == 0) {
         AddLevel(NORMAL);
      } else if (diff < 0) {
         AddLevel(ONLY_SPACE);
         diff += 1;
      } else {
         AddLevel(ONLY_TIME);
         diff -= 1;
      }
   }
}

void
SpaceTimeFiniteElementSpaceHierarchy::AddLevel(Mode mode)
{
   mfem::FiniteElementSpace* time_coarse = &hierarchy_time.GetFinestFESpace();
   mfem::FiniteElementSpace* time_fine = nullptr;
   mfem::Mesh* mesh_time = nullptr;

   mfem::FiniteElementSpace* space_coarse
       = &hierarchy_space.GetFinestFESpace();
   mfem::FiniteElementSpace* space_fine = nullptr;
   mfem::Mesh* mesh_space = nullptr;

   ocst::SparseProlongation* prol_time = nullptr;
   ocst::SparseProlongation* prol_space = nullptr;
   ocst::SpaceTimeProlongation* prol = nullptr;
   if (mode == NORMAL || mode == ONLY_TIME) {
      mesh_time = new mfem::Mesh(*time_coarse->GetMesh());
      mesh_time->UniformRefinement();

      time_fine = new mfem::FiniteElementSpace(*time_coarse, mesh_time);

      prol_time = new ocst::SparseProlongation(*time_coarse, *time_fine);
      hierarchy_time.AddLevel(
          mesh_time, time_fine, prol_time, true, true, true);
   }
   if (mode == NORMAL || mode == ONLY_SPACE) {
      mesh_space = new mfem::Mesh(*space_coarse->GetMesh());
      mesh_space->UniformRefinement();

      space_fine = new mfem::FiniteElementSpace(*space_coarse, mesh_space);

      prol_space = new ocst::SparseProlongation(*space_coarse, *space_fine);
      hierarchy_space.AddLevel(
          mesh_space, space_fine, prol_space, true, true, true);
   }

   switch (mode) {
   case NORMAL:
      prol = new SpaceTimeProlongation(*prol_time, *prol_space, buffer);
      levels_space += 1;
      levels_time += 1;
      break;
   case ONLY_TIME:
      prol = new SpaceTimeProlongation(*prol_time,
                                       SpaceTimeProlongation::Mode::ONLY_TIME,
                                       space_coarse->GetVSize());
      levels_time += 1;
      break;

   case ONLY_SPACE:
      prol = new SpaceTimeProlongation(*prol_space,
                                       SpaceTimeProlongation::Mode::ONLY_SPACE,
                                       time_coarse->GetVSize());
      levels_space += 1;
      break;

   default:
      MFEM_ABORT("Invalid mode for AddLevel()!");
   }

   AddLevel(nullptr, nullptr, prol, false, false, true);
   levels += 1;
}

void
SpaceTimeFiniteElementSpaceHierarchy::SetBuffer(double* buffer_)
{
   ocst::SpaceTimeProlongation* prol;
   for (int i = 0; i < prolongations.Size(); ++i) {
      prol = static_cast<ocst::SpaceTimeProlongation*>(prolongations[i]);
      prol->SetBuffer(buffer_);
   }
}

mfem::FiniteElementSpace&
SpaceTimeFiniteElementSpaceHierarchy::GetFESTimeAtLevel(int i)
{
   MFEM_VERIFY(i < levels,
               "no FES at level =" << i << ", max level = " << levels);
   int diff = levels_time - levels_space;
   int index = (diff > 0) ? (0 + i) : (diff + i);
   return hierarchy_time.GetFESpaceAtLevel((index < 0) ? 0 : index);
}

mfem::FiniteElementSpace&
SpaceTimeFiniteElementSpaceHierarchy::GetFESSpaceAtLevel(int i)
{
   MFEM_VERIFY(i < levels,
               "no FES at level =" << i << ", max level = " << levels);
   int diff = levels_space - levels_time;
   int index = (diff > 0) ? (0 + i) : (diff + i);
   return hierarchy_space.GetFESpaceAtLevel((index < 0) ? 0 : index);
}

GeometricMultigrid::GeometricMultigrid(
    const mfem::FiniteElementSpaceHierarchy& hierarchy,
    const mfem::Array<mfem::Operator*>& operators,
    bool matrix_free /* = true*/,
    int order /*=2*/)
    : ocst::GeometricMultigrid{hierarchy,
                               operators,
                               matrix_free ? OperatorType::MATRIX_FREE
                                           : OperatorType::SPARSE,
                               order}
// mfem::GeometricMultigrid(hierarchy), matrix_free(matrix_free)
{ }

GeometricMultigrid::GeometricMultigrid(
    const mfem::FiniteElementSpaceHierarchy& hierarchy,
    const mfem::Array<mfem::Operator*>& operators,
    OperatorType operator_type /* = OperatorType::MATRIX_FREE*/,
    int order /*= 2*/)
    : mfem::GeometricMultigrid{hierarchy}
    , operator_type{operator_type}
{
   int levels = operators.Size();
   AddCoarsestLevel(*operators[0]);
   for (int i = 1; i < levels; ++i) {
      AddLevelChebyshev(*operators[i], order, -1.0);
   }
   SetCycleType(mfem::Multigrid::CycleType::VCYCLE, 1, 1);
}

GeometricMultigrid::GeometricMultigrid(
    const mfem::FiniteElementSpaceHierarchy& hierarchy,
    const mfem::Array<mfem::Operator*>& operators,
    mfem::Vector& max_evs,
    OperatorType operator_type /*= OperatorType::MATRIX_FREE*/,
    int order /*=2*/)
    : mfem::GeometricMultigrid(hierarchy)
    , operator_type{operator_type}
{
   int levels = operators.Size();
   AddCoarsestLevel(*operators[0]);
   for (int i = 1; i < levels; ++i) {
      AddLevelChebyshev(*operators[i], order, max_evs(i - 1));
   }
   SetCycleType(mfem::Multigrid::CycleType::VCYCLE, 1, 1);
}

void
GeometricMultigrid::AddCoarsestLevel(mfem::Operator& A)
{
   // std::cout << "AddCoarsestLevel " << std::endl << "A.Height() = "
   //            << A.Height() << std::endl;
   switch (operator_type) {
   case GeometricMultigrid::OperatorType::MATRIX_FREE: {
      mfem::CGSolver* solver = new mfem::CGSolver();
      solver->SetOperator(A);
      solver->SetPrintLevel(-1);
      solver->SetMaxIter(100);
      solver->SetRelTol(0);
      solver->SetAbsTol(1e-6);
      AddLevel(&A, solver, false, true);
      break;
   }
   case GeometricMultigrid::OperatorType::SPARSE: {
      mfem::SparseMatrix* A_ = static_cast<mfem::SparseMatrix*>(&A);
      mfem::UMFPackSolver* solver = new mfem::UMFPackSolver;
      solver->Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
      solver->SetOperator(*A_);
      AddLevel(&A, solver, false, true);
      break;
   }
   case GeometricMultigrid::OperatorType::TENSOR: {
      auto* tensor = static_cast<tensor::TensorOperator*>(&A);
      mfem::UMFPackSolver* solver = new mfem::UMFPackSolver;
      solver->Control[UMFPACK_ORDERING] = UMFPACK_ORDERING_METIS;
      solver->SetOperator(tensor->SpMat());
      AddLevel(&A, solver, false, true);
      break;
   }
   default:
      MFEM_ABORT("no valid operator type")
      break;
   }
}

void
GeometricMultigrid::AddLevelChebyshev(mfem::Operator& A,
                                      int order,
                                      double max_eig_estimate)
{

   // std::cout << "AddLevelChebyshev" << std::endl << "A.Height() = "
   //            << A.Height() << "\t max_eig_est = " << max_eig_estimate
   //            << std::endl;
   mfem::Solver* smoother = nullptr;
   mfem::Vector diag{A.Height()};
   mfem::Array<int> esstdof;
   switch (operator_type) {
   case GeometricMultigrid::OperatorType::MATRIX_FREE: {
      // diag = 1.0;
      smoother = new ocst::ChebyshevSmoother(
          A, order, (max_eig_estimate < 0) ? 10 : max_eig_estimate);
      AddLevel(&A, smoother, false, true);
      return;
      break;
   }
   case GeometricMultigrid::OperatorType::SPARSE: {
      mfem::SparseMatrix* A_ = static_cast<mfem::SparseMatrix*>(&A);
      A_->GetDiag(diag);
      break;
   }
   case ocst::GeometricMultigrid::OperatorType::TENSOR: {
      auto* tensor = static_cast<tensor::TensorOperator*>(&A);
      tensor->GetDiag(diag);
      break;
   }
   default:
      break;
   }
   if (max_eig_estimate < 0) {
      smoother = new mfem::OperatorChebyshevSmoother(A, diag, esstdof, order);
   } else {
      smoother = new mfem::OperatorChebyshevSmoother(
          A, diag, esstdof, order, max_eig_estimate);
   }
   AddLevel(&A, smoother, false, true);
}

void
GeometricMultigrid::SetCoarsestParameters(double rel_tol,
                                          double abs_tol,
                                          int max_it)
{
   if (operator_type == GeometricMultigrid::OperatorType::MATRIX_FREE) {
      mfem::CGSolver* solver
          = static_cast<mfem::CGSolver*>(GetSmootherAtLevel(0));
      solver->SetMaxIter(max_it);
      solver->SetRelTol(rel_tol);
      solver->SetAbsTol(abs_tol);
   }
}

SolveMGPCG::SolveMGPCG(ocst::GeometricMultigrid& prec_, int level /*= -1 */)
    : prec{&prec_}
{
   pcg.SetRelTol(1e-5);
   pcg.SetAbsTol(1e-6);
   pcg.SetMaxIter(50);
   pcg.SetPrintLevel(-1);
   if (level < 0) {
      pcg.SetOperator(*prec->GetOperatorAtFinestLevel());
   } else {
      pcg.SetOperator(*prec->GetOperatorAtLevel(level));
   }
   height = pcg.Height();
   width = pcg.Width();
   pcg.SetPreconditioner(*prec);
}

void
SolveMGPCG::Mult(const mfem::Vector& x, mfem::Vector& y) const
{
   pcg.Mult(x, y);
}

void
SolveMGPCG::SetSolverParameters(double relTol,
                                double absTol,
                                int maxIt,
                                int printLevel /*= -1*/)
{
   pcg.SetRelTol(relTol);
   pcg.SetAbsTol(absTol);
   pcg.SetMaxIter(maxIt);
   pcg.SetPrintLevel(printLevel);
   // prec->SetCoarsestParameters(0.1*relTol, 0.1*absTol, 4*maxIt);
}

ChebyshevSmoother::ChebyshevSmoother(mfem::Operator& oper_,
                                     int order,
                                     int power_iterations /* = 10*/,
                                     double power_tolerance /*= 1e-8*/)
    : mfem::Solver{oper_.Height()}
    , order{order}
    , coeffs{order}
    , oper{&oper_}
{
   mfem::PowerMethod power_method;
   mfem::Vector ev(height); // ev=0.0;
   max_eig_estimate = power_method.EstimateLargestEigenvalue(
       oper_, ev, power_iterations, power_tolerance);

   Setup();
}

ChebyshevSmoother::ChebyshevSmoother(mfem::Operator& oper_,
                                     int order,
                                     double max_eig_estimate)
    : mfem::Solver{oper_.Height()}
    , order{order}
    , max_eig_estimate{max_eig_estimate}
    , coeffs{order}
    , oper{&oper_}
{
   Setup();
}

void
ChebyshevSmoother::Setup()
{
   // Set up Chebyshev coefficients
   // For reference, see e.g., Parallel multigrid smoothing: polynomial versus
   // Gauss-Seidel by Adams et al.
   double upper_bound = 1.2 * max_eig_estimate;
   double lower_bound = 0.3 * max_eig_estimate;
   double theta = 0.5 * (upper_bound + lower_bound);
   double delta = 0.5 * (upper_bound - lower_bound);
   if (order > 2) {
      residual.SetSize(oper->Height());
      helperVector.SetSize(oper->Height());
   }
   switch (order - 1) {
   case 0: {
      coeffs[0] = 1.0 / theta;
      break;
   }
   case 1: {
      double tmp_0 = 1.0 / (std::pow(delta, 2) - 2 * std::pow(theta, 2));
      coeffs[0] = -4 * theta * tmp_0;
      coeffs[1] = 2 * tmp_0;
      break;
   }
   case 2: {
      double tmp_0 = 3 * std::pow(delta, 2);
      double tmp_1 = std::pow(theta, 2);
      double tmp_2 = 1.0 / (-4 * std::pow(theta, 3) + theta * tmp_0);
      coeffs[0] = tmp_2 * (tmp_0 - 12 * tmp_1);
      coeffs[1] = 12 / (tmp_0 - 4 * tmp_1);
      coeffs[2] = -4 * tmp_2;
      break;
   }
   case 3: {
      double tmp_0 = std::pow(delta, 2);
      double tmp_1 = std::pow(theta, 2);
      double tmp_2 = 8 * tmp_0;
      double tmp_3
          = 1.0
            / (std::pow(delta, 4) + 8 * std::pow(theta, 4) - tmp_1 * tmp_2);
      coeffs[0] = tmp_3 * (32 * std::pow(theta, 3) - 16 * theta * tmp_0);
      coeffs[1] = tmp_3 * (-48 * tmp_1 + tmp_2);
      coeffs[2] = 32 * theta * tmp_3;
      coeffs[3] = -8 * tmp_3;
      break;
   }
   case 4: {
      double tmp_0 = 5 * std::pow(delta, 4);
      double tmp_1 = std::pow(theta, 4);
      double tmp_2 = std::pow(theta, 2);
      double tmp_3 = std::pow(delta, 2);
      double tmp_4 = 60 * tmp_3;
      double tmp_5 = 20 * tmp_3;
      double tmp_6 = 1.0
                     / (16 * std::pow(theta, 5) - std::pow(theta, 3) * tmp_5
                        + theta * tmp_0);
      double tmp_7 = 160 * tmp_2;
      double tmp_8 = 1.0 / (tmp_0 + 16 * tmp_1 - tmp_2 * tmp_5);
      coeffs[0] = tmp_6 * (tmp_0 + 80 * tmp_1 - tmp_2 * tmp_4);
      coeffs[1] = tmp_8 * (tmp_4 - tmp_7);
      coeffs[2] = tmp_6 * (-tmp_5 + tmp_7);
      coeffs[3] = -80 * tmp_8;
      coeffs[4] = 16 * tmp_6;
      break;
   }
   default:
      MFEM_ABORT("Chebyshev smoother not implemented for order = " << order);
   }
}

void
ChebyshevSmoother::Mult(const mfem::Vector& x, mfem::Vector& y) const
{
   if (iterative_mode) {
      MFEM_ABORT("Chebyshev smoother not implemented for iterative mode");
   }

   if (!oper) {
      MFEM_ABORT("Chebyshev smoother requires operator");
   }

   if (order == 2) {
      oper->Mult(x, y);
      y *= coeffs[1];
      y.Add(coeffs[0], x);
   } else {
      y.Set(coeffs[0], x);
      if (order > 1) {
         residual = x;

         for (int k = 1; k < order; ++k) {
            // Apply
            oper->Mult(residual, helperVector);
            residual = helperVector;
            y.Add(coeffs[k], residual);
         }
      }
   }
}

} // ocst
