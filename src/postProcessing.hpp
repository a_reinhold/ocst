#ifndef OCST_POST_PROCESSING_HPP
#define OCST_POST_PROCESSING_HPP

#include <mfem.hpp>

#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <chrono>
#include <string>
#include <cmath> 

namespace ocst{
class BenchTiming{
private:
   using Clock = std::chrono::high_resolution_clock;
   std::chrono::duration<double, std::milli> elapsed_time; 

   std::vector<std::chrono::time_point<Clock>> start;
   std::vector<unsigned int> nof_evals;
   std::vector<double> time_values;
   std::vector<bool> started;
public: 
   BenchTiming()= default;
   
   BenchTiming(unsigned int nof_fields);
   
   void
   AddField();

   void
   Start(unsigned int index);

   void
   End(unsigned int index);

   double
   GetTime(unsigned int index);
   
   double
   PopTime(unsigned int index);

   double
   GetCumulatedTime(unsigned int index);
};

class BenchData{
public:
  enum Type {Double, Int};
private:

  std::map<int, std::string> print_order;
  std::unordered_map<std::string, Type> data_types;
  std::unordered_map<std::string, std::vector<int>> int_vectors;
  std::unordered_map<std::string, std::vector<double>> double_vectors;
  unsigned int nof_lines = 0; 
public:
  void
  RegisterField(std::string name, Type t, int print_pos = -1);

  template<typename T>
  void
  AddValue(std::string name, T value)
  {
      Type t = data_types[name]; 
      int new_size = 0; 
      switch (t) {
	 case Int:
	    int_vectors[name].push_back(value);
	    new_size = int_vectors[name].size(); 
	    break;
	 case Double:
	    double_vectors[name].push_back(value); 
	    new_size = double_vectors[name].size(); 
	    break;
	 default:
	    MFEM_ABORT("Type not known!");
      }
      nof_lines = (new_size > nof_lines) ? new_size : nof_lines;
  }
   
  void
  PrintHeader();

  void
  PrintData(bool print_header=true);

private:
  void
  PrintLine(unsigned int index);

};
void 
ExportToParaview(mfem::ParaViewDataCollection* pd, std::string name, 
                 mfem::DenseMatrix& mat,
                 mfem::FiniteElementSpace& fes_space,
		 mfem::FiniteElementSpace* fes_time = nullptr,
		 std::vector<int>* indices = nullptr );

void
Export1DSurfData(mfem::FiniteElementSpace& fes_time, 
                 mfem::FiniteElementSpace& fes_space, 
                 mfem::DenseMatrix& data, 
		 std::ostream& out = std::cout, std::string  ="n.a.");
}//ocst
#endif //OCST_POST_PROCESSING_HPP
