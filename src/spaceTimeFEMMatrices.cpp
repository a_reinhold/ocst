#include <spaceTimeFEMMatrices.hpp> 

namespace ocst{

// class SpaceMatrices
SpaceMatrices::
SpaceMatrices(mfem::FiniteElementSpace& fes):
a{&fes}, m{&fes}
{ }


SpaceMatrices::
SpaceMatrices(SpaceMatrices& other, mfem::FiniteElementSpace& fes):
a{&fes},
m{&fes}
{
SetBLF(*other.xi_coeff, *other.kappa_coeff);
}

void
SpaceMatrices::
SetBLF(mfem::Coefficient& xi, mfem::Coefficient& kappa)
{
xi_coeff = &xi;
kappa_coeff = &kappa;
m.AddDomainIntegrator(new mfem::MassIntegrator(one));
a.AddDomainIntegrator(new mfem::DiffusionIntegrator(kappa));
a.AddBoundaryIntegrator(new mfem::MassIntegrator(xi));
}

void
SpaceMatrices::
Assemble()
{
int skip_zeros=0;

m.Assemble(skip_zeros);
m.Finalize(skip_zeros);
a.Assemble(skip_zeros);
a.Finalize(skip_zeros);
}

// class TimeMatrices
TimeMatrices::
TimeMatrices(mfem::FiniteElementSpace& fes_trial, 
	  mfem::FiniteElementSpace& fes_test,
	  bool conditionally_stable /*= true*/):
m_trial{&fes_trial},
a_trial{&fes_trial},
m_test{&fes_test}
{  
   int skip_zeros = 0; 
   m_trial.AddDomainIntegrator(new mfem::MassIntegrator(one));
   a_trial.AddDomainIntegrator(new mfem::DiffusionIntegrator(one));
   m_test.AddDomainIntegrator(new mfem::MassIntegrator(one));

   m_test.Assemble(skip_zeros);
   m_test.Finalize(skip_zeros);

   m_trial.Assemble(skip_zeros);
   m_trial.Finalize(skip_zeros);

   a_trial.Assemble(skip_zeros);
   a_trial.Finalize(skip_zeros);

   if (conditionally_stable){
      
      mfem::MixedBilinearForm m_mixed_(&fes_trial, &fes_test);
      mfem::MixedBilinearForm c_mixed_(&fes_trial, &fes_test);

      m_mixed_.AddDomainIntegrator(new mfem::MassIntegrator(one));
      c_mixed_.AddDomainIntegrator(
			      new mfem::MixedScalarDerivativeIntegrator(one));

      m_mixed_.Assemble(skip_zeros);
      m_mixed_.Finalize(skip_zeros);

      c_mixed_.Assemble(skip_zeros);
      c_mixed_.Finalize(skip_zeros);
      
      c_mixed = c_mixed_.LoseMat();
      m_mixed = m_mixed_.LoseMat();


   } else {
      mfem::FiniteElementSpace fes_trial_ref(fes_trial, fes_test.GetMesh());
      ocst::SparseProlongation prol(fes_trial, fes_trial_ref);
      
      mfem::MixedBilinearForm m_mixed_(&fes_trial_ref, &fes_test);
      mfem::MixedBilinearForm c_mixed_(&fes_trial_ref, &fes_test);

      m_mixed_.AddDomainIntegrator(new mfem::MassIntegrator(one));
      c_mixed_.AddDomainIntegrator(
				new mfem::MixedScalarDerivativeIntegrator(one));

      m_mixed_.Assemble(skip_zeros);
      m_mixed_.Finalize(skip_zeros);

      c_mixed_.Assemble(skip_zeros);
      c_mixed_.Finalize(skip_zeros);

      c_mixed = mfem::Mult(c_mixed_.SpMat(), prol.SpMat());
      m_mixed = mfem::Mult(m_mixed_.SpMat(), prol.SpMat());
   }
   ModifyMixedMatrices();
}
TimeMatrices::
~TimeMatrices()
{
   delete c_mixed;
   delete m_mixed;
   delete c_mixed_mod;
   delete m_mixed_mod;
}

void
TimeMatrices::
ModifyMixedMatrices()
{
   int m = c_mixed->Height();
   int n = c_mixed->Width();
   int* I_C = c_mixed->GetI();
   int* J_C = c_mixed->GetJ();
   double* data_C = c_mixed->GetData();

   int* I_M = m_mixed->GetI();
   int* J_M = m_mixed->GetJ();
   double* data_M = m_mixed->GetData();
   
   //new arrays 
   int* I_C_ = new int[m+2];
   int* J_C_ = new int[I_C[m]+1];
   double* data_C_ = new double[I_C[m]+1];

   int* I_M_ = new int[m+2];
   int* J_M_ = new int[I_C[m]+1];
   double* data_M_ = new double[I_C[m]+1];

   //copy data
   for (int i=0; i<m+1; i++){
      I_C_[i] = I_C[i];
      I_M_[i] = I_M[i]; 
   }
   I_C_[m+1] = I_C[m] + 1;
   I_M_[m+1] = I_M[m] + 1;

   for (int l=0; l< I_C_[m]; ++l){
      J_C_[l] = J_C[l];
      data_C_[l] = data_C[l];
      J_M_[l] = J_M[l];
      data_M_[l] = data_M[l];
   }
   //add element C(m+1,0)=1; 
   J_C_[I_C_[m+1]-1] = 0; 
   data_C_[I_C_[m+1]-1] = 1.0; 
   //add element M(m+1,0)=0; 
   J_M_[I_M_[m+1]-1] = 0; 
   data_M_[I_M_[m+1]-1] = 0.0; 
   
   c_mixed_mod = new mfem::SparseMatrix(I_C_, J_C_, data_C_, m+1, n);
   m_mixed_mod = new mfem::SparseMatrix(I_M_, J_M_, data_M_, m+1, n);
}
} //ocst
