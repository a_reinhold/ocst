#ifndef OCST_SPACE_TIME_DATA_HPP
#define OCST_SPACE_TIME_DATA_HPP


#include "mfem.hpp"

namespace ocst{

/**
 * @brief Base class for data that is available as matrix and as vector.
 *
 * Manages the memory. 
 * Copy constructable.  
 */
class SpaceTimeData{
private:
   bool owns_data; 
   double* data;
protected: 
   int dof_time, dof_space;
   mfem::Vector vec;
   mfem::DenseMatrix mat;

public:
   /**
    * @brief constructor, data is set to value init
    */
   SpaceTimeData(int dof_time, int dof_space, double init);
   
   /**
    *@brief  constructor, data is set to 0
    */
   SpaceTimeData(int dof_time, int dof_space);
   /**
    * @brief constructor, data is not owned by the instance
    */
   SpaceTimeData(int dof_time, int dof_space, double* data);
   
   /**
    * @brief Copy constructor
    *
    * Creates deep copy, even if other doesn't own data
    */
   SpaceTimeData(const SpaceTimeData&);

   ~SpaceTimeData();
   /**
    * @brief Swap data (and ownership) between this and other 
    *
    * The length (dof_time*dof_space) needs to be the same, dimension of the 
    * matrix is not altered!  
    */
   void
   SwapData(SpaceTimeData& other);


   const mfem::DenseMatrix& 
   Mat() const { return mat;}
   
   mfem::DenseMatrix& 
   Mat() { return mat;}
   
   const mfem::Vector& 
   Vec() const { return vec;}
   
   mfem::Vector& 
   Vec() { return vec;}
};

} //ocst


#endif //OCST_SPACE_TIME_DATA_HPP
