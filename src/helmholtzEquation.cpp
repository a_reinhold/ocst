#include "ocstMultigrid.hpp"
#include <helmholtzEquation.hpp>

//fwd decl
namespace mfem{
void add(const mfem::Vector &v1, double alpha, 
         const mfem::Vector &v2, mfem::Vector &v);
}

namespace ocst{

RealValuedIterationB_alpha::
RealValuedIterationB_alpha(mfem::SparseMatrix& R, mfem::SparseMatrix& S,
			   double alpha, double gamma):
   mfem::Operator{R.Height()}, R{&R}, S{&S}, alpha{alpha}, gamma{gamma} 
{    }


void                                                                          
RealValuedIterationB_alpha::
Mult(const mfem::Vector& x, mfem::Vector& y) const
{
   R->Mult(x,y);
   S->AddMult(x,y,gamma*alpha);
}

RealValuedIterationC_alpha::
RealValuedIterationC_alpha(mfem::SparseMatrix& R, mfem::SparseMatrix& S,
			   const double alpha, double gamma, 
			   mfem::Solver& solve_B_alpha):
   mfem::Operator{R.Height()}, R{&R}, S{&S}, alpha{alpha}, gamma{gamma},
   solve_B_alpha{&solve_B_alpha}, tmp{height}
{   }

void                                                                          
RealValuedIterationC_alpha::
Mult(const mfem::Vector& x, mfem::Vector& y) const
{
   S->Mult(x,y);
   solve_B_alpha->Mult(y, tmp); 
   S->Mult(tmp,y);
   y *= (1 + alpha*alpha)*gamma*gamma;
   S->AddMult(x,y,-alpha*gamma);
   R->AddMult(x, y); 
}


SolveHelmholtzRealValued::
SolveHelmholtzRealValued(mfem::Array<mfem::SparseMatrix*>& M, 
		         mfem::Array<mfem::SparseMatrix*>& A,
		         mfem::FiniteElementSpaceHierarchy& hierarchy,
		         mfem::Vector& gamma_):
   Operator{M.Last()->Height()}, K{gamma_.Size()}, levels{M.Size()}, 
   gamma{&gamma_},
   evs_smoother{levels-1,K}, tmp{height},
   R{A.Last()}, S{M.Last()}
{
   //Set up B_alpha for prec Ops
   for (int i=0; i<levels; ++i){
      B_alpha.Append(new RealValuedIterationB_alpha(*A[i], *M[i], alpha,1.0));
   }

   //Calculate all EVS for the smoother (each level and all time steps)
   mfem::PowerMethod pm;
   mfem::Vector tmp(height);
   tmp = 0.0;

   for (int i=1; i<levels; ++i){
      mfem::Vector tmp_(tmp.GetData(), B_alpha[i]->Height());
      for (int k_=0; k_<K; ++k_){
	 B_alpha[i]->SetGamma(gamma->Elem(k_));
	 evs_smoother(i-1,k_) = pm.EstimateLargestEigenvalue(*B_alpha[i],tmp_);
      }
   }
   //std::cout << "Estimated EVS" << std::endl;
   //evs_smoother.Print();
   //Set up prec and solver
   mfem::Vector evs_smoother_k;
   evs_smoother.GetColumnReference(0,evs_smoother_k);
   prec_B = new ocst::GeometricMultigrid(hierarchy, B_alpha, evs_smoother_k);
   solve_B = new ocst::SolveMGPCG(*prec_B);

   C_alpha = new ocst::RealValuedIterationC_alpha(*R, *S, alpha, 1.0, *solve_B);
   

   solver = new mfem::CGSolver;
   
   solver->iterative_mode=false; 
   solver->SetOperator(*C_alpha);
   solver->SetPreconditioner(*solve_B);
   SetSolverParameters(1e-5);
   solver->SetPrintLevel(-1);
   SetTimeStep(0);
}

SolveHelmholtzRealValued::
~SolveHelmholtzRealValued()
{
   delete solver;
   delete prec_B;
   delete solve_B;
   delete C_alpha;
   for (int i=0; i<levels; ++i){
      delete B_alpha[i];
   }
}

void
SolveHelmholtzRealValued::
SetSolverParameters(double tol, int iter_main /*= 10*/, int iter_prec /*=20*/)
{
   double rel_tol = 0.1*tol;
   double abs_tol_prec = 0.0008*tol;
   solve_B->SetSolverParameters(0.0,abs_tol_prec,iter_prec,-1);
   solver->SetRelTol(rel_tol);
   solver->SetAbsTol(tol);
   solver->SetMaxIter(iter_main);
   
}
void
SolveHelmholtzRealValued::
SetTimeStep(int k_)
{
   ocst::ChebyshevSmoother* smoother; 
   k=k_;
   double gamma_k = gamma->Elem(k);
   C_alpha->SetGamma(gamma_k);
   B_alpha[0]->SetGamma(gamma_k);
   for (int i=1; i<levels; ++i){
      B_alpha[i]->SetGamma(gamma_k);
      smoother = static_cast<ocst::ChebyshevSmoother*>(
                                               prec_B->GetSmootherAtLevel(i));
      smoother->Setup(evs_smoother(i-1,k));
   }
}


void                                                                          
SolveHelmholtzRealValued::
Mult(const mfem::Vector& x, mfem::Vector& y) const
{
   //Setup right hand side 
   //(Note: only real rhs are considered imag(x) = psi =0, real(x) = phi
   tmp.Set(-alpha, x);
   solve_B->Mult(tmp, y);
   S->Mult(y,tmp);
   mfem::add(x, gamma->Elem(k), tmp, tmp);
   solver->Mult(tmp,y); 

}

HelmholtzNormalReal::
HelmholtzNormalReal(mfem::SparseMatrix& M, mfem::SparseMatrix& A, double gamma):              
      Operator{M.Height()}, M{&M}, A{&A}, tmp{M.Height()}, gamma{gamma}          
{  }                                                                          
                                                                                 
void                                                                          
HelmholtzNormalReal::
Mult(const mfem::Vector& x, mfem::Vector& y) const 
{                                                                             
   A->Mult(x,tmp);                                                            
   A->Mult(tmp,y);                                                            
   M->Mult(x,tmp);                                                            
   M->AddMult(tmp,y,gamma);                                             
}                                                                             


SolveHelmholtzNormalMG::
SolveHelmholtzNormalMG(mfem::Array<mfem::SparseMatrix*>& M, 
		       mfem::Array<mfem::SparseMatrix*>& A,
		       mfem::FiniteElementSpaceHierarchy& hierarchy,
		       mfem::Vector& evs_time_):
   Operator{M.Last()->Height()}, K{evs_time_.Size()}, levels{M.Size()}, 
   evs_time{&evs_time_},
   evs_smoother{levels-1,K}
{

   //Set up Helmholtz Ops
   for (int i=0; i<levels; ++i){
      ops.Append(new HelmholtzNormalReal(*M[i], *A[i], 1.0));
   }

   //Calculate all EVS for the smoother (each level and all time steps)
   mfem::PowerMethod pm;
   mfem::Vector tmp(height);
   tmp = 0.0;

   for (int i=1; i<levels; ++i){
      mfem::Vector tmp_(tmp.GetData(), ops[i]->Height());
      for (int k_=0; k_<K; ++k_){
	 ops[i]->SetGamma(evs_time->Elem(k_));
	 evs_smoother(i-1,k_) = pm.EstimateLargestEigenvalue(*ops[i],tmp_);
      }
   }
   //std::cout << "Estimated EVS" << std::endl;
   //evs_smoother.Print();
   //Set up prec and solver
   mfem::Vector evs_smoother_k;
   evs_smoother.GetColumnReference(0,evs_smoother_k);
   prec = new ocst::GeometricMultigrid(hierarchy, ops, evs_smoother_k);
   solver = new ocst::SolveMGPCG(*prec);

   solver->SetSolverParameters(0, 1e-6,50,-1);  
}

SolveHelmholtzNormalMG::
~SolveHelmholtzNormalMG()
{
   delete solver;
   delete prec;
   for (int i=0; i<levels; ++i){
      delete ops[i];
   }
}

void
SolveHelmholtzNormalMG::
SetTimeStep(int k_)
{
   ocst::ChebyshevSmoother* smoother; 
   k=k_;

   ops[0]->SetGamma(evs_time->Elem(k));
   for (int i=1; i<levels; ++i){
      ops[i]->SetGamma(evs_time->Elem(k));
      smoother = static_cast<ocst::ChebyshevSmoother*>(
                                               prec->GetSmootherAtLevel(i));
      smoother->Setup(evs_smoother(i-1,k));
   }
}

HelmholtzReal::
HelmholtzReal(mfem::SparseMatrix& M, mfem::SparseMatrix& A, 
	      double sqrt_gamma):
   Operator{2*M.Height()}, M{&M}, A{&A}, sqrt_gamma{sqrt_gamma}
{   }


void
HelmholtzReal::
Mult(const mfem::Vector& x, mfem::Vector& y) const 
{
   double* x_data = const_cast<double*>(x.GetData());
   x_re.SetDataAndSize(x_data ,height/2);
   x_data+=height/2;
   x_im.SetDataAndSize(x_data ,height/2);
   y_re.MakeRef(y,0,height/2);
   y_im.MakeRef(y,height/2,height/2);

   A->Mult(x_re, y_re);
   M->AddMult(x_im, y_re, -sqrt_gamma);
   A->Mult(x_im, y_im);
   M->AddMult(x_re, y_im, sqrt_gamma);
}


} //ocst
