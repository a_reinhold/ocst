#include <spaceTimeRHS.hpp>

namespace mfem {
// v = v1 + alpha v2;
void add(const mfem::Vector& v1,
         double alpha,
         const mfem::Vector& v2,
         mfem::Vector& v);
}

namespace ocst {
PrimalRHS::PrimalRHS(int dof_time, int dof_space)
    : dof_time{dof_time}
    , dof_space{dof_space}
    , data{ocst::SpaceTimeData(dof_time, dof_space)}
    , constant_part{ocst::SpaceTimeData(dof_time, dof_space)}
{ }

PrimalRHS::~PrimalRHS() { }

void
PrimalRHS::SetConstantPart(mfem::FiniteElementSpace& fes_time,
                           mfem::FiniteElementSpace& fes_space,
                           mfem::Coefficient& robin_bc,
                           mfem::Coefficient& y_inital,
                           mfem::SparseMatrix& M_time)
{

   MFEM_VERIFY(dof_space == fes_space.GetVSize(), "PrimalRHS()");

   MFEM_VERIFY(dof_time == M_time.Height() + 1, "PrimalRHS()");

   mfem::LinearForm f(&fes_space);
   f.AddBoundaryIntegrator(new mfem::BoundaryLFIntegrator(robin_bc));

   // Generate the Vector with the evaluation points in Time
   mfem::GridFunction timesteps(&fes_time);
   mfem::FunctionCoefficient identity(
       [](const mfem::Vector& t) { return t(0); });
   timesteps.ProjectCoefficient(identity);

   // fill  tmp matrix with (robin_bc(t),phi)_L2(Gamma),
   // storage from data is used
   mfem::DenseMatrix tmp(data.Mat().GetData(), dof_space, timesteps.Size());
   for (int k = 0; k < timesteps.Size(); ++k) {
      robin_bc.SetTime(timesteps(k));
      f.Assemble();
      tmp.SetCol(k, f);
   }
   // view of the first dof_time-1 cols of constant part
   mfem::DenseMatrix view(
       constant_part.Mat().GetData(), dof_space, dof_time - 1);
   // c_I = tmp * M^T <=> c_I^T = M * tmp^T
   tensor::SparseDenseMM(tensor::Transposed::NON_TRANSPOSED,
                         tensor::Transposed::TRANSPOSED,
                         tensor::Transposed::TRANSPOSED,
                         M_time,
                         tmp,
                         view);

   // Set last col to (y_init,phi)_H
   y_inital.SetTime(timesteps(0));
   mfem::LinearForm y(&fes_space);
   y.AddDomainIntegrator(new mfem::DomainLFIntegrator(y_inital));
   y.Assemble();
   constant_part.Mat().SetCol(dof_time - 1, y);
}

void
PrimalRHS::Assemble(ocst::Control& u, double* buffer)
{
   // Set last column to 0, rest gets handled via view;
   mfem::Vector data_K;
   data.Mat().GetColumnReference(dof_time - 1, data_K);
   data_K = 0.0;
   mfem::DenseMatrix view(data.Mat().GetData(), dof_space, dof_time - 1);

   tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                       1.0,
                       u.M_control_time(),
                       u.M_control_space(),
                       u.Mat(),
                       0.0,
                       buffer,
                       view);
   data.Mat() += constant_part.Mat();
}

void
PrimalRHS::Assemble(ocst::Control& u)
{
   double* buffer = new double[dof_time * dof_space];
   Assemble(u, buffer);
   delete[] buffer;
}

AdjointRHS::AdjointRHS(int dof_time,
                       int dof_space,
                       const double omega_1,
                       const double omega_2)
    : dof_time{dof_time}
    , dof_space{dof_space}
    , data{dof_time, dof_space}
    , constant_part{dof_time, dof_space}
    , omega_1{omega_1}
    , omega_2{omega_2}
{ }

void
AdjointRHS::SetConstantPart(ocst::SpaceTimeData& y_d,
                            mfem::Vector& y_d_T,
                            mfem::SparseMatrix& M_time,
                            mfem::SparseMatrix& M_space)
{
   if (omega_1 > 0.0) {
      // constant_part <- -omega_1 * (M_time x M_space) y_d
      tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                          -omega_1,
                          M_time,
                          M_space,
                          y_d.Mat(),
                          0.0,
                          data.Mat().GetData(),
                          constant_part.Mat());
   } else {
      constant_part.Mat() = 0.0;
   }
   if (omega_2 > 0.0) {
      mfem::Vector constant_part_K;
      constant_part.Mat().GetColumnReference(dof_time - 1, constant_part_K);
      mfem::Vector part_II(y_d_T.Size());
      M_space.Mult(y_d_T, part_II);
      mfem::add(constant_part_K, -omega_2, part_II, constant_part_K);
   }
}

void
AdjointRHS::Assemble(ocst::State& y, double* buffer)
{
   if (omega_1 > 0.0) {
      // data <- omega_1 * (M_time x M_space) y
      tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                          omega_1,
                          y.M_time(),
                          y.M_space(),
                          y.Mat(),
                          0.0,
                          buffer,
                          data.Mat());
   } else {
      data.Mat() = 0.0;
   }
   if (omega_2 > 0.0) {
      mfem::Vector data_K;
      data.Mat().GetColumnReference(dof_time - 1, data_K);

      mfem::Vector y_K;
      y.Mat().GetColumnReference(dof_time - 1, y_K);

      mfem::Vector part_II(y_K.Size());

      y.M_space().Mult(y_K, part_II);
      mfem::add(data_K, omega_2, part_II, data_K);
   }
   data.Mat() += constant_part.Mat();
}

void
AdjointRHS::Assemble(ocst::State& y)
{
   double* buffer = new double[dof_time * dof_space];
   Assemble(y, buffer);
   delete[] buffer;
}

} // ocst
