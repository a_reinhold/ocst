#include <optimalControlSolver.hpp>
namespace ocst{
ProjectedGradientMethod::
ProjectedGradientMethod(ocst::OptimalControlProblem& _ocp,
			ocst::SpaceTimeSolver& _lin_solver,
			const int max_it, 
			const double s_0,
			const double s_min/* = 1E-4*/, 
			const double tol_rel  /*= 1E-4*/, 
			const double tol_abs  /*= 1E-6*/, 
			const double tol_stag /*= 1E-6*/):
   ocp{&_ocp}, lin_solver{&_lin_solver}, max_it{max_it}, 
   s_0{s_0},  s_min{s_min},
   tol_rel{tol_rel}, tol_abs{tol_abs}, tol_stag{tol_stag},
   s{max_it}, J{max_it}, 
   buffer_1{ new double[std::max(_ocp.Control().Vec().Size(),
                                 _ocp.AdjointState().Vec().Size())]},
   buffer_2{ new double[std::max(_ocp.Control().Vec().Size(),
                                 _ocp.AdjointState().Vec().Size())]},
   u_{_ocp.Control()},
   diff{_ocp.Control()},
   v{_ocp.Control()}
{   }

ProjectedGradientMethod::
~ProjectedGradientMethod()
{
   delete[] buffer_1; 
   delete[] buffer_2; 
}

void
ProjectedGradientMethod::
Solve()
{
   
   double norm_ref_tol = 0.0; 
   double norm_diff_l = 0.0;
   int l=0;
   double s_ = 0; 

   lin_solver->SolvePrimal(ocp->Control(), ocp->State(), buffer_1);
   nof_evaluations += 1;

   s[l] = s_0;
   J[l] = ocp->EvalObjectiveFunction(buffer_1, buffer_2); 
   
   for (l=1; l<max_it; ++l){
      lin_solver->SolveAdjoint(ocp->State(), ocp->AdjointState(), buffer_1);
      nof_evaluations += 1;
      
      //Search Direction
      SearchDirection();
      s_ = s[l-1];
      while (true){
	 //u_ is previous control 
	 //u = u_ - s_ * v
	 mfem::Add(u_.Mat(), v.Mat(), -s_, ocp->Control().Mat());
	 Projection();
	 
	 //diff = u_ - u 
	 mfem::Add(u_.Mat(), ocp->Control().Mat(), -1.0, diff.Mat());
	 //calculate norms 
	 if (l==1){
	    norm_diff_l = diff.SquaredInnerProductNorm(buffer_1, buffer_2);
	    norm_ref_tol = tol_rel * norm_diff_l + tol_abs;
	 } else {
	    norm_diff_l = diff.SquaredInnerProductNorm(buffer_1, buffer_2);
	 }

	 lin_solver->SolvePrimal(ocp->Control(), ocp->State(), buffer_1);
	 nof_evaluations += 1;
	 J[l] = ocp->EvalObjectiveFunction(buffer_1, buffer_2);
	 if( norm_diff_l < norm_ref_tol || std::fabs(J[l-1]-J[l]) < tol_stag){
	    
	    u_.SwapData(ocp->Control());
	    s[l] = s_;
	    s.SetSize(l+1); J.SetSize(l+1);
	    std::cout <<"PGM Converged after " << l << " iterations" 
	              << std::endl 
		      << "\tnof evaluations of the linear eq: " 
		      << nof_evaluations << std::endl 
		      << "\t||u(l-1)-u(l)|| = " << norm_diff_l << std::endl
		      << "\ttau_rel||u_0-u_1|| + tau_abs = " << norm_ref_tol 
		      << std::endl
		      << "\t|J(l-1)-J(l)| = "<< std::fabs(J[l-1] -J[l]) 
		      << std::endl
		      << "\tJ(l) = " << J[l] << std::endl;  
	    return; 
	 } else if(J[l]<J[l-1]) { 
	    u_.SwapData(ocp->Control());
	    s[l] = s_; 
	    break;
	 } else if (s_ > s_min) {
	    s_ *= 0.5; 
	 } else {
	    std::cout << "PGM did not converged, stepsize too small after " 
	              << l << " iterations" << std::endl 
		      << "\tnof evaluations of the linear eq: " 
		      << nof_evaluations << std::endl 
		      << "\t||u(l-1)-u(l)|| = " << norm_diff_l << std::endl
		      << std::endl
		      << "\t|J(l-1)-J(l)| = "<< std::fabs(J[l-1] -J[l]) 
		      << std::endl
		      << "\tJ(l) = " << J[l] << std::endl;  
	    
	    return;
	    
	 }
      }
   }
   std::cout << "PGM did not converge, max_it (" << max_it << ") reached "
             << std::endl
	     << "\tnof evaluations of the linear eq: " 
	     << nof_evaluations << std::endl 
	     << "\t||u(l-1)-u(l)|| = " << norm_diff_l << std::endl
	     << "\t|J(l-1)-J(l)| = "<< std::fabs(J[l-1] -J[l]) 
	     << std::endl; 
}

void
ProjectedGradientMethod::Projection()    
{
  mfem::DenseMatrix& u = ocp->Control().Mat();
  const mfem::DenseMatrix& lb = ocp->ControlLB().Mat();
  const mfem::DenseMatrix& ub = ocp->ControlUB().Mat();
  for (int j=0; j<u.Width(); ++j){
      for (int i=0; i<u.Height(); ++i){
	 u(i,j) = (u(i,j) < lb(i,j)) ? lb(i,j) :
	          (u(i,j) > ub(i,j)) ? ub(i,j) :
		  u(i,j);
      }
   }
}

void
ProjectedGradientMethod::SearchDirection()
{
   bool conform_time  = ocp->Control().ConformityTime();
   bool conform_space = ocp->Control().ConformitySpace();
   mfem::DenseMatrix& p_I = ocp->AdjointState().AdjointState_I();  
   if (conform_time && conform_space){
      /* same basis function as part I of adjoint state for control*/
      //v =  p_I + lambda u 
      mfem::Add(1.0, p_I, 
		ocp->Lambda(), ocp->Control().Mat(), v.Mat());
		  
   } else if (conform_time && !conform_space){
      mfem::GridFunction space_in(
			      &ocp->AdjointState().FES_Space(), 
                              p_I.GetData());
      mfem::GridFunction space_out(
                              &ocp->Control().FES_Space(),
			      v.Mat().GetData()); 
      // v = Projection(p_I)(Transfer data in space for each time step)
      for (int k=0; k< p_I.Width(); ++k){
	 space_in.SetData(p_I.GetColumn(k));
	 space_out.SetData(v.Mat().GetColumn(k));
	 space_out.ProjectGridFunction(space_in);
      }
      //v =  p_I + lambda u 
      mfem::Add(1.0, v.Mat(), 
		ocp->Lambda(), ocp->Control().Mat(), v.Mat());

   } else if (!conform_time && conform_space){
      mfem::GridFunction time_in(&ocp->AdjointState().FES_Time());
      mfem::GridFunction time_out(&ocp->Control().FES_Time());
      // v = Projection(p_I)(Transfer data in time for each dof))
      for (int i = 0; i< v.Mat().Height(); ++i){
	 p_I.GetRow(i, time_in);
	 time_out.ProjectGridFunction(time_in);
	 v.Mat().SetRow(i, time_out);
      }
      mfem::Add(1.0, v.Mat(), 
      	       ocp->Lambda(), ocp->Control().Mat(), v.Mat());
   } else {
      int K_test = p_I.Width();
      int n_test = p_I.Height();
      int K_control = v.Mat().Width();
      int n_control = v.Mat().Height();
     
      //INterpolation in time into a tmp matrix 
      mfem::DenseMatrix tmp_mat(buffer_1, n_test, K_control);
      mfem::GridFunction time_in(&ocp->AdjointState().FES_Time());
      mfem::GridFunction time_out(&ocp->Control().FES_Time());
      for (int i = 0; i< n_test; ++i){
	 p_I.GetRow(i, time_in);
	 time_out.ProjectGridFunction(time_in);
	 tmp_mat.SetRow(i, time_out);
      }
      //Interpolation in space from tmp matrix to v 
      mfem::GridFunction space_in(
			      &ocp->AdjointState().FES_Space(), 
                              tmp_mat.GetData());
      mfem::GridFunction space_out(
                              &ocp->Control().FES_Space(),
			      v.Mat().GetData()); 
      // v = Projection(p_I)(Transfer data in space for each time step)
      for (int k=0; k< K_control; ++k){
	 space_in.SetData(tmp_mat.GetColumn(k));
	 space_out.SetData(v.Mat().GetColumn(k));
	 space_out.ProjectGridFunction(space_in);
      }
      mfem::Add(1.0, v.Mat(), 
      	       ocp->Lambda(), ocp->Control().Mat(), v.Mat());


   }


}

void
ProjectedGradientMethod::
PrintResults()
{
   //header 
   std::cout << "iter\tstepsize\tJ" << std::endl;
   for (int i=0; i< s.Size(); ++i){
      std::cout <<
	 i << "\t" << 
	 s[i] << "\t" << 
	 J[i] << "\t" << 
	 std::endl;
   }
}

} //ocst
