#include <optimalControlProblem.hpp>

namespace ocst{

OptimalControlProblem::
OptimalControlProblem(mfem::FiniteElementSpace& fes_time_trial,
                      mfem::FiniteElementSpace& fes_time_test,
                      mfem::FiniteElementSpace& fes_space,
                      mfem::FiniteElementSpace& fes_time_control,
		      mfem::FiniteElementSpace& fes_space_control,
		      bool conform_time, bool conform_space,
		      const double lambda,
		      const double omega_1/* = 1.0*/, 
		      const double omega_2/* = 1.0*/):
   u{fes_time_control, fes_space_control, conform_time, conform_space}, 
   u_a{u}, u_b{u},
   y{fes_time_trial, fes_space}, p{fes_time_test, fes_space}, y_d{y},
   prhs{fes_time_test.GetVSize() + 1, fes_space.GetVSize()},
   arhs{fes_time_trial.GetVSize(), fes_space.GetVSize(), omega_1, omega_2},
   lambda{lambda}, omega_1{omega_1}, omega_2{omega_2}
{
   MFEM_VERIFY(lambda  > 0, "lambda <= 0!");
   MFEM_VERIFY(omega_1 >= 0, "omega_1 < 0!");
   MFEM_VERIFY(omega_2 >= 0, "omega_2 < 0!");
}

OptimalControlProblem::
OptimalControlProblem(mfem::FiniteElementSpace& fes_time_trial,
                      mfem::FiniteElementSpace& fes_time_test,
                      mfem::FiniteElementSpace& fes_space,
		      const double lambda,
		      const double omega_1/* = 1.0*/, 
		      const double omega_2/* = 1.0*/):
   u{fes_time_test, fes_space, true, true}, u_a{u}, u_b{u},
   y{fes_time_trial, fes_space}, p{fes_time_test, fes_space}, y_d{y},
   prhs{fes_time_test.GetVSize() + 1, fes_space.GetVSize()},
   arhs{fes_time_trial.GetVSize(), fes_space.GetVSize(), omega_1, omega_2},
   lambda{lambda}, omega_1{omega_1}, omega_2{omega_2}
{
   MFEM_VERIFY(lambda  > 0, "lambda <= 0!");
   MFEM_VERIFY(omega_1 >= 0, "omega_1 < 0!");
   MFEM_VERIFY(omega_2 >= 0, "omega_2 < 0!");
}

void
OptimalControlProblem::
SetConstantPartRHS(mfem::Coefficient& y_0, mfem::Coefficient& robin_rhs, 
                   mfem::SparseMatrix& M_time_mixed)
{
   prhs.SetConstantPart(y.FES_Time(), y.FES_Space(), 
                        robin_rhs, y_0, M_time_mixed);
   arhs.SetConstantPart(y_d, y_d_T, y.M_time(), y.M_space());
}

double
OptimalControlProblem::
EvalObjectiveFunction()
{ 
   int buffersize = u.Vec().Size()>y.Vec().Size() 
                            ? u.Vec().Size() : y.Vec().Size();
   double* buffer_1 = new double[buffersize];
   double* buffer_2 = new double[buffersize];

   double res = EvalObjectiveFunction(buffer_1, buffer_2); 
   delete[] buffer_1;
   delete[] buffer_2;
   return res; 
}

double
OptimalControlProblem::
EvalObjectiveFunction(double* buffer_1, double* buffer_2)
{
   mfem::Vector y_T, diff;
   y.Mat().GetColumnReference(y.Mat().Width()-1,y_T);
   diff.SetDataAndSize(buffer_1, y.Mat().Height());
   diff.SetVector(y_T,0);
   diff -= y_d_T;
   double res = 0.0;
   if (omega_2 > 0.0){
      res += omega_2 * 0.5 * y.M_space().InnerProduct(diff, diff);
   }
   if (omega_1 > 0.0){
      res += omega_1 * 0.5 * y.SquaredInnerProductNorm(y_d, buffer_1, buffer_2);  
   }
   res += lambda * 0.5 * u.SquaredInnerProductNorm(buffer_1, buffer_2); 	
   return res;
}

void
OptimalControlProblem::
SetControlConstrains(mfem::Coefficient& u_a_coeff, 
		     mfem::Coefficient& u_b_coeff)
{
   u_a.SetValue(u_a_coeff);
   u_b.SetValue(u_b_coeff);

   for (int i=0; i<u_a.Vec().Size(); ++i){
      MFEM_VERIFY(u_b.Vec()(i) - u_a.Vec()(i) > 0.0, 
                       "Upper bound below lower bound");
   }
}

void
OptimalControlProblem::
SetControlConstrains(double u_a_value, double u_b_value)
{
   MFEM_VERIFY(u_b_value - u_a_value > 0.0, 
		     "Upper bound below lower bound");
   u_a.Mat() = u_a_value;
   u_b.Mat() = u_b_value;
}


void
OptimalControlProblem::
SetDesiredState(mfem::Coefficient& y_d_coeff)
{
   y_d.SetValue(y_d_coeff);
   y_d.Mat().GetColumnReference(y.Mat().Width()-1, y_d_T);
}

void
OptimalControlProblem::
SetDesiredState(mfem::Coefficient& y_d_coeff, mfem::Coefficient& y_d_T_coeff,
                double end_time /* = 1.0*/)
{
   y_d.SetValue(y_d_coeff);

   mfem::GridFunction tmp(&y_d.FES_Space());
   y_d_T_coeff.SetTime(end_time);
   tmp.ProjectCoefficient(y_d_T_coeff);
   y_d_T.SetData(tmp.StealData()); 
}


} //ocst
