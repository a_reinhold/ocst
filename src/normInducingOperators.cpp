#include <normInducingOperators.hpp>

namespace ocst {

NormOperator::NormOperator(int size,
                           mfem::SparseMatrix& M_time,
                           mfem::SparseMatrix& M_space,
                           mfem::SparseMatrix& V_space,
                           int K,
                           int n_h,
                           double* buffer_ /* = nullptr*/)
    : mfem::Operator{size}
    , M_time{&M_time}
    , M_space{&M_space}
    , V_space{&V_space}
    , K{K}
    , n_h{n_h}
{
   if (!buffer_) {
      buffer = new double[n_h * K];
      own_buf = true;
   } else {
      buffer = buffer_;
      own_buf = false;
   }
}

NormOperator::~NormOperator()
{
   if (Inv && own_Inv)
      delete Inv;
   if (own_buf)
      delete[] buffer;
}

void
NormOperator::SetInverseOperator(mfem::Operator& solver)
{
   Inv = &solver;
   own_Inv = false;
}

void
NormOperator::MultInverse(const mfem::Vector& x, mfem::Vector& y) const
{
   MFEM_VERIFY(Inv, "Inverse operator not set!");
   Inv->Mult(x, y);
}

void
NormOperator::Mult(const ocst::SpaceTimeData& x, ocst::SpaceTimeData& y) const
{
   Mult(x.Vec(), y.Vec());
}

void
NormOperator::MultInverse(const ocst::SpaceTimeData& x,
                          ocst::SpaceTimeData& y) const
{
   MultInverse(x.Vec(), y.Vec());
}

NormOperatorTest::NormOperatorTest(mfem::SparseMatrix& M_time,
                                   mfem::SparseMatrix& M_space,
                                   mfem::SparseMatrix& V_space,
                                   double* buffer_ /*=nullptr*/)
    : ocst::NormOperator{(M_time.Height() + 1) * M_space.Height(),
                         M_time,
                         M_space,
                         V_space,
                         M_time.Height(),
                         M_space.Height(),
                         buffer_}
{ }

void
NormOperatorTest::SetInverseOperator(mfem::Operator& solve_V,
                                     mfem::Operator& solve_M)
{
   if (Inv && own_Inv)
      delete Inv;
   Inv = new InvNormOperatorTest(*M_time, solve_V, solve_M, K, n_h, buffer);
   own_Inv = true;
}
void
NormOperatorTest::Mult(const mfem::Vector& x, mfem::Vector& y) const
{
   // y_I =(M_t x V_h) * x_I

   double* x_data = const_cast<double*>(x.GetData());
   const mfem::DenseMatrix x_I(x_data, n_h, K);
   mfem::DenseMatrix y_I(y.GetData(), n_h, K);

   tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                       1.0,
                       *M_time,
                       *V_space,
                       x_I,
                       0.0,
                       buffer,
                       y_I);

   // y_II = M_h * x_II
   x_data = x_data + (n_h * K);
   const mfem::Vector x_II(x_data, n_h);
   mfem::Vector y_II(&y(n_h * K), n_h);
   M_space->Mult(x_II, y_II);
}

InvNormOperatorTest::InvNormOperatorTest(const mfem::SparseMatrix& M_time,
                                         mfem::Operator& solve_V,
                                         mfem::Operator& solve_M,
                                         int K,
                                         int n_h,
                                         double* buffer)
    : M_time_inv{M_time, false}
    , solve_V{&solve_V}
    , solve_M{&solve_M}
    , K{K}
    , n_h{n_h}
    , buffer{buffer}
{
   for (int k = 0; k < K; ++k) {
      M_time_inv.GetData()[k] = 1 / M_time_inv.GetData()[k];
   }
}

void
InvNormOperatorTest::Mult(const mfem::Vector& x, mfem::Vector& y) const
{
   ocst::SpaceTimeData tmp(K, n_h, buffer);
   mfem::Vector tmp_;
   mfem::Vector x_;
   double* x_data = const_cast<double*>(x.GetData());
   for (int k = 0; k < K; ++k) {
      tmp_.MakeRef(tmp.Vec(), k * n_h, n_h);
      x_.SetDataAndSize(x_data, n_h);
      solve_V->Mult(x_, tmp_);
      x_data += n_h;
   }
   mfem::DenseMatrix Y(y.GetData(), n_h, K);
   tensor::SparseDenseMM(tensor::Transposed::NON_TRANSPOSED,
                         tensor::Transposed::TRANSPOSED,
                         tensor::Transposed::TRANSPOSED,
                         M_time_inv,
                         tmp.Mat(),
                         Y);
   // x_data += n_h;
   x_.SetDataAndSize(x_data, n_h);
   tmp_.MakeRef(y, K * n_h, n_h);
   solve_M->Mult(x_, tmp_);
}

void
InvNormOperatorTest::Mult(const ocst::SpaceTimeData& x,
                          ocst::SpaceTimeData& y) const
{
   Mult(x.Vec(), y.Vec());
}

NormOperatorTrial::NormOperatorTrial(mfem::SparseMatrix& M_time,
                                     mfem::SparseMatrix& A_time,
                                     mfem::SparseMatrix& M_space,
                                     mfem::SparseMatrix& V_space,
                                     mfem::Operator& solve_V,
                                     bool evalEndTime /*=true*/,
                                     double* buffer_ /*=nullptr*/)
    : NormOperator{M_time.Height() * M_space.Height(),
                   M_time,
                   M_space,
                   V_space,
                   M_time.Height(),
                   M_space.Height(),
                   buffer_}
    , A_time{&A_time}
    , solve_V{&solve_V}
    , evalEndTime{evalEndTime}
{ }

void
NormOperatorTrial::Mult(const mfem::Vector& x, mfem::Vector& y) const
{
   double* x_data = const_cast<double*>(x.GetData());
   ocst::SpaceTimeData X(K, n_h, x_data);
   ocst::SpaceTimeData Y(K, n_h, y.GetData());
   ocst::SpaceTimeData tmp(K, n_h, buffer);
   // I
   // y <- (A_t x M_h inv(V_h) M_h) x

   // tmp <- M_h*X
   tensor::SparseDenseMM(tensor::Transposed::NON_TRANSPOSED,
                         tensor::Transposed::NON_TRANSPOSED,
                         tensor::Transposed::NON_TRANSPOSED,
                         *M_space,
                         X.Mat(),
                         tmp.Mat());
   // Y <- inv(V_h)tmp
   mfem::Vector y_k, tmp_k;
   for (int k = 0; k < K; ++k) {
      y_k.MakeRef(y, k * n_h, n_h);
      tmp_k.MakeRef(tmp.Vec(), k * n_h, n_h);
      solve_V->Mult(tmp_k, y_k);
   }
   // Y <- M_h Y A_t
   tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                       1.0,
                       *A_time,
                       *M_space,
                       Y.Mat(),
                       0.0,
                       buffer,
                       Y.Mat());

   // II
   // y <- y + (M_t x V_h) x
   tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                       1.0,
                       *M_time,
                       *V_space,
                       X.Mat(),
                       1.0,
                       buffer,
                       Y.Mat());

   // III (Optional)
   // y_K += M_h x_K
   if (evalEndTime) {
      y_k.MakeRef(y, (K - 1) * n_h, n_h);
      tmp_k.MakeRef(X.Vec(), (K - 1) * n_h, n_h);
      M_space->AddMult(tmp_k, y_k);
   }
}

void
NormOperatorTrial::SetInverseOperator(
    mfem::Array<mfem::SparseMatrix*>& M_space,
    mfem::Array<mfem::SparseMatrix*>& A_space,
    mfem::FiniteElementSpaceHierarchy& hierarchy)
{
   if (Inv && own_Inv)
      delete Inv;
   evalEndTime = false;
   Inv = new InvNormOperatorTrial(
       *M_time, *A_time, M_space, A_space, hierarchy, buffer);
   own_Inv = true;
}

InvNormOperatorTrial::InvNormOperatorTrial(
    mfem::SparseMatrix& M_time,
    mfem::SparseMatrix& A_time,
    mfem::Array<mfem::SparseMatrix*>& M_space_,
    mfem::Array<mfem::SparseMatrix*>& A_space_,
    mfem::FiniteElementSpaceHierarchy& hierarchy,
    double* buffer)
    : mfem::Operator{M_time.Height() * M_space_.Last()->Height()}
    , dim_time{M_time.Height()}
    , dim_space{M_space_.Last()->Height()}
    , V_time{mfem::DenseMatrix(dim_time)}
    , gamma_time{mfem::Vector(dim_time)}
    , tmp{ocst::SpaceTimeData(dim_time, dim_space, buffer)}
{
   // Create V_t, gamma_t (solve generalized EV Problem)

   mfem::DenseMatrix M_t_dense;
   if (dim_time <= dim_space) {
      M_t_dense.UseExternalData(buffer, dim_time, dim_time);
   } else {
      M_t_dense.SetSize(dim_time);
   }
   M_time.ToDenseMatrix(M_t_dense);
   A_time.ToDenseMatrix(V_time);

#ifndef _OCST_LAPACK
   MFEM_ABORT("InvNormOperatorTrial needs lapack");
#else
#ifdef _OCST_USE_ACCELERATE
   int N = dim_time;
   int itype = 1;

   char jobz = 'V';
   char uplo = 'U';
   int info = 0;
   int lwork = (16 + 2) * N;
   double* work = new double[lwork];
   dsygv_(&itype,
          &jobz,
          &uplo,
          &N,
          V_time.GetData(),
          &N,
          M_t_dense.GetData(),
          &N,
          gamma_time.GetData(),
          work,
          &lwork,
          &info);

   delete[] work;
#else
   lapack_int info = LAPACKE_dsygv(LAPACK_COL_MAJOR,
                                   1,
                                   'V',
                                   'U',
                                   dim_time,
                                   V_time.GetData(),
                                   dim_time,
                                   M_t_dense.GetData(),
                                   dim_time,
                                   gamma_time.GetData());
#endif
   if (info != 0) {
      MFEM_ABORT("Generalized EV Computation not succesfull exit with code "
                 << info);
   }
   for (int k = 0; k < dim_time; ++k) {
      gamma_time(k) = std::sqrt(std::abs(gamma_time(k)));
   }
   solve_hh = new SolveHelmholtzRealValued(
       M_space_, A_space_, hierarchy, gamma_time);

#endif
}

InvNormOperatorTrial::~InvNormOperatorTrial()
{
   // if (solve_hh_normal) delete solve_hh_normal;
   if (solve_hh)
      delete solve_hh;
}

void
InvNormOperatorTrial::Mult(const mfem::Vector& x, mfem::Vector& y) const
{

   double* x_data = const_cast<double*>(x.GetData());
   mfem::DenseMatrix X(x_data, dim_space, dim_time);
   mfem::DenseMatrix Y(y.GetData(), dim_space, dim_time);
#ifdef _OCST_BLAS
   // Y <- X*V_time
   cblas_dgemm(CblasColMajor,
               CblasNoTrans,
               CblasNoTrans,
               dim_space,
               dim_time,
               dim_time,
               1.0,
               x_data,
               dim_space,
               V_time.GetData(),
               dim_time,
               0.0,
               Y.GetData(),
               dim_space);
   ////Tmp <- SolveHelmholtz(Y)
   mfem::Vector tmp_k;
   mfem::Vector y_k;
   for (int k = 0; k < dim_time; ++k) {
      tmp_k.MakeRef(tmp.Vec(), k * dim_space, dim_space);
      y_k.MakeRef(y, k * dim_space, dim_space);
      solve_hh->SetTimeStep(k);
      solve_hh->Mult(y_k, tmp_k);
   }
   // Y <- tmp*V_t^T
   cblas_dgemm(CblasColMajor,
               CblasNoTrans,
               CblasTrans,
               dim_space,
               dim_time,
               dim_time,
               1.0,
               tmp.Mat().GetData(),
               dim_space,
               V_time.GetData(),
               dim_time,
               0.0,
               Y.GetData(),
               dim_space);

#else
   MFEM_ABORT("InvNormOperatorTrial.Mult() needs blas");
#endif
}

} // ocst
