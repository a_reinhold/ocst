import sys
import os

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np


def read_data_from_file(fname):
    with open(fname,'r') as infile:

        all_data = infile.read()
        all_data = all_data.split('#')
        y_raw = all_data[1].split('\n',1)[1]
        y_list = [float(elem) for elem in y_raw.split()]
        Y = np.array(y_list)

        x_raw = all_data[2].split('\n',1)[1]
        x_list = [float(elem) for elem in x_raw.split()]
        X = np.array(x_list)

        data_raw = all_data[3].split('\n',1)[1].split('[')[1:]
        Z = np.empty((X.shape[0],Y.shape[0]))
        row_ind = 0
        for row in data_raw: 
            row_list = [float(elem) for elem in row.split('\n',1)[1].split()]
            Z[row_ind, :] = row_list
            row_ind += 1
        
    ind_X = np.argsort(X)
    ind_Y = np.argsort(Y)

    X , Y = np.meshgrid(X[ind_X], Y[ind_Y])
    
    Z = Z[ind_X]
    Z = Z[:, ind_Y]
    Z = Z.transpose()

    return X, Y, Z

if __name__ == '__main__':
    

    # mode = 'interactive'
    mode = 'pgf'
    if (len(sys.argv)<2):
        input_data = {
            'y_alpha10_K21_t1':'surf_jump_1D_ALPHA_10_K_21_ls_3_type1/y_surfdata.txt',
            'y_alpha10_K21_t2':'surf_jump_1D_ALPHA_10_K_21_ls_3_type2/y_surfdata.txt',
            'y_alpha10_K501_t1':'surf_jump_1D_ALPHA_10_K_501_ls_3_type1/y_surfdata.txt',
            'y_alpha10_K501_t2':'surf_jump_1D_ALPHA_10_K_501_ls_3_type2/y_surfdata.txt',
            'y_alpha50_K21_t1':'surf_jump_1D_ALPHA_50_K_21_ls_3_type1/y_surfdata.txt',
            'y_alpha50_K21_t2':'surf_jump_1D_ALPHA_50_K_21_ls_3_type2/y_surfdata.txt',
            'y_alpha50_K501_t1':'surf_jump_1D_ALPHA_50_K_501_ls_3_type1/y_surfdata.txt',
            'y_alpha50_K501_t2':'surf_jump_1D_ALPHA_50_K_501_ls_3_type2/y_surfdata.txt'
        } 
    else:
        input_data = {'from_cmd':sys.argv[1]}
        



    xlabel = r'$x$'
    ylabel = r'$t$'
    zlabel = r'$y(t,x)$'
    figure_width = 3.4 # in inches
    figure_aspect_ratio = float(4/3) #width /height
    figure_height= 1/figure_aspect_ratio * figure_width
    
    
    tex_prefix = 'surf/'


    for key in input_data:
        out_path = '/home/alex/Dropbox/masterthesis/latex/surf'
        out_fname = key
        out_name = os.path.join(out_path,out_fname)    
        in_file = input_data[key]
        if mode=='pgf':
            matplotlib.use("pgf")
            matplotlib.rcParams.update({
                "pgf.texsystem": "pdflatex",
                'font.family': 'serif',
                'text.usetex': True,
                'pgf.rcfonts': False,
            })

        fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
        fig.set_size_inches(w=figure_width, h=figure_height)

        X, Y, Z = read_data_from_file(in_file)

        # Plot the surface.

        surf = ax.plot_surface(Y, X, Z,
                               rcount = max(X.shape),ccount=max(X.shape), 
                               cmap=cm.viridis,
                               linewidth=0, antialiased=False, 
                               rasterized=True)
        

        # Camera position
        ax.azim = 144.0
        ax.elev =  12.0



        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_zlabel(zlabel)
        # Add a color bar which maps values to colors.
        fig.colorbar(surf, shrink=0.7, pad=0.02,  location='left', orientation='vertical' )
        if mode=='pgf':
            plt.savefig(out_name+'.pgf', dpi=500, bbox_inches='tight')
            ' parse file and insert tex path prefix '
            with open(out_name+'.pgf', 'r')as infile:
                with open(out_name+'mod.pgf', 'w') as outfile:
                    for line in infile:
                        if out_fname in line:
                            new_line=line.replace(out_fname,tex_prefix+out_fname)
                            outfile.write(new_line)
                        else:
                            outfile.write(line)


        else:
            plt.show()

