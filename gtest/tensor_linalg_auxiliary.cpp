
#include "../src/tensor/linalg_auxiliary.hpp"
#include <gtest/gtest.h>
#include <mfem.hpp>
namespace {

#include "tensor_kronecker.data"
using ::testing::TestWithParam;
using ::testing::Values;

typedef std::
    tuple<mfem::SparseMatrix*, mfem::SparseMatrix*, mfem::SparseMatrix*>
        sp_mat_tuple;

class KroneckerMVParamTest : public TestWithParam<sp_mat_tuple> {
protected:
   mfem::SparseMatrix* A{std::get<0>(GetParam())};
   mfem::SparseMatrix* B{std::get<1>(GetParam())};
   mfem::SparseMatrix* AxB{std::get<2>(GetParam())};
};

TEST_P(KroneckerMVParamTest, Mult)
{

   mfem::Vector x{AxB->Width()};
   mfem::Vector y_expected{AxB->Height()}, y{AxB->Height()};
   x = 1.0;
   mfem::DenseMatrix X(x.GetData(), B->Width(), A->Width());
   mfem::DenseMatrix Y(y.GetData(), B->Height(), A->Height());
   size_t buf_size
       = std::min(A->Width() * B->Height(), A->Height() * B->Width());
   std::vector<double> buffer(buf_size);

   AxB->Mult(x, y_expected);
   tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                       1.0,
                       *A,
                       *B,
                       X,
                       0.0,
                       buffer.data(),
                       Y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   x.Randomize(763485);
   AxB->Mult(x, y_expected);
   tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                       1.0,
                       *A,
                       *B,
                       X,
                       0.0,
                       buffer.data(),
                       Y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}
TEST_P(KroneckerMVParamTest, AddMult)
{

   mfem::Vector x{AxB->Width()};
   mfem::Vector y_expected{AxB->Height()}, y{AxB->Height()};
   x = 1.0;
   mfem::DenseMatrix X(x.GetData(), B->Width(), A->Width());
   mfem::DenseMatrix Y(y.GetData(), B->Height(), A->Height());
   size_t buf_size
       = std::min(A->Width() * B->Height(), A->Height() * B->Width());
   std::vector<double> buffer(buf_size);

   y = 1.0;
   y_expected = 1.0;
   double alpha = 7.46572732;
   AxB->AddMult(x, y_expected, alpha);
   tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                       alpha,
                       *A,
                       *B,
                       X,
                       1.0,
                       buffer.data(),
                       Y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   x.Randomize(763485);
   y_expected.Randomize(42);
   y.Randomize(42);
   AxB->AddMult(x, y_expected, alpha);
   tensor::KroneckerMV(tensor::Transposed::NON_TRANSPOSED,
                       alpha,
                       *A,
                       *B,
                       X,
                       1.0,
                       buffer.data(),
                       Y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}
TEST_P(KroneckerMVParamTest, MultTranspose)
{

   mfem::Vector x{AxB->Height()};
   mfem::Vector y_expected{AxB->Width()}, y{AxB->Width()};
   x = 1.0;
   mfem::DenseMatrix X(x.GetData(), B->Height(), A->Height());
   mfem::DenseMatrix Y(y.GetData(), B->Width(), A->Width());
   size_t buf_size
       = std::min(A->Width() * B->Height(), A->Height() * B->Width());
   std::vector<double> buffer(buf_size);

   AxB->MultTranspose(x, y_expected);
   tensor::KroneckerMV(
       tensor::Transposed::TRANSPOSED, 1.0, *A, *B, X, 0.0, buffer.data(), Y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   x.Randomize(1284376);
   AxB->MultTranspose(x, y_expected);
   tensor::KroneckerMV(
       tensor::Transposed::TRANSPOSED, 1.0, *A, *B, X, 0.0, buffer.data(), Y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}

TEST_P(KroneckerMVParamTest, AddMultTranspose)
{

   mfem::Vector x{AxB->Height()};
   mfem::Vector y_expected{AxB->Width()}, y{AxB->Width()};
   x = 1.0;
   mfem::DenseMatrix X(x.GetData(), B->Height(), A->Height());
   mfem::DenseMatrix Y(y.GetData(), B->Width(), A->Width());
   size_t buf_size
       = std::min(A->Width() * B->Height(), A->Height() * B->Width());
   std::vector<double> buffer(buf_size);

   y_expected = 1.0;
   y = 1.0;
   double alpha = -.34585328;
   AxB->AddMultTranspose(x, y_expected, alpha);
   tensor::KroneckerMV(tensor::Transposed::TRANSPOSED,
                       alpha,
                       *A,
                       *B,
                       X,
                       1.0,
                       buffer.data(),
                       Y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   x.Randomize(1284376);
   y_expected.Randomize(42);
   y.Randomize(42);
   AxB->AddMultTranspose(x, y_expected, alpha);
   tensor::KroneckerMV(tensor::Transposed::TRANSPOSED,
                       alpha,
                       *A,
                       *B,
                       X,
                       1.0,
                       buffer.data(),
                       Y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}
INSTANTIATE_TEST_SUITE_P(KroneckerMVMult,
                         KroneckerMVParamTest,
                         Values(std::make_tuple(&A_0, &B_0, &AxB_0),
                                std::make_tuple(&A_1, &B_1, &AxB_1),
                                std::make_tuple(&A_2, &B_2, &AxB_2),
                                std::make_tuple(&A_3, &B_3, &AxB_3)));
} // namespace
