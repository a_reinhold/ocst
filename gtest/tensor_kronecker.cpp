#include "../src/tensor/kronecker.hpp"
#include "linalg/sparsemat.hpp"
#include <gtest/gtest.h>
#include <mfem.hpp>
#include <utility>
namespace {
#include "tensor_kronecker.data"
using ::testing::TestWithParam;
using ::testing::Values;

typedef std::
    tuple<mfem::SparseMatrix*, mfem::SparseMatrix*, mfem::SparseMatrix*>
        sp_mat_tuple;

TEST(TensorKronecker, Construction)
{
   // constructors
   tensor::KroneckerOperator kron_1(A_0, B_0, false, false);
   double* buffer = new double[m_AxB_0];
   tensor::KroneckerOperator kron_2(A_0, B_0, false, false, buffer);

   mfem::SparseMatrix* A_p = new mfem::SparseMatrix(A_0);
   mfem::SparseMatrix* B_p = new mfem::SparseMatrix(B_0);
   tensor::KroneckerOperator kron_3(*A_p, *B_p, true, true, buffer);

   delete[] buffer;
}

class KroneckerParamSquare : public TestWithParam<sp_mat_tuple> {
protected:
   mfem::SparseMatrix* A{std::get<0>(GetParam())};
   mfem::SparseMatrix* B{std::get<1>(GetParam())};
   mfem::SparseMatrix* AxB{std::get<2>(GetParam())};
};

class KroneckerParam : public TestWithParam<sp_mat_tuple> {
protected:
   mfem::SparseMatrix* A{std::get<0>(GetParam())};
   mfem::SparseMatrix* B{std::get<1>(GetParam())};
   mfem::SparseMatrix* AxB{std::get<2>(GetParam())};
};

TEST_P(KroneckerParamSquare, Diag)
{
   tensor::KroneckerOperator kron(*A, *B, false, false);

   mfem::Vector diag_assembled, diag_expected;
   kron.GetDiag(diag_assembled);
   AxB->GetDiag(diag_expected);
   EXPECT_NEAR(diag_assembled.Sum(), diag_expected.Sum(), 1e-6);
}

TEST_P(KroneckerParam, MultOne)
{
   tensor::KroneckerOperator kron(*A, *B, false, false);
   mfem::Vector x_in{kron.Width()};
   mfem::Vector y_expected{kron.Height()}, y{kron.Height()};
   x_in = 1.0;
   AxB->Mult(x_in, y_expected);
   kron.Mult(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   kron.SetTransposed(tensor::Transposed::TRANSPOSED);
   kron.MultTranspose(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}

TEST_P(KroneckerParam, MultRand)
{
   tensor::KroneckerOperator kron(*A, *B, false, false);
   mfem::Vector x_in{kron.Width()};
   mfem::Vector y_expected{kron.Height()}, y{kron.Height()};
   x_in.Randomize(745376);
   AxB->Mult(x_in, y_expected);
   kron.Mult(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   kron.SetTransposed(tensor::Transposed::TRANSPOSED);
   kron.MultTranspose(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}

TEST_P(KroneckerParam, MultTransposeOne)
{
   tensor::KroneckerOperator kron(*A, *B, false, false);
   mfem::Vector x_in{kron.Height()};
   mfem::Vector y_expected{kron.Width()}, y{kron.Width()};
   x_in = 1.0;
   AxB->MultTranspose(x_in, y_expected);
   kron.MultTranspose(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   kron.SetTransposed(tensor::Transposed::TRANSPOSED);
   kron.Mult(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}

TEST_P(KroneckerParam, MultTransposeRand)
{
   tensor::KroneckerOperator kron(*A, *B, false, false);
   mfem::Vector x_in{kron.Height()};
   mfem::Vector y_expected{kron.Width()}, y{kron.Width()};
   x_in.Randomize(6846);
   AxB->MultTranspose(x_in, y_expected);
   kron.MultTranspose(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   kron.SetTransposed(tensor::Transposed::TRANSPOSED);
   kron.Mult(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}

TEST_P(KroneckerParam, Assemble)
{
   tensor::KroneckerOperator kron(*A, *B, false, false);
   kron.Assemble();
   kron.Assemble(); // test double assembly bc. of freeing stuff
   mfem::SparseMatrix* result = mfem::Add(-1.0, kron.SpMat(), 1.0, *AxB);

   EXPECT_NEAR(result->MaxNorm(), 0.0, 1e-6);

   delete result;
}

INSTANTIATE_TEST_SUITE_P(TensorKroneckerSquare,
                         KroneckerParamSquare,
                         Values(std::make_tuple(&A_0, &B_0, &AxB_0),
                                std::make_tuple(&A_1, &B_1, &AxB_1),
                                std::make_tuple(&A_2, &B_2, &AxB_2)));

INSTANTIATE_TEST_SUITE_P(TensorKronecker,
                         KroneckerParam,
                         Values(std::make_tuple(&A_0, &B_0, &AxB_0),
                                std::make_tuple(&A_1, &B_1, &AxB_1),
                                std::make_tuple(&A_2, &B_2, &AxB_2),
                                std::make_tuple(&A_3, &B_3, &AxB_3)));

TEST(TensorKronecker, DiagFail)
{
   tensor::KroneckerOperator kron(A_3, B_3, false, false);
   mfem::Vector diag_assembled;
   EXPECT_DEATH(kron.GetDiag(diag_assembled), "Verification failed:");
}

TEST(TensorKronecker, Large)
{
   //results in approxmately 1e6 matrix dim in the assembled case
   mfem::Mesh msh
       = mfem::Mesh::MakeCartesian2D(12, 16, mfem::Element::Type::TRIANGLE);
   mfem::H1_FECollection fec_1{2, msh.Dimension()};
   mfem::L2_FECollection fec_2{1, msh.Dimension()};

   mfem::FiniteElementSpace fes_1{&msh, &fec_1};
   mfem::FiniteElementSpace fes_2{&msh, &fec_2};

   mfem::BilinearForm a{&fes_1}, b{&fes_2};
   mfem::ConstantCoefficient one{1.0};
   a.AddDomainIntegrator(new mfem::MassIntegrator(one));
   b.AddDomainIntegrator(new mfem::MassIntegrator(one));

   a.Assemble(), a.Finalize();
   b.Assemble(), b.Finalize();

   tensor::KroneckerOperator kron(a.SpMat(), b.SpMat());

   kron.Assemble();

   mfem::Vector x_in{kron.Width()}, y_kron{kron.Height()},
       y_spmat{kron.Height()};

   x_in.Randomize(4565);
   kron.SpMat().Mult(x_in, y_spmat);
   kron.Mult(x_in, y_kron);
   EXPECT_NEAR(y_spmat.Norml2(), y_kron.Norml2(), 1e-6);
}
}
