from scipy.sparse import random_array, kron
import numpy as np
from typing import TextIO
from dataclasses import dataclass
from data_generation_helpers import print_matrix_data


@dataclass
class test_data:
    number: int
    shape_AC: tuple[int, int]
    shape_BD: tuple[int, int]
    density_AC: float = 0.05
    density_BD: float = 0.05


def print_test_case(data: test_data, rng: np.random.Generator, fobj: TextIO):
    # data creation
    A = random_array(data.shape_AC, density=data.density_AC,
                     format='csr', random_state=rng)
    B = random_array(data.shape_BD, density=data.density_BD,
                     format='csr', random_state=rng)
    # C = random_array(data.shape_AC, density=data.density_AC,
    #                  format='csr', random_state=rng)
    # D = random_array(data.shape_BD, density=data.density_BD,
    #                  format='csr', random_state=rng)
    C = A.copy()
    D = B.copy()
    C.data = rng.random(C.data.size)
    D.data = rng.random(D.data.size)
    
    sylv = kron(A, B, format='csr') + kron(C, D, format='csr')

    print_matrix_data(A, f'A_{data.number}', fobj)
    print_matrix_data(B, f'B_{data.number}', fobj)
    print_matrix_data(C, f'C_{data.number}', fobj)
    print_matrix_data(D, f'D_{data.number}', fobj)
    print_matrix_data(sylv, f'assembled_{data.number}', fobj)


if __name__ == "__main__":
    seed = 654864
    rng = np.random.default_rng(seed=seed)

    test_cases = []

    test_cases.append(test_data(0, (5, 5), (4, 4), 1.0, 1.0))
    test_cases.append(test_data(1, (4, 11), (11, 4), .5, .5))
    test_cases.append(test_data(2, (20, 1), (1, 20), .6, .8))
    test_cases.append(test_data(3, (4, 11), (22, 7), .5, .3))

    with open("tensor_general_sylvester.data", 'w') as fobj:
        fobj.write("#include <vector>\n")
        for data in test_cases:
            print_test_case(data, rng, fobj)
