from scipy.sparse import csr_array
from typing import TextIO
from numpy.typing import ArrayLike
from enum import Enum


class cpp_print_type(Enum):
    DOUBLE = ('double', '0.8e')
    INT = ('int', '0d')


def print_cpp_vector(t: cpp_print_type, name: str, data: ArrayLike, fobj: TextIO):
    fobj.write(f"std::vector<{t.value[0]}> {name} {{ ")
    for elem in data:
        fobj.write(f"{elem:{f'{t.value[1]}'}}, ")

    fobj.write("};\n")


def print_sparse_mat_def(name: str, fobj: TextIO):
    fobj.write(f"mfem::SparseMatrix {name}(I_{name}.data(), J_{name}.data(),")
    fobj.write(f"data_{name}.data(), m_{name}, n_{name}, false, false, false);\n")


def print_matrix_data(mat: csr_array, suffix: str, fobj: TextIO):
    fobj.write(f"int m_{suffix} {{ {mat.shape[0]} }},")
    fobj.write(f"n_{suffix} {{ {mat.shape[1]} }};\n")
    print_cpp_vector(cpp_print_type.INT, f"I_{suffix}", mat.indptr, fobj)
    print_cpp_vector(cpp_print_type.INT, f"J_{suffix}", mat.indices, fobj)
    print_cpp_vector(cpp_print_type.DOUBLE, f"data_{suffix}", mat.data, fobj)
    print_sparse_mat_def(suffix, fobj)
