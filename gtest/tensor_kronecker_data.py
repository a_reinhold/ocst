from scipy.sparse import random_array, kron
import numpy as np
from typing import TextIO
from dataclasses import dataclass
from data_generation_helpers import print_matrix_data


@dataclass
class test_data:
    number: int
    shape_A: tuple[int, int]
    shape_B: tuple[int, int]
    density_A: float = 0.05
    density_B: float = 0.05


def print_test_case(data: test_data, rng: np.random.Generator, fobj: TextIO):
    # data creation
    A = random_array(data.shape_A, density=data.density_A,
                     format='csr', random_state=rng)
    B = random_array(data.shape_B, density=data.density_B,
                     format='csr', random_state=rng)
    A_B = kron(A, B, 'csr')

    print_matrix_data(A, f'A_{data.number}', fobj)
    print_matrix_data(B, f'B_{data.number}', fobj)
    print_matrix_data(A_B, f'AxB_{data.number}', fobj)


if __name__ == "__main__":
    seed = 175423
    rng = np.random.default_rng(seed=seed)

    test_cases = []

    test_cases.append(test_data(0, (3, 3), (4, 4), 1.0, 1.0))
    test_cases.append(test_data(1, (4, 11), (11, 4), .5, .5))
    test_cases.append(test_data(2, (20, 1), (1, 20), .6, .8))
    test_cases.append(test_data(3, (4, 11), (16, 7), .5, .5))

    with open("tensor_kronecker.data", 'w') as fobj:
        fobj.write("#include <vector>\n")
        for data in test_cases:
            print_test_case(data, rng, fobj)
