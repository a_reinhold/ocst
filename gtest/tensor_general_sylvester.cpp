#include "../src/tensor/general_sylvester.hpp"
#include "linalg/sparsemat.hpp"
#include <gtest/gtest.h>
#include <mfem.hpp>
#include <utility>
namespace {
#include "tensor_general_sylvester.data"
using ::testing::TestWithParam;
using ::testing::Values;

typedef std::tuple<mfem::SparseMatrix*,
                   mfem::SparseMatrix*,
                   mfem::SparseMatrix*,
                   mfem::SparseMatrix*,
                   mfem::SparseMatrix*>
    sp_mat_tuple;

TEST(TensorGeneralSylvester, Construction)
{
   // constructors
   tensor::GeneralSylvesterOperator sylv_1(A_0, B_0, C_0, D_0);
   double* buffer = new double[m_assembled_0];
   tensor::GeneralSylvesterOperator sylv_2(A_0, B_0, C_0, D_0, buffer);

   mfem::SparseMatrix* A_p = new mfem::SparseMatrix(A_0);
   mfem::SparseMatrix* B_p = new mfem::SparseMatrix(B_0);
   mfem::SparseMatrix* C_p = new mfem::SparseMatrix(C_0);
   mfem::SparseMatrix* D_p = new mfem::SparseMatrix(D_0);
   tensor::GeneralSylvesterOperator sylv_3(
       *A_p, *B_p, *C_p, *D_p, true, true, true, true, buffer);

   delete[] buffer;
}

class GeneralSylvesterParamSquare : public TestWithParam<sp_mat_tuple> {
protected:
   mfem::SparseMatrix* A{std::get<0>(GetParam())};
   mfem::SparseMatrix* B{std::get<1>(GetParam())};
   mfem::SparseMatrix* C{std::get<2>(GetParam())};
   mfem::SparseMatrix* D{std::get<3>(GetParam())};
   mfem::SparseMatrix* assembled{std::get<4>(GetParam())};
};

class GeneralSylvesterParam : public TestWithParam<sp_mat_tuple> {
protected:
   mfem::SparseMatrix* A{std::get<0>(GetParam())};
   mfem::SparseMatrix* B{std::get<1>(GetParam())};
   mfem::SparseMatrix* C{std::get<2>(GetParam())};
   mfem::SparseMatrix* D{std::get<3>(GetParam())};
   mfem::SparseMatrix* assembled{std::get<4>(GetParam())};
};

TEST_P(GeneralSylvesterParamSquare, Diag)
{
   tensor::GeneralSylvesterOperator sylv(*A, *B, *C, *D);

   mfem::Vector diag_assembled, diag_expected;
   sylv.GetDiag(diag_assembled);
   assembled->GetDiag(diag_expected);
   EXPECT_NEAR(diag_assembled.Sum(), diag_expected.Sum(), 1e-6);
}
TEST_P(GeneralSylvesterParam, MultOne)
{
   tensor::GeneralSylvesterOperator sylv(*A, *B, *C, *D);
   mfem::Vector x_in{sylv.Width()};
   mfem::Vector y_expected{sylv.Height()}, y{sylv.Height()};
   x_in = 1.0;
   assembled->Mult(x_in, y_expected);
   sylv.Mult(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   sylv.SetTransposed(tensor::Transposed::TRANSPOSED);
   sylv.MultTranspose(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}
TEST_P(GeneralSylvesterParam, MultRand)
{
   tensor::GeneralSylvesterOperator sylv(*A, *B, *C, *D);
   mfem::Vector x_in{sylv.Width()};
   mfem::Vector y_expected{sylv.Height()}, y{sylv.Height()};
   x_in.Randomize(745376);
   assembled->Mult(x_in, y_expected);
   sylv.Mult(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   sylv.SetTransposed(tensor::Transposed::TRANSPOSED);
   sylv.MultTranspose(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}

TEST_P(GeneralSylvesterParam, MultTransposeOne)
{
   tensor::GeneralSylvesterOperator sylv(*A, *B, *C, *D);
   mfem::Vector x_in{sylv.Height()};
   mfem::Vector y_expected{sylv.Width()}, y{sylv.Width()};
   x_in = 1.0;
   assembled->MultTranspose(x_in, y_expected);
   sylv.MultTranspose(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   sylv.SetTransposed(tensor::Transposed::TRANSPOSED);
   sylv.Mult(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}

TEST_P(GeneralSylvesterParam, MultTransposeRand)
{
   tensor::GeneralSylvesterOperator sylv(*A, *B, *C, *D);
   mfem::Vector x_in{sylv.Height()};
   mfem::Vector y_expected{sylv.Width()}, y{sylv.Width()};
   x_in.Randomize(6846);
   assembled->MultTranspose(x_in, y_expected);
   sylv.MultTranspose(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
   sylv.SetTransposed(tensor::Transposed::TRANSPOSED);
   sylv.Mult(x_in, y);
   y -= y_expected;
   EXPECT_NEAR(y.Norml2(), 0.0, 1e-6);
}

TEST_P(GeneralSylvesterParam, Assemble)
{
   tensor::GeneralSylvesterOperator sylv(*A, *B, *C, *D);
   sylv.Assemble();
   sylv.Assemble();
   mfem::SparseMatrix* result = mfem::Add(-1.0, sylv.SpMat(), 1.0, *assembled);

   EXPECT_NEAR(result->MaxNorm(), 0.0, 1e-6);

   delete result;
}

INSTANTIATE_TEST_SUITE_P(
    TensorGeneralSylvesterSquare,
    GeneralSylvesterParamSquare,
    Values(std::make_tuple(&A_0, &B_0, &C_0, &D_0, &assembled_0),
           std::make_tuple(&A_1, &B_1, &C_1, &D_1, &assembled_1),
           std::make_tuple(&A_2, &B_2, &C_2, &D_2, &assembled_2)));

INSTANTIATE_TEST_SUITE_P(
    TensorGeneralSylvester,
    GeneralSylvesterParam,
    Values(std::make_tuple(&A_0, &B_0, &C_0, &D_0, &assembled_0),
           std::make_tuple(&A_1, &B_1, &C_1, &D_1, &assembled_1),
           std::make_tuple(&A_2, &B_2, &C_2, &D_2, &assembled_2),
           std::make_tuple(&A_3, &B_3, &C_3, &D_3, &assembled_3)));

TEST(TensorGeneralSylvester, DiagFail)
{
   tensor::GeneralSylvesterOperator sylv(A_3, B_3, C_3, D_3);
   mfem::Vector diag_assembled;
   EXPECT_DEATH(sylv.GetDiag(diag_assembled), "Verification failed:");
}
//
TEST(TensorGeneralSylvester, Large)
{
   // results in approxmately 1e6 matrix dim in the assembled case
   mfem::Mesh msh
       = mfem::Mesh::MakeCartesian2D(12, 16, mfem::Element::Type::TRIANGLE);
   mfem::H1_FECollection fec_1{2, msh.Dimension()};
   mfem::L2_FECollection fec_2{1, msh.Dimension()};

   mfem::FiniteElementSpace fes_1{&msh, &fec_1};
   mfem::FiniteElementSpace fes_2{&msh, &fec_2};

   mfem::BilinearForm a{&fes_1}, b{&fes_2}, c{&fes_1}, d{&fes_2};
   mfem::ConstantCoefficient one{1.0};
   a.AddDomainIntegrator(new mfem::MassIntegrator(one));
   b.AddDomainIntegrator(new mfem::MassIntegrator(one));
   c.AddDomainIntegrator(new mfem::DiffusionIntegrator(one));
   d.AddDomainIntegrator(new mfem::DiffusionIntegrator(one));

   a.Assemble(), a.Finalize();
   b.Assemble(), b.Finalize();
   c.Assemble(), c.Finalize();
   d.Assemble(), d.Finalize();

   tensor::GeneralSylvesterOperator sylv(
       a.SpMat(), b.SpMat(), c.SpMat(), d.SpMat());

   sylv.Assemble();

   mfem::Vector x_in{sylv.Width()}, y_sylv{sylv.Height()},
       y_spmat{sylv.Height()};

   x_in.Randomize(4565);
   sylv.SpMat().Mult(x_in, y_spmat);
   sylv.Mult(x_in, y_sylv);
   EXPECT_NEAR(y_spmat.Norml2(), y_sylv.Norml2(), 1e-6);
}
}
