/**
 * Analytic solution Parabolic Problem
 *
 * problem 5 jump time & space 1D
 */

#ifndef OCST_ANALYTIC_SOLUTION_CASE_5_HPP
#define OCST_ANALYTIC_SOLUTION_CASE_5_HPP


#include <math.h>
#include <mfem.hpp>

constexpr double ALPHA = 400.0;

void 
meshNormal(const mfem::Vector& x, mfem::Vector& normal); //fwd
double
y_analytic(const mfem::Vector& x, const double t)
{
   return std::tanh(ALPHA*(t - 0.5)*(x(0)-0.5)); 
}

void
y_grad(const mfem::Vector& x, const double t, mfem::Vector& grad)
{
   grad(0) = (2*ALPHA*(t-0.5))/(std::cosh(2*ALPHA*(x(0)-0.5)*(t-0.5))+ 1.0);
}

double 
partial_t_y(const mfem::Vector& x, const double t)
{
   return (2*ALPHA*(x(0)-0.5))/(std::cosh(2*ALPHA*(x(0)-0.5)*(t-0.5))+ 1.0);
}

double
div_kappa_grad_y(const mfem::Vector& x, const double t)
{
   double arg = ALPHA* (x(0)-0.5) * (t-0.5);
   double res1 = 2 * ALPHA * ALPHA *(t -0.5)*(t-0.5)  * std::sinh(arg); 
   double res2 = std::cosh(arg);
   return -res1/(res2*res2*res2); 
}

double
kappa_func(const mfem::Vector& x) 
{ 
   return 1.0; 
} 

double
xi_func(const mfem::Vector& x) 
{
   //return  std::cos(x(0)*x(1))+ ALPHA;  
   return 1.0; 
}


// Shoud be the same for eaxh function
double
f_func(const mfem::Vector& x, const double t)
{
   return partial_t_y(x,t) - div_kappa_grad_y(x,t); 
}

double
eta_func(const mfem::Vector& x, const double t)
{
   //make shure that a mesh header is loaded! 

   mfem::Vector n(x.Size());
   mfem::Vector grad(x.Size());

   meshNormal(x,n);
   y_grad(x,t,grad);
   return kappa_func(x) * (n * grad) + xi_func(x) * y_analytic(x,t) ; 
}

#endif //OCST_ANALYTIC_SOLUTION_CASE_5_HPP
