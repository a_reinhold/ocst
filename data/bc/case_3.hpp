/**
 * Analytic solution Parabolic Problem
 *
 * problem 3 jump 2D
 */

#ifndef OCST_ANALYTIC_SOLUTION_CASE_3_HPP
#define OCST_ANALYTIC_SOLUTION_CASE_3_HPP

#ifndef ALPHA
#define ALPHA 10.0
#endif

#include <math.h>

#include <mfem.hpp>
void 
meshNormal(const mfem::Vector& x, mfem::Vector& normal); //fwd
double
y_analytic(const mfem::Vector& x, const double t)
{
   return (0.5 -t)* std::atan(ALPHA *(x(0)-x(1))); 
}

void
y_grad(const mfem::Vector& x, const double t, mfem::Vector& grad)
{
   double res1 = ALPHA*(0.5-t);
   double res2 = ALPHA*ALPHA *(x(0)-x(1))*(x(0)-x(1)) +1;
   double res  = res1/res2;
   grad(0) = res;
   grad(1) = -res;
}

double 
partial_t_y(const mfem::Vector& x, const double t)
{
   return -1.0*std::atan(ALPHA*(x(0)-x(1)));
}

double
div_kappa_grad_y(const mfem::Vector& x, const double t)
{
   double res_1 = -4.0* ALPHA*ALPHA*ALPHA * (0.5-t)*(x(0)-x(1));
   double res_2 = ALPHA*ALPHA *(x(0)-x(1))*(x(0)-x(1)) +1;

   return res_1/(res_2*res_2); 
}

double
kappa_func(const mfem::Vector& x) 
{ return 1.0; } 

double
xi_func(const mfem::Vector& x) 
{
   return  x(0)*x(0) + x(1)*x(1) + ALPHA;  
}


// Shoud be the same for eaxh function
double
f_func(const mfem::Vector& x, const double t)
{
   return partial_t_y(x,t) - div_kappa_grad_y(x,t); 
}

double
eta_func(const mfem::Vector& x, const double t)
{
   //make shure that a mesh header is loaded! 

   mfem::Vector n(x.Size());
   mfem::Vector grad(x.Size());

   meshNormal(x,n);
   y_grad(x,t,grad);
   return kappa_func(x) * (n * grad) + xi_func(x) * y_analytic(x,t) ; 
}

#endif //OCST_ANALYTIC_SOLUTION_CASE_3_HPP
