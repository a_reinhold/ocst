/**
 * Analytic solution Parabolic Problem
 *
 * problem 1 polynomial 2D
 */

#ifndef OCST_ANALYTIC_SOLUTION_CASE_1_HPP
#define OCST_ANALYTIC_SOLUTION_CASE_1_HPP

#include <math.h>

#include <mfem.hpp>
void 
meshNormal(const mfem::Vector& x, mfem::Vector& normal); //fwd
double
y_analytic(const mfem::Vector& x, const double t)
{
   return (10.0/(t+1.0))* x(0)*x(0) *x(1); 
}

void
y_grad(const mfem::Vector& x, const double t, mfem::Vector& grad)
{
   grad(0) = (10.0/(1.0+t))* 2 * x(0) *x(1) ;
   grad(1) = (10.0/(1.0+t))* x(0)*x(0)   ;
}

double 
partial_t_y(const mfem::Vector& x, const double t)
{
   return (10.0*x(0)*x(0)*x(1))/((1.0+t)*(1.0+t));
}

double
div_kappa_grad_y(const mfem::Vector& x, const double t)
{
   return (20.0/(t+1.0))* (x(0)*x(0)*x(0)*x(1) + 2.0*x(0)*x(1)*x(1)*x(1)); 
}

double
kappa_func(const mfem::Vector& x) 
{ return x(0)*x(1)*x(1); } 

double
xi_func(const mfem::Vector& x) 
{
   return  1E6*(x(0)*x(0) + x(1)) + 0.1;  
}


// Shoud be the same for eaxh function
double
f_func(const mfem::Vector& x, const double t)
{
   return partial_t_y(x,t) - div_kappa_grad_y(x,t); 
}

double
eta_func(const mfem::Vector& x, const double t)
{
   //make shure that a mesh header is loaded! 

   mfem::Vector n(x.Size());
   mfem::Vector grad(x.Size());

   meshNormal(x,n);
   y_grad(x,t,grad);
   return kappa_func(x) * (n * grad) + xi_func(x) * y_analytic(x,t) ; 
}

#endif //OCST_ANALYTIC_SOLUTION_CASE_1_HPP
