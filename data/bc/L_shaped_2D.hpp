/**
 * Mesh Header
 */
#ifndef OCST_UNITSQUARE2D_HPP
#define OCST_UNITSQUARE2D_HPP

#include <iostream>
#include <mfem.hpp>

const char* mesh_file = "../data/L_shaped_2D.mesh";
bool mesh_from_file = true;

void 
meshNormal(const mfem::Vector& x, mfem::Vector& normal)
{
   if (x(1) == 0.0) { 
      normal(0) = 0.0; normal(1) = -1.0; return;
   } else if (x(1) == 1.0 || x(1) == 2.0) { 
      normal(0) = 0.0; normal(1) = 1.0; return;
   } else if (x(0) == 0.0) { 
      normal(0) =-1.0; normal(1) = 0.0; return;
   } else if (x(0) == 1.0 || x(0) == 2.0) { 
      normal(0) = 1.0; normal(1) = 0.0; return;
   } else { std::cout << "WARNING normal not found  at x = ("
                    <<x(0) << ", " << x(1) << ")^T" << std::endl; 
   }
}
#endif //OCST_UNITSQUARE2D_HPP
