/**
 * Mesh Header
 */
#ifndef OCST_CUBE3D_HPP
#define OCST_CUBE3D_HPP

#include <iostream>
#include <mfem.hpp>

const char* mesh_file = "../data/cube.mesh";

void 
meshNormal(const mfem::Vector& x, mfem::Vector& n)
{
   if      (x(0) == -1.0) { n(0) = -1.0; n(1) =  0.0; n(2) =  0.0; return;}
   else if (x(0) ==  1.0) { n(0) =  1.0; n(1) =  0.0; n(2) =  0.0; return;}
   else if (x(1) == -1.0) { n(0) =  0.0; n(1) = -1.0; n(2) =  0.0; return;}
   else if (x(1) ==  1.0) { n(0) =  0.0; n(1) =  1.0; n(2) =  0.0; return;}
   else if (x(2) == -1.0) { n(0) =  0.0; n(1) =  0.0; n(2) = -1.0; return;}
   else if (x(2) ==  1.0) { n(0) =  0.0; n(1) =  0.0; n(2) =  1.0; return;}
   else { std::cout << "WARNING n not found  at x = ("
                    <<x(0) << ", " << x(1) << ")^T" << std::endl; 
   }
}
#endif //OCST_CUBE3D_HPP
