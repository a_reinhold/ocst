/**
 * Analytic solution Parabolic Problem
 *
 * problem 4 jump time 2D
 */

#ifndef OCST_ANALYTIC_SOLUTION_CASE_3_HPP
#define OCST_ANALYTIC_SOLUTION_CASE_3_HPP

#ifndef ALPHA
#define ALPHA 10.0
#endif

#include <math.h>

#include <mfem.hpp>
void 
meshNormal(const mfem::Vector& x, mfem::Vector& normal); //fwd
double
y_analytic(const mfem::Vector& x, const double t)
{
   return std::atan(ALPHA*(t - 0.5))* (std::sqrt(x(0)+1)- x(1)*x(1)); 
}

void
y_grad(const mfem::Vector& x, const double t, mfem::Vector& grad)
{
   double res1 = std::atan(ALPHA*(t-0.5));
   grad(0) = res1 * (1.0/(2.0* std::sqrt(x(0)+1)));
   grad(1) = res1 * (-2.0*x(1));
}

double 
partial_t_y(const mfem::Vector& x, const double t)
{
   double res1 = ALPHA*(std::sqrt(x(0)+1)- x(1)*x(1));
   double res2 = ALPHA*ALPHA*(t-0.5)*(t-0.5) + 1;
   return res1/res2;
}

double
div_kappa_grad_y(const mfem::Vector& x, const double t)
{

   double res1 = std::atan(ALPHA*(0.5-t));
   return res1 * (((0.25 * x(1))/(std::sqrt(x(0)+1))) - 4.0 * (x(0)+1) *x(1));
}

double
kappa_func(const mfem::Vector& x) 
{ 
   return (x(0)+1)*x(1); 
} 

double
xi_func(const mfem::Vector& x) 
{
   //return  std::cos(x(0)*x(1))+ ALPHA;  
   return 1.0; 
}


// Shoud be the same for eaxh function
double
f_func(const mfem::Vector& x, const double t)
{
   return partial_t_y(x,t) - div_kappa_grad_y(x,t); 
}

double
eta_func(const mfem::Vector& x, const double t)
{
   //make shure that a mesh header is loaded! 

   mfem::Vector n(x.Size());
   mfem::Vector grad(x.Size());

   meshNormal(x,n);
   y_grad(x,t,grad);
   return kappa_func(x) * (n * grad) + xi_func(x) * y_analytic(x,t) ; 
}

#endif //OCST_ANALYTIC_SOLUTION_CASE_3_HPP
