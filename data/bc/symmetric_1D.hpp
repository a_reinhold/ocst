/**
 * Mesh Header
 */
#ifndef OCST_SYMMETRIC_1D
#define OCST_SYMMETRIC_1D

#include <iostream>
#include <mfem.hpp>

double xmax =  1.0;

void 
meshNormal(const mfem::Vector& x, mfem::Vector& normal)
{
   if (x(0) == 0.0) { normal(0) =-1.0; return;}
   else if (x(0) == xmax) { normal(0) = 1.0; return;}
   else { std::cout << "WARNING normal not found  at x = ("
                    <<x(0) << ")" << std::endl; 
   }
}
#endif //OCST_SYMMETRIC_1D
