DEBUG = 0

SRC_DIRS = src src/tensor
OBJ_DIR = $(abspath ./obj)

SRCS := $(foreach dir, $(SRC_DIRS),$(wildcard $(dir)/*.cpp))
$(info $(SRCS))
OBJS := $(SRCS:%.cpp=$(OBJ_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

TESTS := $(wildcard ./gtest/*.cpp)
TEST_OBJS := $(TESTS:%.cpp=$(OBJ_DIR)/%.o)

.PRECIOUS: $(OBJS)
.SUFFIXES:
.SUFFIXES: .o .cpp .mk

## BLAS and LAPACK

# Intel MKL
# MKL_ROOT = /usr/include #celsius
# MKL_ROOT = /opt/intel/mkl/include #revolve
# MKL_ROOT =/opt/intel/compilers_and_libraries/linux/mkl/ #bwuni
# created with intels mkl_link_tool:
# MKL_FLAGS = -m64 -D_OCST_USE_INTEL_MKL
# MKL_LIBS = -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl

# OPENBLAS / LAPACK (HOMEBREW)
# LAPACK_ROOT= /opt/homebrew/Cellar/openblas/0.3.21
# LAPACK_FLAGS= -D_OCST_USE_OPENBLAS
# LAPACK_INCLUDE= $(LAPACK_ROOT)/include
# LAPACK_LIBS= -L$(LAPACK_ROOT)/lib -lopenblas -llapack 

#FRAMEWORK BLAS + HOMEBREW LAPACK

# LAPACK_ROOT= /opt/homebrew/Cellar/lapack/3.10.0
# LAPACK_FLAGS= -D_OCST_USE_BREW_LAPACK
# LAPACK_INCLUDE= $(LAPACK_ROOT)/include
# LAPACK_LIBS= -L$(LAPACK_ROOT)/lib  -llapack 


# FRAMEWORK ACCELERATE MACOS
LAPACK_FLAGS = -D_OCST_USE_ACCELERATE -DACCELERATE_NEW_LAPACK -flax-vector-conversions
LAPACK_LIBS = -framework Accelerate  # -Wl, -rpath, #WTF!!


## MFEM 
MFEM_DIR = /opt/mfem/mfem-4.6
MFEM_LIB_FILE = $(MFEM_DIR)/libmfem.a
MFEM_INCLUDE = $(MFEM_DIR) \
	       $(realpath $(MFEM_DIR)/../SuiteSparse/include/suitesparse) \
	       $(realpath $(MFEM_DIR)/../metis-5.1.0/include)
MFEM_LIBS = -L$(MFEM_DIR) -lmfem \
	    -L$(realpath $(MFEM_DIR)/../metis-5.1.0/lib) -lmetis

SUITE_SPARSE_LIBS = -L$(realpath $(MFEM_DIR)/../SuiteSparse/lib) -lklu -lbtf -lumfpack \
		    # -lcholmod -lcolamd -lamd -lcamd -lccolamd -lsuitesparseconfig


## INCLUDES
INCLUDE_DIRS = $(realpath ./src) ${MKL_ROOT} $(LAPACK_INCLUDE) $(MFEM_INCLUDE)
INCLUDE_FLAGS = $(addprefix -I, $(INCLUDE_DIRS))

## LIBRARYS
LIBS = $(MFEM_LIBS) $(MKL_LIBS) $(LAPACK_LIBS) $(SUITE_SPARSE_LIBS)

## COMPILER
CXX = g++

## Optimazation/DEBUG
ifeq ($(DEBUG),1)
	CXXFLAGS += -O0 -g
else
	CXXFLAGS += -O3
endif

CXXFLAGS += -std=c++14 -Wall $(MKL_FLAGS) $(LAPACK_FLAGS)

.PHONY: all clean clean-build clean-exec documentation .clangd .clangd-test _prep_test

# Remove built-in rule
%: %.cpp

%: %.cpp $(MFEM_LIB_FILE) $(OBJS)
	$(CXX) $(INCLUDE_FLAGS) $(CXXFLAGS) $^ -o $@  $(LIBS) 

$(OBJ_DIR)/%.o : %.cpp
	@mkdir -p '$(@D)'
	$(CXX) -c $(INCLUDE_FLAGS) $(CXXFLAGS) -MMD $< -o $@ 
	
-include $(DEPS)
# Generate an error message if the MFEM library is not built and exit
$(MFEM_LIB_FILE):
	$(error The MFEM library is not built)

clean: clean-build 

clean-build:
	rm -rf obj  

clean-all: clean-build
	$(MAKE) -C ./test clean
	$(MAKE) -C ./bench clean
	$(MAKE) -C ./examples clean
	$(MAKE) -C ./gtest clean

documentation:
	doxygen doc/doxygen.conf

## Unit tests (using googletest)

# Points to the root of Google Test, relative to where this file is.
# Remember to tweak this if you move this file.
GTEST_DIR = /Users/alex/Developer/googletest/googletest
GTEST_MAIN = $(GTEST_DIR)/make/gtest_main.a
# Set Google Test's header directory as a system directory, such that
# the compiler doesn't generate warnings in Google Test headers.
all_tests: INCLUDE_FLAGS += -isystem $(GTEST_DIR)/include 
all_tests: $(GTEST_MAIN) $(OBJS) $(TEST_OBJS) 
	$(CXX) $(INCLUDE_FLAGS) $(CXXFLAGS) $^ -o ./gtest/$@ $(LIBS) 

single_test: INCLUDE_FLAGS += -isystem $(GTEST_DIR)/include
single_test: $(OBJS) $(OBJ_DIR)/gtest/$(TEST).o $(GTEST_MAIN)
	$(CXX) $(INCLUDE_FLAGS) $(CXXFLAGS) $^ -o ./gtest/$(TEST) $(LIBS)

#create clangd config with current Flags
.clangd: 
	echo "CompileFlags:\n  Compiler: $(CXX)" > $@
	echo "  Add: [" >> $@
	echo "  $(INCLUDE_FLAGS)," | sed -e "s/ -/,\n  -/g"  >> $@
	echo "  -I$(GTEST_DIR)/include," >> $@
	echo "  $(CXXFLAGS)," | sed -e "s/ -/,\n  -/g"  >> $@
	echo "  ]" >> $@
	sed -i '' -e "/^ ,/d" $@

